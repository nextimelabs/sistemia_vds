import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './entity/auth/auth.module';
import { UsersModule } from './entity/users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CamerasModule } from './entity/cameras/cameras.module';
import { StreamerModule } from './streamer/streamer.module';
import * as environment from '../environment/environment.json';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { RecorderModule } from './recorder/recorder.module';
import { GroupsModule } from './entity/groups/groups.module';
import { MonitorModule } from './monitor/monitor.module';
import { EntityModule } from './entity/entity.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST || environment.DB_HOST,
      port: parseInt(process.env.DB_PORT) || environment.DB_PORT,
      username: process.env.DB_USERNAME || environment.DB_USERNAME,
      password: process.env.DB_PASSWORD || environment.DB_PASSWORD,
      database: process.env.DB_NAME || environment.DB_NAME,
      autoLoadEntities: true,
      synchronize:
        (process.env.DB_SYNCHRONIZE || environment.DB_SYNCHRONIZE) === 'true',
      logging: ['error'],
      migrations: ['migration/*.js'],
      cli: {
        migrationsDir: 'migration',
      },
    }),
    EventEmitterModule.forRoot(),
    StreamerModule,
    RecorderModule,
    MonitorModule,
    EntityModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
