import { Module } from '@nestjs/common';
import { MonitorService } from './monitor.service';
import { MonitorController } from './monitor.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { EventsModule } from './events/events.module';
import { AlarmsModule } from './alarms/alarms.module';
import { TriggerModule } from './triggers/trigger.module';
import * as environment from '../../environment/environment.json';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'EVENT_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'event',
            brokers: [
              `${process.env.KAFKA_IP || environment.KAFKA_IP}:${process.env.KAFKA_PORT || environment.KAFKA_PORT}`
            ],
          },
          consumer: {
            groupId: 'event-consumer',
          },
        },
      },
    ]),
    EventsModule,
    AlarmsModule,
    TriggerModule,
  ],
  controllers: [MonitorController],
  providers: [MonitorService],
})
export class MonitorModule {}
