import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Logger,
  Inject,
} from '@nestjs/common';
import { MonitorService } from './monitor.service';
import { CreateMonitorDto } from './dto/create-monitor.dto';
import { UpdateMonitorDto } from './dto/update-monitor.dto';
import {
  Client,
  ClientKafka,
  ClientProxy,
  Ctx,
  KafkaContext,
  MessagePattern,
  Payload,
  Transport,
} from '@nestjs/microservices';
import { EventService } from './events/event.service';

@Controller('monitor')
export class MonitorController {
  constructor(
    private readonly eventService: EventService,
    private readonly monitorService: MonitorService,
    @Inject('EVENT_SERVICE') private eventClient: ClientProxy,
  ) {}

  /*
  async onModuleInit() {
    // Need to subscribe to topic
    // so that we can get the response from kafka microservice
    //this.client.subscribeToResponseOf('event');
    await this.eventClient.connect();
  }
   */

  /*
  Questa funzione sta in ascolto sul topic 'event' di Kafka.
  Al verificarsi di un evento, viene aggiunto nel db e viene inviato al servizio
  di monitoraggio.
   */
  @MessagePattern('event')
  addEvent(@Payload() message) {
    if (message.value.cameraId !== undefined) {
      this.eventService.addEvent(message.value);
      this.monitorService.addEvent(message.value);
    }
  }

  @MessagePattern('triggerEvent')
  async onEventAddedToTriggerQueue(
    @Payload() message,
    @Ctx() context: KafkaContext,
  ) {
    await this.monitorService.addTriggerEvent(message.key, message.value);
  }
}
