import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsDate,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  ValidateNested,
  IsISO8601,
  MinLength,
  IsNumber,
  IsJSON,
} from 'class-validator';

export class EventMetadata {
  @ApiProperty()
  key: string;
  @ApiProperty()
  value: string;
}

export class GetEventDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @ApiProperty({ maxLength: 255 })
  @IsNotEmpty()
  @MaxLength(255)
  eventType: string;

  @ApiProperty({ type: [EventMetadata] })
  eventMetadata: EventMetadata[];

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  cameraId?: number;

  @ApiProperty()
  @IsISO8601()
  createdAt: string;

  @ApiProperty()
  @IsISO8601()
  timestamp: string;
}
