import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsDate, IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString, IsUUID, MaxLength,
  ValidateNested, IsISO8601, MinLength, IsNumber, IsJSON,
} from 'class-validator';


export class CreateEventDto {
  @ApiProperty({maxLength: 255})
  @IsNotEmpty()
  @MaxLength(255)
  eventType: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsJSON()
  eventMetadata: object;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  cameraId?: number;
}
