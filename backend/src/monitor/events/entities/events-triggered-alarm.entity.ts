import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { EventEntity } from './events.entity';
import { AlarmTriggeredEntity } from '../../alarms/entities/alarm-triggered.entity';

@Entity('alarms-to-be-triggered')
export class EventsTriggeredAlarmEntity {
  @ManyToOne((type) => EventEntity, (event) => event.id, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  event?: EventEntity;
  @PrimaryColumn()
  eventId?: number;
  //
  @ManyToOne((type) => AlarmTriggeredEntity, (alarmTriggered) => alarmTriggered.id, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  alarmTriggered?: AlarmTriggeredEntity;
  @PrimaryColumn()
  alarmTriggeredId?: number;
}
