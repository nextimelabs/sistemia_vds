import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { CameraEntity } from '../../../entity/cameras/entities/camera.entity';
import { EventMetadata } from '../dto/get-event.dto';

@Entity('events')
export class EventEntity {
  @PrimaryGeneratedColumn()
  id?: number;
  @Column('varchar', { length: 255, nullable: false })
  eventType: string;
  @Column('json', { nullable: false })
  eventMetadata: EventMetadata[];
  @CreateDateColumn({ type: 'timestamp', default: () => 'NOW()' })
  timestamp?: Date;
  @ManyToOne((type) => CameraEntity, (camera) => camera.id, {
    onDelete: 'NO ACTION',
  })
  @JoinColumn()
  camera?: CameraEntity;
  @Column({ nullable: true })
  cameraId?: number;
  @CreateDateColumn({ type: 'timestamp without time zone', default: () => 'NOW()' })
  createdAt?: Date;
}
