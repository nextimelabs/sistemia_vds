import { Injectable } from '@nestjs/common';
import { CreateMonitorDto } from '../dto/create-monitor.dto';
import { UpdateMonitorDto } from '../dto/update-monitor.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CameraEntity } from '../../entity/cameras/entities/camera.entity';
import { Repository } from 'typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CreateEventDto } from './dto/create-event.dto';
import { EventEntity } from './entities/events.entity';
import { from } from 'rxjs';
import { IEvent } from './enums/event-type.enum';

interface ISearchEvents {
  limit: number;
  offset: number;
  cameraId?: number;
  eventType?: string;
  fromTime?: Date;
  toTime?: Date;
  orderColumn: string;
  orderBy: 'ASC' | 'DESC';
}

@Injectable()
export class EventService {
  constructor(
    @InjectRepository(EventEntity) private eventEntityRepository: Repository<EventEntity>,
  ) {}

  async addEvent(event: IEvent) {
    const newEvent: EventEntity = {
      cameraId: event.cameraId,
      timestamp: new Date(event.timestamp),
      eventType: event.eventType,
      eventMetadata: event.eventMetadata,
    };
    return this.eventEntityRepository.save(newEvent);
  }

  async findAll(
    { limit, offset, cameraId, fromTime, toTime, eventType, orderColumn, orderBy }: ISearchEvents
  ): Promise<[EventEntity[], number]> {
    let queryBuilder =
      this.eventEntityRepository.createQueryBuilder('events');
    if (cameraId)
      queryBuilder = queryBuilder.andWhere('events.cameraId = :cameraId', {
        cameraId,
      });
    if (eventType)
      queryBuilder = queryBuilder.andWhere('events.eventType = :eventType', {
        eventType,
      });

    if (fromTime)
      queryBuilder = queryBuilder.andWhere('events.timestamp >= :fromTime', {
        fromTime,
      });
    if (toTime)
      queryBuilder = queryBuilder.andWhere('events.timestamp <= :toTime', {
        toTime,
      });

    return queryBuilder
      .orderBy(`events.${orderColumn}`, orderBy)
      .limit(limit)
      .offset(offset)
      .getManyAndCount();
  }

  async deleteEvent(eventId: number) {
    return this.eventEntityRepository.delete({ id: eventId });
  }

}
