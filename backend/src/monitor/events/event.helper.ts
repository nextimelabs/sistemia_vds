import { IEvent } from './enums/event-type.enum';

export class EventHelper {
  private _camId: number;
  private _type: string;
  private _timestamp: Date;
  private _metadata: { key: string; value: string }[] = [];

  public cam(camId: number) {
    this._camId = camId;
    return this;
  }

  public type(type: string) {
    this._type = type;
    return this;
  }

  public timestamp(date: Date) {
    this._timestamp = date;
    return this;
  }

  public addMetadata(key: string, value: string) {
    this._metadata.push({ key, value });
    return this;
  }

  public event(): IEvent {
    const res = {
      cameraId: this._camId,
      timestamp: this._timestamp ? this._timestamp.toISOString() : new Date().toISOString(),
      eventType: this._type,
      eventMetadata: this._metadata,
    };
    this._camId = null;
    this._type = null;
    this._metadata = [];
    return res;
  }

  public random(): IEvent {
    const items = [
      this.cam(1)
        .type('')
        .addMetadata('PLATE', 'AB123CD')
        .addMetadata('COLOR', 'BLACK')
        .event(),
      this.cam(2)
        .type('VEHICLE_DETECTED')
        .addMetadata('COLOR', 'WHITE')
        .event(),
      this.cam(1)
        .type('PERSON_DETECTED')
        .addMetadata('GENDER', 'WOMAN')
        .event(),
      this.cam(1)
        .type('VEHICLE_DETECTED')
        .addMetadata('PLATE', 'AB321AB')
        .event(),
    ];
    return items[Math.floor(Math.random() * items.length)];
  }
}
