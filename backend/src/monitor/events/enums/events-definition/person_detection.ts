export type PERSON_DETECTED = 'PERSON_DETECTED';

export type PERSON_DETECTED_METADATA =
  | {
      key: 'DIRECTION';
      value: 'NORTH' | 'SOUTH' | 'EAST' | 'OVEST';
    }
  | {
      key: 'SPEED';
      value: string;
    }
  | {
      key: 'GENDER';
      value: 'MAN' | 'WOMAN';
    };
