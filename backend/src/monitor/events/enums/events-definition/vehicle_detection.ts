export type VEHICLE_DETECTED = 'VEHICLE_DETECTED';

export type VEHICLE_DETECTED_METADATA =
  | {
      key: 'DIRECTION';
      value: 'NORTH' | 'SOUTH' | 'EAST' | 'OVEST';
    }
  | {
      key: 'SPEED';
      value: string;
    }
  | {
      key: 'PLATE';
      value: string;
    }
  | {
      key: 'COLOR';
      value: 'BLACK' | 'WHITE' | 'RED' | string;
    }
  | {
      key: 'TYPE';
      value: 'CAR' | 'TRUCK' | 'MOTORCYCLE' | string;
    };
