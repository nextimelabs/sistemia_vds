export type MOVEMENT = 'MOVEMENT';

export type MOVEMENT_METADATA =
  | {
      key: 'DIRECTION';
      value: 'NORTH' | 'SOUTH' | 'EAST' | 'OVEST';
    }
  | {
      key: 'SPEED';
      value: string;
    };
