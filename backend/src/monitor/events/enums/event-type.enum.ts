import * as EVENTS_DEFINITION from './events-definition/index';

export type IEvent =
  | {
      cameraId: number;
      timestamp: string;
      eventType: string;
      eventMetadata: {
        key: string;
        value: string;
      }[];
    }
  | {
      cameraId: number;
      timestamp: string;
      eventType: EVENTS_DEFINITION.MOVEMENT;
      eventMetadata: EVENTS_DEFINITION.MOVEMENT_METADATA[];
    }
  | {
      cameraId: number;
      timestamp: string;
      eventType: EVENTS_DEFINITION.PERSON_DETECTED;
      eventMetadata: EVENTS_DEFINITION.PERSON_DETECTED_METADATA[];
    }
  | {
      cameraId: number;
      timestamp: string;
      eventType: EVENTS_DEFINITION.VEHICLE_DETECTED;
      eventMetadata: EVENTS_DEFINITION.VEHICLE_DETECTED_METADATA[];
    };

// TODO definire l'elenco di eventi possibili con i relativi metadati ed i possibili valori che possono assumere
