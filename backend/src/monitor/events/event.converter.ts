import { Injectable } from '@nestjs/common';
import { GetEventDto } from './dto/get-event.dto';
import { EventEntity } from './entities/events.entity';

@Injectable()
export class EventConverter {
  constructor() {}

  convert(eventEntity: EventEntity): GetEventDto {
    return {
      id: eventEntity.id,
      eventType: eventEntity.eventType,
      eventMetadata: eventEntity.eventMetadata,
      cameraId: eventEntity.cameraId,
      timestamp: new Date(eventEntity.timestamp).toISOString(),
      createdAt: new Date(eventEntity.createdAt).toISOString(),
    };
  }

  converts(eventsEntities: EventEntity[]): GetEventDto[] {
    const res: GetEventDto[] = [];
    eventsEntities.map((eventsEntity) => {
      res.push(this.convert(eventsEntity));
    });
    return res;
  }
}
