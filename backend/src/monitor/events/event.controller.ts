import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { GetEventDto } from './dto/get-event.dto';
import { EventConverter } from './event.converter';
import { EventService } from './event.service';
import { RuoloEnum } from '../../entity/users/enums/ruolo.enum';
import { Roles } from '../../entity/auth/auth.decorator';
import { AuthGuard } from '../../entity/auth/auth.guard';
import { PaginatedDto } from '../../shared/dto/pagination.dto';
import { ApiPaginatedResponse } from '../../shared/pagination.decorator';
import { StatusAlarmTriggeredEnum } from '../alarms/enums/status-alarm-triggered.enum';

@Controller('events')
@ApiExtraModels(GetEventDto)
@ApiTags('Monitor Module - Events')
export class EventController {
  constructor(
    private eventService: EventService,
    private eventsConverter: EventConverter,
  ) {}

  @Get('')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiPaginatedResponse(GetEventDto, 'Get paginated events list')
  @ApiQuery({
    name: 'orderColumn',
    enum: ['id', 'eventType', 'timestamp', 'cameraId', 'createdAt'],
    required: false,
  })
  @ApiQuery({ name: 'orderBy', enum: ['ASC', 'DESC'], required: false })
  @ApiQuery({ name: 'fromTime', type: String, required: false })
  @ApiQuery({ name: 'toTime', type: String, required: false })
  @ApiQuery({ name: 'cameraId', type: Number, required: false })
  @ApiQuery({ name: 'eventType', type: String, required: false })
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @ApiQuery({ name: 'offset', type: Number, required: false })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async get(
    @Query('limit') limit = 10,
    @Query('offset') offset = 0,
    @Query('cameraId') cameraId: number,
    @Query('fromTime') fromTime: string,
    @Query('toTime') toTime: string,
    @Query('eventType') eventType: string,
    @Query('orderColumn') orderColumn: string = 'id',
    @Query('orderBy') orderBy: 'ASC' | 'DESC' = 'DESC',
  ): Promise<PaginatedDto<GetEventDto>> {
    const res = await this.eventService.findAll({
      limit,
      offset,
      cameraId,
      eventType,
      fromTime: fromTime ? new Date(fromTime) : null,
      toTime: toTime ? new Date(toTime) : null,
      orderColumn,
      orderBy,
    });
    return {
      total: res[1],
      limit: limit,
      offset: offset,
      results: await this.eventsConverter.converts(res[0]),
    };
  }

  @Delete(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete segment by id',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async delete(@Param('id') id: number) {
    return await this.eventService.deleteEvent(id);
  }
}
