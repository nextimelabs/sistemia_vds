import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../../entity/users/users.module';
import { AlarmsModule } from '../alarms/alarms.module';
import { MonitorController } from '../monitor.controller';
import { AlarmController } from '../alarms/alarm.controller';
import { MonitorService } from '../monitor.service';
import { AlarmService } from '../alarms/alarm.service';
import { AlarmConverter } from '../alarms/alarm.converter';
import { EventService } from './event.service';
import { EventEntity } from './entities/events.entity';
import { EventConverter } from './event.converter';
import { EventController } from './event.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      EventEntity
    ]),
    UsersModule,
  ],
  controllers: [EventController],
  providers: [EventService, EventConverter],
  exports: [EventService, EventConverter],
})
export class EventsModule {}
