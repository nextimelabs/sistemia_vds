import { Test, TestingModule } from '@nestjs/testing';
import { MonitorService } from './monitor.service';
import { MonitorModule } from './monitor.module';
import { TriggerService } from './triggers/trigger.service';
import { EventsModule } from './events/events.module';
import { AlarmsModule } from './alarms/alarms.module';
import { TriggerModule } from './triggers/trigger.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as environment from '../../environment/environment.json';
import { AuthModule } from '../entity/auth/auth.module';
import { UsersModule } from '../entity/users/users.module';
import { CamerasModule } from '../entity/cameras/cameras.module';
import { StreamerModule } from '../streamer/streamer.module';
import { RecorderModule } from '../recorder/recorder.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { GroupsModule } from '../entity/groups/groups.module';
import {
  ICondition,
  ITriggerEvents,
} from './interfaces/trigger-events.interface';
import { TriggerEntity } from './triggers/entities/triggers.entity';
import { EventHelper } from './events/event.helper';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { CreateAlarmDto } from './alarms/dto/create-alarm.dto';
import * as request from 'supertest';
import { INestApplication, Logger } from '@nestjs/common';
import { LoginUserResponseDto } from '../entity/auth/dto/login-user-response.dto';
import { RuoloEnum } from '../entity/users/enums/ruolo.enum';
import { StatoEnum } from '../entity/users/enums/stato.enum';
import { GetAlarmDto } from './alarms/dto/get-alarm.dto';
import { CreateTriggerDto } from './triggers/dto/create-trigger.dto';
import { GetTriggerDto } from './triggers/dto/get-trigger.dto';
import { createConnection } from 'typeorm';
import { SegmentsService } from '../recorder/segments.service';
import { GetCameraDto } from '../entity/cameras/dto/get-camera.dto';
import { GetSegmentDto } from '../recorder/dto/get-segment.dto';
import { CreateCameraDto } from '../entity/cameras/dto/create-camera.dto';
import { SegmentsEntity } from '../recorder/entities/segments.entity';
import { GetEventDto } from './events/dto/get-event.dto';
import { EventEntity } from './events/entities/events.entity';
import { EventService } from './events/event.service';
import { EventConverter } from './events/event.converter';
import { StatusAlarmTriggeredEnum } from './alarms/enums/status-alarm-triggered.enum';
import { GetAlarmTriggeredDto } from './alarms/dto/get-alarm-triggered.dto';
import { IEvent } from './events/enums/event-type.enum';
const moment = require('moment');

describe('MonitorService', () => {
  let app: INestApplication;

  let eventService: EventService;
  let eventConverter: EventConverter;
  let monitorService: MonitorService;

  const eventHelper = new EventHelper();

  // Start app
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ClientsModule.register([
          {
            name: 'EVENT_SERVICE',
            transport: Transport.KAFKA,
            options: {
              client: {
                clientId: 'event',
                brokers: [
                  `${process.env.KAFKA_IP || environment.KAFKA_IP}:${
                    process.env.KAFKA_PORT || environment.KAFKA_PORT
                  }`,
                ],
              },
              consumer: {
                groupId: 'event-consumer',
              },
            },
          },
        ]),
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: process.env.DB_HOST || environment.DB_HOST,
          port: parseInt(process.env.DB_PORT) || environment.DB_PORT,
          username: process.env.DB_USERNAME || environment.DB_USERNAME,
          password: process.env.DB_PASSWORD || environment.DB_PASSWORD,
          database: process.env.DB_NAME || environment.DB_NAME,
          autoLoadEntities: true,
          synchronize:
            (process.env.DB_SYNCHRONIZE || environment.DB_SYNCHRONIZE) ===
            'true',
          migrations: ['migration/*.js'],
          cli: {
            migrationsDir: 'migration',
          },
        }),
        AuthModule,
        UsersModule,
        CamerasModule,
        StreamerModule,
        RecorderModule,
        EventEmitterModule.forRoot(),
        GroupsModule,
        EventsModule,
        AlarmsModule,
        TriggerModule,
      ],
      providers: [MonitorService],
    }).compile();

    app = module.createNestApplication(null, {});

    await app.init();

    monitorService = module.get<MonitorService>(MonitorService);
    eventService = module.get<EventService>(EventService);
    eventConverter = module.get<EventConverter>(EventConverter);

    // Load triggers in memory
    await monitorService.loadTriggersFromDb();
  }, 10000);
  // Close app
  afterAll(async () => {
    await app.close();
  });

  // Login
  let adminLoginResponse: LoginUserResponseDto;
  beforeAll(async () => {
    const login = await request(app.getHttpServer()).post('/auth/login').send({
      username: 'admin',
      password: 'admin',
    });
    expect(login.body).toEqual({
      user: {
        id: login.body.user.id,
        username: 'admin',
        role: RuoloEnum.Amministratore,
        status: StatoEnum.Attivo,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      },
      token: login.body.token,
    });
    adminLoginResponse = login.body;
  });

  it('should be defined', () => {
    expect(monitorService).toBeDefined();
  });

  it('getTriggersCorrispondence', () => {
    const triggerEntities: TriggerEntity[] = [
      // deve passare un auto nera con targa AB123CD oppure o passa un auto bianca o con targa AB321AB
      {
        id: 1,
        enabled: true,
        condition: {
          and: [
            eventHelper
              .cam(1)
              .type('PASSAGGIO_AUTO')
              .addMetadata('TARGA', 'AB123CD')
              .addMetadata('COLORE', 'NERO')
              .event(),
            {
              or: [
                eventHelper
                  .cam(2)
                  .type('PASSAGGIO_AUTO')
                  .addMetadata('COLORE', 'BIANCA')
                  .event(),
                eventHelper
                  .cam(1)
                  .type('PASSAGGIO_AUTO')
                  .addMetadata('TARGA', 'AB321AB')
                  .event(),
              ],
            },
          ],
        },
      },
      // o passa una persona donna oppure passa un uomo e una macchina nera
      {
        id: 2,
        enabled: true,
        condition: {
          or: [
            eventHelper
              .cam(2)
              .type('PASSAGGIO_PERSONA')
              .addMetadata('GENERE', 'DONNA')
              .event(),
            {
              and: [
                eventHelper
                  .cam(1)
                  .type('PASSAGGIO_PERSONA')
                  .addMetadata('GENERE', 'UOMO')
                  .event(),
                eventHelper
                  .cam(1)
                  .type('PASSAGGIO_AUTO')
                  .addMetadata('COLORE', 'NERO')
                  .event(),
              ],
            },
          ],
        },
      },
      // passa una donna e un uomo
      {
        id: 2,
        enabled: true,
        condition: {
          and: [
            eventHelper
              .cam(2)
              .type('PASSAGGIO_PERSONA')
              .addMetadata('GENERE', 'DONNA')
              .event(),
            eventHelper
              .cam(1)
              .type('PASSAGGIO_PERSONA')
              .addMetadata('GENERE', 'UOMO')
              .event(),
          ],
        },
      },
    ];
    const event: IEvent = eventHelper
      .cam(1)
      .type('PASSAGGIO_AUTO')
      .addMetadata('TARGA', 'AB123CD')
      .addMetadata('COLORE', 'NERO')
      .event();

    const response = monitorService.getTriggersCorrispondence(
      event,
      triggerEntities,
    );
    expect(response).toEqual(
      expect.arrayContaining([triggerEntities[0], triggerEntities[1]]),
    );
  });

  describe('controlloCoincidenza', () => {
    it('controlloCoincidenza - evento ha piu metadati della definizione', () => {
      const event: IEvent = eventHelper
        .cam(1)
        .type('PASSAGGIO_AUTO')
        .addMetadata('TARGA', 'AB123CD')
        .addMetadata('COLORE', 'NERO')
        .addMetadata('TIPO', 'SUV')
        .event();
      const definition: IEvent = eventHelper
        .cam(1)
        .type('PASSAGGIO_AUTO')
        .addMetadata('TARGA', 'AB123CD')
        .addMetadata('COLORE', 'NERO')
        .event();

      const response = monitorService.controlloCoincidenza(event, definition);
      expect(response).toBe(true);
    });
    it('controlloCoincidenza - evento ha meno metadati della definizione', () => {
      const event: IEvent = eventHelper
        .cam(1)
        .type('PASSAGGIO_AUTO')
        .addMetadata('TARGA', 'AB123CD')
        .event();
      const definition: IEvent = eventHelper
        .cam(1)
        .type('PASSAGGIO_AUTO')
        .addMetadata('TARGA', 'AB123CD')
        .addMetadata('COLORE', 'NERO')
        .event();

      const response = monitorService.controlloCoincidenza(event, definition);
      expect(response).toBe(false);
    });
    it('controlloCoincidenza - stessi metadati con valori diversi', () => {
      const event: IEvent = eventHelper
        .cam(1)
        .type('PASSAGGIO_AUTO')
        .addMetadata('TARGA', 'CD345CD')
        .addMetadata('COLORE', 'BIANCA')
        .event();
      const definition: IEvent = eventHelper
        .cam(1)
        .type('PASSAGGIO_AUTO')
        .addMetadata('TARGA', 'AB123CD')
        .addMetadata('COLORE', 'NERO')
        .event();

      const response = monitorService.controlloCoincidenza(event, definition);
      expect(response).toBe(false);
    });
  });

  /*
  describe('Triggers', () => {
    describe('funzionamento', () => {
      let addedTrigger: GetTriggerDto;
      let addedAlarmEMAIL: GetAlarmDto;
      // Add alarm
      beforeAll(async () => {
        const alarmDto: CreateAlarmDto = {
          alarmType: 'EMAIL',
          options: {
            host: 'host.com',
            port: '3456',
            secure: 'false',
            user: 'username',
            password: 'password',

            from: 'mittente@mail.com',
            to: 'destinatario@mail.com',
            subject: 'Allarme',
            text: 'Trigger rilevato ${time}',
            html: 'HTML Trigger rilevato',
          },
          enabled: true,
        };
        const data = await request(app.getHttpServer())
          .post('/alarms')
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .send(alarmDto)
          .expect(201);
        expect(data.body).toEqual({
          ...alarmDto,
          id: expect.any(Number),
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
        addedAlarmEMAIL = data.body;
      });
      // Add trigger
      beforeAll(async () => {
        const triggerEvents: ITriggerEvents = {
          triggers: [
            {
              and: [
                eventHelper
                  .cam(1)
                  .type('PASSAGGIO_AUTO')
                  .addMetadata('TARGA', 'AB123CD')
                  .addMetadata('COLORE', 'NERO')
                  .event(),
                {
                  or: [
                    eventHelper
                      .cam(2)
                      .type('PASSAGGIO_AUTO')
                      .addMetadata('COLORE', 'BIANCA')
                      .event(),
                    {
                      and: [
                        eventHelper
                          .cam(1)
                          .type('PASSAGGIO_PERSONA')
                          .addMetadata('GENERE', 'DONNA')
                          .event(),
                        eventHelper
                          .cam(1)
                          .type('PASSAGGIO_AUTO')
                          .addMetadata('TARGA', 'AB321AB')
                          .event(),
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        };

        const triggerDto: CreateTriggerDto = {
          alarmsToBeTriggered: [addedAlarmEMAIL.id],
          triggerEvents: triggerEvents,
          enabled: true,
        };
        const data = await request(app.getHttpServer())
          .post('/triggers')
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .send(triggerDto)
          .expect(201);
        expect(data.body).toEqual({
          ...triggerDto,
          id: expect.any(Number),
          alarmsToBeTriggered: expect.arrayContaining([addedAlarmEMAIL.id]),
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
        addedTrigger = data.body;
        await monitorService.loadTriggersFromDb();
      });
      // Delete trigger
      afterAll(async () => {
        await request(app.getHttpServer())
          .delete(`/triggers/${addedTrigger.id}`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
      });
      // Delete email alarm
      afterAll(async () => {
        await request(app.getHttpServer())
          .delete(`/alarms/${addedAlarmEMAIL.id}`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
      });

      it('Test funzionamento algoritmo di rilevamento trigger valido', async () => {
        const triggerEvents: IEvent[] = [
          eventHelper
            .cam(1)
            .type('PASSAGGIO_AUTO')
            .addMetadata('TARGA', 'AB123CD')
            .addMetadata('COLORE', 'NERO')
            .event(),
          eventHelper
            .cam(1)
            .type('PASSAGGIO_PERSONA')
            .addMetadata('GENERE', 'DONNA')
            .event(),
        ];
        monitorService.triggersEvent[addedTrigger.id] = triggerEvents;
        const response = await monitorService.addTriggerEvent(
          addedTrigger.id,
          eventHelper
            .cam(1)
            .type('PASSAGGIO_AUTO')
            .addMetadata('TARGA', 'AB321AB')
            .event(),
        );
        expect(response).toBe(true);
      });
    });

    describe('rilevamento non valido', () => {
      let addedTrigger: GetTriggerDto;
      let addedAlarmEMAIL: GetAlarmDto;
      // Add alarm email
      beforeAll(async () => {
        const alarmDto: CreateAlarmDto = {
          alarmType: 'EMAIL',
          options: {
            host: 'host.com',
            port: '3456',
            secure: 'false',
            user: 'username',
            password: 'password',

            from: 'mittente@mail.com',
            to: 'destinatario@mail.com',
            subject: 'Allarme',
            text: 'Trigger rilevato ${time}',
            html: 'HTML Trigger rilevato',
          },
          enabled: true,
        };
        const data = await request(app.getHttpServer())
          .post('/alarms')
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .send(alarmDto)
          .expect(201);
        expect(data.body).toEqual({
          ...alarmDto,
          id: expect.any(Number),
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
        addedAlarmEMAIL = data.body;
      });
      // Add trigger
      beforeAll(async () => {
        const triggerEvents: ITriggerEvents = {
          triggers: [
            {
              and: [
                eventHelper
                  .cam(1)
                  .type('PASSAGGIO_AUTO')
                  .addMetadata('TARGA', 'AB123CD')
                  .addMetadata('COLORE', 'NERO')
                  .event(),
                {
                  or: [
                    eventHelper
                      .cam(2)
                      .type('PASSAGGIO_AUTO')
                      .addMetadata('COLORE', 'BIANCA')
                      .event(),
                    {
                      and: [
                        eventHelper
                          .cam(1)
                          .type('PASSAGGIO_PERSONA')
                          .addMetadata('GENERE', 'DONNA')
                          .event(),
                        eventHelper
                          .cam(1)
                          .type('PASSAGGIO_AUTO')
                          .addMetadata('TARGA', 'AB321AB')
                          .event(),
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        };

        const triggerDto: CreateTriggerDto = {
          alarmsToBeTriggered: [addedAlarmEMAIL.id],
          triggerEvents: triggerEvents,
          enabled: true,
        };
        const data = await request(app.getHttpServer())
          .post('/triggers')
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .send(triggerDto)
          .expect(201);
        expect(data.body).toEqual({
          ...triggerDto,
          id: expect.any(Number),
          alarmsToBeTriggered: expect.arrayContaining([addedAlarmEMAIL.id]),
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
        addedTrigger = data.body;
      });
      // Delete trigger
      afterAll(async () => {
        await request(app.getHttpServer())
          .delete(`/triggers/${addedTrigger.id}`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
      });
      // Delete email alarm
      afterAll(async () => {
        await request(app.getHttpServer())
          .delete(`/alarms/${addedAlarmEMAIL.id}`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
      });

      it('Test funzionamento algoritmo di rilevamento trigger NON valido', async () => {
        const triggerEvents: IEvent[] = [
          eventHelper
            .cam(1)
            .type('PASSAGGIO_AUTO')
            .addMetadata('TARGA', 'AB123CD')
            .addMetadata('COLORE', 'BIANCA')
            .event(),
          eventHelper
            .cam(1)
            .type('PASSAGGIO_PERSONA')
            .addMetadata('GENERE', 'DONNA')
            .event(),
        ];
        monitorService.triggersEvent[addedTrigger.id] = triggerEvents;
        const response = await monitorService.addTriggerEvent(
          addedTrigger.id,
          eventHelper
            .cam(1)
            .type('PASSAGGIO_AUTO')
            .addMetadata('TARGA', 'AB321AB')
            .event(),
        );
        expect(response).toBe(false);
      });
    });

    describe('pulizia eventi', () => {
      let addedTrigger: GetTriggerDto;
      let addedAlarmEMAIL: GetAlarmDto;
      // Add alarm email
      beforeAll(async () => {
        const alarmDto: CreateAlarmDto = {
          alarmType: 'EMAIL',
          options: {
            host: 'host.com',
            port: '3456',
            secure: 'false',
            user: 'username',
            password: 'password',

            from: 'mittente@mail.com',
            to: 'destinatario@mail.com',
            subject: 'Allarme',
            text: 'Trigger rilevato ${time}',
            html: 'HTML Trigger rilevato',
          },
          enabled: true,
        };
        const data = await request(app.getHttpServer())
          .post('/alarms')
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .send(alarmDto)
          .expect(201);
        expect(data.body).toEqual({
          ...alarmDto,
          id: expect.any(Number),
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
        addedAlarmEMAIL = data.body;
      });
      // Add trigger
      beforeAll(async () => {
        const triggerEvents: ITriggerEvents = {
          triggers: [
            {
              and: [
                eventHelper
                  .cam(1)
                  .type('PASSAGGIO_AUTO')
                  .addMetadata('TARGA', 'AB123CD')
                  .addMetadata('COLORE', 'NERO')
                  .event(),
                {
                  or: [
                    eventHelper
                      .cam(2)
                      .type('PASSAGGIO_AUTO')
                      .addMetadata('COLORE', 'BIANCA')
                      .event(),
                    {
                      and: [
                        eventHelper
                          .cam(1)
                          .type('PASSAGGIO_PERSONA')
                          .addMetadata('GENERE', 'DONNA')
                          .event(),
                        eventHelper
                          .cam(1)
                          .type('PASSAGGIO_AUTO')
                          .addMetadata('TARGA', 'AB321AB')
                          .event(),
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        };

        const triggerDto: CreateTriggerDto = {
          alarmsToBeTriggered: [addedAlarmEMAIL.id],
          triggerEvents: triggerEvents,
          enabled: true,
        };
        const data = await request(app.getHttpServer())
          .post('/triggers')
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .send(triggerDto)
          .expect(201);
        expect(data.body).toEqual({
          ...triggerDto,
          id: expect.any(Number),
          alarmsToBeTriggered: expect.arrayContaining([addedAlarmEMAIL.id]),
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
        addedTrigger = data.body;
      });
      // Delete trigger
      afterAll(async () => {
        await request(app.getHttpServer())
          .delete(`/triggers/${addedTrigger.id}`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
      });
      // Delete email alarm
      afterAll(async () => {
        await request(app.getHttpServer())
          .delete(`/alarms/${addedAlarmEMAIL.id}`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
      });
      it('Unit test pulizia eventi', async () => {
        const triggerEvents: IEvent[] = [
          eventHelper
            .cam(1)
            .type('PASSAGGIO_AUTO')
            .addMetadata('TARGA', 'AB123CD')
            .addMetadata('COLORE', 'NERO')
            .event(),
          eventHelper
            .cam(1)
            .type('PASSAGGIO_PERSONA')
            .addMetadata('GENERE', 'DONNA')
            .event(),
        ];
        monitorService.triggersEvent[addedTrigger.id] = triggerEvents;

        await new Promise((resolve, reject) => {
          setTimeout(async () => {
            monitorService.cleanTriggerEvents(addedTrigger.id);
            resolve(true);
          }, 20000);
        });

        expect(monitorService.triggersEvent[addedTrigger.id]).toHaveLength(0);
      }, 20000);
      it('Test funzionamento algoritmo di pulizia eventi', async () => {
        await new Promise((resolve, reject) => {
          setTimeout(async () => {
            await monitorService.addTriggerEvent(
              addedTrigger.id,
              eventHelper
                .cam(1)
                .type('PASSAGGIO_PERSONA')
                .addMetadata('GENERE', 'DONNA')
                .event(),
            );
            resolve(true);
          }, 0);
        });

        await new Promise((resolve, reject) => {
          setTimeout(async () => {
            await monitorService.addTriggerEvent(
              addedTrigger.id,
              eventHelper
                .cam(1)
                .type('PASSAGGIO_AUTO')
                .addMetadata('TARGA', 'AB123CD')
                .addMetadata('COLORE', 'NERO')
                .event(),
            );
            resolve(true);
          }, 5000);
        });

        await new Promise((resolve, reject) => {
          setTimeout(async () => {
            await monitorService.addTriggerEvent(
              addedTrigger.id,
              eventHelper
                .cam(1)
                .type('PASSAGGIO_AUTO')
                .addMetadata('TARGA', 'AB321AB')
                .event(),
            );
            resolve(true);
          }, 10000);
        });

        await setTimeout(async () => {
          const response = await monitorService.addTriggerEvent(
            addedTrigger.id,
            eventHelper
              .cam(1)
              .type('PASSAGGIO_AUTO')
              .addMetadata('TARGA', 'AB321AB')
              .event(),
          );
          expect(response).toBe(false);
        }, 5000);
      }, 20000);
    });
  });
   */

  describe('Alarm Triggered', () => {
    let camera1: GetCameraDto;
    let camera2: GetCameraDto;
    let addedAlarmSMS: GetAlarmDto;
    let addedAlarmEMAIL: GetAlarmDto;
    let trigger1: GetTriggerDto;
    let trigger2: GetTriggerDto;
    const expectedAlarmsTriggered = [];
    let alarmsTriggeredDto: GetAlarmTriggeredDto[];

    // cameras
    beforeAll(async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM 1',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      expect(data.body).toEqual({
        ...camera,
        id: expect.any(Number),
        group: null,
        recorderOptions: null,
        streamerOptions: null,
        createdAt: expect.any(String), //TEST,
        updatedAt: expect.any(String), //TEST,
      });
      camera1 = data.body;
    });
    beforeAll(async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM 2',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      expect(data.body).toEqual({
        ...camera,
        id: expect.any(Number),
        group: null,
        recorderOptions: null,
        streamerOptions: null,
        createdAt: expect.any(String), //TEST,
        updatedAt: expect.any(String), //TEST,
      });
      camera2 = data.body;
    });
    afterAll(async () => {
      await request(app.getHttpServer())
        .delete(`/cameras/${camera1.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
    afterAll(async () => {
      await request(app.getHttpServer())
        .delete(`/cameras/${camera2.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });

    // alarms
    beforeAll(async () => {
      const alarmDto: CreateAlarmDto = {
        alarmType: 'SMS',
        configuration: {
          destinationNumber: '+393481633500',
        },
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post('/alarms')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(alarmDto)
        .expect(201);
      expect(data.body).toEqual({
        ...alarmDto,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedAlarmSMS = data.body;
    });
    beforeAll(async () => {
      const alarmDto: CreateAlarmDto = {
        alarmType: 'EMAIL',
        configuration: {
          host: 'host.com',
          port: '3456',
          secure: 'false',
          user: 'username',
          password: 'password',

          from: 'mittente@mail.com',
          to: 'destinatario@mail.com',
          subject: 'Allarme',
          text: 'Trigger rilevato ${time}',
          html: 'HTML Trigger rilevato',
        },
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post('/alarms')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(alarmDto)
        .expect(201);
      expect(data.body).toEqual({
        ...alarmDto,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedAlarmEMAIL = data.body;
    });
    afterAll(async () => {
      await request(app.getHttpServer())
        .delete(`/alarms/${addedAlarmSMS.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
    afterAll(async () => {
      await request(app.getHttpServer())
        .delete(`/alarms/${addedAlarmEMAIL.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });

    // triggers
    beforeAll(async () => {
      const condition: ICondition<IEvent> = {
        and: [
          {
            cameraId: camera1.id,
            timestamp: new Date().toISOString(),
            eventType: 'MOVIMENTO',
            eventMetadata: [
              {
                key: 'DIREZIONE',
                value: 'SUD',
              },
            ],
          },
        ],
      };
      const triggerDto: CreateTriggerDto = {
        alarmsToBeTriggered: [addedAlarmSMS.id, addedAlarmEMAIL.id],
        condition: condition,
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post('/triggers')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(triggerDto)
        .expect(201);
      expect(data.body).toEqual({
        ...triggerDto,
        id: expect.any(Number),
        alarmsToBeTriggered: expect.arrayContaining([
          addedAlarmSMS.id,
          addedAlarmEMAIL.id,
        ]),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      trigger1 = data.body;
    });
    beforeAll(async () => {
      const condition: ICondition<IEvent> = {
        and: [
          {
            cameraId: camera1.id,
            timestamp: new Date().toISOString(),
            eventType: 'MOVIMENTO',
            eventMetadata: [
              {
                key: 'DIREZIONE',
                value: 'SUD',
              },
            ],
          },
          {
            cameraId: camera2.id,
            timestamp: new Date().toISOString(),
            eventType: 'MOVIMENTO',
            eventMetadata: [
              {
                key: 'DIREZIONE',
                value: 'NORD',
              },
            ],
          },
        ],
      };
      const triggerDto: CreateTriggerDto = {
        alarmsToBeTriggered: [addedAlarmSMS.id],
        condition: condition,
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post('/triggers')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(triggerDto)
        .expect(201);
      expect(data.body).toEqual({
        ...triggerDto,
        id: expect.any(Number),
        alarmsToBeTriggered: expect.arrayContaining([addedAlarmSMS.id]),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      trigger2 = data.body;
    });
    afterAll(async () => {
      await request(app.getHttpServer())
        .delete(`/triggers/${trigger1.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
    afterAll(async () => {
      await request(app.getHttpServer())
        .delete(`/triggers/${trigger2.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });

    it('Triggering trigger 1', async () => {
      const response = await monitorService.addEvent(
        eventHelper
          .cam(camera1.id)
          .type('MOVIMENTO')
          .addMetadata('DIREZIONE', 'SUD')
          .event(),
      );
      expect(response).toBe(true);

      expectedAlarmsTriggered.push({
        id: expect.any(Number),
        trigger: {
          ...trigger1,
          alarmsToBeTriggered: expect.arrayContaining([
            ...trigger1.alarmsToBeTriggered,
          ]),
        },
        alarm: addedAlarmSMS,
        status: StatusAlarmTriggeredEnum.SUCCESSFUL,
        triggeredAt: expect.any(String),
      });
      expectedAlarmsTriggered.push({
        id: expect.any(Number),
        trigger: {
          ...trigger1,
          alarmsToBeTriggered: expect.arrayContaining([
            ...trigger1.alarmsToBeTriggered,
          ]),
        },
        alarm: addedAlarmEMAIL,
        status: StatusAlarmTriggeredEnum.SUCCESSFUL,
        triggeredAt: expect.any(String),
      });
    });
    it('Triggering trigger 2', async () => {
      await new Promise((resolve, reject) => {
        setTimeout(async () => {
          resolve(true);
        }, 2000);
      });
      await monitorService.addEvent(
        eventHelper
          .cam(camera1.id)
          .type('MOVIMENTO')
          .addMetadata('DIREZIONE', 'SUD')
          .event(),
      );
      const response = await monitorService.addEvent(
        eventHelper
          .cam(camera2.id)
          .type('MOVIMENTO')
          .addMetadata('DIREZIONE', 'NORD')
          .event(),
      );
      expect(response).toBe(true);
      expectedAlarmsTriggered.push({
        id: expect.any(Number),
        trigger: {
          ...trigger1,
          alarmsToBeTriggered: expect.arrayContaining([
            ...trigger1.alarmsToBeTriggered,
          ]),
        },
        alarm: addedAlarmSMS,
        status: StatusAlarmTriggeredEnum.SUCCESSFUL,
        triggeredAt: expect.any(String),
      });
      expectedAlarmsTriggered.push({
        id: expect.any(Number),
        trigger: {
          ...trigger1,
          alarmsToBeTriggered: expect.arrayContaining([
            ...trigger1.alarmsToBeTriggered,
          ]),
        },
        alarm: addedAlarmEMAIL,
        status: StatusAlarmTriggeredEnum.SUCCESSFUL,
        triggeredAt: expect.any(String),
      });
      expectedAlarmsTriggered.push({
        id: expect.any(Number),
        trigger: {
          ...trigger2,
          alarmsToBeTriggered: expect.arrayContaining([
            ...trigger2.alarmsToBeTriggered,
          ]),
        },
        alarm: addedAlarmSMS,
        status: StatusAlarmTriggeredEnum.SUCCESSFUL,
        triggeredAt: expect.any(String),
      });
    });

    it('get all', async () => {
      await new Promise((resolve, reject) => {
        setTimeout(async () => {
          resolve(true);
        }, 2000);
      });
      const data = await request(app.getHttpServer())
        .get(`/alarms-triggered`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      Logger.log(JSON.stringify(expectedAlarmsTriggered));
      Logger.log(JSON.stringify(data.body));
      expect(data.body).toHaveLength(5);
      expect(data.body).toEqual(expectedAlarmsTriggered);
      alarmsTriggeredDto = data.body;
    }, 10000);
    it('delete all alarms triggered', async () => {
      return await Promise.all(
        alarmsTriggeredDto.map(async (alarmTriggeredDto) => {
          return request(app.getHttpServer())
            .delete(`/alarms-triggered/${alarmTriggeredDto.id}`)
            .set('Authorization', `Bearer ${adminLoginResponse.token}`)
            .expect(200);
        }),
      );
    });
  });

  describe('Events', () => {
    let camera1: GetCameraDto;
    let camera2: GetCameraDto;
    let events: IEvent[];
    const eventsDto: GetEventDto[] = [];

    beforeAll(async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM 1',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      expect(data.body).toEqual({
        ...camera,
        id: expect.any(Number),
        group: null,
        recorderOptions: null,
        streamerOptions: null,
        createdAt: expect.any(String), //TEST,
        updatedAt: expect.any(String), //TEST,
      });
      camera1 = data.body;
    });
    beforeAll(async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM 2',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      expect(data.body).toEqual({
        ...camera,
        id: expect.any(Number),
        group: null,
        recorderOptions: null,
        streamerOptions: null,
        createdAt: expect.any(String), //TEST,
        updatedAt: expect.any(String), //TEST,
      });
      camera2 = data.body;
    });
    afterAll(async () => {
      await request(app.getHttpServer())
        .delete(`/cameras/${camera1.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
    afterAll(async () => {
      await request(app.getHttpServer())
        .delete(`/cameras/${camera2.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });

    it('add events', async () => {
      events = [
        eventHelper
          .cam(camera1.id)
          .timestamp(new Date('2021-07-28T11:51:00'))
          .type('PASSAGGIO_AUTO')
          .addMetadata('TARGA', 'AB123CD')
          .addMetadata('COLORE', 'NERO')
          .event(),
        eventHelper
          .cam(camera1.id)
          .timestamp(new Date('2021-07-28T15:40:00'))
          .type('PASSAGGIO_PERSONA')
          .addMetadata('GENERE', 'DONNA')
          .event(),
        eventHelper
          .cam(camera2.id)
          .timestamp(new Date('2021-07-28T06:12:00'))
          .type('PASSAGGIO_AUTO')
          .addMetadata('TARGA', 'AB345CD')
          .addMetadata('COLORE', 'GIALLA')
          .event(),
        eventHelper
          .cam(camera1.id)
          .timestamp(new Date('2021-07-27T17:51:00'))
          .type('PASSAGGIO_PERSONA')
          .addMetadata('GENERE', 'UOMO')
          .event(),
        eventHelper
          .cam(camera2.id)
          .timestamp(new Date('2021-07-27T21:53:00'))
          .type('PASSAGGIO_AUTO')
          .addMetadata('TARGA', 'AA456BB')
          .addMetadata('COLORE', 'BIANCA')
          .event(),
        eventHelper
          .cam(camera2.id)
          .timestamp(new Date('2021-07-28T20:30:00'))
          .type('PASSAGGIO_PERSONA')
          .addMetadata('GENERE', 'DONNA')
          .event(),
      ];
      for (const event of events) {
        const data = await eventService.addEvent(event);
        expect(data).toBeDefined();
        eventsDto.push(eventConverter.convert(data));
      }
    });
    it('get all', async () => {
      const data = await request(app.getHttpServer())
        .get(`/events`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([
        eventsDto[3],
        eventsDto[4],
        eventsDto[2],
        eventsDto[0],
        eventsDto[1],
        eventsDto[5],
      ]);
    });
    it('get all dal 27 alle 20:56 al 28 alle 13:05', async () => {
      const data = await request(app.getHttpServer())
        .get(
          `/events?fromTime=${moment(
            new Date('2021-07-27T20:56:00'),
          ).toISOString()}&toTime=${moment(
            new Date('2021-07-28T13:05:00'),
          ).toISOString()}`,
        )
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([eventsDto[4], eventsDto[2], eventsDto[0]]);
    });
    it('get all dal 28 alle 06:13', async () => {
      const data = await request(app.getHttpServer())
        .get(
          `/events?fromTime=${moment(
            new Date('2021-07-28T06:13:00'),
          ).toISOString()}`,
        )
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([eventsDto[0], eventsDto[1], eventsDto[5]]);
    });
    it('get all dal 28 alle 06:12', async () => {
      const data = await request(app.getHttpServer())
        .get(
          `/events?fromTime=${moment(
            new Date('2021-07-28T06:12:00'),
          ).toISOString()}`,
        )
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([
        eventsDto[2],
        eventsDto[0],
        eventsDto[1],
        eventsDto[5],
      ]);
    });
    it('get all fino al 28 alle 06:12', async () => {
      const data = await request(app.getHttpServer())
        .get(
          `/events?toTime=${moment(
            new Date('2021-07-28T06:12:00'),
          ).toISOString()}`,
        )
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([eventsDto[3], eventsDto[4], eventsDto[2]]);
    });
    it('delete all events', async () => {
      return await Promise.all(
        eventsDto.map(async (event) => {
          return request(app.getHttpServer())
            .delete(`/events/${event.id}`)
            .set('Authorization', `Bearer ${adminLoginResponse.token}`)
            .expect(200);
        }),
      );
    });
  });
});
