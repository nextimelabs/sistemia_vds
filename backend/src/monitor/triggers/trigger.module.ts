import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../../entity/users/users.module';
import { TriggerController } from './trigger.controller';
import { TriggerService } from './trigger.service';
import { TriggerConverter } from './trigger.converter';
import { AlarmEntity } from '../alarms/entities/alarms.entity';
import { AlarmsToBeTriggeredEntity } from './entities/alarms-to-be-triggered.entity';
import { TriggerEntity } from './entities/triggers.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      TriggerEntity,
      AlarmsToBeTriggeredEntity
    ]),
    UsersModule
  ],
  controllers: [TriggerController],
  providers: [TriggerService, TriggerConverter],
  exports: [TriggerService, TriggerConverter],
})
export class TriggerModule {}
