import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsDate, IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString, IsUUID, MaxLength,
  ValidateNested, IsISO8601, MinLength, IsNumber, IsJSON, IsArray,
} from 'class-validator';
import { ICondition, ITriggerEvents } from '../../interfaces/trigger-events.interface';
import { IEvent } from '../../events/enums/event-type.enum';


export class GetTriggerDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @ApiProperty({ type: Object })
  @IsNotEmpty()
  condition: ICondition<IEvent>;

  @ApiProperty()
  @IsNotEmpty()
  @IsBoolean()
  enabled: boolean;

  @ApiProperty({ type: [Number]})
  @IsNotEmpty()
  @IsArray()
  alarmsToBeTriggered: number[];

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
