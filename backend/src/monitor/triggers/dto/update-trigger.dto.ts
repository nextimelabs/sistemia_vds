import { PartialType } from '@nestjs/swagger';
import { CreateAlarmDto } from '../../alarms/dto/create-alarm.dto';
import { CreateTriggerDto } from './create-trigger.dto';

export class UpdateTriggerDto extends PartialType(CreateTriggerDto) {}
