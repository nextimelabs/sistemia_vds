import { Injectable } from '@nestjs/common';
import { TriggerService } from './trigger.service';
import { TriggerEntity } from './entities/triggers.entity';
import { GetTriggerDto } from './dto/get-trigger.dto';

@Injectable()
export class TriggerConverter {
  constructor() {}

  convert(triggerEntity: TriggerEntity): GetTriggerDto {
    return {
      id: triggerEntity.id,
      condition: triggerEntity.condition,
      alarmsToBeTriggered: triggerEntity.alarmsToBeTriggered.map((alarmToBeTriggered) => alarmToBeTriggered.alarmId),
      enabled: triggerEntity.enabled,
      createdAt: triggerEntity.createdAt,
      updatedAt: triggerEntity.updatedAt,
    };
  }

  converts(triggerEntities: TriggerEntity[]): GetTriggerDto[] {
    return triggerEntities.map((triggerEntity) => {
      return this.convert(triggerEntity);
    });
  }
}
