import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { CreateMonitorDto } from '../dto/create-monitor.dto';
import { UpdateMonitorDto } from '../dto/update-monitor.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CameraEntity } from '../../entity/cameras/entities/camera.entity';
import { Repository } from 'typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CreateEventDto } from '../events/dto/create-event.dto';
import { CreateTriggerDto } from './dto/create-trigger.dto';
import { UpdateTriggerDto } from './dto/update-trigger.dto';
import { TriggerEntity } from './entities/triggers.entity';
import { AlarmsToBeTriggeredEntity } from './entities/alarms-to-be-triggered.entity';
import { StatusAlarmTriggeredEnum } from '../alarms/enums/status-alarm-triggered.enum';

interface ISearchTrigger {
  limit: number;
  offset: number;
  orderBy: 'ASC' | 'DESC';
  orderColumn: string;
  enabled?: boolean;
}

@Injectable()
export class TriggerService {
  constructor(
    @InjectRepository(TriggerEntity)
    private triggerEntityRepository: Repository<TriggerEntity>,
    private eventEmitter: EventEmitter2,
    @InjectRepository(AlarmsToBeTriggeredEntity)
    private alarmsToBeTriggeredEntityRepository: Repository<AlarmsToBeTriggeredEntity>,
  ) {}

  async create(createTriggerDto: CreateTriggerDto) {
    let triggerEntity: TriggerEntity = {
      condition: createTriggerDto.condition,
      enabled: createTriggerDto.enabled,
    };
    triggerEntity = await this.triggerEntityRepository.save(triggerEntity);
    await Promise.all(
      createTriggerDto.alarmsToBeTriggered.map(async (alarmId) => {
        await this.alarmsToBeTriggeredEntityRepository.save({
          triggerId: triggerEntity.id,
          alarmId: alarmId,
        });
      }),
    );
    this.eventEmitter.emit('triggers.changed');
    return this.findOne(triggerEntity.id);
  }

  async find({ enabled, limit, offset, orderColumn, orderBy }: ISearchTrigger) {
    let queryBuilder =
      this.triggerEntityRepository.createQueryBuilder('trigger');

    if (enabled)
      queryBuilder = queryBuilder.andWhere('trigger.enabled = :enabled', {
        enabled,
      });
    return queryBuilder
      .orderBy(`alarmsTriggered.${orderColumn}`, orderBy)
      .limit(limit)
      .offset(offset)
      .getManyAndCount();
  }

  async findAllEnabled() {
    return this.triggerEntityRepository
      .createQueryBuilder('triggerEntity')
      .leftJoinAndSelect(
        'triggerEntity.alarmsToBeTriggered',
        'alarmsToBeTriggered',
      )
      .leftJoinAndSelect('alarmsToBeTriggered.alarm', 'alarm')
      .where('triggerEntity.enabled = true')
      .getMany();
  }

  async findOne(id: number) {
    const triggerEntity: TriggerEntity =
      await this.triggerEntityRepository.findOne(id, {
        relations: ['alarmsToBeTriggered'],
      });
    if (!triggerEntity) {
      throw new HttpException('TRIGGER_NOT_FOUND', HttpStatus.NOT_FOUND);
    }
    return triggerEntity;
  }

  async update(id: number, updateTriggerDto: UpdateTriggerDto) {
    const oldTriggerEntity = await this.findOne(id);

    let triggerEntity: TriggerEntity = {
      ...oldTriggerEntity,
      condition: updateTriggerDto.condition,
      enabled: updateTriggerDto.enabled,
    };
    triggerEntity = await this.triggerEntityRepository.save(triggerEntity);
    await this.alarmsToBeTriggeredEntityRepository.delete({
      triggerId: triggerEntity.id,
    });
    await Promise.all(
      updateTriggerDto.alarmsToBeTriggered.map(async (alarmId) => {
        await this.alarmsToBeTriggeredEntityRepository.save({
          triggerId: triggerEntity.id,
          alarmId: alarmId,
        });
      }),
    );
    this.eventEmitter.emit('triggers.changed');
    return this.findOne(triggerEntity.id);
  }

  async remove(id: number) {
    const trigger = await this.findOne(id);
    await this.triggerEntityRepository.remove(trigger);
    this.eventEmitter.emit('triggers.changed');
    return true;
  }
}
