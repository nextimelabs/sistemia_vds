import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { ICondition, ITriggerEvents } from '../../interfaces/trigger-events.interface';
import { AlarmsToBeTriggeredEntity } from './alarms-to-be-triggered.entity';
import { IEvent } from '../../events/enums/event-type.enum';

@Entity('triggers')
export class TriggerEntity {
  @PrimaryGeneratedColumn()
  id?: number;
  @Column('boolean', { nullable: false })
  enabled: boolean;
  @Column('json', { nullable: false })
  condition: ICondition<IEvent>;
  @OneToMany((type) => AlarmsToBeTriggeredEntity, (alarmToBeTriggered) => alarmToBeTriggered.trigger)
  alarmsToBeTriggered?: AlarmsToBeTriggeredEntity[];
  @CreateDateColumn({ type: 'timestamp without time zone', default: () => 'NOW()' })
  createdAt?: Date;
  @UpdateDateColumn({ type: 'timestamp without time zone', default: () => "NOW()", onUpdate: "NOW()" })
  updatedAt?: Date;
}
