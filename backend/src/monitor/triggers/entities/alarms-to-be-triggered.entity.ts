import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne, OneToMany, OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { TriggerEntity } from './triggers.entity';
import { AlarmEntity } from '../../alarms/entities/alarms.entity';

@Entity('alarms-to-be-triggered')
export class AlarmsToBeTriggeredEntity {
  @PrimaryGeneratedColumn()
  id?: number;
  @ManyToOne((type) => TriggerEntity, (trigger) => trigger.id, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  trigger?: TriggerEntity;
  @Column({ nullable: true })
  triggerId?: number;
  //
  @ManyToOne((type) => AlarmEntity, (alarm) => alarm.id, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  alarm?: AlarmEntity;
  @Column({ nullable: true })
  alarmId?: number;
}
