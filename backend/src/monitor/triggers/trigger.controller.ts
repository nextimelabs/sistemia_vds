import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  Put,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
  Query,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { Roles } from '../../entity/auth/auth.decorator';
import { RuoloEnum } from '../../entity/users/enums/ruolo.enum';
import { AuthGuard } from '../../entity/auth/auth.guard';
import { TriggerService } from './trigger.service';
import { TriggerConverter } from './trigger.converter';
import { GetTriggerDto } from './dto/get-trigger.dto';
import { CreateTriggerDto } from './dto/create-trigger.dto';
import { UpdateTriggerDto } from './dto/update-trigger.dto';
import { ApiPaginatedResponse } from '../../shared/pagination.decorator';
import { GetAlarmTriggeredDto } from '../alarms/dto/get-alarm-triggered.dto';
import { StatusAlarmTriggeredEnum } from '../alarms/enums/status-alarm-triggered.enum';

@ApiTags('Monitor Module - Triggers')
@Controller('triggers')
export class TriggerController {
  constructor(
    private readonly triggerService: TriggerService,
    private readonly triggerConverter: TriggerConverter,
  ) {}

  @Post()
  @ApiBearerAuth()
  @ApiCreatedResponse({
    description: 'Trigger created succesfull',
    type: GetTriggerDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async create(@Body() createTriggerDto: CreateTriggerDto) {
    try {
      return this.triggerConverter.convert(
        await this.triggerService.create(createTriggerDto),
      );
    } catch (e) {
      if (e.code == 23503)
        throw new HttpException('ALARM_NOT_FOUND', HttpStatus.NOT_FOUND);
      throw new InternalServerErrorException();
    }
  }

  @Get()
  @ApiBearerAuth()
  @ApiPaginatedResponse(GetTriggerDto, 'Get paginated trigger list')
  @ApiQuery({
    name: 'orderColumn',
    enum: ['id', 'enabled', 'createdAt'],
    required: false,
  })
  @ApiQuery({ name: 'enabled', type: Boolean, required: false })
  @ApiQuery({ name: 'orderBy', enum: ['ASC', 'DESC'], required: false })
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @ApiQuery({ name: 'offset', type: Number, required: false })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async find(
    @Query('enabled') enabled: boolean,
    @Query('orderBy') orderBy: 'ASC' | 'DESC' = 'DESC',
    @Query('orderColumn') orderColumn = 'id',
    @Query('limit') limit = 10,
    @Query('offset') offset = 0,
  ) {
    const res = await this.triggerService.find({
      enabled,
      limit,
      offset,
      orderColumn,
      orderBy,
    });
    return {
      total: res[1],
      limit: limit,
      offset: offset,
      results: await this.triggerConverter.converts(res[0]),
    };
  }

  @Get(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get trigger',
    type: GetTriggerDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async findOne(@Param('id') id: number) {
    const triggerEntity = await this.triggerService.findOne(id);
    return this.triggerConverter.convert(triggerEntity);
  }

  @Put(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Edit trigger',
    type: GetTriggerDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async update(
    @Param('id') id: number,
    @Body() updateTriggerDto: UpdateTriggerDto,
  ) {
    try {
      const triggerEntity = await this.triggerService.update(
        id,
        updateTriggerDto,
      );
      return this.triggerConverter.convert(triggerEntity);
    } catch (e) {
      if (e.code == 23503)
        throw new HttpException('ALARM_NOT_FOUND', HttpStatus.NOT_FOUND);
      throw new InternalServerErrorException();
    }
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete trigger',
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async remove(@Param('id') id: number) {
    await this.triggerService.remove(id);
    return null;
  }
}
