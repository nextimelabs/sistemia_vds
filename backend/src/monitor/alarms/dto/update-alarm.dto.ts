import { PartialType } from '@nestjs/swagger';
import { CreateMonitorDto } from '../../dto/create-monitor.dto';
import { CreateAlarmDto } from './create-alarm.dto';

export class UpdateAlarmDto extends PartialType(CreateAlarmDto) {}
