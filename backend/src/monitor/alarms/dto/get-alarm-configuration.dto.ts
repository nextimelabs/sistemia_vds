import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsDate,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  ValidateNested,
  IsISO8601,
  MinLength,
  IsNumber,
  IsJSON,
  IsArray,
} from 'class-validator';
import { IOptionKey } from '../modules/alarm.implements';

export class GetAlarmConfigurationDto {
  @ApiProperty({ maxLength: 255 })
  @IsNotEmpty()
  @MaxLength(255)
  alarmType: string;

  @ApiProperty({ type: [IOptionKey] })
  @IsNotEmpty()
  options: IOptionKey[];
}
