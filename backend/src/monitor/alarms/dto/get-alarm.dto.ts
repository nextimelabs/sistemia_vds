import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsDate, IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString, IsUUID, MaxLength,
  ValidateNested, IsISO8601, MinLength, IsNumber, IsJSON,
} from 'class-validator';


export class GetAlarmDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @ApiProperty({maxLength: 255})
  @IsNotEmpty()
  name: string;

  @ApiProperty({maxLength: 255})
  @IsNotEmpty()
  @MaxLength(255)
  alarmType: string;

  @ApiProperty()
  @IsNotEmpty()
  configuration: object;

  @ApiProperty()
  @IsNotEmpty()
  @IsBoolean()
  enabled: boolean;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
