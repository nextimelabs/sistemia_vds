import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsDate, IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString, IsUUID, MaxLength,
  ValidateNested, IsISO8601, MinLength, IsNumber, IsJSON,
} from 'class-validator';
import { StatusAlarmTriggeredEnum } from '../enums/status-alarm-triggered.enum';
import { GetAlarmDto } from './get-alarm.dto';
import { GetTriggerDto } from '../../triggers/dto/get-trigger.dto';


export class GetAlarmTriggeredDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @ApiProperty({ enum: StatusAlarmTriggeredEnum })
  @IsNotEmpty()
  @IsEnum(StatusAlarmTriggeredEnum)
  status: StatusAlarmTriggeredEnum;

  @ApiPropertyOptional()
  info?: string;

  @ApiProperty({ type: GetAlarmDto })
  alarm: GetAlarmDto;

  @ApiProperty({ type: GetTriggerDto })
  trigger: GetTriggerDto;

  @ApiProperty()
  triggeredAt: Date;
}
