import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsDate, IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString, IsUUID, MaxLength,
  ValidateNested, IsISO8601, MinLength, IsNumber, IsJSON,
} from 'class-validator';
import { IOptionKey } from '../modules/alarm.implements';


export class CreateAlarmDto {
  @ApiProperty({maxLength: 255})
  @IsNotEmpty()
  @MaxLength(255)
  name: string;

  @ApiProperty({maxLength: 255})
  @IsNotEmpty()
  @MaxLength(255)
  alarmType: string;

  @ApiProperty()
  @IsNotEmpty()
  configuration: Record<string, any>;

  @ApiProperty()
  @IsNotEmpty()
  @IsBoolean()
  enabled: boolean;
}
