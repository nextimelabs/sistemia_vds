import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Logger,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { RuoloEnum } from '../../entity/users/enums/ruolo.enum';
import { Roles } from '../../entity/auth/auth.decorator';
import { AuthGuard } from '../../entity/auth/auth.guard';
import { StatusAlarmTriggeredEnum } from './enums/status-alarm-triggered.enum';
import { AlarmService } from './alarm.service';
import { AlarmConverter, AlarmTriggeredConverter } from './alarm.converter';
import { GetEventDto } from '../events/dto/get-event.dto';
import { GetAlarmTriggeredDto } from './dto/get-alarm-triggered.dto';
import { ApiPaginatedResponse } from '../../shared/pagination.decorator';
import { GetSegmentDto } from '../../recorder/dto/get-segment.dto';
import { AlarmTriggeredEntity } from './entities/alarm-triggered.entity';

@Controller('alarms-triggered')
@ApiTags('Monitor Module - Alarms Triggered')
export class AlarmTriggeredController {
  constructor(
    private readonly alarmService: AlarmService,
    private readonly alarmConverter: AlarmConverter,
    private readonly alarmTriggeredConverter: AlarmTriggeredConverter,
  ) {}

  @Get('')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiPaginatedResponse(GetAlarmTriggeredDto, 'Get paginated segments list')
  @ApiQuery({
    name: 'orderColumn',
    enum: ['id', 'alarmId', 'triggerId', 'status'],
    required: false,
  })
  @ApiQuery({ name: 'alarmId', type: Number, required: false })
  @ApiQuery({ name: 'alarmName', type: Number, required: false })
  @ApiQuery({ name: 'triggerId', type: Number, required: false })
  @ApiQuery({ name: 'fromTime', type: String, required: false })
  @ApiQuery({ name: 'toTime', type: String, required: false })
  @ApiQuery({ name: 'orderBy', enum: ['ASC', 'DESC'], required: false })
  @ApiQuery({ name: 'status', enum: StatusAlarmTriggeredEnum, required: false })
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @ApiQuery({ name: 'offset', type: Number, required: false })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async get(
    @Query('alarmId') alarmId: number,
    @Query('alarmName') alarmName: string,
    @Query('triggerId') triggerId: number,
    @Query('fromTime') fromTime: string,
    @Query('toTime') toTime: string,
    @Query('status') status: StatusAlarmTriggeredEnum,
    @Query('orderBy') orderBy: 'ASC' | 'DESC' = 'DESC',
    @Query('orderColumn') orderColumn = 'id',
    @Query('limit') limit = 10,
    @Query('offset') offset = 0,
  ) {
    const res = await this.alarmService.findAllTriggered({
      status,
      alarmId,
      alarmName,
      triggerId,
      fromTime: fromTime ? new Date(fromTime) : null,
      toTime: toTime ? new Date(toTime) : null,
      limit,
      offset,
      orderColumn,
      orderBy,
    });
    return {
      total: res[1],
      limit: limit,
      offset: offset,
      results: await this.alarmTriggeredConverter.converts(res[0]),
    };
  }

  @Delete(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete alarm triggered by id',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async delete(@Param('id') id: number) {
    return await this.alarmService.deleteAlarmTriggered(id);
  }
}
