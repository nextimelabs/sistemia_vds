import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { IOptionKey } from '../modules/alarm.implements';
import { AlarmsToBeTriggeredEntity } from '../../triggers/entities/alarms-to-be-triggered.entity';

@Entity('alarms')
export class AlarmEntity {
  @PrimaryGeneratedColumn()
  id?: number;
  @Column('boolean', { nullable: false })
  enabled: boolean;
  @Column('varchar', { length: 255, nullable: false })
  alarmType: string;
  @Column('varchar', { length: 255, nullable: false })
  name: string;
  @Column('json', { nullable: false })
  configuration: Record<string, any>;
  @CreateDateColumn({ type: 'timestamp without time zone', default: () => 'NOW()' })
  createdAt?: Date;
  @UpdateDateColumn({ type: 'timestamp without time zone', default: () => "NOW()", onUpdate: "NOW()" })
  updatedAt?: Date;
}
