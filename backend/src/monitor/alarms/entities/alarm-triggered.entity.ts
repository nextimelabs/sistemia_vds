import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { AlarmEntity } from './alarms.entity';
import { TriggerEntity } from '../../triggers/entities/triggers.entity';
import { StatusAlarmTriggeredEnum } from '../enums/status-alarm-triggered.enum';

@Entity('alarms-triggered')
export class AlarmTriggeredEntity {
  @PrimaryGeneratedColumn()
  id?: number;
  //
  @ManyToOne((type) => TriggerEntity, (trigger) => trigger.id, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  trigger?: TriggerEntity;
  @Column({ nullable: true })
  triggerId?: number;
  //
  @ManyToOne((type) => AlarmEntity, (alarm) => alarm.id, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  alarm?: AlarmEntity;
  @Column({ nullable: true })
  alarmId?: number;
  //
  @Column('varchar', { nullable: false })
  status: StatusAlarmTriggeredEnum;
  @Column('text', { nullable: true })
  info?: string;
  @CreateDateColumn({ type: 'timestamp without time zone', default: () => 'NOW()' })
  triggeredAt?: Date;
}
