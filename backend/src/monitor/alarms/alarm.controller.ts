import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Logger,
  UseGuards,
  InternalServerErrorException,
  Put,
  HttpCode,
  Query,
} from '@nestjs/common';
import { MonitorService } from '../monitor.service';
import { CreateMonitorDto } from '../dto/create-monitor.dto';
import { UpdateMonitorDto } from '../dto/update-monitor.dto';
import {
  Client,
  ClientKafka,
  MessagePattern,
  Payload,
  Transport,
} from '@nestjs/microservices';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { GetCameraDto } from '../../entity/cameras/dto/get-camera.dto';
import { Roles } from '../../entity/auth/auth.decorator';
import { RuoloEnum } from '../../entity/users/enums/ruolo.enum';
import { AuthGuard } from '../../entity/auth/auth.guard';
import { GetAlarmDto } from './dto/get-alarm.dto';
import { CreateAlarmDto } from './dto/create-alarm.dto';
import { AlarmService } from './alarm.service';
import { AlarmConverter, AlarmTriggeredConverter } from './alarm.converter';
import { GetOnvifDeviceDto } from '../../entity/cameras/dto/get-onvif-device.dto';
import { UpdateCameraDto } from '../../entity/cameras/dto/update-camera.dto';
import { UpdateAlarmDto } from './dto/update-alarm.dto';
import { GetAlarmConfigurationDto } from './dto/get-alarm-configuration.dto';
import { AlarmsConfiguration } from './modules/configuration';
import { GetEventDto } from '../events/dto/get-event.dto';
import { StatusAlarmTriggeredEnum } from './enums/status-alarm-triggered.enum';
import { ApiPaginatedResponse } from '../../shared/pagination.decorator';
import { GetSegmentDto } from '../../recorder/dto/get-segment.dto';

@ApiTags('Monitor Module - Alarms')
@Controller('alarms')
export class AlarmController {
  constructor(
    private readonly alarmService: AlarmService,
    private readonly alarmConverter: AlarmConverter,
    private readonly alarmTriggeredConverter: AlarmTriggeredConverter,
  ) {}

  @Post()
  @ApiBearerAuth()
  @ApiCreatedResponse({
    description: 'Alarm created succesfull',
    type: GetAlarmDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async create(@Body() createAlarmDto: CreateAlarmDto) {
    return this.alarmConverter.convert(
      await this.alarmService.create(createAlarmDto),
    );
  }

  @Get()
  @ApiBearerAuth()
  @ApiPaginatedResponse(GetSegmentDto, 'Get paginated alarms list')
  @ApiQuery({
    name: 'orderColumn',
    enum: ['id', 'enabled', 'alarmType', 'name', 'createdAt'],
    required: false,
  })
  @ApiQuery({ name: 'enabled', type: Boolean, required: false })
  @ApiQuery({ name: 'alarmType', type: String, required: false })
  @ApiQuery({ name: 'name', type: String, required: false })
  @ApiQuery({ name: 'orderBy', enum: ['ASC', 'DESC'], required: false })
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @ApiQuery({ name: 'offset', type: Number, required: false })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async find(
    @Query('limit') limit = 10,
    @Query('offset') offset = 0,
    @Query('enabled') enabled: boolean,
    @Query('alarmType') alarmType: string,
    @Query('name') name: string,
    @Query('orderColumn') orderColumn = 'id',
    @Query('orderBy') orderBy: 'ASC' | 'DESC' = 'DESC',
  ) {

    const res = await this.alarmService.find({
      limit,
      offset,
      enabled,
      alarmType,
      name,
      orderColumn,
      orderBy,
    });
    return {
      total: res[1],
      limit: limit,
      offset: offset,
      results: await this.alarmConverter.converts(res[0]),
    };
  }

  @Get('configurations')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Alarm configuration list',
    type: [GetAlarmConfigurationDto],
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  getAlarmsConfigutaion(): GetAlarmConfigurationDto[] {
    const response: GetAlarmConfigurationDto[] = [];
    for (const alarmKey of Object.keys(AlarmsConfiguration)) {
      response.push({
        alarmType: alarmKey,
        options: AlarmsConfiguration[alarmKey].optionKeys,
      });
    }
    return response;
  }

  @Get(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get alarm',
    type: GetAlarmDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async findOne(@Param('id') id: number) {
    const alarmEntity = await this.alarmService.findOne(id);
    return this.alarmConverter.convert(alarmEntity);
  }

  @Put(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Edit alarm',
    type: GetAlarmDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async update(
    @Param('id') id: number,
    @Body() updateAlarmDto: UpdateAlarmDto,
  ) {
    const alarmEntity = await this.alarmService.update(id, updateAlarmDto);
    return this.alarmConverter.convert(alarmEntity);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete alarm',
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async remove(@Param('id') id: number) {
    await this.alarmService.remove(id);
    return null;
  }
}
