import { ApiProperty } from '@nestjs/swagger';
import { EventEntity } from '../../events/entities/events.entity';
import { TriggerEntity } from '../../triggers/entities/triggers.entity';
import { Logger } from '@nestjs/common';

export enum OptionDataType {
  string = 'string',
  number = 'number',
  boolean = 'boolean',
}
export class IOptionKey {
  @ApiProperty({ description: 'Option object key' })
  key: string;
  @ApiProperty({ description: 'Description' })
  description: string;
  @ApiProperty({
    description: 'Data type',
    enum: OptionDataType,
  })
  type: OptionDataType;
  @ApiProperty({ description: 'Required', default: false })
  required?: boolean;
  @ApiProperty({ description: 'Customizable with event variables', default: false })
  customizable?: boolean;
}

export interface AlarmImplements {
  optionKeys: IOptionKey[];

  run(
    configuration: Record<string, any>,
    trigger: TriggerEntity,
  ): Promise<boolean>;
}

export const replaceVar = (
  configuration: Record<string, any>,
  trigger: TriggerEntity,
  optionKeys: IOptionKey[],
): Record<string, any> => {
  const configurationReplaced = configuration;
  for (const option of optionKeys) {
    //console.log(option);
    if (option.customizable) {
      //console.log(configurationReplaced[option.key]);
      configurationReplaced[option.key] = configurationReplaced[
        option.key
      ].replace('${time}', trigger.createdAt);
    }
  }
  //console.log('CONFIGURAZIONE REPLACED', configurationReplaced);
  return configurationReplaced;
};
