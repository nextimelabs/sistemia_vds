import { AlarmImplements, IOptionKey, OptionDataType, replaceVar } from '../alarm.implements';
import { EventEntity } from '../../../events/entities/events.entity';
import { TriggerEntity } from '../../../triggers/entities/triggers.entity';
import { Logger } from '@nestjs/common';

interface ISmsConfiguration {
  destinationNumber: string;
}

export module SmsAlarm {

  export const optionKeys: IOptionKey[] = [
    {
      key: 'destinationNumber',
      type: OptionDataType.string,
      required: true,
      description: 'Destination number',
    },
  ];

  export async function run(
    smsConfiguration: ISmsConfiguration,
    trigger: TriggerEntity,
  ): Promise<boolean> {
    const configuration = replaceVar(smsConfiguration, trigger, this.optionKeys);
    Logger.verbose(`SMS ALARM - Running alarm with ${JSON.stringify(smsConfiguration)}`);

    return true;
  }
}
