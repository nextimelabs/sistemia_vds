import {
  AlarmImplements,
  IOptionKey,
  OptionDataType,
  replaceVar,
} from '../alarm.implements';
import { EventEntity } from '../../../events/entities/events.entity';
import { TriggerEntity } from '../../../triggers/entities/triggers.entity';
import { Logger } from '@nestjs/common';

interface IEmailConfiguration {
  host: string;
  port: string;
  secure: boolean;
  user: string;
  password: string;

  from: string;
  to: string;
  subject: string;
  text: string;
  html?: string;
}

export module EmailAlarm {
  export const optionKeys: IOptionKey[] = [
    {
      key: 'host',
      type: OptionDataType.string,
      required: true,
      description: 'Server email address',
    },
    {
      key: 'port',
      type: OptionDataType.number,
      required: true,
      description: 'Server email port',
    },
    {
      key: 'secure',
      type: OptionDataType.boolean,
      required: true,
      description: 'True for 465, false for other ports',
    },
    {
      key: 'user',
      type: OptionDataType.string,
      required: true,
      description: 'User email account',
    },
    {
      key: 'password',
      type: OptionDataType.string,
      required: true,
      description: 'Password email account',
    },
    {
      key: 'from',
      type: OptionDataType.string,
      required: true,
      description: 'Sender email address',
    },
    {
      key: 'to',
      type: OptionDataType.string,
      required: true,
      description:
        'Receivers email list (ex. "bar@example.com, baz@example.com")',
    },
    {
      key: 'subject',
      type: OptionDataType.string,
      required: false,
      description: 'Email subject',
      customizable: true,
    },
    {
      key: 'text',
      type: OptionDataType.string,
      required: false,
      description: 'Email plain text body',
      customizable: true,
    },
    {
      key: 'html',
      type: OptionDataType.string,
      required: false,
      description: 'Email html body',
      customizable: true,
    },
  ];

  export async function run(
    emailConfiguration: IEmailConfiguration,
    trigger: TriggerEntity,
  ): Promise<boolean> {
    const configuration = replaceVar(
      emailConfiguration,
      trigger,
      this.optionKeys,
    );
    Logger.verbose(
      `EMAIL ALARM - Running alarm with ${JSON.stringify(emailConfiguration)}`,
    );

    return true;
  }
}
