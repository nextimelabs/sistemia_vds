import { EmailAlarm } from './email/email.configuration';
import { AlarmImplements } from './alarm.implements';
import { SmsAlarm } from './sms/sms.configuration';


export declare type TAlarmConfiguration = {
  [key: string]: AlarmImplements;
};

export const AlarmsConfiguration: TAlarmConfiguration = {
  EMAIL: EmailAlarm,
  SMS: SmsAlarm,
};
