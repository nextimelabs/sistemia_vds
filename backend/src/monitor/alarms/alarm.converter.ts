import { Injectable } from '@nestjs/common';
import { AlarmService } from './alarm.service';
import { GetAlarmDto } from './dto/get-alarm.dto';
import { AlarmEntity } from './entities/alarms.entity';
import { AlarmTriggeredEntity } from './entities/alarm-triggered.entity';
import { GetAlarmTriggeredDto } from './dto/get-alarm-triggered.dto';
import { TriggerConverter } from '../triggers/trigger.converter';

@Injectable()
export class AlarmConverter {
  constructor() {}

  convert(alarmEntity: AlarmEntity): GetAlarmDto {
    return {
      id: alarmEntity.id,
      alarmType: alarmEntity.alarmType,
      name: alarmEntity.name,
      configuration: alarmEntity.configuration,
      enabled: alarmEntity.enabled,
      createdAt: alarmEntity.createdAt,
      updatedAt: alarmEntity.updatedAt,
    };
  }

  converts(alarmEntities: AlarmEntity[]): GetAlarmDto[] {
    return alarmEntities.map((alarmEntity) => {
      return this.convert(alarmEntity);
    });
  }
}

@Injectable()
export class AlarmTriggeredConverter {
  constructor(
    private alarmConverter: AlarmConverter,
    private triggerConverter: TriggerConverter,
  ) {}

  convert(alarmTriggeredEntity: AlarmTriggeredEntity): GetAlarmTriggeredDto {
    return {
      id: alarmTriggeredEntity.id,
      alarm: this.alarmConverter.convert(alarmTriggeredEntity.alarm),
      trigger: this.triggerConverter.convert(alarmTriggeredEntity.trigger),
      status: alarmTriggeredEntity.status,
      triggeredAt: alarmTriggeredEntity.triggeredAt,
    };
  }

  converts(
    alarmTriggeredEntities: AlarmTriggeredEntity[],
  ): GetAlarmTriggeredDto[] {
    return alarmTriggeredEntities.map((alarmTriggeredEntity) => {
      return this.convert(alarmTriggeredEntity);
    });
  }
}
