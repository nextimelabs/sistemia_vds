import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../../entity/users/users.module';
import { AlarmController } from './alarm.controller';
import { AlarmService } from './alarm.service';
import { AlarmConverter, AlarmTriggeredConverter } from './alarm.converter';
import { AlarmEntity } from './entities/alarms.entity';
import { AlarmTriggeredEntity } from './entities/alarm-triggered.entity';
import { TriggerConverter } from '../triggers/trigger.converter';
import { AlarmTriggeredController } from './alarm-triggered.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([AlarmEntity, AlarmTriggeredEntity]),
    UsersModule,
  ],
  controllers: [AlarmController, AlarmTriggeredController],
  providers: [
    AlarmService,
    AlarmConverter,
    AlarmTriggeredConverter,
    TriggerConverter,
  ],
  exports: [AlarmService, AlarmConverter, AlarmTriggeredConverter],
})
export class AlarmsModule {}
