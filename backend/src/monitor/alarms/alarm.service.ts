import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAlarmDto } from './dto/create-alarm.dto';
import { UpdateAlarmDto } from './dto/update-alarm.dto';
import { AlarmEntity } from './entities/alarms.entity';
import { AlarmTriggeredEntity } from './entities/alarm-triggered.entity';
import { StatusAlarmTriggeredEnum } from './enums/status-alarm-triggered.enum';
import { AlarmsConfiguration } from './modules/configuration';
import { TriggerEntity } from '../triggers/entities/triggers.entity';
import { OptionDataType } from './modules/alarm.implements';
import { SegmentsEntity } from '../../recorder/entities/segments.entity';

interface ISearchAlarmTriggered {
  limit: number;
  offset: number;
  orderBy: 'ASC' | 'DESC';
  orderColumn: string;
  status?: StatusAlarmTriggeredEnum;
  triggerId?: number;
  alarmId?: number;
  alarmName?: string;
  fromTime?: Date;
  toTime?: Date;
}
interface ISearchAlarms {
  limit: number;
  offset: number;
  enabled?: boolean;
  alarmType?: string;
  name?: string;
  orderColumn: string;
  orderBy: 'ASC' | 'DESC';
}

@Injectable()
export class AlarmService {
  constructor(
    @InjectRepository(AlarmEntity)
    private alarmEntityRepository: Repository<AlarmEntity>,
    @InjectRepository(AlarmTriggeredEntity)
    private alarmTriggeredEntityRepository: Repository<AlarmTriggeredEntity>,
  ) {}

  async create(createAlarmDto: CreateAlarmDto) {
    /*
      Check alarm configuration
     */
    if (AlarmsConfiguration[createAlarmDto.alarmType] === undefined)
      throw new HttpException('ALARM_TYPE_NOT_FOUND', HttpStatus.NOT_FOUND);
    AlarmsConfiguration[createAlarmDto.alarmType].optionKeys.forEach((key) => {
      // Check if required
      if (key.required && createAlarmDto.configuration[key.key] === undefined) {
        Logger.log(`${key} richiesto`);
        throw new HttpException(
          'INVALID_ALARM_CONFIGURATION',
          HttpStatus.BAD_REQUEST,
        );
      }
      // Check data type
      Logger.log(
        `${typeof createAlarmDto.configuration[key.key]} - ${key.type}`,
      );
      if (typeof createAlarmDto.configuration[key.key] !== key.type) {
        Logger.log(`${key} deve essere di tipo ${key.type}`);
        throw new HttpException(
          'INVALID_ALARM_CONFIGURATION',
          HttpStatus.BAD_REQUEST,
        );
      }
    });
    return this.alarmEntityRepository.save(createAlarmDto);
  }

  async find({
    limit,
    offset,
    enabled,
    alarmType,
    name,
    orderColumn,
    orderBy,
  }: ISearchAlarms): Promise<[AlarmEntity[], number]> {
    let queryBuilder = this.alarmEntityRepository.createQueryBuilder('alarms');
    if (enabled)
      queryBuilder = queryBuilder.andWhere('alarms.enabled = :enabled', {
        enabled,
      });
    if (alarmType)
      queryBuilder = queryBuilder.andWhere('alarms.alarmType = :alarmType', {
        alarmType,
      });
    if (name)
      queryBuilder = queryBuilder.andWhere('alarms.name like :search', {
        search: `%${name}%`,
      });

    return queryBuilder
      .orderBy(`alarms.${orderColumn}`, orderBy)
      .limit(limit)
      .offset(offset)
      .getManyAndCount();
  }

  async findOne(id: number) {
    const alarmEntity: AlarmEntity = await this.alarmEntityRepository.findOne(
      id,
    );
    if (!alarmEntity) {
      throw new HttpException('ALARM_NOT_FOUND', HttpStatus.NOT_FOUND);
    }
    return alarmEntity;
  }

  async update(id: number, updateAlarmDto: UpdateAlarmDto) {
    /*
      Check alarm configuration
     */
    if (AlarmsConfiguration[updateAlarmDto.alarmType] === undefined)
      throw new HttpException('ALARM_TYPE_NOT_FOUND', HttpStatus.NOT_FOUND);
    AlarmsConfiguration[updateAlarmDto.alarmType].optionKeys.forEach((key) => {
      if (updateAlarmDto.configuration[key.key] === undefined)
        throw new HttpException(
          'INVALID_ALARM_CONFIGURATION',
          HttpStatus.BAD_REQUEST,
        );
    });

    const oldAlarmEntity = await this.findOne(id);

    const alarmEntity: AlarmEntity = {
      ...oldAlarmEntity,
      alarmType: updateAlarmDto.alarmType,
      configuration: updateAlarmDto.configuration,
      enabled: updateAlarmDto.enabled,
    };
    return await this.alarmEntityRepository.save(alarmEntity);
  }

  async remove(id: number) {
    const alarm = await this.findOne(id);
    await this.alarmEntityRepository.remove(alarm);
    return true;
  }

  async addAlarmTriggered(
    alarmId: number,
    triggerId: number,
    status: StatusAlarmTriggeredEnum,
    info?: string,
  ) {
    const alarmTriggeredEntity: AlarmTriggeredEntity = {
      alarmId: alarmId,
      triggerId: triggerId,
      triggeredAt: new Date(),
      status: status,
      info,
    };
    return this.alarmTriggeredEntityRepository.save(alarmTriggeredEntity);
  }

  async runAlarm(alarmEntity: AlarmEntity, trigger: TriggerEntity) {
    Logger.verbose(
      `ALARM SERVICE - Running alarm ${alarmEntity.alarmType} triggered by trigger id ${trigger.id}`,
    );
    const alarmConfiguration = AlarmsConfiguration[alarmEntity.alarmType];
    await alarmConfiguration.run(alarmEntity.configuration, trigger);
  }

  async findAllTriggered({
    status,
    alarmId,
    alarmName,
    triggerId,
    fromTime,
    toTime,
    limit,
    offset,
    orderColumn,
    orderBy,
  }: ISearchAlarmTriggered) {
    let queryBuilder = this.alarmTriggeredEntityRepository
      .createQueryBuilder('alarmsTriggered')
      .leftJoinAndSelect('alarmsTriggered.alarm', 'alarm')
      .leftJoinAndSelect('alarmsTriggered.trigger', 'trigger')
      .leftJoinAndSelect('trigger.alarmsToBeTriggered', 'alarmsToBeTriggered');

    if (alarmId)
      queryBuilder = queryBuilder.andWhere(
        'alarmsTriggered.alarmId = :alarmId',
        {
          alarmId,
        },
      );
    if (alarmName)
      queryBuilder = queryBuilder.andWhere('alarm.name = :alarmName', {
        alarmName,
      });
    if (triggerId)
      queryBuilder = queryBuilder.andWhere(
        'alarmsTriggered.triggerId = :triggerId',
        {
          triggerId,
        },
      );
    if (status)
      queryBuilder = queryBuilder.andWhere('alarmsTriggered.status = :status', {
        status,
      });

    if (fromTime)
      queryBuilder = queryBuilder.andWhere(
        'alarmsTriggered.triggeredAt >= :fromTime',
        {
          fromTime,
        },
      );
    if (toTime)
      queryBuilder = queryBuilder.andWhere(
        'alarmsTriggered.triggeredAt <= :toTime',
        {
          toTime,
        },
      );

    return queryBuilder
      .orderBy(`alarmsTriggered.${orderColumn}`, orderBy)
      .limit(limit)
      .offset(offset)
      .getManyAndCount();
  }

  async deleteAlarmTriggered(alarmTriggeredId: number) {
    return this.alarmTriggeredEntityRepository.delete({ id: alarmTriggeredId });
  }
}
