import { ApiProperty } from '@nestjs/swagger';
import { IEvent } from '../events/enums/event-type.enum';

export class ITriggerEvents {
  @ApiProperty({ type: Object })
  triggers: ICondition<IEvent>[];
}

export declare type ICondition<Payload> = {
  [key in 'and' | 'or']?: (Payload | ICondition<Payload>)[];
};

const test: ITriggerEvents = {
  triggers: [
    {
      and: [
        {
          cameraId: 1,
          timestamp: new Date().toISOString(),
          eventType: 'MOVEMENT',
          eventMetadata: [
            {
              key: 'DIRECTION',
              value: 'SOUTH',
            },
          ],
        },
        {
          cameraId: 2,
          timestamp: new Date().toISOString(),
          eventType: 'MOVEMENT',
          eventMetadata: [
            {
              key: 'DIRECTION',
              value: 'NORTH',
            },
          ],
        },
        {
          or: [
            {
              cameraId: 1,
              timestamp: new Date().toISOString(),
              eventType: 'VEHICLE_DETECTED',
              eventMetadata: [
                {
                  key: 'SPEED',
                  value: '25',
                },
                {
                  key: 'TYPE',
                  value: 'CAR',
                },
              ],
            },
            {
              cameraId: 2,
              timestamp: new Date().toISOString(),
              eventType: 'VEHICLE_DETECTED',
              eventMetadata: [
                {
                  key: 'DIRECTION',
                  value: 'EAST',
                },
              ],
            },
          ],
        },
      ],
    },
  ],
};
