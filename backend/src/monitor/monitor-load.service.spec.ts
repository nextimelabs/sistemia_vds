import { Test, TestingModule } from '@nestjs/testing';
import { MonitorService } from './monitor.service';
import { MonitorModule } from './monitor.module';
import { TriggerService } from './triggers/trigger.service';
import { EventsModule } from './events/events.module';
import { AlarmsModule } from './alarms/alarms.module';
import { TriggerModule } from './triggers/trigger.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as environment from '../../environment/environment.json';
import { AuthModule } from '../entity/auth/auth.module';
import { UsersModule } from '../entity/users/users.module';
import { CamerasModule } from '../entity/cameras/cameras.module';
import { StreamerModule } from '../streamer/streamer.module';
import { RecorderModule } from '../recorder/recorder.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { GroupsModule } from '../entity/groups/groups.module';
import { ITriggerEvents } from './interfaces/trigger-events.interface';
import { TriggerEntity } from './triggers/entities/triggers.entity';
import { EventHelper } from './events/event.helper';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { CreateAlarmDto } from './alarms/dto/create-alarm.dto';
import * as request from 'supertest';
import { INestApplication, Logger } from '@nestjs/common';
import { LoginUserResponseDto } from '../entity/auth/dto/login-user-response.dto';
import { RuoloEnum } from '../entity/users/enums/ruolo.enum';
import { StatoEnum } from '../entity/users/enums/stato.enum';
import { GetAlarmDto } from './alarms/dto/get-alarm.dto';
import { CreateTriggerDto } from './triggers/dto/create-trigger.dto';
import { GetTriggerDto } from './triggers/dto/get-trigger.dto';
import { createConnection } from 'typeorm';

describe('MonitorService', () => {
  let app: INestApplication;

  let addedAlarmEMAIL: GetAlarmDto;
  let addedTrigger: GetTriggerDto;

  const eventHelper = new EventHelper();

  let service: MonitorService;

  // Start app
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ClientsModule.register([
          {
            name: 'EVENT_SERVICE',
            transport: Transport.KAFKA,
            options: {
              client: {
                clientId: 'event',
                brokers: [
                  `${process.env.KAFKA_IP || environment.KAFKA_IP}:${
                    process.env.KAFKA_PORT || environment.KAFKA_PORT
                  }`,
                ],
              },
              consumer: {
                groupId: 'event-consumer',
              },
            },
          },
        ]),
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: process.env.DB_HOST || environment.DB_HOST,
          port: parseInt(process.env.DB_PORT) || environment.DB_PORT,
          username: process.env.DB_USERNAME || environment.DB_USERNAME,
          password: process.env.DB_PASSWORD || environment.DB_PASSWORD,
          database: process.env.DB_NAME || environment.DB_NAME,
          autoLoadEntities: true,
          synchronize:
            (process.env.DB_SYNCHRONIZE || environment.DB_SYNCHRONIZE) ===
            'true',
          migrations: ['migration/*.js'],
          cli: {
            migrationsDir: 'migration',
          },
        }),
        AuthModule,
        UsersModule,
        CamerasModule,
        StreamerModule,
        RecorderModule,
        EventEmitterModule.forRoot(),
        GroupsModule,
        EventsModule,
        AlarmsModule,
        TriggerModule,
      ],
      providers: [MonitorService],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    service = module.get<MonitorService>(MonitorService);

    // Load triggers in memory
    await service.loadTriggersFromDb();
  }, 10000);

  // Close app
  afterAll(async () => {
    await app.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  it('Load test 10000 eventi', async () => {
    Logger.verbose(`LOAD TEST - start`);
    const times: number[] = [];
    const startTime = Date.now();
    Logger.verbose(`LOAD TEST - ${startTime}`);
    for (let i = 0; i < 100; i++) {
      const startTime = Date.now();
      await service.addEvent(eventHelper.random());
      const endTime = Date.now();
      const timeDiff = endTime - startTime;
      Logger.verbose(`LOAD TEST - cycle ${i} in ${timeDiff} ms`);
      times.push(timeDiff);
    }
    const endTime = Date.now();
    const timeDiff = endTime - startTime;
    const avg = times.reduce((prev, val) => prev + val, 0) / times.length;
    Logger.verbose(`LOAD TEST - Finished in ${timeDiff} - cycle avg time ${avg} ms`);
  });
});
