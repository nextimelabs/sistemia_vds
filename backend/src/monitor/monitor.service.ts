import { Inject, Injectable, Logger } from '@nestjs/common';
import { TriggerService } from './triggers/trigger.service';
import { EventService } from './events/event.service';
import { AlarmService } from './alarms/alarm.service';
import { ICondition } from './interfaces/trigger-events.interface';
import { TriggerEntity } from './triggers/entities/triggers.entity';
import { OnEvent } from '@nestjs/event-emitter';
import { EventHelper } from './events/event.helper';
import { ClientProxy } from '@nestjs/microservices';
import { StatusAlarmTriggeredEnum } from './alarms/enums/status-alarm-triggered.enum';
import { IEvent } from './events/enums/event-type.enum';

type TriggerEvent = {
  [key: number]: IEvent[];
};

@Injectable()
export class MonitorService {
  eventHelper = new EventHelper();

  /*public triggers: TriggerEntity[] = [
    // deve passare un auto nera con targa AB123CD oppure o passa un auto bianca o con targa AB321AB
    {
      id: 1,
      enabled: true,
      triggerEvents: {
        triggers: [
          {
            and: [
              this.eventHelper
                .cam(1)
                .type('PASSAGGIO_AUTO')
                .addMetadata('TARGA', 'AB123CD')
                .addMetadata('COLORE', 'NERO')
                .event(),
              {
                or: [
                  this.eventHelper
                    .cam(2)
                    .type('PASSAGGIO_AUTO')
                    .addMetadata('COLORE', 'BIANCA')
                    .event(),
                  this.eventHelper
                    .cam(1)
                    .type('PASSAGGIO_AUTO')
                    .addMetadata('TARGA', 'AB321AB')
                    .event(),
                ],
              },
            ],
          },
        ],
      },
    },
    // o passa una persona donna oppure passa un uomo e una macchina nera
    {
      id: 2,
      enabled: true,
      triggerEvents: {
        triggers: [
          {
            or: [
              this.eventHelper
                .cam(2)
                .type('PASSAGGIO_PERSONA')
                .addMetadata('GENERE', 'DONNA')
                .event(),
              {
                and: [
                  this.eventHelper
                    .cam(1)
                    .type('PASSAGGIO_PERSONA')
                    .addMetadata('GENERE', 'UOMO')
                    .event(),
                  this.eventHelper
                    .cam(1)
                    .type('PASSAGGIO_AUTO')
                    .addMetadata('COLORE', 'NERO')
                    .event(),
                ],
              },
            ],
          },
        ],
      },
    },
    // passa una donna e un uomo
    {
      id: 2,
      enabled: true,
      triggerEvents: {
        triggers: [
          {
            and: [
              this.eventHelper
                .cam(2)
                .type('PASSAGGIO_PERSONA')
                .addMetadata('GENERE', 'DONNA')
                .event(),
              this.eventHelper
                .cam(1)
                .type('PASSAGGIO_PERSONA')
                .addMetadata('GENERE', 'UOMO')
                .event(),
            ],
          },
        ],
      },
    },
  ];
   */
  public triggers: TriggerEntity[];

  triggersEvent: TriggerEvent = {
    21: [
      {
        cameraId: 1,
        timestamp: new Date(1624459287831).toISOString(),
        eventType: 'MOVIMENTO',
        eventMetadata: [
          {
            key: 'DIREZIONE',
            value: 'SUD',
          },
        ],
      },
    ],
    212: [
      {
        cameraId: 1,
        timestamp: new Date(1624459287831).toISOString(),
        eventType: 'MOVIMENTO',
        eventMetadata: [
          {
            key: 'DIREZIONE',
            value: 'SUD',
          },
        ],
      },
    ],
  };

  constructor(
    private triggerService: TriggerService,
    private eventService: EventService,
    private alarmService: AlarmService,
    @Inject('EVENT_SERVICE') private eventClient: ClientProxy,
  ) {}

  @OnEvent('triggers.changed', { async: true })
  async loadTriggersFromDb() {
    this.triggers = await this.triggerService.findAllEnabled();
    Logger.verbose(`MONITOR SERVICE - Triggers loaded ${this.triggers.length}`);
  }

  async addEvent(event: IEvent) {
    if (event.timestamp === undefined) event.timestamp = new Date().toISOString();
    Logger.verbose(
      `MONITOR SERVICE - Event received ${event.eventType} on camera ${event.cameraId}`,
    );

    /*
     Controllo in quali triggers corrisponde questo evento e li invia
     nel topic triggerEvent
     */
    const triggersCorrispondenti: TriggerEntity[] =
      this.getTriggersCorrispondence(event, this.triggers);
    for (const trigger of triggersCorrispondenti) {
      Logger.verbose(
        `MONITOR SERVICE - Publishing event on triggerEvent topic for trigger id: ${trigger.id}`,
      );
      await this.addTriggerEvent(trigger.id, event);
      /*
      this.eventClient.emit(`triggerEvent`, {
        headers: {
          realm: 'Monitor',
        },
        key: trigger.id,
        value: JSON.stringify(event),
      });
       */
    }
    return true;
  }

  getTriggersCorrispondence(
    event: IEvent,
    triggerEntities: TriggerEntity[],
  ): TriggerEntity[] {
    const recursion = (payload: (IEvent | ICondition<IEvent>)[]): boolean => {
      for (let k = 0; k < payload.length; k++) {
        const keys = Object.keys(payload[k]);
        if (keys[0] === 'and' || keys[0] === 'or') {
          // keys[i] is ICondition
          if (recursion(payload[k][keys[0]])) return true;
        } else if (keys[0] === 'cameraId') {
          // keys[i] is IEvent
          // @ts-ignore
          const coincidenza = this.controlloCoincidenza(event, payload[k]);
          if (coincidenza) return true;
        }
      }
    };

    const res = [];
    triggerEntities.forEach((triggerEntity) => {
      const rico = recursion([triggerEntity.condition]);
      if (rico) {
        res.push(triggerEntity);
      }
    });
    return res;
  }

  controlloCoincidenza(event: IEvent, definition: IEvent): boolean {
    if (event.cameraId !== definition.cameraId) return false;
    if (event.eventType !== definition.eventType) return false;

    for (let i = 0; i < definition.eventMetadata.length; i++) {
      // Controllo se questo metadata è presente nei metadati di event
      let presente = false;
      for (let j = 0; j < event.eventMetadata.length; j++) {
        if (
          definition.eventMetadata[i].key == event.eventMetadata[j].key &&
          definition.eventMetadata[i].value == event.eventMetadata[j].value
        )
          presente = true;
      }
      if (!presente) return false;
    }
    return true;
  }

  async addTriggerEvent(triggerId: number, event: IEvent) {
    Logger.verbose(
      `MONITOR SERVICE - Attaching event ${event.eventType} on trigger ${triggerId} in memory`,
    );

    const triggerEntity: TriggerEntity = this.triggers.filter(
      (trigger) => trigger.id == triggerId,
    )[0];
    if (triggerEntity === undefined) {
      Logger.error(
        `MONITOR SERVICE - Trigger ${triggerId} not found in memory`,
      );
      return false;
    }
    this.cleanTriggerEvents(triggerId);
    if (this.triggersEvent[triggerId] === undefined) {
      Logger.verbose(
        `MONITOR SERVICE - Trigger ${triggerId} events bucket empty. Initialized`,
      );
      this.triggersEvent[triggerId] = [];
    }
    this.triggersEvent[triggerId].push(event);
    const check = this.checkTrigger(
      this.triggersEvent[triggerId],
      triggerEntity,
    );
    Logger.verbose(
      `MONITOR SERVICE - Trigger ${triggerId} events bucket verification: ${check}`,
    );
    if (check) {
      Logger.verbose(
        `MONITOR SERVICE - Trigger ${triggerId} events bucket triggered with ${this.triggersEvent[triggerId].length} events`,
      );

      for (const alarmToBeTriggeredEntity of triggerEntity.alarmsToBeTriggered) {
        Logger.verbose(
          `MONITOR SERVICE - Alarm triggered id ${alarmToBeTriggeredEntity.alarm.id}`,
        );
        try {
          await this.alarmService.runAlarm(
            alarmToBeTriggeredEntity.alarm,
            triggerEntity,
          );
          await this.alarmService.addAlarmTriggered(
            alarmToBeTriggeredEntity.alarm.id,
            triggerId,
            StatusAlarmTriggeredEnum.SUCCESSFUL,
          );
        } catch (error) {
          await this.alarmService.addAlarmTriggered(
            alarmToBeTriggeredEntity.alarm.id,
            triggerId,
            StatusAlarmTriggeredEnum.SUCCESSFUL,
            error,
          );
        }
      }
      this.triggersEvent[triggerId] = [];
      return true;
    }
    return false;
  }

  cleanTriggerEvents(triggerId: number) {
    let counter = 0;
    if (this.triggersEvent[triggerId] !== undefined) {
      this.triggersEvent[triggerId] = this.triggersEvent[triggerId].filter(
        (event) => {
          if (+new Date() - +new Date(event.timestamp) > 10000) {
            counter++;
            return false;
          } else {
            return true;
          }
        },
      );
    }
    Logger.verbose(
      `MONITOR SERVICE - Cleared ${counter} events from trigger ${triggerId} events bucket`,
    );
  }

  checkTrigger(events: IEvent[], triggerEntity: TriggerEntity): boolean {
    const recursion = (payload: (IEvent | ICondition<IEvent>)[]): number[] => {
      const response: number[] = [];
      for (let k = 0; k < payload.length; k++) {
        const keys = Object.keys(payload[k]);
        if (keys[0] === 'and') {
          // keys[i] is ICondition
          const items: number[] = recursion(payload[k][keys[0]]);
          if (items.every((item) => item === 1)) response.push(1);
          else response.push(0);
        } else if (keys[0] === 'or') {
          // keys[i] is ICondition
          const items: number[] = recursion(payload[k][keys[0]]);
          if (items.includes(1)) response.push(1);
          else response.push(0);
        } else if (keys[0] === 'cameraId') {
          // keys[i] is IEvent
          let found = false;
          events.map((event) => {
            // @ts-ignore
            const coincidenza = this.controlloCoincidenza(event, payload[k]);
            if (coincidenza) {
              found = true;
              return;
            }
          });
          if (found) response.push(1);
          else response.push(0);
        }
      }
      return response;
    };

    const rico = recursion([triggerEntity.condition]);
    if (rico[0] === 1) {
      return true;
    } else {
      return false;
    }
  }
}
