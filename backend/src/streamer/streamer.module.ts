import { forwardRef, Module } from '@nestjs/common';
import { StreamerService } from './streamer.service';
import { CamerasModule } from '../entity/cameras/cameras.module';
import { StreamerController } from './streamer.controller';
import { CameraEntity } from '../entity/cameras/entities/camera.entity';
import { UsersModule } from '../entity/users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StreamerConverter } from './streamer.converter';
import { StreamerOptionsEntity } from './entities/streamer-options.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      CameraEntity,
      StreamerOptionsEntity
    ]),
    UsersModule,
    forwardRef(() => CamerasModule),
  ],
  controllers: [StreamerController],
  providers: [StreamerService, StreamerConverter],
  exports: [StreamerService, StreamerConverter]
})
export class StreamerModule {}
