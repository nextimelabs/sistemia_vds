import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne, OneToOne, PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { StatusEnum } from '../../shared/dto/status.enum';
import { CameraEntity } from '../../entity/cameras/entities/camera.entity';

@Entity('streamerOptions')
export class StreamerOptionsEntity {
  @Column('integer', { nullable: false, default: 30 })
  fps: number;
  @Column('boolean', { nullable: false })
  enabled: boolean;
  @Column('integer', {nullable: false, default: StatusEnum.Stopped})
  status?: number;
  @Column('varchar', { nullable: true })
  errorMessage?: string;
  @Column('integer', { nullable: false })
  wsPort: number;
  @OneToOne((type) => CameraEntity, (camera) => camera.id, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  camera?: CameraEntity;
  @PrimaryColumn({ nullable: false })
  cameraId: number;
}
