import {ApiProperty} from '@nestjs/swagger';
import {
  IsBoolean,
  IsEnum,
  IsNotEmpty, IsNumber,
} from 'class-validator';
import { StatusEnum } from '../../shared/dto/status.enum';


export class CreateStreamerDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  fps: number;

  @ApiProperty()
  @IsBoolean()
  enabled: boolean;
}
