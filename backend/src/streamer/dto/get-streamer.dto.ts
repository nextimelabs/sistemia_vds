import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MaxLength,
  IsNumber,
  IsEnum,
  IsBoolean,
  IsString,
} from 'class-validator';
import { StatusEnum } from '../../shared/dto/status.enum';

export class GetStreamerDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  fps: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  wsPort: number;

  @ApiProperty({ enum: StatusEnum })
  @IsNotEmpty()
  status: string;

  @ApiProperty()
  @IsBoolean()
  enabled: boolean;

  @ApiProperty()
  @IsString()
  errorMessage: string | null;
}
