import {
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
} from '@nestjs/common';
import { VideoStream } from './lib/videoStream';
import { CameraEntity } from '../entity/cameras/entities/camera.entity';
import { StatusEnum } from '../shared/dto/status.enum';
import { CamerasService } from '../entity/cameras/cameras.service';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateStreamerDto } from './dto/create-streamer.dto';
import { UpdateStreamerDto } from './dto/update-streamer.dto';
import { StreamerOptionsEntity } from './entities/streamer-options.entity';
import { OnEvent } from '@nestjs/event-emitter';
import { ProcessEvents } from '../shared/dto/events.enum';
import * as environment from '../../environment/environment.json';

interface IStream {
  cameraId: number;
  stream: VideoStream;
}

@Injectable()
export class StreamerService {
  streamers: IStream[] = [];

  constructor(
    @Inject(forwardRef(() => CamerasService))
    private camerasService: CamerasService,
    @InjectRepository(StreamerOptionsEntity)
    private streamerOptionsEntityRepository: Repository<StreamerOptionsEntity>,
  ) {}

  @OnEvent('camera.updated')
  async handleCameraUpdatedEvent(cameraEntity: CameraEntity) {
    const streamerOptionsEntity: StreamerOptionsEntity =
      await this.streamerOptionsEntityRepository.findOne({
        cameraId: cameraEntity.id,
      });
    if (streamerOptionsEntity) await this.editStream(cameraEntity);
  }

  async get(idCamera: number) {
    return await this.findOne(idCamera);
  }

  async add(idCamera: number, createStreamerDto: CreateStreamerDto) {
    const checkExisting: StreamerOptionsEntity =
      await this.streamerOptionsEntityRepository.findOne({
        cameraId: idCamera,
      });
    if (checkExisting) {
      throw new HttpException(
        'STREAMER_OPTIONS_ALREADY_EXISTS',
        HttpStatus.BAD_REQUEST,
      );
    }
    const cameraEntity = await this.camerasService.findOne(idCamera);
    const streamerOptionsEntity: StreamerOptionsEntity = {
      fps: createStreamerDto.fps,
      enabled: createStreamerDto.enabled,
      cameraId: cameraEntity.id,
      status: StatusEnum.Stopped,
      wsPort: await this.getFreeWsPort(),
    };
    await this.streamerOptionsEntityRepository.save(streamerOptionsEntity);
    await this.addStream(cameraEntity);
    return this.findOne(idCamera);
  }

  async edit(idCamera: number, updateStreamerDto: UpdateStreamerDto) {
    const streamerOptionsEntity = await this.findOne(idCamera);
    let streamerEntity: StreamerOptionsEntity = {
      ...streamerOptionsEntity,
      fps: updateStreamerDto.fps,
      enabled: updateStreamerDto.enabled,
    };
    streamerEntity = await this.streamerOptionsEntityRepository.save(
      streamerEntity,
    );
    await this.editStream(streamerEntity.camera);
    return this.findOne(idCamera);
  }

  async delete(idCamera: number) {
    const streamerOptionsEntity = await this.findOne(idCamera);
    await this.removeStream(idCamera);
    await this.streamerOptionsEntityRepository.remove(streamerOptionsEntity);
    return true;
  }

  async findOne(cameraId: number) {
    const streamerOptionsEntity: StreamerOptionsEntity =
      await this.streamerOptionsEntityRepository.findOne(
        { cameraId: cameraId },
        { relations: ['camera'] },
      );
    if (!streamerOptionsEntity) {
      throw new HttpException(
        'STREAMER_PROCESS_NOT_FOUND',
        HttpStatus.NOT_FOUND,
      );
    }
    return streamerOptionsEntity;
  }

  async findAll() {
    return await this.streamerOptionsEntityRepository.find({
      relations: ['camera'],
    });
  }

  // Memory DB functions
  async addStream(cameraEntity: CameraEntity) {
    const streamerIndex = this.streamers.findIndex(
      (recorder) => recorder.cameraId == cameraEntity.id,
    );
    if (streamerIndex !== -1)
      throw new HttpException(
        'STREAMER_PROCESS_ALREADY_STARTED',
        HttpStatus.BAD_REQUEST,
      );

    const streamerOptionsEntity = await this.findOne(cameraEntity.id);
    if (streamerOptionsEntity.enabled === false)
      throw new HttpException(
        'STREAMER_PROCESS_DISABLED',
        HttpStatus.BAD_REQUEST,
      );

    await this.setCameraStreamingStatus(cameraEntity.id, StatusEnum.Starting);

    const newStream = new VideoStream({
      name: cameraEntity.alias,
      url: cameraEntity.camSourcePath,
      port: streamerOptionsEntity.wsPort,
      fps: streamerOptionsEntity.fps,
    })
      .on(ProcessEvents.STOPPED, async () => {
        await this.setCameraStreamingStatus(
          cameraEntity.id,
          StatusEnum.Stopped,
        );
      })
      .on(ProcessEvents.STARTED, async () => {
        await this.setCameraStreamingStatus(
          cameraEntity.id,
          StatusEnum.Running,
        );
      })
      .on(ProcessEvents.ERROR, async (errorMessage) => {
        await this.removeStream(cameraEntity.id);
        await this.setCameraStreamingStatus(
          cameraEntity.id,
          StatusEnum.Error,
          errorMessage,
        );
      })
      .pipeStreamToSocketServer();

    this.streamers.push({
      cameraId: cameraEntity.id,
      stream: newStream,
    });
  }

  // Return true if stream exists and removed else false if not exists
  async removeStream(cameraId: number) {
    const streamerOptionsEntity: StreamerOptionsEntity = await this.findOne(
      cameraId,
    );
    const streamerIndex = this.streamers.findIndex(
      (streamer) => streamer.cameraId == cameraId,
    );
    if (
      streamerIndex !== -1 &&
      streamerOptionsEntity.status !== StatusEnum.Stopped
    ) {
      await this.setCameraStreamingStatus(cameraId, StatusEnum.Stopping);
      this.streamers[streamerIndex].stream.stop();
    }
    this.streamers.splice(streamerIndex, 1);
    return true;
  }

  async editStream(cameraEntity: CameraEntity) {
    if (await this.removeStream(cameraEntity.id)) {
      const streamerOptionsEntity = await this.findOne(cameraEntity.id);
      if (streamerOptionsEntity.enabled === true)
        await this.addStream(cameraEntity);
    }
  }

  async startStream(cameraId: number) {
    const streamerOptionsEntity: StreamerOptionsEntity = await this.findOne(
      cameraId,
    );
    const streamerIndex = this.streamers.findIndex(
      (streamer) => streamer.cameraId == cameraId,
    );
    if (streamerIndex === -1) return false;
    if (streamerOptionsEntity.enabled === false)
      throw new HttpException(
        'STREAMER_PROCESS_DISABLED',
        HttpStatus.BAD_REQUEST,
      );
    if (
      [StatusEnum.Starting, StatusEnum.Running].includes(
        streamerOptionsEntity.status,
      )
    )
      throw new HttpException(
        'STREAMER_PROCESS_ALREADY_STARTED',
        HttpStatus.BAD_REQUEST,
      );
    this.streamers[streamerIndex].stream.stop();
    this.streamers[streamerIndex].stream.start();
  }

  async startAll() {
    const streamerOptions = await this.streamerOptionsEntityRepository
      .createQueryBuilder('streamerOptions')
      .leftJoinAndSelect('streamerOptions.camera', 'camera')
      .where('streamerOptions.enabled = true')
      .getMany();
    streamerOptions.map(async (streamerOption) => {
      await this.addStream(streamerOption.camera);
    });
  }

  async setCameraStreamingStatus(
    cameraId: number,
    status: StatusEnum,
    errorMessage?: string,
  ): Promise<StreamerOptionsEntity> {
    const streamerOptionsEntity = await this.findOne(cameraId);
    return await this.streamerOptionsEntityRepository.save({
      ...streamerOptionsEntity,
      status,
      errorMessage: status === StatusEnum.Error ? errorMessage : null,
    });
  }

  async getFreeWsPort(): Promise<number> {
    const [_firstWsPort, _lastWsPort] = (
      process.env.WS_PORT_RANGE || environment.WS_PORT_RANGE
    ).split('-');
    const firstWsPort: number = _firstWsPort ? parseInt(_firstWsPort) : 0;
    const lastWsPort: number = _lastWsPort ? parseInt(_lastWsPort) : 65535;

    const streamerOptionsEntities: StreamerOptionsEntity[] =
      await this.streamerOptionsEntityRepository.find({
        order: { wsPort: 'ASC' },
      });
    if (streamerOptionsEntities.length === 0) return firstWsPort;

    for (let i = firstWsPort; i <= lastWsPort; i++) {
      let portBusy = false;
      for (const option of streamerOptionsEntities) {
        if (option.wsPort === i) portBusy = true;
      }
      if (!portBusy) return i;
    }
    return -1;
  }
}
