import { ProcessEvents } from '../../shared/dto/events.enum';

const child_process = require('child_process');
const EventEmitter = require('events');

export class Mpeg1Muxer extends EventEmitter {
  constructor(options) {
    super(options);

    this.url = options.url;
    this.fps = options.fps;

    this.stream = child_process.spawn(
      'ffmpeg',
      [
        '-rtsp_transport',
        'tcp',
        '-i',
        this.url,
        '-f',
        'mpegts',
        '-codec:v',
        'mpeg1video',
        '-bf',
        '0',
        '-codec:a',
        'mp2',
        '-r',
        this.fps,
        '-',
      ],
      {
        detached: false,
      },
    );

    console.log(
      'ffmpeg ' +
        [
          '-rtsp_transport',
          'tcp',
          '-i',
          this.url,
          '-f',
          'mpegts',
          '-codec:v',
          'mpeg1video',
          '-bf',
          '0',
          '-codec:a',
          'mp2',
          '-r',
          this.fps,
          '-',
        ].join(' '),
    );

    this.inputStreamStarted = true;
    const self = this;
    this.stream.stdout.on('data', (buffer) => {
      const data = buffer.toString();
      //console.log(data);
      if (data.includes('401 Unauthorized')) {
        self.stop();
        self.emit(ProcessEvents.ERROR, 'CAM_CONNECTION_UNAUTHORIZED');
      } else if (data.includes('Error')) {
        self.stop();
        self.emit(ProcessEvents.ERROR, 'GENERIC_ERROR');
      }

      return this.emit('mpeg1data', buffer);
    });
  }

  stop() {
    try {
      this.stream.stdout.removeAllListeners();
      this.stream.stderr.removeAllListeners();
      this.stream.kill();
      this.emit('exit');
      this.stream = undefined;
    } catch (e) {
      console.log(e);
    }
  }
}
