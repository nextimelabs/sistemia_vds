import { Mpeg1Muxer } from './mpeg1muxer';
import EventEmitter = require('events');
import WebSocket = require('ws');
import { Logger } from '@nestjs/common';
import * as Process from 'process';
import { ProcessEvents } from '../../shared/dto/events.enum';
import { Cron, CronExpression } from '@nestjs/schedule';

const STREAM_MAGIC_BYTES = 'jsmp';

export class VideoStream extends EventEmitter {
  options;
  name; // Camera alias
  url; // Camera url path
  width; // Camera video width
  height; // Camera video height
  port; // WebSocket port
  fps; // FPS
  stream; // IDK
  inputStreamStarted; // IDK
  server; // WebSocket server
  wsServer; // webSocket
  mpeg1Muxer; // Istanza MPEG1Muxer

  constructor(options) {
    super(options);
    this.options = options;
    this.name = options.name;
    this.url = options.url;
    this.width = options.width;
    this.height = options.height;
    this.port = options.port;
    this.fps = options.fps;
    this.inputStreamStarted = false;
    this.stream = undefined;
    this.checkConnection();
  }

  public stop() {
    this.wsServer.close();
    if (this.inputStreamStarted) {
      this.mpeg1Muxer.stop();
      this.inputStreamStarted = false;
    }
    this.emit(ProcessEvents.STOPPED);
    return this;
  }

  prevSegment = null;
  prevSegmentTimestamp = null;

  public start() {
    //this.pipeStreamToSocketServer();
    let gettingInputData, gettingOutputData, inputData, outputData;
    this.mpeg1Muxer = new Mpeg1Muxer({
      ffmpegOptions: this.options.ffmpegOptions,
      url: this.url,
      fps: this.fps,
      ffmpegPath:
        this.options.ffmpegPath == undefined
          ? 'ffmpeg'
          : this.options.ffmpegPath,
    });
    this.stream = this.mpeg1Muxer.stream;
    this.prevSegment = null;

    this.mpeg1Muxer.on('mpeg1data', (data) => {
      if (this.prevSegment === null) {
        this.prevSegment = data;
        this.prevSegmentTimestamp = +new Date();
        this.emit(ProcessEvents.STARTED);
      }
      this.prevSegment = data;
      this.prevSegmentTimestamp = +new Date();
      //console.log(data);
      return this.emit('camdata', data);
    });
    gettingInputData = false;
    // eslint-disable-next-line prefer-const
    inputData = [];
    gettingOutputData = false;
    // eslint-disable-next-line prefer-const
    outputData = [];
    this.mpeg1Muxer.on('ffmpegStderr', (data) => {
      let size;
      data = data.toString();
      if (data.indexOf('Input #') !== -1) {
        gettingInputData = true;
      }
      if (data.indexOf('Output #') !== -1) {
        gettingInputData = false;
        gettingOutputData = true;
      }
      if (data.indexOf('frame') === 0) {
        gettingOutputData = false;
      }
      if (gettingInputData) {
        inputData.push(data.toString());
        size = data.match(/\d+x\d+/);
        if (size != null) {
          size = size[0].split('x');
          if (this.width == null) {
            this.width = parseInt(size[0], 10);
          }
          if (this.height == null) {
            return (this.height = parseInt(size[1], 10));
          }
        }
      }
    });
    this.mpeg1Muxer.on('exit', (err) => {
      //console.log('EXIT');
      return this.emit('exit');
    });
    this.mpeg1Muxer.on('error', (errorCode) => {
      //console.log('EXIT');
      this.emit(ProcessEvents.ERROR, errorCode);
      return this.emit('exit');
    });
    this.inputStreamStarted = true;
    return this;
  }

  async checkConnection() {
    console.log('check connection');
    while (1) {
      await new Promise((resolve) => {
        setTimeout(() => {
          if (this.inputStreamStarted && this.prevSegment !== null) {
            console.log(this.prevSegmentTimestamp, +new Date());
            console.log((+new Date() - this.prevSegmentTimestamp) / 1000 + 's');
            if (+new Date() - this.prevSegmentTimestamp > 5000) {
              this.mpeg1Muxer.stop();
              this.inputStreamStarted = false;
              this.start();
            }
          }
          resolve(true);
        }, 5000);
      });
    }
  }

  public pipeStreamToSocketServer() {
    Logger.verbose(
      `STREAMER - Starting WebSocket Server on port ${this.port} for camera ${this.name}`,
    );
    this.wsServer = new WebSocket.Server({
      port: this.port,
    });
    this.wsServer.on('connection', (socket, request) => {
      return this.onSocketConnect(socket, request);
    });
    this.wsServer.broadcast = function (data, opts) {
      let results;
      results = [];
      for (const client of this.clients) {
        if (client.readyState === 1) {
          results.push(client.send(data, opts));
        } else {
          results.push(
            console.log(
              'Error: Client from remoteAddress ' +
                client.remoteAddress +
                ' not connected.',
            ),
          );
        }
      }
      return results;
    };
    this.emit(ProcessEvents.STARTED);
    return this.on('camdata', (data) => {
      return this.wsServer.broadcast(data);
    });
  }

  onSocketConnect(socket, request) {
    let streamHeader;
    // Send magic bytes and video size to the newly connected socket
    // struct { char magic[4]; unsigned short width, height;}
    // eslint-disable-next-line prefer-const
    streamHeader = new Buffer(8);
    streamHeader.write(STREAM_MAGIC_BYTES);
    streamHeader.writeUInt16BE(this.width, 4);
    streamHeader.writeUInt16BE(this.height, 6);
    socket.send(streamHeader, {
      binary: true,
    });

    Logger.verbose(
      `STREAMER - New WeSocket Connection on port ${this.port} for camera ${this.name} (${this.wsServer.clients.size} total)`,
    );
    // On first connection start ffmpeg process
    if (this.wsServer.clients.size === 1) {
      Logger.verbose(
        `STREAMER - Starting FFMPEG process on port ${this.port} for camera ${this.name}`,
      );
      this.start();
    }

    socket.remoteAddress = request.connection.remoteAddress;

    return socket.on('close', (code, message) => {
      // On last disconnect stop ffmpeg process
      if (this.wsServer.clients.size === 0) {
        Logger.verbose(
          `STREAMER - Stopping FFMPEG process on port ${this.port} for camera ${this.name}`,
        );
        this.mpeg1Muxer.stop();
        this.inputStreamStarted = false;
      }
      return Logger.verbose(
        `STREAMER - Disconnected WebSocket from port ${this.port} for camera ${this.name}  (${this.wsServer.clients.size} total)`,
      );
    });
  }
}
