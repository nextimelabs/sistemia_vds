import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
  Logger,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { LoggedUserId, Roles } from '../entity/auth/auth.decorator';
import { AuthGuard } from '../entity/auth/auth.guard';
import { RuoloEnum } from '../entity/users/enums/ruolo.enum';
import { StreamerService } from './streamer.service';
import { CamerasService } from '../entity/cameras/cameras.service';
import { GetStreamerDto } from './dto/get-streamer.dto';
import { RecorderConverter } from '../recorder/recorder.converter';
import { StreamerConverter } from './streamer.converter';
import { CreateStreamerDto } from './dto/create-streamer.dto';
import { UpdateStreamerDto } from './dto/update-streamer.dto';
import { GetRecorderDto } from '../recorder/dto/get-recorder.dto';
import { StatusEnum } from '../shared/dto/status.enum';

@Controller('streamer')
@ApiTags('Streamer Module')
export class StreamerController {
  constructor(
    private streamerService: StreamerService,
    private camerasService: CamerasService,
    private streamerConverter: StreamerConverter,
  ) {}

  @Post('start')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'All streamer processes started successful',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async startAll() {
    const recorderOptions = await this.streamerService.findAll();
    for (const recorderOption of recorderOptions) {
      if (
        recorderOption.enabled &&
        recorderOption.status === StatusEnum.Stopped
      )
        await this.streamerService.addStream(recorderOption.camera);
    }
    return null;
  }

  @Post('stop')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'All streamer processes stopped successful',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async stopAll() {
    const recorderOptions = await this.streamerService.findAll();
    for (const recorderOption of recorderOptions) {
      if (
        recorderOption.enabled &&
        recorderOption.status === StatusEnum.Running
      )
        await this.streamerService.removeStream(recorderOption.cameraId);
    }
    return null;
  }

  @Get(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get stream process configuration',
    type: GetStreamerDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async get(@Param('id') id: number) {
    return this.streamerConverter.convert(await this.streamerService.get(id));
  }

  @Post(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Add stream process configuration',
    type: GetStreamerDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async add(
    @Param('id') id: number,
    @Body() createStreamerDto: CreateStreamerDto,
  ) {
    return this.streamerConverter.convert(
      await this.streamerService.add(id, createStreamerDto),
    );
  }

  @Put(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Edit stream process configuration',
    type: GetStreamerDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async edit(
    @Param('id') id: number,
    @Body() updateStreamerDto: UpdateStreamerDto,
  ) {
    return this.streamerConverter.convert(
      await this.streamerService.edit(id, updateStreamerDto),
    );
  }

  @Delete(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete stream process configuration',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async delete(@Param('id') id: number) {
    return await this.streamerService.delete(id);
  }

  @Post(':id/start')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Stream process started succesfull',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async start(@Param('id') id: number) {
    const camera = await this.camerasService.findOne(id);
    await this.streamerService.addStream(camera);
    return this.streamerConverter.convert(await this.streamerService.get(id));
  }

  @Post(':id/stop')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Stream process stopped succesfull',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async stop(@Param('id') id: number) {
    await this.streamerService.removeStream(id);
    return this.streamerConverter.convert(await this.streamerService.get(id));
  }
}
