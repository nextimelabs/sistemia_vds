import { Injectable } from '@nestjs/common';
import { StreamerService } from './streamer.service';
import { StreamerOptionsEntity } from './entities/streamer-options.entity';
import { GetStreamerDto } from './dto/get-streamer.dto';
import { StatusEnum } from '../shared/dto/status.enum';

@Injectable()
export class StreamerConverter {
  constructor(private streamerService: StreamerService) {}

  convert(streamerOptionsEntity: StreamerOptionsEntity): GetStreamerDto {
    return {
      fps: streamerOptionsEntity.fps,
      status: Object.keys(StatusEnum).filter((key) => isNaN(Number(key)))[
        streamerOptionsEntity.status
      ],
      errorMessage: streamerOptionsEntity.errorMessage,
      enabled: streamerOptionsEntity.enabled,
      wsPort: parseInt(streamerOptionsEntity.wsPort.toString()),
    };
  }

  converts(streamerOptionsEntities: StreamerOptionsEntity[]): GetStreamerDto[] {
    return streamerOptionsEntities.map((streamerOptionsEntity) => {
      return this.convert(streamerOptionsEntity);
    });
  }
}
