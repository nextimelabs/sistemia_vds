import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { CamerasModule } from './cameras/cameras.module';
import { GroupsModule } from './groups/groups.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    CamerasModule,
    GroupsModule,
  ]
})
export class EntityModule {}
