import { forwardRef, Module } from '@nestjs/common';
import { CamerasService } from './cameras.service';
import { CamerasController } from './cameras.controller';
import { CamerasConverter } from './cameras.converter';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CameraEntity } from './entities/camera.entity';
import { UsersModule } from '../users/users.module';
import { RecorderModule } from '../../recorder/recorder.module';
import { StreamerModule } from '../../streamer/streamer.module';
import { GroupsModule } from '../groups/groups.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      CameraEntity
    ]),
    UsersModule,
    forwardRef(() => GroupsModule),
    forwardRef(() => RecorderModule),
    forwardRef(() => StreamerModule),
  ],
  controllers: [CamerasController],
  providers: [CamerasService, CamerasConverter],
  exports: [CamerasService, CamerasConverter]
})
export class CamerasModule {}
