import {
  HttpException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CameraEntity } from './entities/camera.entity';
import { CreateCameraDto } from './dto/create-camera.dto';
import { UpdateCameraDto } from './dto/update-camera.dto';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { GetOnvifDeviceDto } from './dto/get-onvif-device.dto';
import { GetOnvifDeviceInfoDto } from './dto/get-onvif-device-info.dto';
import { EventEntity } from '../../monitor/events/entities/events.entity';
import { Point } from 'geojson';
const onvif = require('node-onvif');

interface ISearchCameras {
  limit: number;
  offset: number;
  groupId?: number;
  search?: string;
  orderColumn: string;
  orderBy: 'ASC' | 'DESC';
}

@Injectable()
export class CamerasService {
  constructor(
    @InjectRepository(CameraEntity)
    private cameraEntityRepository: Repository<CameraEntity>,
    private eventEmitter: EventEmitter2,
  ) {}

  async create(createCameraDto: CreateCameraDto) {
    // Add camera entity in db
    /*
    const geoJson: Point = {
      type: 'Point',
      coordinates: [createCameraDto.longitude, createCameraDto.latitude],
    };
     */
    const cameraEntity: CameraEntity = {
      alias: createCameraDto.alias,
      camSourcePath: createCameraDto.camSourcePath,
      latitude: createCameraDto.latitude,
      longitude: createCameraDto.longitude,
      groupId: createCameraDto.groupId,
    };

    const addedCamere: CameraEntity = await this.cameraEntityRepository.save(
      cameraEntity,
    );

    this.eventEmitter.emit('camera.created', cameraEntity);

    return this.findOne(addedCamere.id);
  }

  async update(id: number, updateCameraDto: UpdateCameraDto) {
    const oldCameraEntity = await this.findOne(id);

    const cameraEntity: CameraEntity = {
      ...oldCameraEntity,
      alias: updateCameraDto.alias,
      camSourcePath: updateCameraDto.camSourcePath,
      latitude: updateCameraDto.latitude,
      longitude: updateCameraDto.longitude,
      groupId: updateCameraDto.groupId,
    };

    this.eventEmitter.emit('camera.updated', oldCameraEntity, cameraEntity);

    await this.cameraEntityRepository.save(cameraEntity);

    return this.findOne(cameraEntity.id);
  }

  async findOne(id: number): Promise<CameraEntity> {
    const cameraEntity: CameraEntity =
      await this.cameraEntityRepository.findOne(id, {
        relations: ['streamerOptions', 'recorderOptions', 'group'],
      });
    if (!cameraEntity) {
      throw new HttpException('CAMERA_NOT_FOUND', HttpStatus.NOT_FOUND);
    }
    return cameraEntity;
  }

  async find({
    limit,
    offset,
    groupId,
    search,
    orderColumn,
    orderBy,
  }: ISearchCameras): Promise<[CameraEntity[], number]> {
    let queryBuilder = this.cameraEntityRepository
      .createQueryBuilder('cameras')
      .leftJoinAndSelect('cameras.group', 'group')
      .leftJoinAndSelect('cameras.streamerOptions', 'streamerOptions')
      .leftJoinAndSelect('cameras.recorderOptions', 'recorderOptions');
    if (groupId)
      queryBuilder = queryBuilder.andWhere('cameras.groupId = :groupId', {
        groupId,
      });
    if (search)
      queryBuilder = queryBuilder.andWhere(
        `(cameras.alias like :search OR group.name like :search)`,
        {
          search: `%${search}%`,
        },
      );
    if (orderColumn === 'groupName')
      queryBuilder = queryBuilder.orderBy('group.name', orderBy);
    else if (orderColumn === 'streamerStatus')
      queryBuilder = queryBuilder.orderBy('streamerOptions.status', orderBy);
    else if (orderColumn === 'recorderStatus')
      queryBuilder = queryBuilder.orderBy('recorderOptions.status', orderBy);
    else queryBuilder = queryBuilder.orderBy(`cameras.${orderColumn}`, orderBy);

    return queryBuilder.limit(limit).offset(offset).getManyAndCount();
  }

  async findByGroupId(groupId: number): Promise<CameraEntity[]> {
    return await this.cameraEntityRepository.find({
      relations: ['streamerOptions', 'recorderOptions', 'group'],
      where: {
        groupId,
      },
    });
  }
  async findBySourcePath(camSourcePath: string): Promise<CameraEntity> {
    return await this.cameraEntityRepository.findOne({
      where: {
        camSourcePath,
      },
    });
  }

  async remove(id: number) {
    const camera = await this.findOne(id);
    await this.cameraEntityRepository.remove(camera);
    this.eventEmitter.emit('camera.removed', id);
    return true;
  }

  async scanOnvifCameras() {
    return new Promise((resolve, reject) => {
      onvif
        .startProbe()
        .then((device_info_list) => {
          const onvifCamerasList: GetOnvifDeviceDto[] = [];
          device_info_list.forEach((info) => {
            onvifCamerasList.push({
              urn: info.urn,
              name: info.name,
              xaddr: info.xaddrs[0],
            });
          });
          resolve(onvifCamerasList);
        })
        .catch((error) => {
          Logger.error(error);
          reject(error);
        });
    });
  }

  async getDeviceInfo(xaddr, user, password) {
    return new Promise((resolve, reject) => {
      // Create an OnvifDevice object
      const device = new onvif.OnvifDevice({
        xaddr: xaddr,
        user: user,
        pass: password,
      });
      device
        .init()
        .then((info) => {
          // Get the UDP stream URL
          const deviceInfo: GetOnvifDeviceInfoDto = {
            manufacturer: info.Manufacturer,
            model: info.Model,
            firmwareVersion: info.FirmwareVersion,
            serialNumber: info.SerialNumber,
            hardwareId: info.HardwareId,
            streamUrl: device.getUdpStreamUrl(),
          };
          resolve(deviceInfo);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }
}
