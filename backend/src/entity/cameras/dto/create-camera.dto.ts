import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsDate,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  ValidateNested,
  IsISO8601,
  MinLength,
  IsNumber, IsLatitude, IsLongitude,
} from 'class-validator';
import { Type } from 'class-transformer';
import {
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

export class CreateCameraDto {
  @ApiProperty({ maxLength: 50, description: 'Camera name' })
  @IsNotEmpty()
  @MaxLength(50)
  alias: string;

  @ApiProperty({
    maxLength: 255,
    description: 'Camera streaming source path',
    examples: [
      'rtsp://192.168.1.83:554/11',
      'rtsp://admin:Cisco123@192.168.1.89:554/StreamingSetting?version=1.0&action=getRTSPStream&ChannelID=1&ChannelName=Channel1',
    ],
  })
  @IsNotEmpty()
  @MaxLength(255)
  camSourcePath: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  groupId?: number;

  @ApiPropertyOptional({
    description:
      'Latitude of the coordinate in the reference system EPSG:4326 (WGS84)',
    example: 41.89021,
  })
  @IsOptional()
  @IsLatitude()
  latitude: string;

  @ApiPropertyOptional({
    description:
      'Longitude of the coordinate in the reference system EPSG:4326 (WGS84)',
    example: 12.492231,
  })
  @IsOptional()
  @IsLongitude()
  longitude: string;
}
