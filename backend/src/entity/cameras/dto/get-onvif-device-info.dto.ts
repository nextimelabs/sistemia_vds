import { ApiProperty } from '@nestjs/swagger';

export class GetOnvifDeviceInfoDto {
  @ApiProperty()
  manufacturer: string;
  @ApiProperty()
  model: string;
  @ApiProperty()
  firmwareVersion: string;
  @ApiProperty()
  serialNumber: string;
  @ApiProperty()
  hardwareId: string;
  @ApiProperty({ description: 'Camera stream source path' })
  streamUrl: string;
}
