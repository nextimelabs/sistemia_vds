import { ApiProperty } from '@nestjs/swagger';

export class GetOnvifDeviceDto {
  @ApiProperty({ description: 'Unique resource name' })
  urn: string;
  @ApiProperty()
  name: string;
  @ApiProperty({ description: 'ONVIF url endpoint' })
  xaddr: string;
}
