import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MaxLength,
  IsNumber, IsLatitude, IsLongitude,
} from 'class-validator';
import { GetStreamerDto } from '../../../streamer/dto/get-streamer.dto';
import { GetRecorderDto } from '../../../recorder/dto/get-recorder.dto';
import { GetGroupDto } from '../../groups/dto/get-group.dto';

export class GetCameraDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @ApiProperty({maxLength: 30})
  @IsNotEmpty()
  @MaxLength(30)
  alias: string;

  @ApiProperty({maxLength: 255})
  @IsNotEmpty()
  @MaxLength(255)
  camSourcePath: string;

  @ApiProperty({
    description:
      'Latitude of the coordinate in the reference system EPSG:4326 (WGS84)',
    example: 41.89021,
  })
  @IsNotEmpty()
  @IsLatitude()
  latitude: string;

  @ApiProperty({
    description:
      'Longitude of the coordinate in the reference system EPSG:4326 (WGS84)',
    example: 12.492231,
  })
  @IsNotEmpty()
  @IsLongitude()
  longitude: string;

  @ApiPropertyOptional({ type: GetGroupDto })
  group: GetGroupDto;

  @ApiPropertyOptional({ type: GetRecorderDto })
  recorderOptions?: GetRecorderDto;

  @ApiPropertyOptional( { type: GetStreamerDto })
  streamerOptions?: GetStreamerDto;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
