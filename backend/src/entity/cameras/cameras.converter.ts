import { Injectable } from '@nestjs/common';
import { CamerasService } from './cameras.service';
import { CameraEntity } from './entities/camera.entity';
import { GetCameraDto } from './dto/get-camera.dto';
import { RecorderConverter } from '../../recorder/recorder.converter';
import { StreamerConverter } from '../../streamer/streamer.converter';
import { GroupsConverter } from '../groups/groups.converter';

@Injectable()
export class CamerasConverter {
  constructor(
    private recorderConverter: RecorderConverter,
    private streamerConverter: StreamerConverter,
    private groupsConverter: GroupsConverter,
  ) {}

  convert(cameraEntity: CameraEntity): GetCameraDto {
    return {
      id: cameraEntity.id,
      alias: cameraEntity.alias,
      camSourcePath: cameraEntity.camSourcePath,
      latitude: cameraEntity.latitude,
      longitude: cameraEntity.longitude,
      group: cameraEntity.group
        ? this.groupsConverter.convert(cameraEntity.group)
        : null,
      recorderOptions: cameraEntity.recorderOptions
        ? this.recorderConverter.convert(cameraEntity.recorderOptions)
        : null,
      streamerOptions: cameraEntity.streamerOptions
        ? this.streamerConverter.convert(cameraEntity.streamerOptions)
        : null,
      createdAt: cameraEntity.createdAt,
      updatedAt: cameraEntity.updatedAt,
    };
  }

  converts(cameraEntities: CameraEntity[]): GetCameraDto[] {
    return cameraEntities.map((cameraEntity) => {
      return this.convert(cameraEntity);
    });
  }
}
