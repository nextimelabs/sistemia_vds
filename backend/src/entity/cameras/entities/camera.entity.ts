import { Column, Entity, Index, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from "typeorm";
import { GroupEntity } from '../../groups/entities/group.entity';
import { StreamerOptionsEntity } from '../../../streamer/entities/streamer-options.entity';
import { RecorderOptionsEntity } from '../../../recorder/entities/recorder-options.entity';
import {Point} from "geojson";

@Entity('cameras')
export class CameraEntity {
  @PrimaryGeneratedColumn()
  id?: number;
  @Column("varchar", {length: 50, nullable: false})
  alias: string;
  @Column("varchar", {length: 255, nullable: false})
  camSourcePath: string;
  @Column('text', { nullable: true })
  latitude: string;
  @Column('text', { nullable: true })
  longitude: string;
  @ManyToOne((type) => GroupEntity, (group) => group.id, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  group?: GroupEntity;
  @Column({ nullable: true })
  groupId?: number;
  @OneToOne(() => StreamerOptionsEntity, streamerOptions => streamerOptions.camera)
  streamerOptions?: StreamerOptionsEntity;
  @OneToOne(() => RecorderOptionsEntity, recorderOptions => recorderOptions.camera) // specify inverse side as a second parameter
  recorderOptions?: RecorderOptionsEntity;
  @CreateDateColumn({ type: 'timestamp without time zone', default: () => "NOW()" })
  createdAt?: Date;
  @UpdateDateColumn({ type: 'timestamp without time zone', default: () => "NOW()", onUpdate: "NOW()" })
  updatedAt?: Date;
}
