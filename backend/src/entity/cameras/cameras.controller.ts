import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
  Logger,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiTags,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiQuery,
} from '@nestjs/swagger';
import { LoggedUserId, Roles } from '../auth/auth.decorator';
import { AuthGuard } from '../auth/auth.guard';
import { CamerasService } from './cameras.service';
import { CamerasConverter } from './cameras.converter';
import { GetCameraDto } from './dto/get-camera.dto';
import { RuoloEnum } from '../users/enums/ruolo.enum';
import { CreateCameraDto } from './dto/create-camera.dto';
import { CameraEntity } from './entities/camera.entity';
import { UpdateCameraDto } from './dto/update-camera.dto';
import { StreamerService } from '../../streamer/streamer.service';
import { RecorderService } from '../../recorder/recorder.service';
import { GetOnvifDeviceDto } from './dto/get-onvif-device.dto';
import { GetOnvifDeviceInfoDto } from './dto/get-onvif-device-info.dto';
import { ApiPaginatedResponse } from '../../shared/pagination.decorator';
import { GetEventDto } from '../../monitor/events/dto/get-event.dto';
import { DeleteBulkCameraDto } from './dto/delete-bulk-camera.dto';

@Controller('cameras')
@ApiTags('Cameras')
export class CamerasController {
  constructor(
    private camerasService: CamerasService,
    private camerasConverter: CamerasConverter,
  ) {}

  @Post()
  @ApiBearerAuth()
  @ApiCreatedResponse({
    description: 'Camera created succesfull',
    type: GetCameraDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async create(
    @Body() createCameraDto: CreateCameraDto,
  ): Promise<GetCameraDto> {
    if (
      (await this.camerasService.findBySourcePath(
        createCameraDto.camSourcePath,
      )) !== undefined
    ) {
      throw new HttpException(
        'SOURCE_URL_ALREADY_USED',
        HttpStatus.BAD_REQUEST,
      );
    }

    try {
      const cameraEntity: CameraEntity = await this.camerasService.create(
        createCameraDto,
      );
      return this.camerasConverter.convert(cameraEntity);
    } catch (e) {
      if (e.code == 23503)
        throw new HttpException('GROUP_NOT_FOUND', HttpStatus.NOT_FOUND);
      throw new InternalServerErrorException();
    }
  }

  @Get('onvif')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Scan ONVIF compatible network cameras',
    type: [GetOnvifDeviceDto],
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async scanOnvif() {
    try {
      return await this.camerasService.scanOnvifCameras();
    } catch (e) {
      new InternalServerErrorException(e);
    }
  }

  @Get('onvif/info')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Scan ONVIF compatible network cameras',
    type: GetOnvifDeviceInfoDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async getDeviceInfo(
    @Query('xaddr') xaddr: string,
    @Query('user') user: string,
    @Query('password') password: string,
  ) {
    try {
      return await this.camerasService.getDeviceInfo(xaddr, user, password);
    } catch (e) {
      if (e.toString().includes('Sender not authorized'))
        throw new HttpException(
          'AUTHENTICATION_FAILED',
          HttpStatus.UNAUTHORIZED,
        );
      throw new InternalServerErrorException();
    }
  }

  @Get()
  @ApiBearerAuth()
  @ApiPaginatedResponse(GetCameraDto, 'Get paginated cameras list')
  @ApiQuery({
    name: 'orderColumn',
    enum: [
      'id',
      'alias',
      'groupName',
      'streamerStatus',
      'recorderStatus',
      'createdAt',
    ],
    required: false,
  })
  @ApiQuery({ name: 'orderBy', enum: ['ASC', 'DESC'], required: false })
  @ApiQuery({ name: 'groupId', type: Number, required: false })
  @ApiQuery({ name: 'search', type: String, required: false })
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @ApiQuery({ name: 'offset', type: Number, required: false })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async find(
    @Query('limit') limit = 10,
    @Query('offset') offset = 0,
    @Query('groupId') groupId: number,
    @Query('search') search: string,
    @Query('orderColumn') orderColumn = 'id',
    @Query('orderBy') orderBy: 'ASC' | 'DESC' = 'DESC',
  ) {
    const res = await this.camerasService.find({
      limit,
      offset,
      groupId,
      search,
      orderColumn,
      orderBy,
    });
    return {
      total: res[1],
      limit: limit,
      offset: offset,
      results: await this.camerasConverter.converts(res[0]),
    };
  }

  @Get(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get camera',
    type: GetCameraDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async findOne(@Param('id') id: number) {
    const cameraEntity = await this.camerasService.findOne(id);
    return this.camerasConverter.convert(cameraEntity);
  }

  @Put(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Edit camera',
    type: GetCameraDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async update(
    @Param('id') id: number,
    @Body() updateCameraDto: UpdateCameraDto,
  ) {
    try {
      const cameraEntity = await this.camerasService.update(
        id,
        updateCameraDto,
      );
      return this.camerasConverter.convert(cameraEntity);
    } catch (e) {
      if (e.code == 23503)
        throw new HttpException('GROUP_NOT_FOUND', HttpStatus.NOT_FOUND);
      throw new InternalServerErrorException();
    }
  }

  @Delete('bulk')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete camera bulk',
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async removeBulk(@Body() deleteBulkCameraDto: DeleteBulkCameraDto) {
    Logger.log(JSON.stringify(deleteBulkCameraDto));
    await Promise.all(
      deleteBulkCameraDto.ids.map(async (id) => {
        await this.camerasService.remove(id);
      }),
    );
    return null;
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete camera',
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async remove(@Param('id') id: number) {
    await this.camerasService.remove(id);
    return null;
  }
}
