import { Injectable } from '@nestjs/common';
import { UsersService } from './users.service';
import { GetUserDto } from './dto/get-user.dto';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UsersConverter {
  constructor(
    private usersService: UsersService
  ) {}

  convert(userEntity: UserEntity): GetUserDto {
    return {
      id: userEntity.id,
      username: userEntity.username,
      role: userEntity.role,
      status: userEntity.status,
      createdAt: userEntity.createdAt,
      updatedAt: userEntity.updatedAt
    }
  }

  converts(userEntities: UserEntity[]): GetUserDto[] {
    return userEntities.map(userEntity => { return this.convert(userEntity)});
  }
}
