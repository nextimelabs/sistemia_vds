import {RuoloEnum} from "../enums/ruolo.enum";
import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {
  IsBoolean,
  IsDate, IsDefined,
  IsEnum, IsISO8601,
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString, IsUUID, MaxLength, MinLength,
  ValidateNested,
} from 'class-validator';
import {Type} from "class-transformer";
import {StatoEnum} from "../enums/stato.enum";
import { CreateUserDto } from './create-user.dto';


export class GetUserDto {
  @ApiProperty({required: true})
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @ApiProperty({required: true, maxLength: 30})
  @IsNotEmpty()
  @MaxLength(30)
  username: string;

  @ApiProperty({enum: RuoloEnum})
  @IsEnum(RuoloEnum)
  @IsNotEmpty()
  role: RuoloEnum;

  @ApiProperty({enum: StatoEnum})
  @IsEnum(StatoEnum)
  @IsNotEmpty()
  status: StatoEnum;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
