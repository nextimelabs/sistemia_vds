import {Ruolo, RuoloEnum} from "../enums/ruolo.enum";
import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsDate, IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString, IsUUID, MaxLength,
  ValidateNested, IsISO8601, MinLength,
} from 'class-validator';
import {Type} from "class-transformer";
import { Column, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { StatoEnum } from '../enums/stato.enum';

export class UpdateUserDto {
  @ApiProperty({required: true, maxLength: 30})
  @IsNotEmpty()
  @MaxLength(30)
  username: string;

  @ApiProperty({enum: RuoloEnum})
  @IsEnum(RuoloEnum)
  @IsNotEmpty()
  role: RuoloEnum;

  @ApiProperty({enum: StatoEnum})
  @IsEnum(StatoEnum)
  @IsNotEmpty()
  status: StatoEnum;
}
