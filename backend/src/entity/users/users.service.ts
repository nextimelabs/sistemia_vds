import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import * as bcryptjs from 'bcryptjs';
import { UpdateUserDto } from './dto/update-user.dto';
import { RuoloEnum } from './enums/ruolo.enum';

interface ISearchUsers {
  limit: number;
  offset: number;
  search?: string;
  orderColumn: string;
  orderBy: 'ASC' | 'DESC';
}

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private userEntityRepository: Repository<UserEntity>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const userEntity: UserEntity = {
      username: createUserDto.username,
      role: createUserDto.role,
      status: createUserDto.status,
      password: await bcryptjs.hash(createUserDto.password, 10),
    };
    return await this.userEntityRepository.save(userEntity);
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const oldUserEntity = await this.findOne(id);

    const userEntity: UserEntity = {
      ...oldUserEntity,
      username: updateUserDto.username,
      role: updateUserDto.role,
      status: updateUserDto.status,
    };
    return await this.userEntityRepository.save(userEntity);
  }

  async findOne(id: number): Promise<UserEntity> {
    const userEntity: UserEntity = await this.userEntityRepository.findOne(id);
    if (!userEntity) {
      throw new HttpException('USER_NOT_FOUND', HttpStatus.NOT_FOUND);
    }
    return userEntity;
  }

  async isThereLastOneAdmin(): Promise<UserEntity | false> {
    const [userEntities, count] = await this.userEntityRepository.findAndCount({
      role: RuoloEnum.Amministratore,
    });
    Logger.log(userEntities);
    Logger.log(count);
    if (count === 1) return userEntities[0];
    else return false;
  }

  async find({
    limit,
    offset,
    search,
    orderColumn,
    orderBy,
  }: ISearchUsers): Promise<[UserEntity[], number]> {
    let queryBuilder = this.userEntityRepository.createQueryBuilder('users');
    if (search)
      queryBuilder = queryBuilder.andWhere('users.username like :search', {
        search: `%${search}%`,
      });
    return queryBuilder
      .orderBy(`users.${orderColumn}`, orderBy)
      .limit(limit)
      .offset(offset)
      .getManyAndCount();
  }

  async remove(id: number) {
    await this.userEntityRepository.delete(id);
    return true;
  }
}
