import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Logger,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiTags,
  ApiBearerAuth,
  ApiCreatedResponse, ApiQuery,
} from '@nestjs/swagger';
import { UsersService } from './users.service';
import { LoggedUserId, Roles } from '../auth/auth.decorator';
import { AuthGuard } from '../auth/auth.guard';
import { RuoloEnum } from './enums/ruolo.enum';
import { GetUserDto } from './dto/get-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UserEntity } from './entities/user.entity';
import { UsersConverter } from './users.converter';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiPaginatedResponse } from '../../shared/pagination.decorator';
import { GetCameraDto } from '../cameras/dto/get-camera.dto';
import { DeleteBulkCameraDto } from '../cameras/dto/delete-bulk-camera.dto';
import { DeleteBulkUserDto } from './dto/delete-bulk-user.dto';

@Controller('users')
@ApiTags('Users')
export class UsersController {
  constructor(
    private usersService: UsersService,
    private usersConverter: UsersConverter,
  ) {}

  @Post()
  @ApiBearerAuth()
  @ApiCreatedResponse({
    description: 'User created succesfull',
    type: GetUserDto,
  })
  @Roles(RuoloEnum.Amministratore)
  @UseGuards(AuthGuard)
  async create(@Body() createUserDto: CreateUserDto): Promise<GetUserDto> {
    const userEntity: UserEntity = await this.usersService.create(
      createUserDto,
    );
    return this.usersConverter.convert(userEntity);
  }

  @Get()
  @ApiBearerAuth()
  @ApiPaginatedResponse(
    GetCameraDto,
    'Get paginated users list'
  )
  @ApiQuery({ name: 'orderColumn', enum: ['id', 'username', 'role', 'status', 'createdAt'], required: false })
  @ApiQuery({ name: 'orderBy', enum: ['ASC', 'DESC'], required: false })
  @ApiQuery({ name: 'search', type: String, required: false })
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @ApiQuery({ name: 'offset', type: Number, required: false })
  @Roles(RuoloEnum.Amministratore)
  @UseGuards(AuthGuard)
  async find(
    @Query('limit') limit = 10,
    @Query('offset') offset = 0,
    @Query('search') search: string,
    @Query('orderColumn') orderColumn: string = 'id',
    @Query('orderBy') orderBy: 'ASC' | 'DESC' = 'DESC',
  ) {
    const res = await this.usersService.find({
      limit,
      offset,
      search,
      orderColumn,
      orderBy,
    });
    return {
      total: res[1],
      limit: limit,
      offset: offset,
      results: await this.usersConverter.converts(res[0]),
    };
  }

  @Get(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get user',
    type: GetUserDto,
  })
  @Roles(RuoloEnum.Amministratore)
  @UseGuards(AuthGuard)
  async findOne(@Param('id') id: number) {
    const userEntity = await this.usersService.findOne(id);
    return this.usersConverter.convert(userEntity);
  }

  @Put(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Edit user',
    type: GetUserDto,
  })
  @Roles(RuoloEnum.Amministratore)
  @UseGuards(AuthGuard)
  async update(@Param('id') id: number, @Body() updateUserDto: UpdateUserDto) {
    const userEntity = await this.usersService.update(id, updateUserDto);
    return this.usersConverter.convert(userEntity);
  }

  @Delete('bulk')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete users bulk',
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async removeBulk(@Body() deleteBulkUserDto: DeleteBulkUserDto) {
    await Promise.all(
      deleteBulkUserDto.ids.map(async (id) => {
        await this.usersService.remove(id);
      }),
    );
    return null;
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete user',
  })
  @Roles(RuoloEnum.Amministratore)
  @UseGuards(AuthGuard)
  async remove(@LoggedUserId() loggedUserId: number, @Param('id') id: number) {
    if (loggedUserId == id) {
      throw new HttpException(
        `CANNOT_DELETE_LOGGED_USER`,
        HttpStatus.BAD_REQUEST,
      );
    }
    const isThereLastAdmin = await this.usersService.isThereLastOneAdmin();
    if (isThereLastAdmin && isThereLastAdmin.id === id) {
      throw new HttpException(
        'CANNOT_DELETE_LAST_ADMIN',
        HttpStatus.BAD_REQUEST,
      );
    }
    await this.usersService.remove(id);
    return null;
  }
}
