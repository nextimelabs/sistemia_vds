import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';
import { UsersConverter } from './users.converter';
import { UsersController } from './users.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity
    ])
  ],
  providers: [UsersService, UsersConverter],
  exports: [UsersService, UsersConverter],
  controllers: [UsersController]
})
export class UsersModule {}
