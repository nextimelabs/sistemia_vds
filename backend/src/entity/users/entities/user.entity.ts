import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import { CreateDateColumn,UpdateDateColumn } from "typeorm";
import { StatoEnum } from '../enums/stato.enum';
import { RuoloEnum } from '../enums/ruolo.enum';

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id?: number;
  @Column("varchar", {length: 30, nullable: false, unique: true})
  username: string;
  @Column("varchar", {length: 255, nullable: false})
  password: string;
  @Column("varchar", {length: 30, nullable: false})
  role: RuoloEnum;
  @Column("varchar", {length: 30, nullable: false})
  status: StatoEnum;

  @CreateDateColumn({ type: 'timestamp without time zone', default: () => "NOW()" })
  createdAt?: Date;
  @UpdateDateColumn({ type: 'timestamp without time zone', default: () => "NOW()", onUpdate: "NOW()" })
  updatedAt?: Date;
}
