export enum StatoEnum {
  Attivo = 'Attivo',
  Sospeso = 'Sospeso',
  CambioPassword = 'CambioPassword',
  PrimoAccesso = 'PrimoAccesso',
}
export const Stato = [
  'Attivo',
  'Sospeso',
  'CambioPassword',
  'PrimoAccesso',
];
