export enum RuoloEnum {
  Amministratore = 'Amministratore',
  Sistemista = 'Sistemista',
  Operatore = 'Operatore',
}
export const Ruolo = [
  'Amministratore',
  'Sistemista',
  'Operatore',
];
