import {createParamDecorator, ExecutionContext, HttpException, HttpStatus, Logger, SetMetadata} from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import * as environment from '../../../environment/environment.json';
import { TokenPayloadInterface } from './token-payload.interface';

export const Roles = (...roles: string[]) => SetMetadata('roles', roles); //Tutti gli utenti con un determinato ruolo
export const Logged = () => SetMetadata('logged', null);
export const LoggedUserId = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const headers = ctx.switchToHttp().getRequest().headers;

    // Se non e' presente la chiave di autorizzazione nell'header allora non si autorizza la richiesta
    if (headers['authorization'] === undefined) {
      throw new HttpException('TOKEN_NOT_FOUND', HttpStatus.UNAUTHORIZED);
    }

    const token = headers['authorization'].split(' ')[1];
    if (token.length == 0) {
      throw new HttpException('INVALID_TOKEN', HttpStatus.UNAUTHORIZED);
    }

    let payload: TokenPayloadInterface | any;
    if (!(payload = jwt.verify(token, environment.SECRET, {ignoreExpiration: true}))) {
      throw new HttpException('INVALID_TOKEN', HttpStatus.UNAUTHORIZED);
    }
    return payload.userId;
  },
);
