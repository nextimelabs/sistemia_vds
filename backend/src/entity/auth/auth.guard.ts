import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import * as jwt from 'jsonwebtoken';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as environment from '../../../environment/environment.json';
import { UsersService } from '../users/users.service';
import { TokenPayloadInterface } from './token-payload.interface';
import { UserEntity } from '../users/entities/user.entity';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private usersService: UsersService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    const request = context.switchToHttp().getRequest();
    const headers = request.headers;

    // Se non e' presente la chiave di autorizzazione nell'header allora non si autorizza la richiesta
    if (headers['authorization'] === undefined) {
      throw new HttpException('TOKEN_NOT_FOUND', HttpStatus.UNAUTHORIZED);
    }

    const token = headers['authorization'].split(' ')[1];
    if (token.length == 0) {
      throw new HttpException('INVALID_TOKEN', HttpStatus.UNAUTHORIZED);
    }

    let payload: TokenPayloadInterface | any;
    if (
      !(payload = jwt.verify(token, environment.SECRET, {
        ignoreExpiration: true,
      }))
    ) {
      throw new HttpException('INVALID_TOKEN', HttpStatus.UNAUTHORIZED);
    }

    const loggedUser: UserEntity = await this.usersService.findOne(
      payload.userId,
    );

    if (!loggedUser)
      throw new HttpException('USER_NOT_FOUND', HttpStatus.FORBIDDEN);

    // Roles: se l'utente loggato non fa parte dei ruoli ammessi ritorna false
    // Controllo se il ruolo dell'utente e' incluso nell'elenco dei ruoli a cui e' autorizzata la rotta o se la rotta e' autorizzata a tutti
    if (roles && roles.length > 0 && !roles.includes(loggedUser.role)) {
      Logger.warn(
        `Il ruolo dell'utente e' non autorizzato alla rotta: [ruoloUtente][ruoliAmmessi] [${loggedUser.role}] [${roles}]`,
      );
      return false;
    }
    return true;
  }
}
