import { IsAlphanumeric, IsEmail, IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import {ApiProperty} from '@nestjs/swagger';

export class LoginUserDto {
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(30)
  @IsString()
  username: string;

  @ApiProperty()
  @IsNotEmpty()
  password: string;
}
