import {IsAlphanumeric, IsEmail, IsNotEmpty, MaxLength, MinLength, ValidateNested} from 'class-validator';
import {ApiProperty} from '@nestjs/swagger';
import {Type} from "class-transformer";
import { GetUserDto } from '../../users/dto/get-user.dto';

export class LoginUserResponseDto {
  @ApiProperty()
  user: GetUserDto;

  @ApiProperty()
  token: string;
}
