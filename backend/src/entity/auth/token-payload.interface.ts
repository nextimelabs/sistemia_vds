export interface TokenPayloadInterface {
  userId: number;
  exp: number;
}
