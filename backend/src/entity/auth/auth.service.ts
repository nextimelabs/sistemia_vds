import { HttpException, Injectable, Logger } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { LoginUserDto } from './dto/login-user.dto';
import { UserEntity } from '../users/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as jwt from 'jsonwebtoken';
import * as bcryptjs from 'bcryptjs';
import { Repository } from 'typeorm';
import * as environment from '../../../environment/environment.json';
import { StatoEnum } from '../users/enums/stato.enum';
import { RuoloEnum } from '../users/enums/ruolo.enum';
import { TokenPayloadInterface } from './token-payload.interface';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    @InjectRepository(UserEntity)
    private userEntityRepository: Repository<UserEntity>,
  ) {}
  public generateJWT(user: UserEntity) {
    const today = new Date();
    const exp = new Date(today);
    exp.setDate(today.getDate() + 60);
    const payload: TokenPayloadInterface = {
      userId: user.id,
      exp: exp.getTime() / 1000,
    };
    return jwt.sign(payload, process.env.SECRET || environment.SECRET);
  }

  async login({ username, password }: LoginUserDto) {
    const userEntity: UserEntity = await this.userEntityRepository
      .createQueryBuilder('user')
      .andWhere('user.username = :username', { username: username })
      .andWhere('user.status NOT IN (:stati) ', { stati: [StatoEnum.Sospeso] })
      .getOne();

    if (!userEntity) {
      throw new HttpException('Username o password errati!', 401);
    }

    if (await bcryptjs.compare(password, userEntity.password)) {
      return userEntity;
    } else {
      throw new HttpException('Username o password errati!', 401);
    }
  }

  async addDefaultUser() {
    const users = await this.userEntityRepository.find();
    if (users.length === 0) {
      const user = await this.usersService.create({
        username: process.env.SW_USERNAME || environment.SW_USERNAME,
        password: process.env.SW_PASSWORD || environment.SW_PASSWORD,
        role: RuoloEnum.Amministratore,
        status: StatoEnum.Attivo,
      });
    }
  }
}
