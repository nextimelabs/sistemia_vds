import { Body, Controller, Get, HttpCode, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LocalAuthGuard } from './local-auth.guard';
import { LoginUserResponseDto } from './dto/login-user-response.dto';
import {ApiOkResponse, ApiTags} from "@nestjs/swagger";
import { LoginUserDto } from './dto/login-user.dto';
import { AuthService } from './auth.service';
import { UserEntity } from '../users/entities/user.entity';
import { UsersConverter } from '../users/users.converter';

@ApiTags('Auth')
@Controller()
export class AuthController {
  constructor(
    private authService: AuthService,
    private usersCoverter: UsersConverter
  ){}

  @UseGuards(LocalAuthGuard)
  @HttpCode(200)
  @ApiOkResponse({
    description: 'Registrazione effettuata correttamente',
    type: LoginUserResponseDto
  })
  @Post('auth/login')
  async login(@Body() loginUserDto: LoginUserDto) {
    const userEntity: UserEntity = await this.authService.login(loginUserDto);
    const token = await this.authService.generateJWT(userEntity);
    const res = { user: await this.usersCoverter.convert(userEntity), token };
    return res;
  }
}
