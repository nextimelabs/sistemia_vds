import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { CreateDateColumn } from 'typeorm';

@Entity('groups')
export class GroupEntity {
  @PrimaryGeneratedColumn()
  id?: number;
  @Column('varchar', { length: 50, nullable: false })
  name: string;
  @ManyToOne((type) => GroupEntity, (group) => group.id, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  parentGroup?: GroupEntity;
  @Column({ nullable: true })
  parentGroupId: number;

  @CreateDateColumn({ type: 'timestamp without time zone', default: () => 'NOW()' })
  createdAt?: Date;
  @UpdateDateColumn({
    type: 'timestamp without time zone',
    default: () => 'NOW()',
    onUpdate: 'NOW()',
  })
  updatedAt?: Date;
}
