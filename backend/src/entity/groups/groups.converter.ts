import { Injectable } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { GroupEntity } from './entities/group.entity';
import { GetGroupDto } from './dto/get-group.dto';

@Injectable()
export class GroupsConverter {
  constructor(
    private groupsService: GroupsService
  ) {}

  convert(groupEntity: GroupEntity): GetGroupDto {
    return {
      id: groupEntity.id,
      name: groupEntity.name,
      parentGroupId: groupEntity.parentGroupId,
      createdAt: groupEntity.createdAt,
      updatedAt: groupEntity.updatedAt
    }
  }

  converts(groupEntities: GroupEntity[]): GetGroupDto[] {
    return groupEntities.map(groupEntity => { return this.convert(groupEntity) });
  }
}
