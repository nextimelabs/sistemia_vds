import { forwardRef, Module } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { GroupsController } from './groups.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CameraEntity } from '../cameras/entities/camera.entity';
import { UsersModule } from '../users/users.module';
import { GroupsConverter } from './groups.converter';
import { GroupEntity } from './entities/group.entity';
import { CamerasModule } from '../cameras/cameras.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      GroupEntity
    ]),
    UsersModule
  ],
  controllers: [GroupsController],
  providers: [GroupsService, GroupsConverter],
  exports: [GroupsService, GroupsConverter],
})
export class GroupsModule {}
