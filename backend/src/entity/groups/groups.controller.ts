import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Put,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
  Query, Logger,
} from '@nestjs/common';
import { GroupsService } from './groups.service';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { GetCameraDto } from '../cameras/dto/get-camera.dto';
import { Roles } from '../auth/auth.decorator';
import { RuoloEnum } from '../users/enums/ruolo.enum';
import { AuthGuard } from '../auth/auth.guard';
import { GetGroupDto } from './dto/get-group.dto';
import { GroupsConverter } from './groups.converter';
import { ApiPaginatedResponse } from '../../shared/pagination.decorator';

@ApiTags('Groups')
@Controller('groups')
export class GroupsController {
  constructor(
    private readonly groupsService: GroupsService,
    private readonly groupsConverter: GroupsConverter,
  ) {}

  @Post()
  @ApiBearerAuth()
  @ApiCreatedResponse({
    description: 'Group created succesfull',
    type: GetGroupDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async create(@Body() createGroupDto: CreateGroupDto) {
    try {
      const groupEntity = await this.groupsService.create(createGroupDto);
      return this.groupsConverter.convert(groupEntity);
    } catch (e) {
      if (e.code == 23503)
        throw new HttpException('GROUP_NOT_FOUND', HttpStatus.NOT_FOUND);
      throw new InternalServerErrorException();
    }
  }

  @Get()
  @ApiBearerAuth()
  @ApiPaginatedResponse(GetGroupDto, 'Get paginated groups list')
  @ApiQuery({
    name: 'orderColumn',
    enum: ['id', 'alias', 'groupId', 'createdAt'],
    required: false,
  })
  @ApiQuery({ name: 'orderBy', enum: ['ASC', 'DESC'], required: false })
  @ApiQuery({ name: 'search', type: String, required: false })
  @ApiQuery({ name: 'limit', type: String, required: false })
  @ApiQuery({ name: 'offset', type: String, required: false })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async find(
    @Query('limit') limit = 10,
    @Query('offset') offset = 0,
    @Query('search') search: string,
    @Query('orderColumn') orderColumn: string = 'id',
    @Query('orderBy') orderBy: 'ASC' | 'DESC' = 'DESC',
  ) {
    const res = await this.groupsService.findAll({
      limit,
      offset,
      search,
      orderColumn,
      orderBy,
    });
    return {
      total: res[1],
      limit: limit,
      offset: offset,
      results: await this.groupsConverter.converts(res[0]),
    };
  }

  @Get(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get group',
    type: GetGroupDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async findOne(@Param('id') id: number) {
    const groupEntity = await this.groupsService.findOne(id);
    return this.groupsConverter.convert(groupEntity);
  }

  @Get(':id/child')
  @ApiBearerAuth()
  @ApiPaginatedResponse(GetGroupDto, 'Get child groups')
  @ApiQuery({
    name: 'orderColumn',
    enum: ['id', 'alias', 'groupId', 'createdAt'],
    required: false,
  })
  @ApiQuery({ name: 'orderBy', enum: ['ASC', 'DESC'], required: false })
  @ApiQuery({ name: 'search', type: String, required: false })
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @ApiQuery({ name: 'offset', type: Number, required: false })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async findOneChild(
    @Param('id') id: number,
    @Query('limit') limit = 10,
    @Query('offset') offset = 0,
    @Query('search') search: string,
    @Query('orderColumn') orderColumn: string = 'id',
    @Query('orderBy') orderBy: 'ASC' | 'DESC' = 'DESC',
  ) {
    const res = await this.groupsService.findOneChild(
      {
        limit,
        offset,
        search,
        orderColumn,
        orderBy,
      },
      id,
    );
    return {
      total: res[1],
      limit: limit,
      offset: offset,
      results: await this.groupsConverter.converts(res[0]),
    };
  }

  @Put(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Edit group',
    type: GetGroupDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async update(
    @Param('id') id: number,
    @Body() updateGroupDto: UpdateGroupDto,
  ) {
    try {
      const groupEntity = await this.groupsService.update(id, updateGroupDto);
      return this.groupsConverter.convert(groupEntity);
    } catch (e) {
      if (e.code == 23503)
        throw new HttpException('GROUP_NOT_FOUND', HttpStatus.NOT_FOUND);
      throw e;
    }
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete group',
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async remove(@Param('id') id: number) {
    await this.groupsService.remove(id);
    return null;
  }
}
