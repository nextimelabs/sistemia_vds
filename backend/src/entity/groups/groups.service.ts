import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { CameraEntity } from '../cameras/entities/camera.entity';
import { GroupEntity } from './entities/group.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';

interface ISearchGroups {
  limit: number;
  offset: number;
  search?: string;
  orderColumn?: string;
  orderBy?: 'ASC' | 'DESC';
}
@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(GroupEntity)
    private groupEntityRepository: Repository<GroupEntity>,
  ) {}

  async create(createGroupDto: CreateGroupDto) {
    return await this.groupEntityRepository.save({
      ...createGroupDto,
    });
  }

  async findAll({
    limit,
    offset,
    search,
    orderColumn,
    orderBy,
  }: ISearchGroups): Promise<[GroupEntity[], number]> {
    let queryBuilder = this.groupEntityRepository
      .createQueryBuilder('groups')
      .where('groups.parentGroupId is null');
    if (search)
      queryBuilder = queryBuilder.andWhere('groups.name like :search', {
        search: `%${search}%`,
      });
    return queryBuilder
      .orderBy(`groups.${orderColumn}`, orderBy)
      .limit(limit)
      .offset(offset)
      .getManyAndCount();
  }

  async findOne(id: number) {
    const groupEntity: GroupEntity = await this.groupEntityRepository.findOne(
      id,
    );
    if (!groupEntity) {
      throw new HttpException('GROUP_NOT_FOUND', HttpStatus.NOT_FOUND);
    }
    return groupEntity;
  }

  async findOneChild(
    { limit, offset, search, orderColumn, orderBy }: ISearchGroups,
    groupId: number,
  ): Promise<[GroupEntity[], number]> {
    const groupEntity: GroupEntity = await this.findOne(groupId);
    if (!groupEntity) {
      throw new HttpException('GROUP_NOT_FOUND', HttpStatus.NOT_FOUND);
    }

    let queryBuilder = this.groupEntityRepository.createQueryBuilder('groups');
    queryBuilder = queryBuilder.andWhere('groups.parentGroupId = :groupId', {
      groupId,
    });
    if (search)
      queryBuilder = queryBuilder.andWhere('groups.name like :search', {
        search: `%${search}%`,
      });
    return queryBuilder
      .orderBy(`groups.${orderColumn}`, orderBy)
      .limit(limit)
      .offset(offset)
      .getManyAndCount();
  }

  async update(id: number, updateGroupDto: UpdateGroupDto) {
    if (updateGroupDto.parentGroupId == id) {
      throw new HttpException(null, HttpStatus.BAD_REQUEST);
    }
    const oldGroupEntity = await this.findOne(id);
    return await this.groupEntityRepository.save({
      ...oldGroupEntity,
      ...updateGroupDto,
    });
  }

  async remove(id: number) {
    const groupEntity = await this.findOne(id);
    await this.groupEntityRepository.remove(groupEntity);
    return true;
  }
}
