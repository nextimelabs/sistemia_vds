import { PartialType } from '@nestjs/swagger';
import { CreateRecorderDto } from './create-recorder.dto';

export class UpdateRecorderDto extends PartialType(CreateRecorderDto) {}
