import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsDate,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  ValidateNested,
  IsISO8601,
  MinLength,
  IsNumber,
  IsIn,
} from 'class-validator';
import { Type } from 'class-transformer';
import {
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { StatusEnum } from '../../shared/dto/status.enum';

export class CreateRecorderDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  fps: number;

  @ApiProperty({ enum: [60, 600, 1800, 3600] })
  @IsNumber()
  @IsIn([60, 600, 1800, 3600])
  timeSegmentLimit: 60 | 600 | 1800 | 3600;

  @ApiProperty({ description: 'Max segments size limit in kilobyte' })
  @IsNumber()
  segmentsSizeLimit: number;

  @ApiProperty({
    description: 'Folder to save segments. Without final /',
    default: 'recordings',
  })
  @IsString()
  segmentsFolder = 'recordings';

  @ApiProperty()
  @IsBoolean()
  audio: boolean;

  @ApiProperty()
  @IsBoolean()
  enabled: boolean;
}
