import {ApiProperty} from '@nestjs/swagger';
import {
  IsNotEmpty,
  MaxLength,
  IsNumber, IsEnum, IsBoolean, IsString,
} from 'class-validator';
import { StatusEnum } from '../../shared/dto/status.enum';

export class GetRecorderDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  fps: number;

  @ApiProperty()
  @IsNumber()
  timeSegmentLimit: number;

  @ApiProperty()
  @IsNumber()
  segmentsSizeLimit: number; // Max segments size limit in kilobytes

  @ApiProperty({ description: 'Folder to save segments' })
  @IsString()
  segmentsFolder: string;

  @ApiProperty()
  @IsBoolean()
  audio: boolean;

  @ApiProperty()
  @IsBoolean()
  enabled: boolean;

  @ApiProperty({ enum: StatusEnum })
  @IsNotEmpty()
  status: string;

  @ApiProperty()
  @IsString()
  errorMessage: string | null;
}
