import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsNumber } from 'class-validator';

export class DeleteBulkSegmentDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber({}, {each: true})
  ids: number[];
}
