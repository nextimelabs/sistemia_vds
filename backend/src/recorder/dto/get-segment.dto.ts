import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MaxLength,
  IsNumber, IsEnum, IsBoolean, IsISO8601, IsString,
} from 'class-validator';
import { StatusEnum } from '../../shared/dto/status.enum';

export class GetSegmentDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  fps: number;

  @ApiProperty()
  @IsISO8601()
  startTime: Date;

  @ApiProperty()
  @IsISO8601()
  endTime: Date;

  @ApiProperty()
  @IsNumber()
  duration: number;

  @ApiProperty()
  @IsNumber()
  fileSize: number;

  @ApiProperty()
  @IsString()
  filePath: string;

  @ApiProperty()
  @IsBoolean()
  audio: boolean;

  @ApiProperty()
  @IsNumber()
  cameraId: number;

  @ApiProperty()
  createdAt: Date;
}
