import { Injectable } from '@nestjs/common';
import { RecorderService } from './recorder.service';
import { RecorderOptionsEntity } from './entities/recorder-options.entity';
import { GetRecorderDto } from './dto/get-recorder.dto';
import { StatusEnum } from '../shared/dto/status.enum';

@Injectable()
export class RecorderConverter {
  constructor(private recorderService: RecorderService) {}

  convert(recorderOptionsEntity: RecorderOptionsEntity): GetRecorderDto {
    return {
      fps: recorderOptionsEntity.fps,
      enabled: recorderOptionsEntity.enabled,
      status: Object.keys(StatusEnum).filter((key) => isNaN(Number(key)))[
        recorderOptionsEntity.status
      ],
      errorMessage: recorderOptionsEntity.errorMessage,
      timeSegmentLimit: recorderOptionsEntity.timeSegmentLimit,
      segmentsSizeLimit: recorderOptionsEntity.segmentsSizeLimit,
      segmentsFolder: recorderOptionsEntity.segmentsFolder,
      audio: recorderOptionsEntity.audio,
    };
  }

  converts(recorderOptionsEntities: RecorderOptionsEntity[]): GetRecorderDto[] {
    return recorderOptionsEntities.map((recorderOptionsEntity) => {
      return this.convert(recorderOptionsEntity);
    });
  }
}
