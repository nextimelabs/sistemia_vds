import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { StatusEnum } from '../../shared/dto/status.enum';
import { CameraEntity } from '../../entity/cameras/entities/camera.entity';

@Entity('segments')
export class SegmentsEntity {
  @PrimaryGeneratedColumn()
  id?: number;
  @Column('integer', { nullable: false })
  fps: number;
  @Column('timestamp without time zone', { nullable: false })
  startTime: Date;
  @Column('timestamp without time zone', { nullable: false })
  endTime: Date;
  @Column('integer', { nullable: false })
  duration: number; // Duration of one video file (in seconds)
  @Column('double precision', { nullable: false })
  fileSize: number; // Segment file size (in k)
  @Column('text', { nullable: false })
  filePath: string; // Segment file size (in k)
  @Column('boolean', { nullable: false, default: true })
  audio: boolean;
  @JoinColumn()
  camera?: CameraEntity;
  @Column({ nullable: false })
  cameraId: number;
  @CreateDateColumn({ type: 'timestamp without time zone', default: () => "NOW()" })
  createdAt?: Date;
}
