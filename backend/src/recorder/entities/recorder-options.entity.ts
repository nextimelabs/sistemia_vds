import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { StatusEnum } from '../../shared/dto/status.enum';
import { CameraEntity } from '../../entity/cameras/entities/camera.entity';

@Entity('recorderOptions')
export class RecorderOptionsEntity {
  @Column('integer', { nullable: false, default: 30 })
  fps: number;
  @Column('integer', { nullable: false, default: 600 })
  timeSegmentLimit: number; // Duration of one video file (in seconds)
  @Column('integer', { nullable: true })
  segmentsSizeLimit: number; // Max segments size limit in kilobytes
  @Column('boolean', { nullable: false, default: true })
  audio: boolean;
  @Column('boolean', { nullable: false, default: true })
  enabled: boolean;
  @Column('text', { nullable: false })
  segmentsFolder: string; // Segment folder
  @Column('integer', {nullable: false, default: StatusEnum.Stopped})
  status?: number;
  @Column('varchar', { nullable: true })
  errorMessage?: string;
  @OneToOne((type) => CameraEntity, (camera) => camera.id, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  camera?: CameraEntity;
  @PrimaryColumn()
  cameraId: number;
}
