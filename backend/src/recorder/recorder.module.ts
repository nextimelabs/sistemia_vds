import { forwardRef, Module } from '@nestjs/common';
import { RecorderService } from './recorder.service';
import { CamerasModule } from '../entity/cameras/cameras.module';
import { RecorderController } from './recorder.controller';
import { CameraEntity } from '../entity/cameras/entities/camera.entity';
import { UsersModule } from '../entity/users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RecorderOptionsEntity } from './entities/recorder-options.entity';
import { RecorderConverter } from './recorder.converter';
import { SegmentsService } from './segments.service';
import { SegmentsConverter } from './segments.converter';
import { SegmentsController } from './segments.controller';
import { SegmentsEntity } from './entities/segments.entity';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      CameraEntity,
      RecorderOptionsEntity,
      SegmentsEntity,
    ]),
    UsersModule,
    forwardRef(() => CamerasModule),
    ScheduleModule.forRoot(),
  ],
  controllers: [RecorderController, SegmentsController],
  providers: [
    RecorderService,
    RecorderConverter,
    SegmentsService,
    SegmentsConverter,
  ],
  exports: [
    RecorderService,
    RecorderConverter,
    SegmentsService,
    SegmentsConverter,
  ],
})
export class RecorderModule {}
