import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  InternalServerErrorException,
  Param,
  Post,
  Put,
  Res,
  UseGuards,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { Roles } from '../entity/auth/auth.decorator';
import { AuthGuard } from '../entity/auth/auth.guard';
import { RuoloEnum } from '../entity/users/enums/ruolo.enum';
import { CamerasService } from '../entity/cameras/cameras.service';
import { RecorderService } from './recorder.service';
import { GetRecorderDto } from './dto/get-recorder.dto';
import { CreateRecorderDto } from './dto/create-recorder.dto';
import { RecorderConverter } from './recorder.converter';
import { UpdateRecorderDto } from './dto/update-recorder.dto';
import { GetStreamerDto } from '../streamer/dto/get-streamer.dto';
import fs from 'fs';
import { RecorderOptionsEntity } from './entities/recorder-options.entity';
import { Response } from 'express';
import { StatusEnum } from '../shared/dto/status.enum';

@Controller('recorder')
@ApiTags('Recorder Module')
export class RecorderController {
  constructor(
    private recorderService: RecorderService,
    private camerasService: CamerasService,
    private recorderConverter: RecorderConverter,
  ) {}

  @Post('start')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'All recorder processes started successful',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async startAll() {
    const recorderOptions = await this.recorderService.findAll();
    for (const recorderOption of recorderOptions) {
      if (
        recorderOption.enabled &&
        recorderOption.status === StatusEnum.Stopped
      )
        await this.recorderService.addRecording(recorderOption.camera);
    }
    return null;
  }

  @Post('stop')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'All recorder processes stopped successful',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async stopAll() {
    const recorderOptions = await this.recorderService.findAll();
    for (const recorderOption of recorderOptions) {
      if (
        recorderOption.enabled &&
        recorderOption.status === StatusEnum.Running
      )
        await this.recorderService.removeRecording(recorderOption.cameraId);
    }
    return null;
  }

  @Get(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get recorder process configuration',
    type: GetRecorderDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async get(@Param('id') id: number) {
    return this.recorderConverter.convert(
      await this.recorderService.findOne(id),
    );
  }

  @Post(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Add recorder process configuration',
    type: GetRecorderDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async add(
    @Param('id') id: number,
    @Body() createRecorderDto: CreateRecorderDto,
  ) {
    return this.recorderConverter.convert(
      await this.recorderService.add(id, createRecorderDto),
    );
  }

  @Put(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Edit recorder process configuration',
    type: GetRecorderDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async edit(
    @Param('id') id: number,
    @Body() updateRecorderDto: UpdateRecorderDto,
  ) {
    return this.recorderConverter.convert(
      await this.recorderService.edit(id, updateRecorderDto),
    );
  }

  @Delete(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete recorder process configuration',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async delete(@Param('id') id: number) {
    return await this.recorderService.delete(id);
  }

  @Post(':id/start')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Recorder process started successful',
    type: GetRecorderDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async start(@Param('id') id: number) {
    const camera = await this.camerasService.findOne(id);
    await this.recorderService.addRecording(camera);
    return this.recorderConverter.convert(
      await this.recorderService.findOne(id),
    );
  }

  @Post(':id/stop')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Recorder process stopped successful',
    type: GetRecorderDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async stop(@Param('id') id: number) {
    await this.recorderService.removeRecording(id);
    return this.recorderConverter.convert(
      await this.recorderService.findOne(id),
    );
  }
}
