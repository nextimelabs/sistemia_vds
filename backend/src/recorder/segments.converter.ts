import { Injectable } from '@nestjs/common';
import { GetSegmentDto } from './dto/get-segment.dto';
import { SegmentsEntity } from './entities/segments.entity';

@Injectable()
export class SegmentsConverter {
  constructor() {}

  convert(segmentsEntity: SegmentsEntity): GetSegmentDto {
    return {
      id: segmentsEntity.id,
      fps: segmentsEntity.fps,
      startTime: new Date(segmentsEntity.startTime),
      endTime: new Date(segmentsEntity.endTime),
      duration: segmentsEntity.duration,
      audio: segmentsEntity.audio,
      fileSize: segmentsEntity.fileSize,
      filePath: segmentsEntity.filePath,
      cameraId: segmentsEntity.cameraId,
      createdAt: segmentsEntity.createdAt,
    };
  }

  converts(segmentsEntities: SegmentsEntity[]): GetSegmentDto[] {
    const test: GetSegmentDto[] = [];
    segmentsEntities.map((segmentsEntity) => {
      test.push(this.convert(segmentsEntity));
    });
    return test;
  }
}
