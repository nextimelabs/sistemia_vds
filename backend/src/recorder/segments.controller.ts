import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  InternalServerErrorException,
  Logger,
  Param,
  Query,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Response, Request } from 'express';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { Roles } from '../entity/auth/auth.decorator';
import { RuoloEnum } from '../entity/users/enums/ruolo.enum';
import { AuthGuard } from '../entity/auth/auth.guard';
import { GetSegmentDto } from './dto/get-segment.dto';
import { SegmentsService } from './segments.service';
import { SegmentsConverter } from './segments.converter';
import { CamerasService } from '../entity/cameras/cameras.service';
import { ApiPaginatedResponse } from '../shared/pagination.decorator';
import { GetEventDto } from '../monitor/events/dto/get-event.dto';
import { PaginatedDto } from '../shared/dto/pagination.dto';
import { GetRecorderDto } from './dto/get-recorder.dto';
import { RecorderOptionsEntity } from './entities/recorder-options.entity';
import * as fs from 'fs';
import { DeleteBulkUserDto } from '../entity/users/dto/delete-bulk-user.dto';
import { DeleteBulkSegmentDto } from './dto/delete-bulk-segment.dto';

@Controller('segments')
@ApiTags('Recorder Module')
export class SegmentsController {
  constructor(
    private segmentService: SegmentsService,
    private camerasService: CamerasService,
    private segmentsConverter: SegmentsConverter,
  ) {}

  @Get('download')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiPaginatedResponse(
    GetSegmentDto,
    'Download video recording segments as zip package by query',
  )
  @ApiQuery({
    name: 'orderColumn',
    enum: ['id', 'startTime', 'endTime', 'fileSize', 'cameraId', 'createdAt'],
    required: false,
  })
  @ApiQuery({ name: 'cameraId', type: Number, required: false })
  @ApiQuery({ name: 'orderBy', enum: ['ASC', 'DESC'], required: false })
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @ApiQuery({ name: 'offset', type: Number, required: false })
  @ApiQuery({ name: 'fromTime', type: String, required: false })
  @ApiQuery({ name: 'toTime', type: String, required: false })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  async downloadSegments(
    @Query('cameraId') cameraId: number,
    @Query('fromTime') fromTime: string,
    @Query('toTime') toTime: string,
    @Query('orderColumn') orderColumn = 'id',
    @Query('orderBy') orderBy: 'ASC' | 'DESC' = 'DESC',
    @Res() res: Response,
  ) {
    const [segmentsEntities, count] =
      await this.segmentService.findAllSegmentsByCameraId({
        limit: 999999,
        offset: 0,
        cameraId,
        fromTime: fromTime ? new Date(fromTime) : null,
        toTime: toTime ? new Date(toTime) : null,
        orderColumn,
        orderBy,
      });
    const bufferResponse =
      this.segmentService.downloadSegmentsZip(segmentsEntities);
    //const cameraEntity = await this.camerasService.findOne(cameraId);
    res.writeHead(200, {
      'Content-Disposition': `attachment; filename="recordings_${new Date().toISOString()}.zip"`,
      'Content-Type': 'application/zip',
    });
    return res.end(bufferResponse);
  }

  @Get('')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiPaginatedResponse(GetSegmentDto, 'Get paginated segments list')
  @ApiQuery({
    name: 'orderColumn',
    enum: ['id', 'startTime', 'endTime', 'fileSize', 'cameraId', 'createdAt'],
    required: false,
  })
  @ApiQuery({ name: 'cameraId', type: Number, required: false })
  @ApiQuery({ name: 'orderBy', enum: ['ASC', 'DESC'], required: false })
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @ApiQuery({ name: 'offset', type: Number, required: false })
  @ApiQuery({ name: 'fromTime', type: String, required: false })
  @ApiQuery({ name: 'toTime', type: String, required: false })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async get(
    @Query('limit') limit = 10,
    @Query('offset') offset = 0,
    @Query('cameraId') cameraId: number,
    @Query('fromTime') fromTime: string,
    @Query('toTime') toTime: string,
    @Query('orderColumn') orderColumn: string = 'id',
    @Query('orderBy') orderBy: 'ASC' | 'DESC' = 'DESC',
  ): Promise<PaginatedDto<GetSegmentDto>> {
    const res = await this.segmentService.findAllSegmentsByCameraId({
      limit,
      offset,
      cameraId,
      fromTime: fromTime ? new Date(fromTime) : null,
      toTime: toTime ? new Date(toTime) : null,
      orderColumn,
      orderBy,
    });
    return {
      total: res[1],
      limit: limit,
      offset: offset,
      results: await this.segmentsConverter.converts(res[0]),
    };
  }

  @Get(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get segment info',
    type: GetSegmentDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista, RuoloEnum.Operatore)
  @UseGuards(AuthGuard)
  async getOne(@Param('id') id: number) {
    return this.segmentsConverter.convert(
      await this.segmentService.findOne(id),
    );
  }

  @Delete('bulk')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete segments bulk',
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async removeBulk(@Body() deleteBulkSegmentDto: DeleteBulkSegmentDto) {
    await Promise.all(
      deleteBulkSegmentDto.ids.map(async (id) => {
        await this.segmentService.deleteSegment(id);
      }),
    );
    return null;
  }

  @Delete(':id')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Delete segment by id',
    type: null,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Sistemista)
  @UseGuards(AuthGuard)
  async delete(@Param('id') id: number) {
    return await this.segmentService.deleteSegment(id);
  }
}
