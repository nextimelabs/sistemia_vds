import {
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  Logger,
} from '@nestjs/common';
import { CameraEntity } from '../entity/cameras/entities/camera.entity';
import { CamerasService } from '../entity/cameras/cameras.service';
import { CreateRecorderDto } from './dto/create-recorder.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { RecorderOptionsEntity } from './entities/recorder-options.entity';
import { StatusEnum } from '../shared/dto/status.enum';
import { UpdateRecorderDto } from './dto/update-recorder.dto';
import { OnEvent } from '@nestjs/event-emitter';
import { RTSPRecorder } from './lib/recorder';
import { ProcessEvents } from '../shared/dto/events.enum';
import { SegmentsEntity } from './entities/segments.entity';
import { SegmentsService } from './segments.service';
import * as fs from 'fs';
import * as moment from 'moment';

interface IRecording {
  cameraId: number;
  recorder: RTSPRecorder;
}

@Injectable()
export class RecorderService {
  recorders: IRecording[] = [];
  lastSegmentEntity: Omit<SegmentsEntity, 'fileSize'> = null;

  constructor(
    @Inject(forwardRef(() => CamerasService))
    private camerasService: CamerasService,
    private segmentsService: SegmentsService,
    @InjectRepository(RecorderOptionsEntity)
    private recorderOptionsEntityRepository: Repository<RecorderOptionsEntity>,
  ) {}

  @OnEvent('camera.updated')
  async handleCameraUpdatedEvent(
    oldCameraEntity: CameraEntity,
    cameraEntity: CameraEntity,
  ) {
    if (oldCameraEntity.camSourcePath != cameraEntity.camSourcePath)
      await this.editRecording(cameraEntity);
  }

  async add(idCamera: number, createRecorderDto: CreateRecorderDto) {
    const cameraEntity = await this.camerasService.findOne(idCamera);
    const recorderEntity: RecorderOptionsEntity = {
      fps: createRecorderDto.fps,
      enabled: createRecorderDto.enabled,
      timeSegmentLimit: createRecorderDto.timeSegmentLimit,
      segmentsSizeLimit: createRecorderDto.segmentsSizeLimit,
      segmentsFolder: createRecorderDto.segmentsFolder,
      audio: createRecorderDto.audio,
      cameraId: cameraEntity.id,
      status: StatusEnum.Stopped,
    };
    await this.recorderOptionsEntityRepository.save(recorderEntity);
    await this.addRecording(cameraEntity);
    return this.findOne(idCamera);
  }

  async edit(idCamera: number, updateRecorderDto: UpdateRecorderDto) {
    const oldRecorderOptionsEntity = await this.findOne(idCamera);
    const recorderOptionsEntity: RecorderOptionsEntity = {
      ...oldRecorderOptionsEntity,
      enabled: updateRecorderDto.enabled,
      timeSegmentLimit: updateRecorderDto.timeSegmentLimit,
      segmentsSizeLimit: updateRecorderDto.segmentsSizeLimit,
      segmentsFolder: updateRecorderDto.segmentsFolder,
      audio: updateRecorderDto.audio,
      fps: updateRecorderDto.fps,
    };
    await this.recorderOptionsEntityRepository.save(recorderOptionsEntity);
    if (
      oldRecorderOptionsEntity.timeSegmentLimit !=
        recorderOptionsEntity.timeSegmentLimit ||
      oldRecorderOptionsEntity.segmentsFolder !=
        recorderOptionsEntity.segmentsFolder ||
      oldRecorderOptionsEntity.audio != recorderOptionsEntity.audio ||
      oldRecorderOptionsEntity.fps != recorderOptionsEntity.fps ||
      oldRecorderOptionsEntity.enabled != recorderOptionsEntity.enabled
    )
      await this.editRecording(recorderOptionsEntity.camera);
    return this.findOne(idCamera);
  }

  async delete(idCamera: number) {
    const recorderOptionsEntity = await this.findOne(idCamera);
    await this.recorderOptionsEntityRepository.remove(recorderOptionsEntity);
    await this.removeRecording(idCamera);
    return true;
  }

  async findOne(cameraId: number) {
    const recorderOptionsEntity: RecorderOptionsEntity =
      await this.recorderOptionsEntityRepository.findOne(
        { cameraId: cameraId },
        { relations: ['camera'] },
      );
    if (!recorderOptionsEntity) {
      throw new HttpException(
        'RECORDER_PROCESS_NOT_FOUND',
        HttpStatus.NOT_FOUND,
      );
    }
    return recorderOptionsEntity;
  }

  async findAll() {
    return await this.recorderOptionsEntityRepository.find({
      relations: ['camera'],
    });
  }

  // Memory DB functions
  async addRecording(cameraEntity: CameraEntity) {
    const recorderIndex = this.recorders.findIndex(
      (recorder) => recorder.cameraId == cameraEntity.id,
    );
    if (recorderIndex !== -1)
      throw new HttpException(
        'RECORDER_PROCESS_ALREADY_STARTED',
        HttpStatus.BAD_REQUEST,
      );

    const recorderOptions = await this.findOne(cameraEntity.id);
    if (recorderOptions.enabled === false)
      throw new HttpException(
        'RECORDER_PROCESS_DISABLED',
        HttpStatus.BAD_REQUEST,
      );

    await this.setCameraRecordingStatus(cameraEntity.id, StatusEnum.Starting);
    const newRecording = new RTSPRecorder({
      id: cameraEntity.id,
      name: cameraEntity.alias,
      url: cameraEntity.camSourcePath,
      fps: recorderOptions.fps,
      folder: 'recordings',
      timeLimit: recorderOptions.timeSegmentLimit,
      categoryType: 'video',
      audio: recorderOptions.audio,
    })
      .on(ProcessEvents.STOPPED, async () => {
        await this.setCameraRecordingStatus(
          cameraEntity.id,
          StatusEnum.Stopped,
        );
      })
      .on(ProcessEvents.STARTED, async () => {
        await this.setCameraRecordingStatus(
          cameraEntity.id,
          StatusEnum.Running,
        );
      })
      .on(ProcessEvents.ERROR, async (errorMessage) => {
        await this.removeRecording(cameraEntity.id);
        await this.setCameraRecordingStatus(
          cameraEntity.id,
          StatusEnum.Error,
          errorMessage,
        );
      })
      .on(ProcessEvents.SEGMENT_RECORDED, async (segmentFilePath) => {
        return Promise.all([
          new Promise(async (resolve, reject) => {
            // Save last segment
            await new Promise(async (resolve, reject) => {
              await new Promise((resolve, reject) => {
                setTimeout(() => {
                  resolve(true);
                }, 5000);
              });
              if (!this.lastSegmentEntity) return resolve(true);

              const segmentFile = fs.statSync(this.lastSegmentEntity.filePath);
              const segmentEntity: SegmentsEntity = {
                ...this.lastSegmentEntity,
                fileSize: segmentFile.size / 1000 || 0,
              };
              const segmentCreated = await this.segmentsService.createSegment(
                segmentEntity,
              );
              Logger.verbose(
                `RECORDER SERVICE - Created segment ${segmentCreated.id} - ${segmentCreated.filePath}`,
              );
              resolve(true);
            });

            // Save started segment recording
            await new Promise((resolve, reject) => {
              this.lastSegmentEntity = {
                cameraId: recorderOptions.cameraId,
                audio: recorderOptions.audio,
                duration: recorderOptions.timeSegmentLimit,
                startTime: new Date(),
                filePath: segmentFilePath,
                endTime: moment()
                  .add(recorderOptions.timeSegmentLimit, 'seconds')
                  .toDate(),
                fps: recorderOptions.fps,
              };
              Logger.verbose(
                `RECORDER SERVICE - Starting recording segment ${segmentFilePath}`,
              );
              resolve(true);
            });
            resolve(true);
          }),
          // Clean exceeded segments
          new Promise(async (resolve, reject) => {
            // Check total segments size and remove last segment
            let totalSegmentsSize = await this.segmentsService.getTotalSize(
              recorderOptions.cameraId,
            );
            Logger.verbose(
              `RECORDER SERVICE - Cleaning exceeding segments for camera id ${recorderOptions.cameraId} - Limit: ${recorderOptions.segmentsSizeLimit}, Total Used: ${totalSegmentsSize}`,
            );
            while (totalSegmentsSize > recorderOptions.segmentsSizeLimit) {
              Logger.verbose(
                `RECORDER SERVICE - Excess segments size for ${
                  totalSegmentsSize - recorderOptions.segmentsSizeLimit
                }`,
              );
              const deletedSegment =
                await this.segmentsService.deleteLastSegment(
                  recorderOptions.cameraId,
                );
              Logger.verbose(
                `RECORDER SERVICE - Deleted last segment id: ${deletedSegment.id}`,
              );
              totalSegmentsSize = await this.segmentsService.getTotalSize(
                recorderOptions.cameraId,
              );
            }
            Logger.verbose(`RECORDER SERVICE - Cleaning finished`);
            resolve(true);
          }),
        ]);
      })
      .start();
    this.recorders.push({
      cameraId: cameraEntity.id,
      recorder: newRecording,
    });
  }

  // Return true if stream exists and removed else false if not exists
  async removeRecording(cameraId: number) {
    const recorderIndex = this.recorders.findIndex(
      (recorder) => recorder.cameraId == cameraId,
    );
    if (recorderIndex === -1) return false;
    this.recorders[recorderIndex].recorder.stop();
    this.recorders.splice(recorderIndex, 1);
    await this.setCameraRecordingStatus(cameraId, StatusEnum.Stopping);
    return true;
  }

  async editRecording(cameraEntity: CameraEntity) {
    if (await this.removeRecording(cameraEntity.id)) {
      const streamOptions = await this.findOne(cameraEntity.id);
      if (streamOptions.enabled === true) await this.addRecording(cameraEntity);
    }
  }

  async startAll() {
    const recorderOptions = await this.recorderOptionsEntityRepository
      .createQueryBuilder('recorderOptions')
      .leftJoinAndSelect('recorderOptions.camera', 'camera')
      .where('recorderOptions.enabled = true')
      .getMany();
    recorderOptions.map(async (recorderOption) => {
      await this.addRecording(recorderOption.camera);
    });
  }

  async setCameraRecordingStatus(
    cameraId: number,
    status: StatusEnum,
    errorMessage?: string,
  ): Promise<RecorderOptionsEntity> {
    const recorderOptionsEntity = await this.findOne(cameraId);
    return await this.recorderOptionsEntityRepository.save({
      ...recorderOptionsEntity,
      status,
      errorMessage: status === StatusEnum.Error ? errorMessage : null,
    });
  }
}
