import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { CamerasService } from '../entity/cameras/cameras.service';
import { InjectRepository } from '@nestjs/typeorm';
import { RecorderOptionsEntity } from './entities/recorder-options.entity';
import { Repository } from 'typeorm';
import { SegmentsEntity } from './entities/segments.entity';
import { SegmentsConverter } from './segments.converter';
import * as fs from 'fs';
import * as moment from 'moment';
import * as AdmZip from 'adm-zip';

interface ISearchSegments {
  limit: number;
  offset: number;
  cameraId?: number;
  fromTime?: Date;
  toTime?: Date;
  orderColumn: string;
  orderBy: 'ASC' | 'DESC';
}

@Injectable()
export class SegmentsService {
  constructor(
    @InjectRepository(SegmentsEntity)
    private segmentsEntityRepository: Repository<SegmentsEntity>,
    private segmentsConverter: SegmentsConverter,
    @InjectRepository(RecorderOptionsEntity)
    private recorderOptionsEntityRepository: Repository<RecorderOptionsEntity>,
  ) {}

  async createSegment(segmentsEntity: SegmentsEntity) {
    return this.segmentsConverter.convert(
      await this.segmentsEntityRepository.save(segmentsEntity),
    );
  }


  @Cron(CronExpression.EVERY_10_MINUTES)
  async syncronizeDatbase() {
    Logger.verbose('SYNCING SEGMENTS - SYNCING SEGMENTS IN DB');
    const recorderOptionsEntities = await this.recorderOptionsEntityRepository.find();
    for (const recorderOptionsEntity of recorderOptionsEntities) {
      const folderPath = `${recorderOptionsEntity.segmentsFolder}/${recorderOptionsEntity.cameraId}/video/`;
      //Logger.debug(`SYNCING SEGMENTS - RECORDER OPTIONS PATH FOR CAMERA ${recorderOptionsEntity.cameraId}: ${folderPath}`);
      try {
        if (fs.lstatSync(folderPath).isDirectory()) {
          await Promise.all([
            fs
              .readdirSync(
                `${recorderOptionsEntity.segmentsFolder}/${recorderOptionsEntity.cameraId}/video/`,
              )
              .map(async (filename) => {
                const segmentFilePath = `${recorderOptionsEntity.segmentsFolder}/${recorderOptionsEntity.cameraId}/video/${filename}`;
                //Logger.debug(`SYNCING SEGMENTS - FILE FOUND: ${segmentFilePath}`);
                const segmentFile = fs.statSync(segmentFilePath);
                const segment = await this.findOneByFilepath(segmentFilePath);
                // If segment exists in db, check if file size is updated
                if (segment) {
                  if (segment.fileSize != segmentFile.size / 1000) {
                    Logger.verbose(
                      `SYNCING SEGMENTS - Updating segment ${segment.id}: ${
                        segment.fileSize
                      } -> ${segmentFile.size / 1000}`,
                    );
                    segment.fileSize = segmentFile.size / 1000;
                    await this.updateSegment(segment);
                  }
                } else {
                  // If segment not exists in db, add it
                  const segmentEntity: SegmentsEntity = {
                    cameraId: recorderOptionsEntity.cameraId,
                    audio: recorderOptionsEntity.audio,
                    duration: recorderOptionsEntity.timeSegmentLimit,
                    startTime: segmentFile.ctime,
                    filePath: segmentFilePath,
                    endTime: moment(segmentFile.ctime)
                      .add(recorderOptionsEntity.timeSegmentLimit, 'seconds')
                      .toDate(),
                    fileSize: segmentFile.size / 1000 || 0,
                    fps: recorderOptionsEntity.fps,
                  };
                  const segmentCreated = await this.createSegment(
                    segmentEntity,
                  );
                  Logger.verbose(
                    `SYNCING SEGMENTS - Created segment ${segmentCreated.id} - ${segmentFilePath}`,
                  );
                }
              }),
            new Promise(async (resolve, reject) => {
              // Check total segments size and remove last segment
              let totalSegmentsSize = await this.getTotalSize(
                recorderOptionsEntity.cameraId,
              );
              while (
                totalSegmentsSize > recorderOptionsEntity.segmentsSizeLimit
                ) {
                Logger.verbose(
                  `SYNCING SEGMENTS - Excess segments size for ${
                    totalSegmentsSize - recorderOptionsEntity.segmentsSizeLimit
                  }`,
                );
                const deletedSegment = await this.deleteLastSegment(
                  recorderOptionsEntity.cameraId,
                );
                Logger.verbose(
                  `SYNCING SEGMENTS - Deleted last segment id: ${deletedSegment.id}`,
                );
                totalSegmentsSize = await this.getTotalSize(
                  recorderOptionsEntity.cameraId,
                );
              }
              resolve(true);
            }),
          ]);
        }
      } catch (e) {
      }
    }
  }

  async updateSegment(segmentsEntity: SegmentsEntity) {
    return this.segmentsConverter.convert(
      await this.segmentsEntityRepository.save(segmentsEntity),
    );
  }

  async deleteSegment(segmentId: number) {
    const segmentEntity = await this.findOne(segmentId);
    try {
      fs.unlinkSync(segmentEntity.filePath);
    } catch (e) {
      Logger.debug('ERRORE ELIMINAZIONE FILE', e);
    }
    return this.segmentsEntityRepository.remove(segmentEntity);
  }

  async getTotalSize(cameraId: number): Promise<number> {
    const res = await this.segmentsEntityRepository
      .createQueryBuilder('segments')
      .where('segments.cameraId = :cameraId', { cameraId })
      .select('SUM(segments.fileSize)', 'totalSize')
      .getRawOne();
    return res.totalSize;
  }

  async deleteLastSegment(cameraId: number) {
    const lastSegment = await this.segmentsEntityRepository.findOne(
      { cameraId },
      {
        order: { createdAt: 'ASC' },
      },
    );
    Logger.debug(`Last segment: ${lastSegment.id}`);
    await this.deleteSegment(lastSegment.id);
    Logger.debug(`Segment deleted`);
    return lastSegment;
  }

  async findOne(segmentId: number) {
    return await this.segmentsEntityRepository.findOne({ id: segmentId });
  }

  async findOneByFilepath(filePath: string) {
    return await this.segmentsEntityRepository.findOne({ filePath: filePath });
  }

  async findAllSegmentsByCameraId({
    limit,
    offset,
    cameraId,
    fromTime,
    toTime,
    orderColumn,
    orderBy,
  }: ISearchSegments): Promise<[SegmentsEntity[], number]> {
    let queryBuilder =
      this.segmentsEntityRepository.createQueryBuilder('segments');
    if (cameraId)
      queryBuilder = queryBuilder.andWhere('segments.cameraId = :cameraId', {
        cameraId,
      });

    if (fromTime && !toTime)
      queryBuilder = queryBuilder.andWhere('segments.endTime > :fromTime', {
        fromTime,
      });
    if (!fromTime && toTime)
      queryBuilder = queryBuilder.andWhere('segments.startTime < :toTime', {
        toTime,
      });
    if (fromTime && toTime)
      queryBuilder = queryBuilder.andWhere(
        '(segments.startTime < :fromTime AND segments.endTime > :fromTime OR segments.startTime >= :fromTime AND segments.endTime <= :toTime OR segments.startTime < :toTime AND segments.endTime > :toTime)',
        {
          fromTime,
          toTime,
        },
      );
    return queryBuilder
      .orderBy(`segments.${orderColumn}`, orderBy)
      .limit(limit)
      .offset(offset)
      .getManyAndCount();
  }

  downloadSegmentsZip(segmentsEntities: SegmentsEntity[]): Buffer {
    const zip = new AdmZip();

    for (const segmentEntity of segmentsEntities) {
      zip.addLocalFile(segmentEntity.filePath);
    }
    return zip.toBuffer();
  }
}
