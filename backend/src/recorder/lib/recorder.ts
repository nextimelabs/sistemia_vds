//
//  recorder.js
//  node-rtsp-recorder
//
//  Created by Sahil Chaddha on 24/08/2018.
//

import { FileHandler } from './fileHandler';
import EventEmitter = require('events');
import { ProcessEvents } from '../../shared/dto/events.enum';
import { SegmentsEntity } from '../entities/segments.entity';
import fs from 'fs';
import { Logger } from '@nestjs/common';

const moment = require('moment');
const childProcess = require('child_process');
const path = require('path');

export interface IConfig {
  id: number;
  name: string;
  url: string;
  fps: number;
  audio: boolean;
  timeLimit: number;
  folder: string;
  categoryType?: 'video' | 'audio' | 'image';
  directoryPathFormat?: string;
  fileNameFormat?: string;
}

export class RTSPRecorder extends EventEmitter {
  config;
  id; // Cam id
  name; // Cam name
  url; // Cam url path
  fps; // Cam url path
  audio; // Record audio
  timeLimit; // File recording time limit
  folder; // Folder
  categoryType;
  directoryPathFormat; // Format directory path
  fileNameFormat; // File name format
  disableStreaming; // True if streaming is going off
  timer;
  writeStream;
  fh;

  lastComunicationTime;

  constructor(config: IConfig) {
    super();
    this.config = config;
    this.id = config.id;
    this.name = config.name;
    this.url = config.url;
    this.fps = config.fps || 30;
    this.audio = config.audio || true;
    this.timeLimit = config.timeLimit || 60;
    this.folder = config.folder || 'media/';
    this.categoryType = config.categoryType || 'video';
    this.fileNameFormat = config.fileNameFormat || 'out%Y-%m-%d_%H-%M-%S';
    this.fh = new FileHandler();
    this.fh.createDirIfNotExists(this.getDirectoryPath());
  }

  getDirectoryPath() {
    return path.join(this.folder, this.id.toString(), this.categoryType);
  }

  getFilename(folderPath) {
    return path.join(folderPath, this.fileNameFormat + this.getExtenstion());
  }

  getExtenstion() {
    if (this.categoryType === 'audio') {
      return '.avi';
    }
    if (this.categoryType === 'image') {
      return '.jpg';
    }

    return '.mp4';
  }

  getArguments() {
    if (this.categoryType === 'audio') {
      return ['-vn', '-acodec', 'copy'];
    }
    if (this.categoryType === 'image') {
      return ['-vframes', '1'];
    }
  }

  getChildProcess(fileName) {
    const args = [
      '-rtsp_transport',
      'tcp',
      '-i',
      this.url,
      '-c:v',
      'copy',
      '-c:a',
      'aac',
      '-map',
      '0',
      '-f',
      'segment',
      '-segment_time',
      this.timeLimit,
      '-reset_timestamps',
      '1',
      '-bf',
      '0',
      '-strftime',
      '1',
      '-r',
      this.fps,
      '-t',
      '3600',
      fileName,
    ];
    const child = childProcess.spawn('ffmpeg', args, { detached: false });

    console.log(`pid: ${child.pid}`);
    console.log('ffmpeg ' + args.join(' '));
    return child;
  }

  stop() {
    this.disableStreaming = true;
    this.writeStream.kill();
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }
  }

  start() {
    if (!this.url) {
      return this;
    }
    this.disableStreaming = false;
    this.recordStream();
    return this;
  }

  captureImage(cb) {
    this.writeStream = null;
    const folderPath = this.getDirectoryPath();
    this.fh.createDirIfNotExists(folderPath);
    const fileName = this.getFilename(folderPath);
    this.writeStream = this.getChildProcess(fileName);
    this.writeStream.once('exit', () => {
      if (cb) {
        cb();
      }
    });
  }

  exitsCount = 0;
  retryTimes = 5;
  recordStream() {
    if (this.categoryType === 'image') {
      return;
    }
    const self = this;
    if (this.timer) {
      clearTimeout(this.timer);
    }

    if (this.writeStream && this.writeStream.binded) {
      return false;
    }

    if (this.writeStream && this.writeStream.connected) {
      this.writeStream.binded = true;
      this.writeStream.once('exit', () => {
        self.recordStream();
      });
      return false;
    }

    this.writeStream = null;
    const folderPath = this.getDirectoryPath();
    this.fh.createDirIfNotExists(folderPath);
    const fileName = this.getFilename(folderPath);
    //console.log('ANALYSIS', 'fileName', fileName);
    this.writeStream = this.getChildProcess(fileName);

    this.emit(ProcessEvents.STARTED);

    this.writeStream.once('exit', (data) => {
      //console.log('RECORDER', 'once exit', data);
      if (self.disableStreaming) {
        self.emit(ProcessEvents.STOPPED);
        return this;
      }
      this.exitsCount++;
      // Se il processo viene riavviato piu di retryTimes volte in 5 secondi allora lo blocca
      setTimeout(() => {
        if (this.exitsCount >= this.retryTimes) {
          this.stop();
          self.emit(ProcessEvents.ERROR, 'GENERIC_ERROR');
        } else {
          this.exitsCount = 0;
        }
      }, 5000);
      self.recordStream();
    });
    this.writeStream.stdout.on('error', function (data) {
      console.log('RECORDER', 'stdout on error', data);
    });
    this.writeStream.stderr.on('error', function (data) {
      console.log('RECORDER', 'stderr on error', data);
    });
    this.writeStream.stderr.on('data', function (buffer) {
      const data = buffer.toString();
      //Logger.log(data);
      if (data.includes('Opening') && data.includes('for writing')) {
        self.emit(ProcessEvents.SEGMENT_RECORDED, data.split("Opening '")[1].split("'")[0]);
      } else if (data.includes('401 Unauthorized')) {
        self.stop();
        self.emit(ProcessEvents.ERROR, 'CAM_CONNECTION_UNAUTHORIZED');
      } else if (data.includes('Error')) {
        self.stop();
        self.emit(ProcessEvents.ERROR, 'GENERIC_ERROR');
      } else if (data.includes('Host is unreachable')) {
        self.stop();
        self.emit(ProcessEvents.ERROR, 'CAM_UNREACHABLE');
      } else if (data.includes('Connection refused')) {
        self.stop();
        self.emit(ProcessEvents.ERROR, 'CONNECTION_REFUSED');
      } else if (data.includes('Invalid data found when processing input')) {
        self.stop();
        self.emit(ProcessEvents.ERROR, 'GENERIC_ERROR');
      }
    });
  }
}
