
export enum ProcessEvents {
  STARTED = 'started',
  STOPPED = 'stopped',
  ERROR = 'error',
  SEGMENT_RECORDED = 'segment_recorded',
}
