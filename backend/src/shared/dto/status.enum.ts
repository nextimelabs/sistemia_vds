export enum StatusEnum {
  Error,
  Stopping,
  Stopped,
  Starting,
  Running,
}
