import { ApiOkResponse, getSchemaPath } from '@nestjs/swagger';
import { applyDecorators, Type } from '@nestjs/common';
import { PaginatedDto } from './dto/pagination.dto';

export const ApiPaginatedResponse = <TModel extends Type<any>>(
  model: TModel,
  description: string,
) => {
  return applyDecorators(
    ApiOkResponse({
      schema: {
        description: description,
        title: `PaginatedResponseOf${model.name}`,
        allOf: [
          {
            required: [
              'total',
              'offset',
              'limit',
              'results'
            ],
            properties: {
              total: {
                type: 'number',
                description: 'Total number of results',
              },
              offset: {
                type: 'number',
                description: 'Pagination offset',
              },
              limit: {
                type: 'number',
                description: 'Pagination limit',
              },
              results: {
                type: 'array',
                items: { $ref: getSchemaPath(model) },
              },
            },
          },
        ],
      },
    }),
  );
};
