import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { UsersModule } from './entity/users/users.module';
import { AppModule } from './app.module';
import { LoginUserResponseDto } from './entity/auth/dto/login-user-response.dto';
import { RuoloEnum } from './entity/users/enums/ruolo.enum';
import { StatoEnum } from './entity/users/enums/stato.enum';
import { CreateUserDto } from './entity/users/dto/create-user.dto';
import { UpdateUserDto } from './entity/users/dto/update-user.dto';
import { GetUserDto } from './entity/users/dto/get-user.dto';
import { CreateCameraDto } from './entity/cameras/dto/create-camera.dto';
import { GetCameraDto } from './entity/cameras/dto/get-camera.dto';
import { UpdateCameraDto } from './entity/cameras/dto/update-camera.dto';
import { AuthService } from './entity/auth/auth.service';
import { CreateRecorderDto } from './recorder/dto/create-recorder.dto';
import { StatusEnum } from './shared/dto/status.enum';
import { UpdateRecorderDto } from './recorder/dto/update-recorder.dto';
import { GetRecorderDto } from './recorder/dto/get-recorder.dto';
import { GetStreamerDto } from './streamer/dto/get-streamer.dto';
import { CreateStreamerDto } from './streamer/dto/create-streamer.dto';
import { UpdateStreamerDto } from './streamer/dto/update-streamer.dto';
import { GetGroupDto } from './entity/groups/dto/get-group.dto';
import { CreateGroupDto } from './entity/groups/dto/create-group.dto';
import { UpdateGroupDto } from './entity/groups/dto/update-group.dto';
import { GetAlarmDto } from './monitor/alarms/dto/get-alarm.dto';
import { CreateAlarmDto } from './monitor/alarms/dto/create-alarm.dto';
import { UpdateAlarmDto } from './monitor/alarms/dto/update-alarm.dto';
import { GetTriggerDto } from './monitor/triggers/dto/get-trigger.dto';
import { CreateTriggerDto } from './monitor/triggers/dto/create-trigger.dto';
import {
  ICondition,
  ITriggerEvents,
} from './monitor/interfaces/trigger-events.interface';
import { UpdateTriggerDto } from './monitor/triggers/dto/update-trigger.dto';
import { GetSegmentDto } from './recorder/dto/get-segment.dto';
import { SegmentsEntity } from './recorder/entities/segments.entity';
import { SegmentsService } from './recorder/segments.service';
import { IEvent } from './monitor/events/enums/event-type.enum';
const moment = require('moment');

jest.setTimeout(30000);

describe('API', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication(null, {
      logger: ['error', 'warn'],
    });

    // Create default user
    const authService = app.get(AuthService);
    await authService.addDefaultUser();

    await app.init();
  });
  let adminLoginResponse: LoginUserResponseDto;
  beforeAll(async () => {
    const login = await request(app.getHttpServer()).post('/auth/login').send({
      username: 'admin',
      password: 'admin',
    });
    expect(login.body).toEqual({
      user: {
        id: login.body.user.id,
        username: 'admin',
        role: RuoloEnum.Amministratore,
        status: StatoEnum.Attivo,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      },
      token: login.body.token,
    });
    adminLoginResponse = login.body;
  });

  afterAll(async () => {
    await app.close();
  });

  describe('UsersModule', () => {
    let userAdmin: GetUserDto;
    it('post', async () => {
      const user: CreateUserDto = {
        username: 'sysadmin',
        password: 'pass,123',
        role: RuoloEnum.Amministratore,
        status: StatoEnum.Attivo,
      };
      const data = await request(app.getHttpServer())
        .post('/users')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(user)
        .expect(201);
      expect(data.body).toEqual({
        username: 'sysadmin',
        role: RuoloEnum.Amministratore,
        status: StatoEnum.Attivo,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      userAdmin = data.body;
    });
    it('get all', async () => {
      const data = await request(app.getHttpServer())
        .get('/users')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body.length).toBeGreaterThanOrEqual(1);
    });
    it('get by id', async () => {
      const data = await request(app.getHttpServer())
        .get(`/users/${userAdmin.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual(userAdmin);
    });
    it('get by id 404', async () => {
      const data = await request(app.getHttpServer())
        .get(`/users/${userAdmin.id + 1}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(404);
      expect(data.body).toEqual({
        message: 'USER_NOT_FOUND',
        statusCode: 404,
      });
    });
    it('put', async () => {
      const updateUser: UpdateUserDto = {
        username: 'adminsystem',
        role: RuoloEnum.Amministratore,
        status: StatoEnum.Attivo,
      };
      const data = await request(app.getHttpServer())
        .put(`/users/${userAdmin.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(updateUser)
        .expect(200);
      expect(data.body).toEqual({
        ...userAdmin,
        ...updateUser,
        updatedAt: expect.any(String),
      });
    });

    it('delete', async () => {
      await request(app.getHttpServer())
        .delete(`/users/${userAdmin.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
    it('delete logged user 400', async () => {
      const data = await request(app.getHttpServer())
        .delete(`/users/${adminLoginResponse.user.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(400);
      expect(data.body).toEqual({
        message: 'CANNOT_DELETE_LOGGED_USER',
        statusCode: 400,
      });
    });
  });

  describe('GroupsModule', () => {
    let settoreA: GetGroupDto;
    let cameraSettoreA: GetCameraDto;
    let cameraSettoreB: GetCameraDto;
    let settoreB: GetGroupDto;
    let esternoA1: GetGroupDto;
    let cameraEsternoA1: GetCameraDto;
    let esternoA2: GetGroupDto;
    let cameraEsternoA2_1: GetCameraDto;
    let cameraEsternoA2_2: GetCameraDto;
    let esternoB1: GetGroupDto;
    it('post settoreA', async () => {
      const groupDto: CreateGroupDto = {
        name: 'SETTORE A',
      };
      const data = await request(app.getHttpServer())
        .post('/groups')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(groupDto)
        .expect(201);
      expect(data.body).toEqual({
        ...groupDto,
        parentGroupId: null,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      settoreA = data.body;
    });
    it('post settoreB', async () => {
      const groupDto: CreateGroupDto = {
        name: 'SETTORE B',
      };
      const data = await request(app.getHttpServer())
        .post('/groups')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(groupDto)
        .expect(201);
      expect(data.body).toEqual({
        ...groupDto,
        parentGroupId: null,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      settoreB = data.body;
    });

    it('post esternoA1', async () => {
      const groupDto: CreateGroupDto = {
        name: 'ESTERNO A 1',
        parentGroupId: settoreA.id,
      };
      const data = await request(app.getHttpServer())
        .post('/groups')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(groupDto)
        .expect(201);
      expect(data.body).toEqual({
        ...groupDto,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      esternoA1 = data.body;
    });
    it('post esternoA2', async () => {
      const groupDto: CreateGroupDto = {
        name: 'ESTERNO A 2',
        parentGroupId: settoreA.id,
      };
      const data = await request(app.getHttpServer())
        .post('/groups')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(groupDto)
        .expect(201);
      expect(data.body).toEqual({
        ...groupDto,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      esternoA2 = data.body;
    });
    it('post esternoB1', async () => {
      const groupDto: CreateGroupDto = {
        name: 'ESTERNO B1',
        parentGroupId: settoreB.id,
      };
      const data = await request(app.getHttpServer())
        .post('/groups')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(groupDto)
        .expect(201);
      expect(data.body).toEqual({
        ...groupDto,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      esternoB1 = data.body;
    });
    it('get all', async () => {
      const data = await request(app.getHttpServer())
        .get('/groups')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([
        settoreA,
        settoreB,
        esternoA1,
        esternoA2,
        esternoB1,
      ]);
    });
    it('get all child', async () => {
      const data = await request(app.getHttpServer())
        .get(`/groups/${settoreA.id}/child`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([esternoA1, esternoA2]);
    });
    it('get by id', async () => {
      const data = await request(app.getHttpServer())
        .get(`/groups/${settoreB.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual(settoreB);
    });
    it('get by id 404', async () => {
      const data = await request(app.getHttpServer())
        .get(`/groups/9999`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(404);
      expect(data.body).toEqual({
        message: 'GROUP_NOT_FOUND',
        statusCode: 404,
      });
    });

    // add cameras
    it('post camera in settoreA', async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM SETTORE A',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
        groupId: settoreA.id,
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      const { groupId, ..._camera } = camera;
      expect(data.body).toEqual({
        ..._camera,
        id: expect.any(Number),
        recorderOptions: null,
        streamerOptions: null,
        group: settoreA,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      cameraSettoreA = data.body;
    });
    it('post camera in settoreB', async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM SETTORE B',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
        groupId: settoreB.id,
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      const { groupId, ..._camera } = camera;
      expect(data.body).toEqual({
        ..._camera,
        id: expect.any(Number),
        recorderOptions: null,
        streamerOptions: null,
        group: settoreB,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      cameraSettoreB = data.body;
    });
    it('post camera in esternoA1', async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM ESTERNO A 1',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
        groupId: esternoA1.id,
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      const { groupId, ..._camera } = camera;
      expect(data.body).toEqual({
        ..._camera,
        id: expect.any(Number),
        recorderOptions: null,
        streamerOptions: null,
        group: esternoA1,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      cameraEsternoA1 = data.body;
    });
    it('post camera 1 in esternoA2', async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM ESTERNO A 2',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
        groupId: esternoA2.id,
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      const { groupId, ..._camera } = camera;
      expect(data.body).toEqual({
        ..._camera,
        id: expect.any(Number),
        recorderOptions: null,
        streamerOptions: null,
        group: esternoA2,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      cameraEsternoA2_1 = data.body;
    });
    it('post camera 2 in esternoA2', async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM ESTERNO A 3',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
        groupId: esternoA2.id,
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      const { groupId, ..._camera } = camera;
      expect(data.body).toEqual({
        ..._camera,
        id: expect.any(Number),
        recorderOptions: null,
        streamerOptions: null,
        group: esternoA2,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      cameraEsternoA2_2 = data.body;
    });

    it('get all cameras settoreA', async () => {
      const data = await request(app.getHttpServer())
        .get(`/groups/${settoreA.id}/cameras`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([cameraSettoreA]);
    });
    it('get all cameras settoreB', async () => {
      const data = await request(app.getHttpServer())
        .get(`/groups/${settoreB.id}/cameras`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([cameraSettoreB]);
    });
    it('get all cameras esternoA1', async () => {
      const data = await request(app.getHttpServer())
        .get(`/groups/${esternoA1.id}/cameras`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([cameraEsternoA1]);
    });
    it('get all cameras esternoA2', async () => {
      const data = await request(app.getHttpServer())
        .get(`/groups/${esternoA2.id}/cameras`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual([cameraEsternoA2_1, cameraEsternoA2_2]);
    });

    it('delete all cameras', async () => {
      return await Promise.all(
        [
          cameraSettoreA,
          cameraSettoreB,
          cameraEsternoA1,
          cameraEsternoA2_1,
          cameraEsternoA2_2,
        ].map(async (camera) => {
          return request(app.getHttpServer())
            .delete(`/cameras/${camera.id}`)
            .set('Authorization', `Bearer ${adminLoginResponse.token}`)
            .expect(200);
        }),
      );
    });

    it('put', async () => {
      const updateGroupDto: UpdateGroupDto = {
        name: 'SETTORE B EDIT',
      };
      const data = await request(app.getHttpServer())
        .put(`/groups/${settoreB.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(updateGroupDto)
        .expect(200);
      expect(data.body).toEqual({
        ...settoreB,
        ...updateGroupDto,
        updatedAt: expect.any(String),
      });
    });
    it('delete all', async () => {
      return await Promise.all(
        [settoreA, settoreB].map(async (group) => {
          return await request(app.getHttpServer())
            .delete(`/groups/${group.id}`)
            .set('Authorization', `Bearer ${adminLoginResponse.token}`)
            .expect(200);
        }),
      );
    });
  });

  describe('CamerasModule', () => {
    let addedCamera: GetCameraDto;
    it('post', async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM 1',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      expect(data.body).toEqual({
        ...camera,
        id: expect.any(Number),
        group: null,
        recorderOptions: null,
        streamerOptions: null,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedCamera = data.body;
    });
    it('get all', async () => {
      const data = await request(app.getHttpServer())
        .get('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body.length).toBeGreaterThanOrEqual(1);
    });
    it('get by id', async () => {
      const data = await request(app.getHttpServer())
        .get(`/cameras/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual(addedCamera);
    });

    it('get by id 404', async () => {
      const data = await request(app.getHttpServer())
        .get(`/cameras/${addedCamera.id + 1}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(404);
      expect(data.body).toEqual({
        message: 'CAMERA_NOT_FOUND',
        statusCode: 404,
      });
    });
    it('put', async () => {
      const updateCameraDto: UpdateCameraDto = {
        alias: 'CAM 1 EDIT',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
      };
      const data = await request(app.getHttpServer())
        .put(`/cameras/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(updateCameraDto)
        .expect(200);
      expect(data.body).toEqual({
        ...addedCamera,
        ...updateCameraDto,
        updatedAt: expect.any(String),
      });
    });
    it('delete', async () => {
      await request(app.getHttpServer())
        .delete(`/cameras/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
  });

  describe('RecorderModule', () => {
    let addedCamera: GetCameraDto;
    let addedRecorder: GetRecorderDto;

    beforeAll(async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM 1',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      expect(data.body).toEqual({
        ...camera,
        id: expect.any(Number),
        group: null,
        recorderOptions: null,
        streamerOptions: null,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedCamera = data.body;
    });
    afterAll(async () => {
      await request(app.getHttpServer())
        .delete(`/cameras/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });

    it('post configuration', async () => {
      const recorder: CreateRecorderDto = {
        fps: 30,
        audio: true,
        timeSegmentLimit: 600,
        segmentsSizeLimit: 1000000,
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post(`/recorder/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(recorder)
        .expect(200);
      expect(data.body).toEqual({
        ...recorder,
        status: StatusEnum.starting,
      });
      addedRecorder = data.body;
    });
    it('get camera', async () => {
      const data = await request(app.getHttpServer())
        .get(`/cameras/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual({
        ...addedCamera,
        recorderOptions: {
          ...addedRecorder,
          status: expect.any(String),
        },
      });
    });
    it('post configuration 400 already done', async () => {
      const recorder: CreateRecorderDto = {
        fps: 30,
        audio: true,
        timeSegmentLimit: 600,
        segmentsSizeLimit: 1000000,
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post(`/recorder/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(recorder)
        .expect(400);
      expect(data.body).toEqual({
        message: 'RECORDER_PROCESS_ALREADY_STARTED',
        statusCode: 400,
      });
    });
    it('get configuration', async () => {
      const data = await request(app.getHttpServer())
        .get(`/recorder/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual({
        ...addedRecorder,
        status: StatusEnum.running,
      });
    });
    it('stop recording', async () => {
      const data = await request(app.getHttpServer())
        .post(`/recorder/${addedCamera.id}/stop`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual({
        ...addedRecorder,
        status: StatusEnum.stopping,
      });
    });
    it('start recording', async () => {
      const data = await request(app.getHttpServer())
        .post(`/recorder/${addedCamera.id}/start`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual({
        ...addedRecorder,
        status: StatusEnum.starting,
      });
    });
    // Controllo se lo stato di running viene spento se viene modificato lo stato su disabilitato
    it('put configuration', async () => {
      const recorder: UpdateRecorderDto = {
        fps: 30,
        enabled: false,
      };
      const data = await request(app.getHttpServer())
        .put(`/recorder/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(recorder)
        .expect(200);
      expect(data.body).toEqual({
        ...addedRecorder,
        fps: 30,
        enabled: false,
        status: StatusEnum.stopping,
      });
      addedRecorder = data.body;
    });
    it('get configuration', async () => {
      const data = await request(app.getHttpServer())
        .get(`/recorder/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual(addedRecorder);
    });
    it('start recording 400 disabled', async () => {
      const data = await request(app.getHttpServer())
        .post(`/recorder/${addedCamera.id}/start`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(400);
      expect(data.body).toEqual({
        statusCode: 400,
        message: 'RECORDER_PROCESS_DISABLED',
      });
    });
    it('get configuration', async () => {
      const data = await request(app.getHttpServer())
        .get(`/recorder/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual(addedRecorder);
    });
    it('delete configuration', async () => {
      const data = await request(app.getHttpServer())
        .delete(`/recorder/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });

    describe('Segments', () => {
      let segmentsService: SegmentsService;
      let camera1: GetCameraDto;
      let camera2: GetCameraDto;
      let segment1: GetSegmentDto;
      let segment2: GetSegmentDto;
      let segment3: GetSegmentDto;
      let segment4: GetSegmentDto;
      const nowTimestamp = new Date().toISOString();

      /*
      console.log('NOW', new Date().toISOString());
      console.log(
        '-50',
        moment(nowTimestamp).subtract(50, 'seconds').toISOString(),
      );
      console.log(
        '-20',
        moment(nowTimestamp).subtract(20, 'seconds').toISOString(),
      );
      console.log(
        '-10',
        moment(nowTimestamp).subtract(10, 'seconds').toISOString(),
      );
      console.log(
        '+10',
        moment(nowTimestamp).add(10, 'seconds').toISOString(),
      );
      console.log(
        '+20',
        moment(nowTimestamp).add(20, 'seconds').toISOString(),
      );
      console.log(
        '+25',
        moment(nowTimestamp).add(25, 'seconds').toISOString(),
      );
      console.log(
        '+50',
        moment(nowTimestamp).add(25, 'seconds').toISOString(),
      );
       */

      beforeAll(async () => {
        segmentsService = app.get<SegmentsService>(SegmentsService);
      });
      beforeAll(async () => {
        const camera: CreateCameraDto = {
          alias: 'CAM 1',
          camSourcePath:
            'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
        };
        const data = await request(app.getHttpServer())
          .post('/cameras')
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .send(camera)
          .expect(201);
        expect(data.body).toEqual({
          ...camera,
          id: expect.any(Number),
          group: null,
          recorderOptions: null,
          streamerOptions: null,
          createdAt: expect.any(String), //TEST,
          updatedAt: expect.any(String), //TEST,
        });
        camera1 = data.body;
      });
      beforeAll(async () => {
        const camera: CreateCameraDto = {
          alias: 'CAM 2',
          camSourcePath:
            'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
        };
        const data = await request(app.getHttpServer())
          .post('/cameras')
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .send(camera)
          .expect(201);
        expect(data.body).toEqual({
          ...camera,
          id: expect.any(Number),
          group: null,
          recorderOptions: null,
          streamerOptions: null,
          createdAt: expect.any(String), //TEST,
          updatedAt: expect.any(String), //TEST,
        });
        camera2 = data.body;
      });
      afterAll(async () => {
        await request(app.getHttpServer())
          .delete(`/cameras/${camera1.id}`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
      });
      afterAll(async () => {
        await request(app.getHttpServer())
          .delete(`/cameras/${camera2.id}`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
      });

      it('add segment1', async () => {
        const segmentEntity: SegmentsEntity = {
          cameraId: camera1.id,
          startTime: moment(nowTimestamp).subtract(10, 'seconds').toDate(),
          endTime: moment(nowTimestamp).add(10, 'seconds').toDate(),
          duration: 10,
          fps: 30,
          audio: true,
          fileSize: 3000,
          filePath: 'path',
        };
        const data = await segmentsService.createSegment(segmentEntity);
        expect(data).toEqual({
          ...segmentEntity,
          id: expect.any(Number),
          createdAt: expect.any(Date), //TEST,
        });
        segment1 = JSON.parse(JSON.stringify(data));
      });
      it('add segment2', async () => {
        const segmentEntity: SegmentsEntity = {
          cameraId: camera1.id,
          startTime: moment(nowTimestamp).add(20, 'seconds').toDate(),
          endTime: moment(nowTimestamp).add(40, 'seconds').toDate(),
          duration: 20,
          fps: 30,
          audio: true,
          fileSize: 3000,
          filePath: 'path',
        };
        const data = await segmentsService.createSegment(segmentEntity);
        expect(data).toEqual({
          ...segmentEntity,
          id: expect.any(Number),
          createdAt: expect.any(Date), //TEST,
        });
        segment2 = JSON.parse(JSON.stringify(data));
      });
      it('add segment3', async () => {
        const segmentEntity: SegmentsEntity = {
          cameraId: camera2.id,
          startTime: moment(nowTimestamp).subtract(25, 'seconds').toDate(),
          endTime: moment(nowTimestamp).add(25, 'seconds').toDate(),
          duration: 20,
          fps: 30,
          audio: true,
          fileSize: 3000,
          filePath: 'path',
        };
        const data = await segmentsService.createSegment(segmentEntity);
        expect(data).toEqual({
          ...segmentEntity,
          id: expect.any(Number),
          createdAt: expect.any(Date), //TEST,
        });
        segment3 = JSON.parse(JSON.stringify(data));
      });
      it('add segment4', async () => {
        const segmentEntity: SegmentsEntity = {
          cameraId: camera2.id,
          startTime: moment(nowTimestamp).subtract(30, 'seconds').toDate(),
          endTime: moment(nowTimestamp).subtract(5, 'seconds').toDate(),
          duration: 15,
          fps: 30,
          audio: true,
          fileSize: 3000,
          filePath: 'path',
        };
        const data = await segmentsService.createSegment(segmentEntity);
        expect(data).toEqual({
          ...segmentEntity,
          id: expect.any(Number),
          createdAt: expect.any(Date), //TEST,
        });
        segment4 = JSON.parse(JSON.stringify(data));
      });
      it('get all', async () => {
        const data = await request(app.getHttpServer())
          .get(`/segments`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([segment4, segment3, segment2, segment1]);
      });
      it('get all camera 1', async () => {
        const data = await request(app.getHttpServer())
          .get(`/segments?cameraId=${camera1.id}`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([segment2, segment1]);
      });
      it('get all camera 2', async () => {
        const data = await request(app.getHttpServer())
          .get(`/segments?cameraId=${camera2.id}`)
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([segment4, segment3]);
      });
      it('get all -20 a -10', async () => {
        const data = await request(app.getHttpServer())
          .get(
            `/segments?fromTime=${moment(nowTimestamp)
              .subtract(20, 'seconds')
              .toISOString()}&toTime=${moment(nowTimestamp)
              .subtract(10, 'seconds')
              .toISOString()}`,
          )
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([segment4, segment3]);
      });
      it('get all -10 a +20', async () => {
        console.log(
          `/segments?fromTime=${moment(nowTimestamp)
            .subtract(10, 'seconds')
            .toISOString()}&toTime=${moment(nowTimestamp)
            .add(20, 'seconds')
            .toISOString()}`,
        );
        const data = await request(app.getHttpServer())
          .get(
            `/segments?fromTime=${moment(nowTimestamp)
              .subtract(10, 'seconds')
              .toISOString()}&toTime=${moment(nowTimestamp)
              .add(20, 'seconds')
              .toISOString()}`,
          )
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([segment4, segment3, segment1]);
      });
      it('get all 0 a +50', async () => {
        const data = await request(app.getHttpServer())
          .get(
            `/segments?fromTime=${nowTimestamp}&toTime=${moment(nowTimestamp)
              .add(50, 'seconds')
              .toISOString()}`,
          )
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([segment3, segment2, segment1]);
      });
      it('get all null a +50', async () => {
        const data = await request(app.getHttpServer())
          .get(
            `/segments?toTime=${moment(nowTimestamp)
              .add(50, 'seconds')
              .toISOString()}`,
          )
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([segment4, segment3, segment2, segment1]);
      });
      it('get all -50 a null', async () => {
        const data = await request(app.getHttpServer())
          .get(
            `/segments?fromTime=${moment(nowTimestamp)
              .subtract(50, 'seconds')
              .toISOString()}`,
          )
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([segment4, segment3, segment2, segment1]);
      });
      it('get all 0 a 10', async () => {
        const data = await request(app.getHttpServer())
          .get(
            `/segments?fromTime=${nowTimestamp}&toTime=${moment(nowTimestamp)
              .add(10, 'seconds')
              .toISOString()}`,
          )
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([segment3, segment1]);
      });
      it('get all 20 a 25', async () => {
        const data = await request(app.getHttpServer())
          .get(
            `/segments?fromTime=${moment(nowTimestamp)
              .add(20, 'seconds')
              .toISOString()}&toTime=${moment(nowTimestamp)
              .add(25, 'seconds')
              .toISOString()}`,
          )
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([segment3, segment2]);
      });
      it('get all camera 1 -20 a -10', async () => {
        const data = await request(app.getHttpServer())
          .get(
            `/segments?cameraId=${camera1.id}&fromTime=${moment(nowTimestamp)
              .subtract(20, 'seconds')
              .toISOString()}&toTime=${moment(nowTimestamp)
              .subtract(10, 'seconds')
              .toISOString()}`,
          )
          .set('Authorization', `Bearer ${adminLoginResponse.token}`)
          .expect(200);
        expect(data.body).toEqual([]);
      });

      it('delete all segments', async () => {
        return await Promise.all(
          [segment1, segment2, segment3, segment4].map(async (segment) => {
            return request(app.getHttpServer())
              .delete(`/segments/${segment.id}`)
              .set('Authorization', `Bearer ${adminLoginResponse.token}`)
              .expect(200);
          }),
        );
      });
    });
  });

  describe('StreamerModule', () => {
    let addedCamera: GetCameraDto;
    let addedStreamer: GetStreamerDto;

    beforeAll(async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM 1',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      expect(data.body).toEqual({
        ...camera,
        id: expect.any(Number),
        group: null,
        recorderOptions: null,
        streamerOptions: null,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedCamera = data.body;
    });
    afterAll(async () => {
      await request(app.getHttpServer())
        .delete(`/cameras/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });

    it('post configuration', async () => {
      const streamer: CreateStreamerDto = {
        fps: 30,
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post(`/streamer/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(streamer)
        .expect(200);
      expect(data.body).toEqual({
        ...streamer,
        status: StatusEnum.starting,
        wsPort: expect.any(Number),
      });
      addedStreamer = data.body;
    });
    it('get camera', async () => {
      const data = await request(app.getHttpServer())
        .get(`/cameras/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual({
        ...addedCamera,
        streamerOptions: {
          ...addedStreamer,
          status: expect.any(String),
        },
      });
    });

    it('post configuration 400 already done', async () => {
      const streamer: CreateStreamerDto = {
        fps: 30,
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post(`/streamer/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(streamer)
        .expect(400);
      expect(data.body).toEqual({
        message: 'STREAMER_OPTIONS_ALREADY_EXISTS',
        statusCode: 400,
      });
    });
    it('get configuration', async () => {
      const data = await request(app.getHttpServer())
        .get(`/streamer/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual(addedStreamer);
    });
    it('stop recording', async () => {
      const data = await request(app.getHttpServer())
        .post(`/streamer/${addedCamera.id}/stop`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual({
        ...addedStreamer,
        status: StatusEnum.stopping,
      });
    });
    it('start recording', async () => {
      const data = await request(app.getHttpServer())
        .post(`/streamer/${addedCamera.id}/start`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual({
        ...addedStreamer,
        status: StatusEnum.starting,
      });
    });
    // Controllo se lo stato di running viene spento se viene modificato lo stato su disabilitato
    it('put configuration', async () => {
      const streamer: UpdateStreamerDto = {
        fps: 30,
        enabled: false,
      };
      const data = await request(app.getHttpServer())
        .put(`/streamer/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(streamer)
        .expect(200);
      expect(data.body).toEqual({
        ...streamer,
        status: StatusEnum.stopping,
        wsPort: expect.any(Number),
      });
      addedStreamer = data.body;
    });
    it('get configuration', async () => {
      const data = await request(app.getHttpServer())
        .get(`/streamer/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual({
        ...addedStreamer,
        status: StatusEnum.stopped,
      });
      addedStreamer = data.body;
    });
    it('stop recording', async () => {
      const data = await request(app.getHttpServer())
        .post(`/streamer/${addedCamera.id}/stop`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual({
        ...addedStreamer,
        status: StatusEnum.stopped,
      });
      addedStreamer = data.body;
    });
    it('start recording 400 disabled', async () => {
      const data = await request(app.getHttpServer())
        .post(`/streamer/${addedCamera.id}/start`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(400);
      expect(data.body).toEqual({
        statusCode: 400,
        message: 'STREAMER_PROCESS_DISABLED',
      });
    });
    it('get configuration', async () => {
      const data = await request(app.getHttpServer())
        .get(`/streamer/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual(addedStreamer);
    });
    it('delete configuration', async () => {
      const data = await request(app.getHttpServer())
        .delete(`/streamer/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
  });

  describe('AlarmsModule', () => {
    let addedAlarmSMS: GetAlarmDto;
    let addedAlarmEMAIL: GetAlarmDto;

    it('post sms', async () => {
      const alarmDto: CreateAlarmDto = {
        alarmType: 'SMS',
        configuration: {
          destinationNumber: '+393481633500',
        },
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post('/alarms')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(alarmDto)
        .expect(201);
      expect(data.body).toEqual({
        ...alarmDto,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedAlarmSMS = data.body;
    });
    it('post email', async () => {
      const alarmDto: CreateAlarmDto = {
        alarmType: 'EMAIL',
        configuration: {
          host: 'host.com',
          port: '3456',
          secure: 'false',
          user: 'username',
          password: 'password',

          from: 'mittente@mail.com',
          to: 'destinatario@mail.com',
          subject: 'Allarme',
          text: 'Trigger rilevato ${time}',
          html: 'HTML Trigger rilevato',
        },
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post('/alarms')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(alarmDto)
        .expect(201);
      expect(data.body).toEqual({
        ...alarmDto,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedAlarmEMAIL = data.body;
    });
    it('get all', async () => {
      const data = await request(app.getHttpServer())
        .get('/alarms')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body.length).toBeGreaterThanOrEqual(1);
    });
    it('get by id', async () => {
      const data = await request(app.getHttpServer())
        .get(`/alarms/${addedAlarmSMS.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual(addedAlarmSMS);
    });
    it('get by id 404', async () => {
      const data = await request(app.getHttpServer())
        .get(`/alarms/999`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(404);
      expect(data.body).toEqual({
        message: 'ALARM_NOT_FOUND',
        statusCode: 404,
      });
    });
    it('put', async () => {
      const updateAlarmDto: UpdateAlarmDto = {
        alarmType: 'SMS',
        configuration: {
          destinationNumber: '+393316113684',
        },
        enabled: false,
      };
      const data = await request(app.getHttpServer())
        .put(`/alarms/${addedAlarmSMS.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(updateAlarmDto)
        .expect(200);
      expect(data.body).toEqual({
        ...addedAlarmSMS,
        ...updateAlarmDto,
        updatedAt: expect.any(String),
      });
    });
    it('put bad request', async () => {
      const updateAlarmDto: UpdateAlarmDto = {
        alarmType: 'SMS',
        configuration: {
          number: '+393316113684',
        },
        enabled: false,
      };
      const data = await request(app.getHttpServer())
        .put(`/alarms/${addedAlarmSMS.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(updateAlarmDto)
        .expect(400);
      expect(data.body).toEqual({
        message: 'INVALID_ALARM_CONFIGURATION',
        statusCode: 400,
      });
    });
    it('delete', async () => {
      await request(app.getHttpServer())
        .delete(`/alarms/${addedAlarmSMS.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
    it('delete', async () => {
      await request(app.getHttpServer())
        .delete(`/alarms/${addedAlarmEMAIL.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
  });

  describe('TriggersModule', () => {
    let addedTrigger: GetTriggerDto;

    let addedCamera: GetCameraDto;
    it('create camera', async () => {
      const camera: CreateCameraDto = {
        alias: 'CAM 1',
        camSourcePath:
          'rtsp://freja.hiof.no:1935/rtplive/definst/hessdalen03.stream',
      };
      const data = await request(app.getHttpServer())
        .post('/cameras')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(camera)
        .expect(201);
      expect(data.body).toEqual({
        ...camera,
        id: expect.any(Number),
        group: null,
        recorderOptions: null,
        streamerOptions: null,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedCamera = data.body;
    });

    let addedAlarmSMS: GetAlarmDto;
    let addedAlarmEMAIL: GetAlarmDto;
    it('create alarm sms', async () => {
      const alarmDto: CreateAlarmDto = {
        alarmType: 'SMS',
        configuration: {
          destinationNumber: '+393481633500',
        },
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post('/alarms')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(alarmDto)
        .expect(201);
      expect(data.body).toEqual({
        ...alarmDto,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedAlarmSMS = data.body;
    });
    it('create alarm email', async () => {
      const alarmDto: CreateAlarmDto = {
        alarmType: 'EMAIL',
        configuration: {
          host: 'host.com',
          port: '3456',
          secure: 'false',
          user: 'username',
          password: 'password',

          from: 'mittente@mail.com',
          to: 'destinatario@mail.com',
          subject: 'Allarme',
          text: 'Trigger rilevato ${time}',
          html: 'HTML Trigger rilevato',
        },
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post('/alarms')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(alarmDto)
        .expect(201);
      expect(data.body).toEqual({
        ...alarmDto,
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedAlarmEMAIL = data.body;
    });

    it('post trigger', async () => {
      const condition: ICondition<IEvent> = {
        and: [
          {
            cameraId: addedCamera.id,
            timestamp: new Date().toISOString(),
            eventType: 'MOVIMENTO',
            eventMetadata: [
              {
                key: 'DIREZIONE',
                value: 'SUD',
              },
            ],
          },
          {
            cameraId: addedCamera.id,
            timestamp: new Date().toISOString(),
            eventType: 'MOVIMENTO',
            eventMetadata: [
              {
                key: 'DIREZIONE',
                value: 'NORD',
              },
            ],
          },
          {
            or: [
              {
                cameraId: addedCamera.id,
                timestamp: new Date().toISOString(),
                eventType: 'PASSAGGIO AUTO',
                eventMetadata: [
                  {
                    key: 'VELOCITA',
                    value: '25',
                  },
                ],
              },
              {
                cameraId: addedCamera.id,
                timestamp: new Date().toISOString(),
                eventType: 'PASSAGGIO AUTO',
                eventMetadata: [
                  {
                    key: 'DIREZIONE',
                    value: 'EST',
                  },
                ],
              },
            ],
          },
        ],
      };
      const triggerDto: CreateTriggerDto = {
        alarmsToBeTriggered: [addedAlarmSMS.id, addedAlarmEMAIL.id],
        condition: condition,
        enabled: true,
      };
      const data = await request(app.getHttpServer())
        .post('/triggers')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(triggerDto)
        .expect(201);
      expect(data.body).toEqual({
        ...triggerDto,
        id: expect.any(Number),
        alarmsToBeTriggered: expect.arrayContaining([
          addedAlarmSMS.id,
          addedAlarmEMAIL.id,
        ]),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      });
      addedTrigger = data.body;
    });
    it('get all', async () => {
      const data = await request(app.getHttpServer())
        .get('/triggers')
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body.length).toBeGreaterThanOrEqual(1);
    });
    it('get by id', async () => {
      const data = await request(app.getHttpServer())
        .get(`/triggers/${addedTrigger.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
      expect(data.body).toEqual(addedTrigger);
    });
    it('get by id 404', async () => {
      const data = await request(app.getHttpServer())
        .get(`/triggers/999`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(404);
      expect(data.body).toEqual({
        message: 'TRIGGER_NOT_FOUND',
        statusCode: 404,
      });
    });
    it('put', async () => {
      const updateTriggerDto: UpdateTriggerDto = {
        alarmsToBeTriggered: [addedAlarmSMS.id, addedAlarmEMAIL.id],
        enabled: false,
      };
      const data = await request(app.getHttpServer())
        .put(`/triggers/${addedTrigger.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .send(updateTriggerDto)
        .expect(200);
      expect(data.body).toEqual({
        ...addedTrigger,
        ...updateTriggerDto,
        alarmsToBeTriggered: expect.arrayContaining([
          addedAlarmSMS.id,
          addedAlarmEMAIL.id,
        ]),
        updatedAt: expect.any(String),
      });
    });
    it('delete', async () => {
      await request(app.getHttpServer())
        .delete(`/triggers/${addedTrigger.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
    it('delete camera', async () => {
      await request(app.getHttpServer())
        .delete(`/cameras/${addedCamera.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
    it('delete alarm sms', async () => {
      await request(app.getHttpServer())
        .delete(`/alarms/${addedAlarmSMS.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
    it('delete alarm email', async () => {
      await request(app.getHttpServer())
        .delete(`/alarms/${addedAlarmEMAIL.id}`)
        .set('Authorization', `Bearer ${adminLoginResponse.token}`)
        .expect(200);
    });
  });
});
