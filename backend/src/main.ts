import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AuthService } from './entity/auth/auth.service';
import { StreamerService } from './streamer/streamer.service';
import { RecorderService } from './recorder/recorder.service';
import { RecorderModule } from './recorder/recorder.module';
import { StreamerModule } from './streamer/streamer.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { MonitorModule } from './monitor/monitor.module';
import { MonitorService } from './monitor/monitor.service';
import * as environment from '../environment/environment.json';
declare const module: any;

async function bootstrap() {
  // RestAPI
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();

  // Build Swagger UI
  const config = new DocumentBuilder()
    .setTitle('Sistemia API')
    .setDescription('The sistemia rest api')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  // Kafka
  /*
  const microserviceKafka = app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.KAFKA,
    options: {
      client: {
        ssl: false,
        brokers: [
          `${process.env.KAFKA_IP || environment.KAFKA_IP}:${
            process.env.KAFKA_PORT || environment.KAFKA_PORT
          }`,
        ],
      },
    },
  });
   */

  // Load triggers in memory
  const monitorModule = app.select(MonitorModule);
  const monitorService = monitorModule.get(MonitorService);
  await monitorService.loadTriggersFromDb();

  await app.startAllMicroservicesAsync();
  await app.listen(process.env.PORT || environment.PORT);

  // Create default user
  const authService = app.get(AuthService);
  await authService.addDefaultUser();

  // Start streaming process
  const streamerModule = app.select(StreamerModule);
  const streamerService = streamerModule.get(StreamerService);
  streamerService.startAll();

  // Start recorder process
  const recorderModule = app.select(RecorderModule);
  const recorderService = recorderModule.get(RecorderService);
  recorderService.startAll();

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
