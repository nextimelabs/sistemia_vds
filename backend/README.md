Il backend gestisce la maggior parte degli errori e delle eccezioni che vengono intercettati a runtime.
Quando viene intercettato un errore, il backend genera la seguente risposta sotto forma di json:
{
    "statusCode": STATUS_CODE,
    "message": "ERROR_CODE"
}
dove STATUS_CODE è il codice di stato di risposta HTTP mentre ERROR_CODE è il codice di errore gestito dal backend.
Di seguito la tabella con la lista degli errori gestiti dal backend con la relativa descrizione.

Error codes
TOKEN_NOT_FOUND: Token non trovato nell'header
INVALID_TOKEN: Token non valido
USER_NOT_FOUND: Utente non trovato
CANNOT_DELETE_LOGGED_USER: Impossibile eliminare utente loggato
CANNOT_DELETE_LAST_ADMIN: Impossibile eliminare l'ultimo amministratore
CAMERA_NOT_FOUND: Camera non trovata
AUTHENTICATION_FAILED: Autenticazione fallita
GROUP_NOT_FOUND: Gruppo non trovato
ALARM_NOT_FOUND: Allarme non trovato
ALARM_TYPE_NOT_FOUND: Tipo di allarme non trovato
INVALID_ALARM_CONFIGURATION: Configurazione allarme non valida
STREAMER_OPTIONS_ALREADY_EXISTS: Opzioni streamer gia' inserite
STREAMER_PROCESS_NOT_RUNNING: Processo di streaming non avviato
STREAMER_PROCESS_NOT_FOUND: Processo streamer non trovato
STREAMER_PROCESS_ALREADY_STARTED: Processo streamer gia' in esecuzione
STREAMER_PROCESS_DISABLED: Processo streamer disabilitato
RECORDER_OPTIONS_ALREADY_EXISTS: Opzioni recorder gia' inserite
RECORDER_PROCESS_NOT_FOUND: Processo recorder non trovato
RECORDER_PROCESS_ALREADY_STARTED: Processo recorder gia' in esecuzione
RECORDER_PROCESS_DISABLED: Processo recorder disabilitato

CAM_CONNECTION_UNAUTHORIZED: Connessione alla telecamera non autorizzata (credenziali errate)



# KAFKA COMMANDS
PUBBLICARE UN MESSAGGIO
kafka-console-producer.bat --broker-list localhost:9092 --topic event
ASCOLTARE MESSAGGI
kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic event --group test-consumer --from-beginning
