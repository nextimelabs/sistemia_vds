import esMessages from '../locales/it_IT';

const EsLang = {
  messages: {
    ...esMessages,
  },
  locale: 'it-IT',
};
export default EsLang;
