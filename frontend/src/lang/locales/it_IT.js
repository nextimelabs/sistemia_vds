/* vds Language Texts
*/

module.exports = {
  /* General */
  'general.copyright': 'VDS Sistemia © 2021 All Rights Reserved.',
  'app.name': 'VDS Sistemia',
  'app.slogan': 'Piattaforma AI di Video Sorveglianza in Cloud, sistema di registrazione, controllo e gestione di telecamere.',

  'unauthorized.title': 'Unauthorized Access Attempt',
  'unauthorized.detail':
    'You are not authorized to view the page you are trying to access.',

  /* HOME */
  'app.function-1': 'Gestione telecamere;',
  'app.function-3': 'Visualizzazione flussi entità tramite Heat Map;',
  'app.function-2': 'Analisi flussi entità (Persone e/o Mezzi);',

  'app.plate.detection': "Rilevazione targhe",
  'app.face.detection': "Rilevazione volti",
  'app.track.objects': "Tracciamento oggetti",


  'app.info.plate.detection': "VDS Sistemia, è in grado di rilevare la targa da un'immagine utilizzando tecniche di visione artificiale e quindi utilizzare il riconoscimento ottico dei caratteri per riconoscere il numero di targa.",
  'app.info.face.detection': "VDS Sistemia rileva le persone identificando i volti all'interno dei video. Puoi aggiungere il concetto di machine learning e addestrare i dati allo scopo di riconoscere i volti.",
  'app.info.track.objects': "Il tracciamento degli oggetti di VDS Sistemia, consente di applicare un identificativo a ciascuna entità rilevata tracciandone il movimento. Il tracciamento è fondamentale per costruire un contatore di entità",


  /* ----- ERROR API MESSAGE ----- */
  'TOKEN_NOT_FOUND': 'Token non rilevato',
  'INVALID_TOKEN': 'Token non valido',
  'USER_NOT_FOUND': 'Utente non trovato',
  'CANNOT_DELETE_LOGGED_USER': 'Impossibile eliminare utente loggato',
  'CANNOT_DELETE_LAST_ADMIN': 'Impossibile eliminare l\'ultimo amministratore',
  'CAMERA_NOT_FOUND': 'Camera non trovata',
  'SOURCE_URL_ALREADY_USED': 'Camera con source url già utilizzato',
  'AUTHENTICATION_FAILED': 'Autenticazione fallita',
  'GROUP_NOT_FOUND': 'Gruppo non trovato',
  'ALARM_NOT_FOUND': 'Allarme non trovato',
  'ALARM_TYPE_NOT_FOUND': 'Tipo di allarme non trovato',
  'INVALID_ALARM_CONFIGURATION': 'Configurazione allarme non valida',

  'STREAMER_OPTIONS_ALREADY_EXISTS': 'Opzioni streamer già inserite',
  'STREAMER_PROCESS_NOT_RUNNING': 'Processo di streaming non avviato',
  'STREAMER_PROCESS_NOT_FOUND': 'Processo streamer non trovato',
  'STREAMER_PROCESS_ALREADY_STARTED': 'Processo streamer già in esecuzione',
  'STREAMER_PROCESS_DISABLED': 'Processo streamer disabilitato',

  'RECORDER_OPTIONS_ALREADY_EXISTS': 'Opzioni recorder già inserite',
  'RECORDER_PROCESS_NOT_FOUND': 'Processo recorder non trovato',
  'RECORDER_PROCESS_ALREADY_STARTED': 'Processo recorder già in esecuzione',
  'RECORDER_PROCESS_DISABLED': 'Processo recorder disabilitato',

  'CAM_CONNECTION_UNAUTHORIZED': 'Connessione alla telecamera non autorizzata (credenziali errate)',
  'CONNECTION_REFUSED': 'Connessione rifiutata',
  'GENERIC_ERROR': 'Errore imprevisto non riconosciuto',

  /* ----- MESSAGE ----- */
  'user.creation.title': 'Utente creato',
  'user.creation.message': 'Usente creato con successo',
  'user.deletion.title': 'Utente eliminato',
  'user.deletion.message': 'Usente eliminato con successo',
  'user.updated.title': 'Utente aggiornato',
  'user.updated.message': 'Usente aggiornato con successo',

  'alarm.creation.title': 'Allarme creato',
  'alarm.creation.message': 'Allarme creato con successo',
  'alarm.updated.title': 'Allarme aggiornato',
  'alarm.updated.message': 'Allarme aggiornato con successo',
  'alarm.deletion.title': 'Allarme eliminato',
  'alarm.deletion.message': 'Allarme eliminato con successo',

  'camera.creation.title': 'Telecamera aggiunta',
  'camera.creation.message': 'Telecamera aggiunta con successo',
  'camera.deletion.title': 'Telecamera eliminata',
  'camera.deletion.message': 'Telecamera eliminata con successo',
  'camera.updated.title': 'Telecamera aggiornata',
  'camera.updated.message': 'Telecamera aggiornata con successo',
  'camera.delete': "Elimina telecamere",
  'camera.delete.confirm-message': "Sei sicuro di voler eliminare le telecamere selezionate?",

  'group.creation.title': 'Gruppo creato',
  'group.creation.message': 'Gruppo creato con successo',
  'group.deletion.title': 'Gruppo eliminato',
  'group.deletion.message': 'Gruppo eliminato con successo',


  'group.update.title': 'Gruppo aggiornato',
  'group.update.message': 'Gruppo aggiornato con successo',

  'streaming.update.title': 'Streaming aggiornato',
  'streaming.update.message': 'Impostazioni Streaming aggiornate con successo',

  'recorder.update.title': 'Recorder aggiornato',
  'recorder.update.message': 'Impostazioni di registrazione aggiornate con successo',

  'streaming.activate.title': 'Streaming attivato',
  'streaming.activate.message': 'Streaming attivato con successo',
  'streaming.deactivate.title': 'Streaming disattivato',
  'streaming.deactivate.message': 'Streaming disattivato con successo',

  'recorder.activate.title': 'Recorder attivato',
  'recorder.activate.message': 'Registrazione attivata con successo',
  'recorder.deactivate.title': 'Recorder disattivato',
  'recorder.deactivate.message': 'Registrazione disattivata con successo',

  'video.deletion.title': 'Video eliminato',
  'video.deletion.message': 'Video eliminato con successo',
  'video.delete': "Elimina video",
  'video.delete.confirm-message': "Sei sicuro di voler eliminare il video ?",

  /* ----- COMMON BUTTON ----- */
  'button.confirm': 'Conferma',
  'button.cancel': 'Annulla',
  'button.update': 'Aggiorna',
  'button.delete': 'Elimina',
  'button.new': 'Nuovo',
  'button.search': 'Cerca',
  'button.add': 'Aggiungi',
  'button.signin': 'Accedi',
  'button.start-search': 'Avvia scansione',
  'button.view-all': 'Vedi tutte',
  'button.details': 'Dettagli',
  'button.save': 'Salva',
  'button.activate': 'Attiva',
  'button.deactivate': 'Disattiva',
  'button.actions': 'Azioni',
  'button.edit': 'Modifica',
  'button.signout': 'Esci',

  /* ----- COMMON TEXT ----- */
  'text.orderby': "Ordina per: ",
  'pages.display-options': 'Opzioni di visualizzazione',
  'text.error': 'Errore',
  'text.id': 'Identificativo',
  'text.creation-date': "Data di creazione",
  'text.status': "Stato",
  'text.active': "Attivo",
  'text.disabled': "Disattivato",
  'text.from': "Dal",
  'text.to': "Al",
  'text.order': "Ordine: ",
  'text.yes': "Si",
  'text.no': "No",
  'text.coordinates': "Coordinate",
  'text.latitude': "Latitudine",
  'text.longitude': "Longitudine",
  'text.values_required': 'Inserire correttamente i dati richiesti',
  'text.account': 'Profilo',
  
  /* TIME */
  'time.last.week': 'Ultima settimana',
  'time.last.month': 'Ultimo mese',
  'time.last.year': 'Ultimo anno',


  /* TITLE */
  'title.user.list': 'Utenti',
  'title.camera.list': 'Telecamere',
  'title.camera.management': 'Gestione telecamere',
  'title.alarms.list': 'Allarmi',
  'title.alarms.management': 'Gestione allarmi',
  'title.events.list': 'Eventi',
  'title.camera.map': 'Mappa telecamere',
  'title.camera.group': 'Gruppi telecamere',
  'title.camera.search': 'Ricerca telecamere',
  'title.monitoring.entities': 'Monitoraggio entità',
  'title.plates.detect-list': 'Targhe rilevate',
  'title.plates.faces-list': 'Volti rilevati',
  'title.settings.entities': 'Entità',
  'title.videos.management': 'Gestione video',


  /* Entities */
  'entity.people': 'Persone',
  'entity.people.detect': 'Persone Rilevate',
  'entity.number.people.detect': 'Numero Persone Rilevate',
  'entity.vehicle': 'Veicoli',
  'entity.vehicle.detect': 'Veicoli Rilevati',
  'entity.number.vehicle.detect': 'Numero Veicoli Rilevate',
  'entity.plates.detect': 'Targhe Rilevate',
  'entity.cameras.actived': 'Telecamere Attive',
  'entity.other': 'Altre entità',

  /* FORM MESSAGE */
  'camera.required.name': "Nome telecamera necessario!",
  'camera.required.camSourcePath': "Percorso sorgente necessario!",
  'camera.required.maingroup': "Gruppo principale necessario!",
  'camera.required.childgroup': "Sottogruppo necessario!",
  'group.required.name': "Nome del gruppo necessario!",
  'group.required.childname': "Nome del sottogruppo necessario!",

  /* Login, Logout, Register */
  'user.login-title': 'Login',
  'user.register': 'Register',
  'user.forgot-password': 'Password Dimenticata',
  'user.email': 'E-mail',
  'user.forgot-password-question': 'Hai dimenticato la password?',
  'user.fullname': 'Full Name',
  'user.login-button': 'LOGIN',
  'user.register-button': 'REGISTRATI',
  'user.reset-password-button': 'RESET',
  'auth_title_page': 'Autenticazione',
  'auth_message_page': 'Utilizza le tue credenziali per accedere. Se non sei autorizzato, per favore abbandona la pagina.',
  'forgot.password_title_page': 'Password Dimenticata',
  'forgot.password_message_page': 'Inserisci la tua e-mail per poter resettare la tua password. Se non sei autorizzato, per favore abbandona la pagina.',
  'forgot.password-reset_message_page': 'Reimposta la tua password',

  /* ----- Menu ----- */
  'menu.home': 'Home',
  'menu.app': 'Home',
  'menu.list': 'Lista',
  'menu.details': 'Dettagli',
  'menu.movement-map': 'Mappa spostamenti',
  'menu.alarms': 'Allarmi',
  'menu.events': 'Eventi',
  'menu.cameras-list': 'Lista',
  'menu.account':'Account',
  'menu.vds': "VDS",

  'menu.dashboards': 'Dashboards',
  'menu.dashboard': 'Dashboard',
  'menu.alarms-list': 'Allarmi',
  'menu.ai.module': 'Moduli AI',

  /* Cameras Menu */
  'menu.cameras': 'Telecamere',
  'menu.cameras-map': 'Mappa',
  'menu.cameras-groups': 'Gruppi',
  'menu.cameras-scan': 'Scansione',

  /* Users Menu */
  'menu.users': 'Utenti',

  /* Management Menu */
  'menu.management': 'Gestione',
  'menu.videos': 'Video',
  'menu.video-details': 'Dettagli video',

  /* ----- Monitoring Menu ----- */
  'menu.monitoring': 'Monitoraggio',
  'menu.entities': 'Entità',
  'menu.monitoring.events': 'Eventi',
  'menu.monitoring.alarms': 'Allarmi',
  'menu.plates': 'Targhe',
  'menu.faces': 'Volti',

  /* ----- /Menu ----- */

  /* ----- User Page ----- */
  'new.user': "Nuovo utente",
  'user.delete': "Elimina utente",
  'user.username': "Username",
  'user.password': "Password",
  'update.user': "Dati utente",
  'user.delete.confirm-message': "Sei sicuro di voler eliminare l'utente?",

  'user.id': "ID utente",
  'user.name': "Nome utente",
  'user.role': "Ruolo utente",
  'user.status': "Stato utente",

  /* ----- Camera Page ----- */
  'new.camera': "Nuova telecamera",

  'settings.camera': "Impostazioni telecamera",
  'settings.recorderOptions': "Impostazioni registrazione",
  'settings.streamingOptions': "Impostazioni streaming",
  'settings.fps': "Frequenza dei fotogrammi",
  'settings.timeSegmentLimit': "Limite tempo video registrato",
  'settings.segmentsSizeLimit': "Limite dimensione video registrato",
  'settings.segmentsFolder': "Nome cartella video registrati",
  'settings.audio': "Attiva registrazione audio",
  'settings.enabled-recorder': "Attiva registrazione video",
  'settings.enabled-streaming': "Attiva Video Streaming",

  'camera.name': "Nome telecamera",
  'camera.camSourcePath': "Percorso sorgente telecamera",
  'camera.id': "ID telecamera",
  'camera.name-group': "Nome gruppo telecamera",
  'camera.streaming-status': "Stato dello Streaming",
  'camera.recorder-status': "Stato del Recorder",
  'camera.creation-date': "Data di creazione",
  'cameras.selected.delete': "Elimina telecamere selezionate",

  'camera.streaming-disabled': "Streaming disabilitato",
  'camera.recorder-disabled': "Registrazione disabilitata",
  'camera.status-starting': "in avvio",
  'camera.status-running': "in esecuzione",
  'camera.status-stopping': "in spegnimento",
  'camera.status-stopped': "in stop",
  'camera.status-error': "errore",
  'camera.select': "Seleziona videocamera",


  /* CAMERAS GROUP PAGE */
  'group.id': "ID gruppo",
  'group.name': "Nome gruppo",
  'subgroup.name': "Nome sottogruppo",

  'group.main': "Gruppo padre",
  'group.creation-date': "Data di creazione",
  'new.camera-groups': "Nuovo gruppo",
  'group.delete': "Elimina gruppo",
  'group.delete.confirm-message': "Sei sicuro di voler eliminare il gruppo?",
  'group.data': "Dati gruppo",
  'group.child-data': "Sottogruppi",
  'group.child-select': "Seleziona sottogruppo",
  'group.select': "Seleziona gruppo",

  /* CAMERAS SEARCH PAGE */
  'add.new-cameras': "Aggiungi telecamere",
  'add.new-camera': "Aggiungi telecamera",

  'camera.info': "Informazioni telecamera",
  'camera.onvif.auth': "Accesso alla telecamera",

  'camera.manufacturer': "Produttore",
  'camera.model': "Modello",
  'camera.firmwareVersion': "Versione firmware",
  'camera.serialNumber': "Numero di serie",
  'camera.hardwareId': "Identificativo Hardware",
  'camera.streamUrl': "Url streaming",

  /* CAMERAS ONVIF INFO PAGE */
  'add.camera-system': "Aggiungi telecamera al sistema",

  /* ----- MONITORING PAGE ----- */
  'monitoring.people.detect': 'Persone rilevate',
  'monitoring.number.people.present': 'Numero persone attualmente presenti',
  'monitoring.vehicle.detect': 'Veicoli rilevati',
  'monitoring.number.vehicle.detect': 'Numero veicoli attualmente presenti',
  'monitoring.other.entities': 'Altre entità rilevate',
  'monitoring.number.other.entities': 'Numero altre entità attualmente presenti',
  'monitoring.plate.detect': 'Targhe rilevate',
  'monitoring.faces.detect': 'Volti rilevati',
  'monitoring.date': 'Data rilevazione',

  /* ----- ALARM PAGE ----- */
  'alarm.type': 'Tipo di allarme',
  'alarm.status': 'Stato allarme',
  'alarm.name': 'Nome allarme',
  'alarm.all': 'Tutti gli allarmi',
  'alarm.status-successful': 'Allarmi inviati',
  'alarm.status-error': 'Allarmi non inviati',
  'alarm.date-range': 'Seleziona intervallo di date',
  'alarm.id': 'Identificativo allarme',
  'alarm.creation-date': "Data di creazione",
  'alarm.select': 'Seleziona un allarme',
  'alarm.select.type': 'Seleziona tipo di allarme',
  'alarm.select.configuration': 'Seleziona configurazione',
  'alarm.enabled': 'Abilita allarme',
  'alarm.data': "Dati allarme",
  'alarm.delete': "Elimina allarme",
  'alarm.delete.confirm-message': "Sei sicuro di voler eliminare l'allarme?",

  /* ----- Events PAGE ----- */
  'event.id': "Identificativo evento",
  'event.eventType': "Tipo di evento",
  'event.timestamp': "Timestamp",
  'event.cameraId': "Identificativo telecamera",
  'event.createdAt': "Data di creazione",
  'event.type.select': 'Seleziona tipo di evento',


  /* ----- MANAGEMENT PAGE ----- */
  'settings.alarm.new': "Nuovo allarme",
  'settings.vehicle.detection': "Attiva rilevazione veicoli",
  'settings.people.detection': "Attiva rilevazione persone",
  'settings.maximum.vehicle': "Capienza massima veicoli",
  'settings.maximum.people': "Capienza massima persone",
  'settings.entity.detection': "Rilevazione entità",
  'settings.cameras.module': "Moduli telecamere",
  'settings.streamer.module': "Attiva o disattiva modulo di streaming",
  'settings.recorder.module': "Attiva o disattiva modulo di registrazione",
  'settings.message.module': "Attenzione: il modulo sarà attivo/disattivato su tutte le telecamere",
  'settings.message.saved': "Impostazioni salvate correttamente",
  'settings.title.saved': "Impostazioni salvate",
  'video.audio.true': "Audio",
  'video.audio.false': "Muto",

  /* ----- VIDEO PAGE ----- */
  'video.id': "Identificativo video",
  'video.details': "Dettagli video",
  'video.startTime': "Data inizio registrazione",
  'video.endTime': "Data fine registrazione",
  'video.duration': "Durata",
  'video.fileSize': "Dimensioni",
  'video.filePath': "Percorso",
  'video.audio': "Audio",
  'video.camera': "Telecamera",


  /* Error Page */
  'pages.error-title': 'Ooops... looks like an error occurred!',
  'pages.error-code': 'Error code',
  'pages.go-back-home': 'GO BACK HOME',
};
