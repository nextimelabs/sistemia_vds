/* vds Language Texts
*/

module.exports = {
  /* 01.General */
  'general.copyright': 'VDS sistemia © 2018 All Rights Reserved.',
  'app.name': 'VDS Sistemia',
  'app.slogan': 'AI platform for Cloud Video Surveillance, camera recording, control and management system.',

  'unauthorized.title': 'Unauthorized Access Attempt',
  'unauthorized.detail':
    'You are not authorized to view the page you are trying to access.',

  /* HOME */
  'app.function-1': 'Cameras management;',
  'app.function-3': 'Entity flows visualization through Heat Map;',
  'app.function-2': 'Entity flow analysis (People and / or Means);',
  'app.plate.detection': "Plate detection",
  'app.face.detection': "Face detection",
  'app.track.objects': "Track objects",

  'app.info.plate.detection': "Recognizing the license plate of the car is a very important task for a security system based on video surveillance. VDS Sistemia, is able to detect the license plate from an image using artificial vision techniques and then use optical character recognition to recognize the license plate number.",
  'app.info.face.detection': "VDS Sistemia detects people by identifying faces in videos. You can add the concept of machine learning and train the data for the purpose of recognizing faces.",
  'app.info.track.objects': "The tracking of VDS Sistemia objects allows you to apply an identification to each detected entity by tracing its movement. Tracking is key to building an entity counter",



  /* ----- ERROR API MESSAGE ----- */
  'TOKEN_NOT_FOUND': 'Token not detected',
  'INVALID_TOKEN': 'Invalid Token',
  'USER_NOT_FOUND': 'User not found',
  'CANNOT_DELETE_LOGGED_USER': 'Cannot delete logged in user',
  'CANNOT_DELETE_LAST_ADMIN': 'Could not delete last administrator',
  'CAMERA_NOT_FOUND': 'Camera not found',
  'SOURCE_URL_ALREADY_USED': 'Camera with source url already used',
  'AUTHENTICATION_FAILED': 'Authentication failed',
  'GROUP_NOT_FOUND': 'Group not found',
  'ALARM_NOT_FOUND': 'Alarm not found',
  'ALARM_TYPE_NOT_FOUND': 'Alarm type not found',
  'INVALID_ALARM_CONFIGURATION': 'Invalid alarm configuration',

  'STREAMER_OPTIONS_ALREADY_EXISTS': 'Streamer options already entered',
  'STREAMER_PROCESS_NOT_RUNNING': 'Streaming process not started',
  'STREAMER_PROCESS_NOT_FOUND': 'Streamer process not found',
  'STREAMER_PROCESS_ALREADY_STARTED': 'Streamer process already running',
  'STREAMER_PROCESS_DISABLED': 'Streamer process disabled',

  'RECORDER_OPTIONS_ALREADY_EXISTS': 'Recorder options already entered',
  'RECORDER_PROCESS_NOT_FOUND': 'Recorder process not found',
  'RECORDER_PROCESS_ALREADY_STARTED': 'Recorder process already running',
  'RECORDER_PROCESS_DISABLED': 'Recorder process disabled',

  'CAM_CONNECTION_UNAUTHORIZED': 'Unauthorized camera connection (wrong credentials)',
  'CONNECTION_REFUSED': 'Connection refused',
  'GENERIC_ERROR': 'Unexpected error not recognized',

  /* -----  MESSAGE ----- */
  'user.creation.title': 'User created',
  'user.creation.message': 'User created successfully',
  'user.deletion.title': 'User deleted',
  'user.deletion.message': 'User deleted successfully',
  'user.update.title': 'User updated',
  'user.update.message': 'User updated successfully',

  'alarm.creation.title': 'Alarm created',
  'alarm.creation.message': 'Alarm created successfully',
  'alarm.update.title': 'Alarm updated',
  'alarm.update.message': 'Alarm updated successfully',
  'alarm.deletion.title': 'Alarm deleted',
  'alarm.deletion.message': 'Alarm deleted successfully',

  'camera.creation.title': 'Camera added',
  'camera.creation.message': 'Camera added successfully',
  'camera.deletion.title': 'Camera deleted',
  'camera.deletion.message': 'Camera deleted successfully',
  'camera.update.title': 'Camera updated',
  'camera.update.message': 'Camera updated successfully',

  'camera.delete': "Delete cameras",
  'camera.delete.confirm-message': "Are you sure you want to delete the cameras selected?",

  'group.creation.title': 'Group created',
  'group.creation.message': 'Group created successfully',
  'group.deletion.title': 'Group deleted',
  'group.deletion.message': 'Group deleted successfully',
  'group.update.title': 'Group updated',
  'group.update.message': 'Group updated successfully',

  'streaming.update.title': 'Streaming updated',
  'streaming.update.message': 'Streaming settings updated successfully',

  'recorder.update.title': 'Recorder updated',
  'recorder.update.message': 'Registration settings updated successfully',

  'streaming.activate.title': 'Streaming enabled',
  'streaming.activate.message': 'Streaming enabled successfully',
  'streaming.deactivate.title': 'Streaming disabled',
  'streaming.deactivate.message': 'Streaming disabled successfully',

  'recorder.activate.title': 'Recorder enabled',
  'recorder.activate.message': 'Registration enabled successfully',
  'recorder.deactivate.title': 'Recorder disabled',
  'recorder.deactivate.message': 'Registration disabled successfully',

  'video.deletion.title': 'Video deleted',
  'video.deletion.message': 'Video deleted successfully',
  'video.delete': "Delete video",
  'video.delete.confirm-message': "Are you sure you want to delete the video?",

  /* ----- COMMON BUTTON ----- */
  'button.confirm': 'Confirm',
  'button.cancel': 'Cancel',
  'button.update': 'Update',
  'button.delete': 'Delete',
  'button.new': 'Nuovo',
  'button.search': 'Search',
  'button.add': 'Add',
  'button.signin': 'Sign in',
  'button.start-search': 'Start search',
  'button.view-all': 'Visualizza tutti',
  'button.details': 'Details',
  'button.save': 'Save',
  'button.activate': 'Enable',
  'button.deactivate': 'Disable',
  'button.actions': 'Actions',
  'button.edit': 'Edit',
  'button.signout': 'Sign Out',

  /* ----- COMMON TEXT ----- */
  'text.orderby': "Order By: ",
  'pages.display-options': 'Display Options',
  'text.error': 'Error',
  'text.id': 'Identifier',
  'text.creation-date': "Creation date",
  'text.active': "Active",
  'text.disabled': "Disabled",
  'text.from': "From",
  'text.to': "To",
  'text.order': "Order: ",
  'text.yes': "Yes",
  'text.no': "No",
  'text.coordinates': "Coordinates",
  'text.values_required': 'Enter the required data correctly',
  'text.account': 'Account',

  /* TIME */
  'time.last.week': 'Last week',
  'time.last.month': 'Last month',
  'time.last.year': 'Last year',

  /* TITLE */
  'title.user.list': 'User',
  'title.camera.list': 'Cameras',
  'title.camera.management': 'Cameras management',
  'title.alarms.list': 'Alarms',
  'title.alarms.management': 'Alarms management',
  'title.events.list': 'Event',
  'title.camera.map': 'Cameras Map',
  'title.camera.group': 'Cameras groups',
  'title.camera.search': 'Search cameras',
  'title.monitoring.entities': 'Entity monitoring',
  'title.plates.detect-list': 'Plates detect',
  'title.plates.faces-list': 'Faces detect',
  'title.settings.entities': 'Entities',
  'title.videos.management': 'Videos management',


  /* Entities */
  'entity.people': 'People',
  'entity.people.detect': 'People Detect',
  'entity.number.people.detect': 'Number of people detected',
  'entity.vehicle': 'Vehicles',
  'entity.vehicle.detect': 'Vehicles Detect',
  'entity.number.vehicle.detect': 'Number of vehicle detected',
  'entity.plates.detect': 'Plates Detect',
  'entity.cameras.actived': 'Cameras Detect',
  'entity.other': 'Other entities',

  /* FORM MESSAGE */
  'camera.required.name': "Name is required!",
  'camera.required.camSourcePath': "Source path is required!",
  'camera.required.maingroup': "Main group is required!",
  'camera.required.childgroup': "Child group is required!",
  'group.required.name': "Group name is required!",
  'group.required.childname': "Subgroup name is required!",

  /*  Login, Logout, Register */
  'user.login-title': 'Login',
  'user.register': 'Register',
  'user.forgot-password': 'Forgot Password',
  'user.email': 'E-mail',
  'user.forgot-password-question': 'Forget password?',
  'user.fullname': 'Full Name',
  'user.login-button': 'LOGIN',
  'user.register-button': 'REGISTER',
  'user.reset-password-button': 'RESET',
  'auth_title_page': 'Autenticazione',
  'auth_message_page': 'Use your credentials to log in. If you are not authorized, please leave the page.',
  'forgot.password_title_page': 'Forgot password',
  'forgot.password_message_page': 'Enter your e-mail address to be able to reset your password. If you are not authorized, please leave the page.',
  'forgot.password-reset_message_page': 'Reset your password',

  /* ----- Menu ----- */
  'menu.home': 'Home',
  'menu.app': 'Home',
  'menu.list': 'List',
  'menu.details': 'Details',
  'menu.movement-map': 'Movement map',
  'menu.alarms': 'Alarms',
  'menu.events': 'Events',
  'menu.cameras-list': 'List',

  'menu.dashboards': 'Dashboards',
  'menu.dashboard': 'Dashboard',
  'menu.ai.module': 'AI modules',

  /* Cameras Menu */
  'menu.cameras': 'Cameras',
  'menu.cameras-map': 'Map',
  'menu.cameras-groups': 'Groups',
  'menu.cameras-scan': 'Scan',

  /* Users Menu */
  'menu.users': 'Users',

  /* Management Menu */
  'menu.management': 'Management',
  'menu.videos': 'Videos',
  'menu.video-details': 'Video details',


  /* ----- Monitoring Menu ----- */
  'menu.monitoring': 'Monitoring',
  'menu.monitoring.entity': 'Entities',
  'menu.monitoring.events': 'Events',
  'menu.monitoring.alarms': 'Alarms',
  'menu.plates': 'Plates',
  'menu.faces': 'Faces',

  /* ----- /Menu ----- */

  /* ----- User Page ----- */
  'new.user': "New user",
  'user.delete': "Delete user",
  'user.username': "Username",
  'user.password': "Password",
  'update.user': "User data",
  'user.delete.confirm-message': "Are you sure you want to delete the user?",

  'user.name': "User name",
  'user.id': "User ID",
  'user.role': "User role",
  'user.status': "User status",

  /* ----- Camera Page ----- */
  'new.camera': "New camera",

  'settings.camera': "Camera settings",
  'settings.recorderOptions': "Recorder options",
  'settings.streamingOptions': "Streaming options",
  'settings.fps': "Frame rate",
  'settings.timeSegmentLimit': "Recorded video time limit",
  'settings.segmentsSizeLimit': "Recorded video size limit",
  'settings.segmentsFolder': "Recorded video folder name",
  'settings.audio': "Enable audio recording",
  'settings.enabled-recorder': "Enable video recording",
  'settings.enabled-streaming': "Enable Video Streaming",

  'camera.name': "Camera name",
  'camera.camSourcePath': "Camera source path",
  'camera.id': "Camera ID",
  'camera.name-group': "Camera group name",
  'camera.streaming-status': "Streaming status",
  'camera.recorder-status': "Recorder status",
  'camera.creation-date': "Creation date",
  'cameras.selected.delete': "Delete selected cameras",

  'camera.streaming-disabled': "Streaming disabled",
  'camera.recorder-disabled': "Recorder disabled",
  'camera.status-starting': "in starting",
  'camera.status-running': "in running",
  'camera.status-stopping': "in stopping",
  'camera.status-stopped': "stopped",
  'camera.status-error': "error",
  'camera.select': "Select cameraa",


  /* CAMERAS GROUP PAGE */
  'group.id': "Group ID",
  'group.name': "Group name",
  'subgroup.name': "Subgroup name",
  'group.main': "Main group",
  'group.creation-date': "Creation date",
  'new.camera-groups': "New group",
  'group.delete': "Delete group",
  'group.delete.confirm-message': "Are you sure you want to delete the group?",
  'group.data': "Group data",
  'group.child-data': "Childs group",
  'group.child-select': "Childs group select",
  'group.select': "Group select",

  /* CAMERAS SEARCH PAGE */
  'add.new-cameras': "Add cameras",
  'add.new-camera': "Add camera",
  'camera.info': "Info camera",
  'camera.onvif.auth': "Camera access",

  'camera.manufacturer': "Manufacturer",
  'camera.model': "Model",
  'camera.firmwareVersion': "Firmware version",
  'camera.serialNumber': "Serial number",
  'camera.hardwareId': "Hardware Id",
  'camera.streamUrl': "Stream Url",

  /* CAMERAS ONVIF INFO PAGE */
  'add.camera-system': "Add camera to system",

  /* ----- MONITORING PAGE ----- */
  'monitoring.people.detect': 'People detect',
  'monitoring.number.people.present': 'Number of people currently present',
  'monitoring.vehicle.detect': 'Vehicles detect',
  'monitoring.number.vehicle.detect': 'Number of vehicles currently present',
  'monitoring.other.entities': 'Other entities etect',
  'monitoring.number.other.entities': 'Number of other entities currently present',
  'monitoring.plate.detect': 'Place detect',
  'monitoring.faces.detect': 'Faces detect',
  'monitoring.date': 'Detection date',

  /* ----- ALARM PAGE ----- */
  'alarm.type': 'Alarm type',
  'alarm.status': 'Alarm status',
  'alarm.name': 'Alarm name',
  'alarm.all': 'All alarms',
  'alarm.status-successful': 'Alarms successful',
  'alarm.status-error': 'Alarms error',
  'alarm.date-range': 'Select date range',
  'alarm.id': 'Alarm ID',
  'alarm.creation-date': "Creation date",
  'alarm.select': 'Select an alarm',
  'alarm.select.type': 'Select type of alarm',
  'alarm.select.configuration': 'Select configuration',
  'alarm.enabled': 'Enable alarm',
  'alarm.data': "Alarm data",
  'alarm.delete': "Delete alarm",
  'alarm.delete.confirm-message': "Are you sure you want to delete the alarm?",

  /* ----- Events PAGE ----- */
  'event.id': "Event ID",
  'event.eventType': "Event type",
  'event.timestamp': "Timestamp",
  'event.cameraId': "Camera ID",
  'event.createdAt': "Creation date",
  'event.type.select': 'Select type of event',

  /* ----- SETTINGS PAGE ----- */
  'settings.alarm.new': "New alarm",
  'settings.vehicle.detection': "Enable vehicle detection",
  'settings.vehicle.people': "Enable people detection",
  'settings.maximum.vehicle': "Maximum capacity of vehicle",
  'settings.maximum.people': "Maximum capacity of people",
  'settings.entity.detection': "Entity detection",
  'settings.cameras.module': "Cameras module",
  'settings.streamer.module': "Enable or disable streamer module",
  'settings.recorder.module': "Enable or disable recorder module",
  'settings.message.module': "Attention: the module will be Enable/Disable on all cameras",
  'settings.message.saved': "Settings saved successfully",
  'settings.title.saved': "Saved settings",
  'video.audio.true': "Audio",
  'video.audio.false': "Mute",

  /* ----- VIDEO PAGE ----- */
  'video.id': "Video id",
  'video.details': "Video details",
  'video.startTime': "Registration start date",
  'video.endTime': "Registration end date",
  'video.duration': "Duration",
  'video.fileSize': "File size",
  'video.filePath': "File path",
  'video.audio': "Audio",
  'video.camera': "Camera",

  /* Error Page */
  'pages.error-title': 'Ooops... looks like an error occurred!',
  'pages.error-code': 'Error code',
  'pages.go-back-home': 'GO BACK HOME'
};
