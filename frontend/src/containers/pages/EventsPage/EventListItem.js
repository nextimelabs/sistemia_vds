import React from 'react';
import { Card, CardBody, Badge, CustomInput } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';

const EventListItem = ({ event, handleCheckChange, isSelected }) => {
  return (
    <Colxx xxs="12">
      <Card className="card d-flex mb-3">
        <div className="d-flex flex-grow-1 min-width-zero">
          <CardBody className="align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
            <NavLink
              to="#"
              location={{}}
              id={`toggler${event.id}`}
              className="list-item-heading mb-0 truncate w-40 w-xs-100  mb-1 mt-1"
            >
              <span className="align-middle d-inline-block">{event.eventType}</span>
            </NavLink>
            <p className="mb-1 text-muted text-small w-15 w-xs-100">
              {event.eventType}
            </p>
            <p className="mb-1 text-muted text-small w-15 w-xs-100">
              {event.timestamp}
            </p>
           
          </CardBody>
        </div>

      </Card>
    </Colxx>
  );
};

export default React.memo(EventListItem);
