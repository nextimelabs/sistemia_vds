import React, { Component } from 'react';
import {
  CustomInput,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Label,
  Spinner
} from 'reactstrap';
import Select from 'react-select';
import CustomSelectInput from 'components/common/CustomSelectInput';
import IntlMessages from 'helpers/IntlMessages';
import { OnvifAPI } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

import {
  AvField,
  AvForm,
  AvGroup,
  AvInput,
  AvFeedback,
} from 'availity-reactstrap-validation';

const axios = require('axios');

class CameraOnvifAuthModalView extends Component {


  constructor(props) {
    super(props);

    this.state = {
      currentCameraXaddr: undefined,
      loading: false,

      setInfoCameraOnvif: props.setInfoCameraOnvif,
      toggleModal: props.toggleModal
    }
  }

  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }

  getCameraOnvifInfo = (event, errors, values) => {

    if (errors.length === 0 && event.type === "submit") {

      this.setLoadingStatus()

      const { currentCameraXaddr, setInfoCameraOnvif, toggleModal } = this.state

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      axios.get(
        `${OnvifAPI}/info?xaddr=${currentCameraXaddr}&user=${values.username}&password=${values.password}`,
        config
      ).then(response => {

        if (response.status === 200) {
          setInfoCameraOnvif(response.data)
          toggleModal()
          this.setLoadingStatus();
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus()
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
  }

  render() {

    const { loading, currentCameraXaddr, toggleModal } = this.state

    const { modalOpen, cameraSelectedXaddr } = this.props

    console.log("TEST--cameraSelectedXaddr", cameraSelectedXaddr);

    if (cameraSelectedXaddr !== currentCameraXaddr
      && cameraSelectedXaddr !== -1) {
      this.setState({
        currentCameraXaddr: cameraSelectedXaddr
      })
    }


    return (
      <Modal
        isOpen={modalOpen}
        toggle={() => toggleModal(!modalOpen)}
      >
        <AvForm
          className="av-tooltip tooltip-label-right"
          onSubmit={(event, errors, values) => this.getCameraOnvifInfo(event, errors, values)}
        >
          <ModalHeader>
            <IntlMessages id="camera.onvif.auth" />
          </ModalHeader>
          <ModalBody>



            <AvGroup >
              <Label> <IntlMessages id="user.username" /></Label>
              <AvInput name="username" required />
              <AvFeedback>Username is required!</AvFeedback>
            </AvGroup>

            <AvGroup >
              <Label> <IntlMessages id="user.password" /></Label>
              <AvInput name="password" type="password" />
              <AvFeedback>Password is required!</AvFeedback>
            </AvGroup>




          </ModalBody>
          <ModalFooter>
            <Button color="secondary" outline onClick={toggleModal}>
              <IntlMessages id="button.cancel" />
            </Button>

            {loading ?
              <Spinner type="grow" color="primary" />
              :
              <Button color="primary" type="Submit">
                <IntlMessages id="button.signin" />
              </Button>
            }

          </ModalFooter>
        </AvForm>

      </Modal>
    );
  }
};

export default CameraOnvifAuthModalView;
