import React from 'react';
import { Card, CustomInput, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import { adminRoot } from 'constants/defaultValues'

const CameraSearchListSingleView = ({ camera, isSelect, collect }) => {
  console.log("TEST--camera", camera);

  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={camera.name} collect={collect}>
        <NavLink
          to={{
            pathname: `${adminRoot}/cameras/cameras-details`,
            xaddr: camera.xaddr,
          }}
          exact
        >
          <Card
            className={classnames('d-flex flex-row')}
          >
            <div className="pl-2 d-flex flex-grow-1 min-width-zero">
              <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
                <p className="list-item-heading mb-1 truncate">
                  {camera.name}
                </p>
                <p className="mb-1 text-muted text-small w-15 w-sm-100">
                  {camera.urn}
                </p>
                <p className="mb-1 text-muted text-small w-15 w-sm-100">
                  {camera.xaddr}
                </p>
                <div className="w-15 w-sm-100">
                  <Button color="primary" outline >
                    <IntlMessages id="button.add" />
                  </Button>{' '}
                </div>
              </div>

            </div>
          </Card>
        </NavLink>

      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(CameraSearchListSingleView);
