import React from 'react';
import { Row } from 'reactstrap';
import Pagination from '../Pagination';
import ContextMenuContainer from '../ContextMenuContainer';
import CameraSearchListSingleView from './CameraSearchListSingleView';

function collect(props) {
  return { data: props.data };
}


const CameraSearchListView = ({
  items,
  currentPage,
  totalPage,
  onChangePage,
}) => {

  return (
    <Row>
      {items.map((camera) => {
        return (
          <CameraSearchListSingleView
            key={camera.id}
            camera={camera}
            collect={collect}
          />
        );
      })}
      <Pagination
        currentPage={currentPage}
        totalPage={totalPage}
        onChangePage={(i) => onChangePage(i)}
      />
    </Row>
  );
};

export default React.memo(CameraSearchListView);
