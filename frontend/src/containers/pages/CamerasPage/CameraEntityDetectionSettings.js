import React, { useState } from 'react';
import {
  Row,
  Card,
  Collapse,
  CardBody,
  CardFooter,
  Button,
  Label,
  CardTitle
} from 'reactstrap';
import { injectIntl } from 'react-intl';
import IntlMessages from 'helpers/IntlMessages';
import Switch from 'rc-switch';
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation';
import { Separator, Colxx } from 'components/common/CustomBootstrap';

const CameraEntityDetectionSettings = ({ match, intl }) => {
  const [enableVehicleDetect, setEnableVehicleDetect] = useState(false);
  const [enablePeopleDetect, setEnablePeopleDetect] = useState(false);

  const { messages } = intl;

  const onSubmit = (event, errors, values) => {
    console.log(errors);
    console.log(values);
    if (errors.length === 0) {
      // submit
    }
  };


  return (
    <Card>
      <CardBody>

        <CardTitle>
          <IntlMessages id="settings.entity.detection" />
        </CardTitle>

        <AvForm
          className="av-tooltip tooltip-label-right"
          onSubmit={(event, errors, values) => onSubmit(event, errors, values)}
        >

          <Row>
            <Colxx xxs="12" xl="12">
              <Card className="d-flex flex-row mb-3">
                <Colxx xxs="12" xl="12">
                  <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
                    <p className="list-item-heading mb-1 truncate">
                      <IntlMessages id="settings.vehicle.detection" />
                    </p>
                    <div className="w-15 w-sm-100">
                      <Switch
                        className="custom-switch custom-switch-primary"
                        checked={enableVehicleDetect}
                        onChange={(primary) => setEnableVehicleDetect(primary)}
                      />
                    </div>
                  </div>

                  <Collapse isOpen={enableVehicleDetect}>
                    <CardBody>



                      <Colxx xxs="12" xl="6" className="col-left">
                        <AvGroup className="error-l-100 error-t-negative">
                          <Label><IntlMessages id="settings.maximum.vehicle" /></Label>
                          <AvField
                            name="numberProp"
                            type="number"
                            validate={{
                              number: {
                                value: true,
                                errorMessage: 'Value must be a number',
                              },
                              required: {
                                value: true,
                                errorMessage: 'Please enter a number',
                              },
                            }}
                          />
                        </AvGroup>
                      </Colxx>

                    </CardBody>
                  </Collapse>
                </Colxx>
              </Card>
            </Colxx>
          </Row >


          <Row>
            <Colxx xxs="12" xl="12">
              <Card className="d-flex flex-row mb-3">
                <Colxx xxs="12" xl="12">
                  <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
                    <p className="list-item-heading mb-1 truncate">
                      <IntlMessages id="settings.people.detection" />
                    </p>
                    <div className="w-15 w-sm-100">
                      <Switch
                        className="custom-switch custom-switch-primary"
                        checked={enablePeopleDetect}
                        onChange={(primary) => setEnablePeopleDetect(primary)}
                      />
                    </div>
                  </div>

                  <Collapse isOpen={enablePeopleDetect}>
                    <CardBody>



                      <Colxx xxs="12" xl="6" className="col-left">
                        <AvGroup className="error-l-100 error-t-negative">
                          <Label><IntlMessages id="settings.maximum.people" /></Label>
                          <AvField
                            name="numberProp"
                            type="number"
                            validate={{
                              number: {
                                value: true,
                                errorMessage: 'Value must be a number',
                              },
                              required: {
                                value: true,
                                errorMessage: 'Please enter a number',
                              },
                            }}
                          />
                        </AvGroup>
                      </Colxx>

                    </CardBody>
                  </Collapse>
                </Colxx>
              </Card>
            </Colxx>
          </Row >
        </AvForm>
      </CardBody>

          <CardFooter>
        <Row className="align-items-center justify-content-center">
           
            <Colxx xxs="12" xl="12" className="text-center text-md-right">
               
                  <Button
                    color="primary"
                    size="lg"
                    className="top-right-button"
                  >
                    <IntlMessages id="button.save" />
                  </Button>
              
            </Colxx>
        </Row >
      </CardFooter>

    </Card>
  );
};
export default injectIntl(CameraEntityDetectionSettings);
