import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  Spinner
} from 'reactstrap';


import {
  AvField,
  AvForm,
  AvGroup,
  AvInput,
  AvFeedback,
} from 'availity-reactstrap-validation';

import { FPSOptions, TimeSegmentLimitOptions } from 'constants/defaultValues'
import { SliderMegaByteyteTooltips } from 'components/common/SliderMegaByteyteTooltips';
import Switch from 'rc-switch';
import IntlMessages from 'helpers/IntlMessages';
import { CameraRoot, GroupsCameraRoot, StreamerAPI, RecorderAPI } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

const axios = require('axios');


class UpdateCameraModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentCameraID: undefined,
      loading: false,

      /* Camera Settings */
      isAudioEnabled: false,
      isRecorderEnabled: false,
      isStreamingEnabled: false,
      segmentsSizeLimit: 0,

      /* Camera info */
      infoCamera: null,
      MainGroupList: [],
      ChildGroupList: []
    }

    this.getMainGroupList()
  }

  setLoadingStatus = (state) => {
    this.setState({
      loading: state
    })
  }

  setAudioSetting = (status) => {
    this.setState({
      isAudioEnabled: status
    })
  }

  setRecorderSetting = (status) => {
    this.setState({
      isRecorderEnabled: status
    })
  }

  setStreamingSetting = (status) => {
    this.setState({
      isStreamingEnabled: status
    })
  }

  setSegmentsSizeLimit = (value) => {
    this.setState({
      segmentsSizeLimit: value * 1024
    })
  }

  getCameraInfo = (cameraSelectedID) => {
    this.setState({
      currentCameraID: cameraSelectedID
    }, () => {

      this.setLoadingStatus(true)

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      axios.get(
        `${CameraRoot}/${cameraSelectedID}`,
        config
      ).then(response => {

        if (response.status === 200) {

          this.setState({
            infoCamera: response.data,
            isRecorderEnabled: response.data.recorderOptions ? response.data.recorderOptions.audio : false,
            isAudioEnabled: response.data.recorderOptions ? response.data.recorderOptions.audio : false,
            isStreamingEnabled: response.data.streamerOptions ? response.data.streamerOptions.enabled : false,
            segmentsSizeLimit: response.data.recorderOptions ? response.data.recorderOptions.segmentsSizeLimit : 0,
          }, () => {
            this.getGroupChild(response.data.group.id)
            this.setLoadingStatus(false);
          })
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus(false)
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });

    })

  }

  getMainGroupList = () => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.get(
      `${GroupsCameraRoot}?search=`,
      config
    ).then(response => {

      if (response.status === 200) {

        this.setState({
          MainGroupList: response.data.results,
        })
      }
    }
    )
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  getGroupChild = (groupId) => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.get(
      `${GroupsCameraRoot}/${groupId}/child?id=${groupId}&search=`,
      config
    ).then(response => {

      if (response.status === 200) {
        this.setState({
          ChildGroupList: response.data.results,
        })
      }
    }
    )
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }


  /* API SALVA IMPOSTAZIONI TELECAMERA */
  updateCameraSettings = (event, errors, values) => {

    const { currentCameraID, isStreamingEnabled, isRecorderEnabled } = this.state

    if (errors.length === 0 && event.type === "submit") {

      this.setLoadingStatus(true)

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      const bodyParameters = {
        "alias": values.inputAlias,
        "camSourcePath": values.inputCamSourcePath,
        "groupId": Number(values.inputGroupId),
        "latitude": values.inputLatitude,
        "longitude": values.inputLongitude
      };

      axios.put(
        `${CameraRoot}/${currentCameraID}?id=${currentCameraID}`,
        bodyParameters,
        config
      ).then(response => {

        if (response.status === 200) {

          this.setLoadingStatus(false);

          NotificationManager.success(
            <IntlMessages id="camera.updated.message" />,
            <IntlMessages id="camera.updated.title" />,
            5000,
            () => {
              window.location.reload()
            },
            null,
            "mb-3"
          );

          if (isStreamingEnabled) this.updateStreamerModule(values)
          if (isRecorderEnabled) this.updateRecorderModule(values)

        }
      })
        .catch(err => {
          this.setLoadingStatus(false)
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
  }

  updateStreamerModule = (values) => {

    const { currentCameraID, isStreamingEnabled } = this.state

    this.setLoadingStatus(true);


    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    const bodyParameters = isStreamingEnabled ? {
      "fps": Number(values.inputStreamerFTP),
      "enabled": isStreamingEnabled,
    }
      :
      {
        "enabled": isStreamingEnabled,
      };

    axios.post(
      `${StreamerAPI}/${currentCameraID}?id=${currentCameraID}`,
      bodyParameters,
      config
    ).then(response => {

      if (response.status === 200) {
        this.setLoadingStatus(false);

        NotificationManager.success(
          <IntlMessages id="streaming.update.message" />,
          <IntlMessages id="streaming.update.title" />,
          5000,
          () => {
            window.location.reload()
          },
          null,
          "mb-3"
        );
      }
    })
      .catch(err => {
        this.setLoadingStatus(false)
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  updateRecorderModule = (values) => {

    const { currentCameraID, isRecorderEnabled, isAudioEnabled, segmentsSizeLimit } = this.state

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    const bodyParameters = isRecorderEnabled ? {
      "fps": Number(values.inputRecorderFPS),
      "timeSegmentLimit": Number(values.inputTimeSegmentLimit),
      "segmentsSizeLimit": Number(segmentsSizeLimit),
      "segmentsFolder": values.inputSegmentsFolder,
      "audio": isAudioEnabled,
      "enabled": isRecorderEnabled,
    }
      :
      {
        "enabled": isRecorderEnabled,
      };

    axios.put(
      `${RecorderAPI}/${currentCameraID}?id=${currentCameraID}`,
      bodyParameters,
      config
    ).then(response => {

      if (response.status === 200) {
        this.setLoadingStatus(false);

        NotificationManager.success(
          <IntlMessages id="recorder.update.message" />,
          <IntlMessages id="recorder.update.title" />,
          5000,
          () => {
            window.location.reload()
          },
          null,
          "mb-3"
        );
      }
    }
    )
      .catch(err => {
        this.setLoadingStatus(false)
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  render() {

    const { loading, currentCameraID, infoCamera,
      MainGroupList, ChildGroupList, isAudioEnabled,
      isRecorderEnabled, isStreamingEnabled, segmentsSizeLimit
    } = this.state


    const { modalOpen, toggleModal, cameraSelectedID } = this.props

    if (cameraSelectedID !== currentCameraID && cameraSelectedID !== -1) {
      this.getCameraInfo(cameraSelectedID)
    }

    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
        wrapClassName="modal-right"
        backdrop="static"
      >

        {infoCamera ?

          <AvForm
            className="av-tooltip tooltip-label-right"
            onSubmit={(event, errors, values) => this.updateCameraSettings(event, errors, values)}
          >

            <ModalHeader toggle={toggleModal}>
              <IntlMessages id="settings.camera" />
            </ModalHeader>

            <ModalBody>

              <AvGroup >
                <Label> <IntlMessages id="camera.name" /></Label>
                <AvInput name="inputAlias" required value={infoCamera.alias || ""} />
                <AvFeedback>name is required!</AvFeedback>
              </AvGroup>

              <AvGroup >
                <Label> <IntlMessages id="camera.camSourcePath" /></Label>
                <AvInput name="inputCamSourcePath" required value={infoCamera.camSourcePath || ""} />
                <AvFeedback>source path is required!</AvFeedback>
              </AvGroup>

              <AvField
                type="select"
                name="inputGroupId"
                required
                label={<IntlMessages id="group.select" />}
                errorMessage="Please select an option!"
                value={infoCamera.group ? infoCamera.group.id || "" : ""}
                onChange={e => this.getGroupChild(e.target.value)}
              >
                {
                  MainGroupList.map(item => {
                    return (
                      <option key={item.id} value={item.id}>{item.name}</option>
                    )
                  })
                }
              </AvField>

              {ChildGroupList.length !== 0 ?
                <AvField
                  type="select"
                  name="inputCameraChildGroup"
                  required
                  label={<IntlMessages id="group.child-select" />}
                  errorMessage="Please select an option!"
                  value={infoCamera.group ? infoCamera.group.id || "" : ""}
                  onChange={e => this.getGroupChild(e.target.value)}
                >
                  {
                    ChildGroupList.map(item => {
                      return (
                        <option key={item.id} value={item.id}>{item.name}</option>
                      )
                    })
                  }
                </AvField>
                :
                null
              }

              <AvGroup >
                <Label> <IntlMessages id="text.latitude" /></Label>
                <AvInput
                  name="inputLatitude"
                  value={infoCamera.latitude || ""}
                />
              </AvGroup>

              <AvGroup >
                <Label> <IntlMessages id="text.longitude" /></Label>
                <AvInput
                  name="inputLongitude"
                  value={infoCamera.longitude || ""}
                />
              </AvGroup>

              <AvGroup>
                <Label style={{ marginTop: '21px' }}>
                  <IntlMessages id="settings.enabled-streaming" />
                </Label>
                <Switch
                  className="custom-switch custom-switch-primary"
                  checked={isStreamingEnabled}
                  onChange={(primary) => this.setStreamingSetting(primary)}
                />
              </AvGroup>

              {isStreamingEnabled ?
                <>
                  <h6 style={{ marginBottom: '21px', marginTop: '34px' }}> <IntlMessages id="settings.streamingOptions" /></h6>

                  <AvGroup >
                    <AvField
                      type="select"
                      name="inputStreamerFTP"
                      required
                      label={<IntlMessages id="settings.fps" />}
                      errorMessage="Please select an option!"
                      value={infoCamera.streamerOptions ? infoCamera.streamerOptions.fps : ""}
                    >
                      {
                        FPSOptions.map(item => {
                          return (
                            <option key={item.key} value={item.value}>{item.label}</option>
                          )
                        })
                      }
                    </AvField>
                  </AvGroup>
                </>
                :
                null
              }

              <AvGroup>
                <Label style={{ marginTop: '21px' }}>
                  <IntlMessages id="settings.enabled-recorder" />
                </Label>
                <Switch
                  className="custom-switch custom-switch-primary"
                  checked={isRecorderEnabled}
                  onChange={(primary) => this.setRecorderSetting(primary)}
                />
              </AvGroup>

              {isRecorderEnabled ?
                <>
                  <h6 style={{ marginBottom: '21px', marginTop: '34px' }}> <IntlMessages id="settings.recorderOptions" /></h6>

                  <AvGroup >
                    <Label> <IntlMessages id="settings.segmentsFolder" /></Label>
                    <AvInput name="inputSegmentsFolder" required value={infoCamera.recorderOptions ? infoCamera.recorderOptions.segmentsFolder || "" : ""} />
                    <AvFeedback>folder name is required!</AvFeedback>
                  </AvGroup>

                  <AvGroup >
                    <AvField
                      type="select"
                      name="inputRecorderFPS"
                      required
                      label={<IntlMessages id="settings.fps" />}
                      errorMessage="Please select an option!"
                      value={infoCamera.recorderOptions ? infoCamera.recorderOptions.fps || "" : ""}
                    >
                      {
                        FPSOptions.map(item => {
                          return (
                            <option key={item.key} value={item.value}>{item.label}</option>
                          )
                        })
                      }
                    </AvField>
                  </AvGroup>

                  <AvGroup >
                    <AvField
                      type="select"
                      name="inputTimeSegmentLimit"
                      required
                      label={<IntlMessages id="settings.timeSegmentLimit" />}
                      errorMessage="Please select an option!"
                      value={infoCamera.recorderOptions ? infoCamera.recorderOptions.timeSegmentLimit || "" : ""}
                      onChange={e => this.getGroupChild(e.target.value)}
                    >
                      {
                        TimeSegmentLimitOptions.map(item => {
                          return (
                            <option key={item.key} value={item.value}>{item.label}</option>
                          )
                        })
                      }
                    </AvField>
                  </AvGroup>

                  <AvGroup>
                    <Label>
                      <IntlMessages id="settings.segmentsSizeLimit" />
                    </Label>
                    <SliderMegaByteyteTooltips
                      min={0}
                      max={2500}
                      defaultValue={segmentsSizeLimit / 1024 || 0}
                      className="mb-5"
                      step={1}
                      onChange={(e) => this.setSegmentsSizeLimit(e)}
                    />
                  </AvGroup>

                  <AvGroup>
                    <Label style={{ marginTop: '21px' }}>
                      <IntlMessages id="settings.audio" />
                    </Label>
                    <Switch
                      className="custom-switch custom-switch-primary"
                      checked={isAudioEnabled}
                      onChange={(primary) => this.setAudioSetting(primary)}
                    />
                  </AvGroup>
                </>
                :
                null
              }

            </ModalBody>

            <ModalFooter>
              <Button color="secondary" outline onClick={toggleModal}>
                <IntlMessages id="button.cancel" />
              </Button>

              {loading ?
                <Spinner type="grow" color="primary" />
                :
                <Button color="primary" type="Submit">
                  <IntlMessages id="button.update" />
                </Button>
              }

            </ModalFooter>
          </AvForm>

          :
          <Spinner type="grow" color="primary" />
        }
      </Modal>
    );
  }
};

export default UpdateCameraModal;
