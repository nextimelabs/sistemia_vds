import React from 'react';
import {
  Row,
  Card,
  CardBody,
  CardSubtitle,
  CardText,
  Badge,
  Button
} from 'reactstrap';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import { getBadgeCameraStatus } from 'constants/defaultValues';
import IntlMessages from 'helpers/IntlMessages';
import StreamVideo from './StreamVideo';


const CameraPreView = ({ camera, collect }) => {


  function statusTranslate(status) {
    switch (status) {
      case 'Starting':
        return <IntlMessages id="camera.status-starting" />;
      case 'Running':
        return <IntlMessages id="camera.status-running" />;
      case 'Stopping':
        return <IntlMessages id="camera.status-stopping" />;
      case 'Stopped':
        return <IntlMessages id="camera.status-stopped" />;
      case 'Error':
        return <IntlMessages id="camera.status-error" />;
      default:
        return '';
    }
  }

  return (
    <>
      <Colxx sm="6" lg="4" xl="3" className="mb-3" key={camera.id}>
        <ContextMenuTrigger id="menu_id" data={camera.id} collect={collect}>
          <Card>
            <div className="position-relative">
              {camera.streamerOptions ?
                <>
               
                  <StreamVideo wsPort={camera.streamerOptions.wsPort} />

                  <Badge pill color={getBadgeCameraStatus(camera.streamerOptions.status)}
                    className="position-absolute badge-top-left"
                  >
                    {statusTranslate(camera.streamerOptions.status)}
                  </Badge>
                </>
                :
                <>
                  <canvas style={{ width: '100%' }} ></canvas>
                  <Badge pill color="dark"
                    className="position-absolute badge-center"
                  >
                    <IntlMessages id="camera.streaming-disabled" />
                  </Badge>
                </>
              }
            </div>

            <CardBody>

              <Row>
                <Colxx xxs="12" className="mb-3">
                  <CardSubtitle>   {camera.alias ? camera.alias : ""}</CardSubtitle>
                  <CardText className="text-muted text-small mb-0 font-weight-light">
                    {camera.group ? camera.group.name : ""}
                  </CardText>
                </Colxx>
              </Row>


            </CardBody>
          </Card>
        </ContextMenuTrigger>
      </Colxx>
    </>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(CameraPreView);

