import React from 'react';
import { FullScreen, useFullScreenHandle } from "react-full-screen";

import JSMpeg from '@cycjimmy/jsmpeg-player';

function StreamVideo(props) {

  const handle = useFullScreenHandle();
  const { wsPort } = props

  const canvas = document.getElementById(`canvas-${wsPort}`);
  const url = `ws://localhost:${wsPort}`
  const player = new JSMpeg.Player(url, { canvas: canvas });

  return (<>
    <FullScreen handle={handle}>
      <canvas
        onClick={handle.enter}
        style={{ width: '100%' }}
        id={`canvas-${wsPort}`}
      />
    </FullScreen>
  </>
  )
}

export default StreamVideo;
