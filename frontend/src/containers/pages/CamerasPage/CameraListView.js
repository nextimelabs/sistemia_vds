import React from 'react';
import { Row } from 'reactstrap';
import Pagination from '../Pagination';
import ContextMenuContainer from '../ContextMenuContainer';
import CameraListSingleView from './CameraListSingleView';
import CameraListSinglePreView from './CameraListSinglePreView';

function collect(props) {
  return { data: props.data };
}

const UserListView = ({
  items,
  displayMode,
  onCheckItem,
  currentPage,
  totalPage,
  onContextMenuClick,
  onContextMenu,
  onChangePage,
  toggleUpdateCameraModal,
  toggleDeleteCameraModal,
  toggleSetCameraID,
  selectedItems
}) => {

  return (
    <Row>
      {items.map((camera) => {
        if (displayMode === 'imagelist') {
          return (
            <CameraListSinglePreView
              key={camera.id}
              camera={camera}
              collect={collect}
            />
          );
        }

        return (
          <CameraListSingleView
            key={camera.id}
            camera={camera}
            toggleUpdateCameraModal={toggleUpdateCameraModal}
            toggleDeleteCameraModal={toggleDeleteCameraModal}
            toggleSetCameraID={toggleSetCameraID}
            collect={collect}
            onCheckItem={onCheckItem}
            isSelect={selectedItems.includes(camera.id)}
          />
        );
      })}
      <Pagination
        currentPage={currentPage}
        totalPage={totalPage}
        onChangePage={(i) => onChangePage(i)}
      />
      <ContextMenuContainer
        onContextMenuClick={onContextMenuClick}
        onContextMenu={onContextMenu}
      />
    </Row>
  );
};

export default React.memo(UserListView);

