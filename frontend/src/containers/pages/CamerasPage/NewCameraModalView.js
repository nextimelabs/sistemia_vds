import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Spinner,
  Label,
} from 'reactstrap';

import {
  AvField,
  AvForm,
  AvGroup,
  AvInput,
  AvFeedback,
} from 'availity-reactstrap-validation';

import IntlMessages from 'helpers/IntlMessages';
import { CameraRoot, GroupsCameraRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';
import axios from 'axios';

class NewCameraModalView extends Component {


  constructor(props) {
    super(props);

    this.state = {
      loading: false,

      /* Camera data */
      MainGroupList: [],
      ChildGroupList: [],
      groupIDSelected: -1
    }

    this.getMainGroupList()
  }

  setLoadingStatus = (state) => {
    this.setState({
      loading: state
    })
  }

  setGroupIDSelected = (groupID) => {
    this.setState({
      groupIDSelected: groupID
    })
  }

  getMainGroupList = () => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.get(
      `${GroupsCameraRoot}?search=`,
      config
    ).then((res) => {
      return res.data;
    })
      .then((data) => {
        this.setState({
          MainGroupList: data.results,
        })
      }
      )
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  getGroupChild = (groupId) => {

    this.setState({
      ChildGroupList: [],
    }, () => {
      this.setGroupIDSelected(groupId)
    })

    if (groupId !== "-1") {

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      axios.get(
        `${GroupsCameraRoot}/${groupId}/child?id=${groupId}&search=`,
        config
      ).then((res) => {
        return res.data;
      })
        .then((data) => {

          this.setState({
            ChildGroupList: data.results,
          })
        }
        )
        .catch(err => {
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
  }


  addNewCamera = (event, errors, values) => {

    const { groupIDSelected } = this.state

    if (errors.length === 0 && event.type === "submit") {

      this.setLoadingStatus(true)

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      const bodyParameters = {
        "alias": values.inputAlias,
        "camSourcePath": values.inputCamSourcePath,
        "groupId": Number(groupIDSelected),
        "latitude": Number(values.inputLatitude),
        "longitude": Number(values.inputLongitude)
      };

      axios.post(
        CameraRoot,
        bodyParameters,
        config
      ).then(response => {

        if (response.status === 201) {

          this.setLoadingStatus(false);

          NotificationManager.success(
            <IntlMessages id="camera.creation.message" />,
            <IntlMessages id="camera.creation.title" />,
            5000,
            () => {
              alert('callback');
            },
            null,
            "mb-3"
          );
          window.location.reload();
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus(false)
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
  }

  render() {

    const { loading,
      MainGroupList, ChildGroupList
    } = this.state

    const { modalOpen, toggleModal } = this.props

    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
        wrapClassName="modal-right"
        backdrop="static"
      >

        <AvForm
          className="av-tooltip tooltip-label-right"
          onSubmit={(event, errors, values) => this.addNewCamera(event, errors, values)}
        >

          <ModalHeader >
            <IntlMessages id="add.new-camera" />
          </ModalHeader>

          <ModalBody>

            <AvGroup >
              <Label> <IntlMessages id="camera.name" /></Label>
              <AvInput name="inputAlias" required />
              <AvFeedback><IntlMessages id="camera.required.name" /></AvFeedback>
            </AvGroup>

            <AvGroup >
              <Label> <IntlMessages id="camera.camSourcePath" /></Label>
              <AvInput name="inputCamSourcePath" required />
              <AvFeedback><IntlMessages id="camera.required.camSourcePath" /></AvFeedback>
            </AvGroup>

            <AvField
              type="select"
              name="inputGroupId"
              required
              label={<IntlMessages id="group.select" />}
              errorMessage={<IntlMessages id="group.select" />}
              onChange={e => this.getGroupChild(e.target.value)}
            >
              <option key={-1} value={-1}></option>
              {
                MainGroupList.map(item => {
                  return (
                    <option key={item.id} value={item.id}>{item.name}</option>
                  )
                })
              }
            </AvField>


            {ChildGroupList.length !== 0 &&
              <AvField
                type="select"
                name="inputCameraChildGroup"
                label={<IntlMessages id="group.child-select" />}
                errorMessage={<IntlMessages id="group.child-select" />}
                onChange={e => this.setGroupIDSelected(e.target.value)}
              >
                <option key={-1} value={-1}></option>

                {
                  ChildGroupList.map(item => {
                    return (
                      <option key={item.id} value={item.id}>{item.name}</option>
                    )
                  })
                }
              </AvField>}

            <h6 style={{ marginBottom: '21px', marginTop: '34px' }}> <IntlMessages id="text.coordinates" /></h6>

            <AvGroup >
              <Label> <IntlMessages id="text.latitude" /></Label>
              <AvInput name="inputLatitude" />
            </AvGroup>

            <AvGroup >
              <Label> <IntlMessages id="text.longitude" /></Label>
              <AvInput name="inputLongitude" />
            </AvGroup>

          </ModalBody>
          <ModalFooter>
            <Button color="secondary" outline onClick={toggleModal}>
              <IntlMessages id="button.cancel" />
            </Button>

            {loading ?
              <Spinner type="grow" color="primary" />
              :
              <Button color="primary" type="Submit">
                <IntlMessages id="button.add" />
              </Button>
            }

          </ModalFooter>
        </AvForm>
      </Modal>
    );
  }
};

export default NewCameraModalView;
