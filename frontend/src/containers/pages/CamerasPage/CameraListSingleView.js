import React from 'react';
import { Card, Badge, CustomInput } from 'reactstrap';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import { getBadgeCameraStatus } from 'constants/defaultValues';
import IntlMessages from 'helpers/IntlMessages';
import { getFormatIsoDate } from 'helpers/Utils';



const CameraListSingleView = ({ camera, collect, onCheckItem, toggleUpdateCameraModal, toggleSetCameraID, isSelect }) => {

  function cameraUpdate() {
    toggleUpdateCameraModal(true);
    toggleSetCameraID(camera.id);
  }

  function statusTranslate(status) {
    switch (status) {
      case 'Starting':
        return <IntlMessages id="camera.status-starting" />;
      case 'Running':
        return <IntlMessages id="camera.status-running" />;
      case 'Stopping':
        return <IntlMessages id="camera.status-stopping" />;
      case 'Stopped':
        return <IntlMessages id="camera.status-stopped" />;
      case 'Error':
        return <IntlMessages id="camera.status-error" />;
      default:
        return '';
    }
  }

  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={camera.id} collect={collect}>
        <Card
          className={classnames('d-flex flex-row', {
            active: isSelect,
          })}
        >
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <div className="w-15 w-sm-100">
                <p className="list-item-heading mb-1 truncate">
                  {camera.alias ? camera.alias : ""}
                </p>
              </div>
              <p className="mb-1 text-muted text-small w-15 w-sm-100">
                {camera.group ? camera.group.name : ""}
              </p>
              <p className="mb-1 text-muted text-small w-15 w-sm-100">
                {camera.updatedAt ? getFormatIsoDate(camera.updatedAt) : ""}
              </p>
              <div className="w-15 w-sm-100">

                {camera.streamerOptions ?
                  <Badge style={{ width: "100%" }} pill color={getBadgeCameraStatus(camera.streamerOptions.status)}>
                    Streaming {statusTranslate(camera.streamerOptions.status)}
                  </Badge>
                  :
                  <Badge style={{ width: "100%" }} pill color="light">
                    <IntlMessages id="camera.streaming-disabled" />
                  </Badge>
                }

                <br></br>
                <br></br>
                {camera.recorderOptions ?
                  <Badge style={{ width: "100%" }} pill color={getBadgeCameraStatus(camera.recorderOptions.status)}>
                    Recorder {statusTranslate(camera.recorderOptions.status)}
                  </Badge>
                  :
                  <Badge style={{ width: "100%" }} pill color="light">
                    <IntlMessages id="camera.recorder-disabled" />
                  </Badge>
                }
              </div>
            </div>
            <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">

              <button
                className="header-icon btn btn-empty d-none d-sm-inline-block"
                type="button"
                onClick={() => cameraUpdate()}
              >
                <i className="simple-icon-pencil d-block" />
              </button>
            </div>

            <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
              <CustomInput
                type="checkbox"
                id={`check_${camera.id}`}
                checked={isSelect}
                onChange={(event) => onCheckItem(event, camera.id)}
                label=""
              />
              
            </div>

          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(CameraListSingleView);

