import React, { useState } from 'react';
import {
  Row,
  Card,
  Button,
  CardBody,
  CardFooter,
  CardTitle
} from 'reactstrap';
import { injectIntl } from 'react-intl';
import IntlMessages from 'helpers/IntlMessages';
import Switch from 'rc-switch';
import { Colxx } from 'components/common/CustomBootstrap';

/* API */
import axios from 'axios';
import { StreamerAllStartAPI, StreamerAllStopAPI, RecorderAllStartAPI, RecorderAllStopAPI } from 'constants/requestAPI'
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

const CameraModuleSettings = ({ match, intl }) => {

  const [loadingActivateStreaming, setLoadingActivateStreaming] = useState(false)
  const [loadingDeactivateStreaming, setLoadingDeactivateStreaming] = useState(false)

  const [loadingActivateRecorder, setLoadingActivateRecorder] = useState(false)
  const [loadingDeactivateRecorder, setLoadingDeactivateRecorder] = useState(false)

  const streamerSettingsSave = (enableStreamer) => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    let rootAPI;
    let idSuccessMessage;
    let idSuccessTitle;

    if (enableStreamer) {
      setLoadingActivateStreaming(true)
      rootAPI = StreamerAllStartAPI
      idSuccessTitle = "streaming.activate.title"
      idSuccessMessage = "streaming.activate.message"
    } else {
      setLoadingDeactivateStreaming(true)
      rootAPI = StreamerAllStopAPI
      idSuccessTitle = "streaming.deactivate.title"
      idSuccessMessage = "streaming.deactivate.message"
    }

    axios
      .post(
        rootAPI,
        config
      )
      .then((response) => {

        if (response.status === 200) {

          NotificationManager.success(
            <IntlMessages id= {idSuccessMessage} />,
            <IntlMessages id= {idSuccessTitle} />,
            5000,
            () => {
              alert('callback');
            },
            null,
            "mb-3"
          );
          setLoadingActivateStreaming(false)
          setLoadingDeactivateStreaming(false)
        }

      })
      .catch(err => {
        setLoadingActivateStreaming(false)
        setLoadingDeactivateStreaming(false)
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }


  const recorderSettingsSave = (enableRecorder) => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    let rootAPI
     let idSuccessMessage;
    let idSuccessTitle;


    if (enableRecorder) {
      setLoadingActivateRecorder(true)
      rootAPI = RecorderAllStartAPI
      idSuccessMessage = "recorder.activate.message"
      idSuccessTitle = "recorder.activate.title"
    } else {
      setLoadingDeactivateRecorder(true)
      rootAPI = RecorderAllStopAPI
      idSuccessMessage = "recorder.deactivate.message"
      idSuccessTitle = "recorder.deactivate.title"
    }

    axios
      .post(
        rootAPI,
        config
      )
      .then((response) => {

        if (response.status === 200) {
          setLoadingActivateRecorder(false)
          setLoadingDeactivateRecorder(false)

          NotificationManager.success(
              <IntlMessages id= {idSuccessMessage} />,
            <IntlMessages id= {idSuccessTitle} />,
            5000,
            () => {
              alert('callback');
            },
            null,
            "mb-3"
          );
        }

      })
      .catch(err => {
        setLoadingActivateRecorder(false)
        setLoadingDeactivateRecorder(false)
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }


  return (
    <Card>
      <CardBody>

        <CardTitle>
          <IntlMessages id="settings.cameras.module" />
        </CardTitle>

        <Row>
          <Colxx xxs="12" xl="8">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <p className="list-item-heading mb-1 truncate">
                <IntlMessages id="settings.streamer.module" />
              </p>
            </div>
          </Colxx>

          <Colxx xxs="6" xl="2">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <div className="w-15 w-sm-100">

                <Button
                  color="primary"
                  className={`btn-shadow btn-multiple-state ${loadingActivateStreaming ? 'show-spinner' : ''
                    }`}
                  size="lg"
                  onClick={() => streamerSettingsSave(true)}
                >
                  <span className="spinner d-inline-block">
                    <span className="bounce1" />
                    <span className="bounce2" />
                    <span className="bounce3" />
                  </span>
                  <span className="label">
                    <IntlMessages id="button.activate" />
                  </span>
                </Button>
              </div>

            </div>
          </Colxx>

          <Colxx xxs="6" xl="2">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <div className="w-15 w-sm-100">

                <Button
                  color="primary"
                  className={`btn-shadow btn-multiple-state ${loadingDeactivateStreaming ? 'show-spinner' : ''
                    }`}
                  size="lg"
                  onClick={() => streamerSettingsSave(false)}
                >
                  <span className="spinner d-inline-block">
                    <span className="bounce1" />
                    <span className="bounce2" />
                    <span className="bounce3" />
                  </span>
                  <span className="label">
                    <IntlMessages id="button.deactivate" />
                  </span>
                </Button>

              </div>

            </div>
          </Colxx>

        </Row >


        <Row>
          <Colxx xxs="12" xl="8">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <p className="list-item-heading mb-1 truncate">
                <IntlMessages id="settings.recorder.module" />
              </p>
            </div>
          </Colxx>

          <Colxx xxs="6" xl="2">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <div className="w-15 w-sm-100">
                <Button
                  color="primary"
                  className={`btn-shadow btn-multiple-state ${loadingActivateRecorder ? 'show-spinner' : ''
                    }`}
                  size="lg"
                  onClick={() => recorderSettingsSave(true)}
                >
                  <span className="spinner d-inline-block">
                    <span className="bounce1" />
                    <span className="bounce2" />
                    <span className="bounce3" />
                  </span>
                  <span className="label">
                    <IntlMessages id="button.activate" />
                  </span>
                </Button>

              </div>

            </div>
          </Colxx>

          <Colxx xxs="6" xl="2">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <div className="w-15 w-sm-100">
                <Button
                  color="primary"
                  className={`btn-shadow btn-multiple-state ${loadingDeactivateRecorder ? 'show-spinner' : ''
                    }`}
                  size="lg"
                  onClick={() => recorderSettingsSave(false)}
                >
                  <span className="spinner d-inline-block">
                    <span className="bounce1" />
                    <span className="bounce2" />
                    <span className="bounce3" />
                  </span>
                  <span className="label">
                    <IntlMessages id="button.deactivate" />
                  </span>
                </Button>

              </div>

            </div>
          </Colxx>

        </Row >

      </CardBody>

      <CardFooter>
        <Row className="align-items-center justify-content-center">
          <Colxx xxs="12" xl="12" >
            <p >
              <IntlMessages id="settings.message.module" />
            </p>

          </Colxx>

        </Row >
      </CardFooter>

    </Card>
  );
};
export default injectIntl(CameraModuleSettings);
