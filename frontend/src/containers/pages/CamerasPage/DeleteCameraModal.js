import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Spinner
} from 'reactstrap';

import IntlMessages from 'helpers/IntlMessages';
import { CameraRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

const axios = require('axios');


class DeleteCameraModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    }
  }

  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }

  deleteUser = () => {

    const { camerasIdSelected } = this.props

    const cameraIds = camerasIdSelected.map((i) => Number(i));

    this.setLoadingStatus()

    axios.delete(
      `${CameraRoot}/bulk`,
      {
        headers: {
          Authorization: `Bearer ${getCurrentToken()}`
        },
        data: {
          ids: cameraIds
        }
      }

    ).then(response => {

      if (response.status === 200) {
        this.setLoadingStatus();
        NotificationManager.success(
          <IntlMessages id="camera.deletion.message" />,
          <IntlMessages id="camera.deletion.title" />,
          5000,
          null,
          "mb-3"
        );
        window.location.reload();
      }
    }
    )
      .catch(err => {
        this.setLoadingStatus()
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  render() {

    const { loading } = this.state
    const { modalOpen, toggleModal } = this.props


    return (
      <Modal
        isOpen={modalOpen}
        toggle={() => toggleModal()}
      >
        <ModalHeader>
          <IntlMessages id="camera.delete" />
        </ModalHeader>

        <ModalBody>
          <IntlMessages id="camera.delete.confirm-message" />
        </ModalBody>

        <ModalFooter>

          <Button color="secondary" outline onClick={() => toggleModal()}>
            <IntlMessages id="button.cancel" />
          </Button>

          {loading ?
            <Spinner type="grow" color="primary" />
            :
            <Button color="primary" onClick={() => this.deleteUser()}>
              <IntlMessages id="button.delete" />
            </Button>
          }

        </ModalFooter>
      </Modal>
    );
  }
};

export default DeleteCameraModal;
