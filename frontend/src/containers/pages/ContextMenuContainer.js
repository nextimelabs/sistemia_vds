import React from 'react';
import { ContextMenu, MenuItem } from 'react-contextmenu';
import IntlMessages from 'helpers/IntlMessages';

const ContextMenuContainer = ({ onContextMenu, onContextMenuClick }) => {
  return (
    <ContextMenu id="menu_id" onShow={(e) => onContextMenu(e, e.detail.data)}>
      <MenuItem onClick={onContextMenuClick} data={{ action: 'update' }}>
        <i className="simple-icon-docs" /> <span><IntlMessages id="button.update"/></span>
      </MenuItem>
      <MenuItem onClick={onContextMenuClick} data={{ action: 'delete' }}>
        <i className="simple-icon-trash" /> <span><IntlMessages id="button.delete"/></span>
      </MenuItem>
    </ContextMenu>
  );
};

export default ContextMenuContainer;
