import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  Spinner
} from 'reactstrap';


import {
  AvForm,
  AvField,
  AvGroup,
  AvInput,
  AvFeedback,
} from 'availity-reactstrap-validation';


import IntlMessages from 'helpers/IntlMessages';
import { GroupsCameraRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

const axios = require('axios');


class AddNewGroupModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      MainGroupList: [],

    }

    this.getMainGroupList()
  }

  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }

  getMainGroupList = () => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.get(
      `${GroupsCameraRoot}?search=`,
      config
    ).then((res) => {
      return res.data;
    })
      .then(data => {
        this.setState({
          MainGroupList: data.results,
        })
      }
      )
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  addNewGroup = (event, errors, values) => {

    if (errors.length === 0 && event.type === "submit") {

      this.setLoadingStatus()

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      

      const bodyParameters = values.inputParentGroupId ? {
        "name": values.inputName,
        "parentGroupId": Number(values.inputParentGroupId)
      }
      :
      {
        "name": values.inputName,
      }
      ;

      axios.post(
        GroupsCameraRoot,
        bodyParameters,
        config
      ).then(response => {

        if (response.status === 201) {
          this.setLoadingStatus();

          NotificationManager.success(
            <IntlMessages id="group.creation.message" />,
            <IntlMessages id="group.creation.title" />,
            5000,
            null,
            "mb-3"
          );

          window.location.reload();
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus()
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
  }

  render() {

    const { loading, MainGroupList } = this.state
    const { modalOpen, toggleModal } = this.props

    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
        wrapClassName="modal-right"
        backdrop="static"
      >

        <AvForm
          className="av-tooltip tooltip-label-right"
          onSubmit={(event, errors, values) => this.addNewGroup(event, errors, values)}
        >

          <ModalHeader toggle={toggleModal}>
            <IntlMessages id="new.camera-groups" />
          </ModalHeader>

          <ModalBody>

            <AvGroup>
              <Label> <IntlMessages id="group.name" /></Label>
              <AvInput name="inputName" required />
              <AvFeedback><IntlMessages id="group.required.name" /></AvFeedback>
            </AvGroup>


            <AvField
              type="select"
              name="inputParentGroupId"
              label={<IntlMessages id="group.main" />}
              errorMessage={<IntlMessages id="group.select" />}
            >
              <option key={-1} value={-1}></option>
              {
                MainGroupList.map(item => {
                  return (
                    <option key={item.id} value={item.id}>{item.name}</option>
                  )
                })
              }
            </AvField>

          </ModalBody>
          <ModalFooter>
            <Button color="secondary" outline onClick={toggleModal}>
              <IntlMessages id="button.cancel" />
            </Button>

            {loading ?
              <Spinner type="grow" color="primary" />
              :
              <Button color="primary" type="Submit">
                <IntlMessages id="button.confirm" />
              </Button>
            }

          </ModalFooter>
        </AvForm>
      </Modal>
    );
  }
};

export default AddNewGroupModal;
