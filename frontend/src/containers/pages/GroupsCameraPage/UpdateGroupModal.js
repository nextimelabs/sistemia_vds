import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  Spinner,
  Input
} from 'reactstrap';


import {
  AvForm,
  AvGroup,
  AvInput,
  AvFeedback,
} from 'availity-reactstrap-validation';


import IntlMessages from 'helpers/IntlMessages';
import { GroupsCameraRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';
import DeleteGroupModal from 'containers/pages/GroupsCameraPage/DeleteGroupModal';

const axios = require('axios');


class UpdateGroupModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentGroupID: -1,
      loading: false,
      modalDeleteGroupOpen: false,

      /* Group info */
      infoGroup: undefined,
      childGroupList: [],
      childGroupIdSelected: undefined,
      childGroupIdDelete: undefined,
      childGroupNameSelected: undefined
    }
  }


  setModalDeleteGroupOpen = (state, groupID) => {
    this.setState({
      childGroupIdDelete: groupID,
      modalDeleteGroupOpen: state
    })
  }


  setLoadingStatus = (state) => {
    this.setState({
      loading: state
    })
  }

  setChildGroupIdSelected = (group) => {
    this.setState({
      childGroupIdSelected: group.id,
      childGroupNameSelected: group.name

    })
  }

  setGroupChildName = (event) => {
    this.setState({
      childGroupNameSelected: event.target.value
    })
  }

  getGroupInfo = () => {

    const { groupSelectedID } = this.props

    this.setState({
      currentGroupID: groupSelectedID
    }, () => {

      this.setLoadingStatus()

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      axios.get(
        `${GroupsCameraRoot}/${groupSelectedID}?id=${groupSelectedID}`,
        config
      ).then((res) => {
        this.setState({
          infoGroup: res.data,
        }, () => {
          this.getGroupChild();
        })
      })
        .catch(err => {
          this.setLoadingStatus()
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });

    })
  }

  getGroupChild = () => {

    const { groupSelectedID } = this.props

    this.setLoadingStatus(true)

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.get(
      `${GroupsCameraRoot}/${groupSelectedID}/child?id=${groupSelectedID}`,
      config
    ).then((res) => {
      return res.data;
    })
      .then((data) => {
        this.setState({
          childGroupList: data.results
        }, () => {
          this.setLoadingStatus(false)
        })
      }
      )
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  updateGroup = (event, errors, values, groupID) => {

    if (errors.length === 0 && event.type === "submit") {

      this.setLoadingStatus(true)

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      const bodyParameters = {
        "name": values.inputName,
      };

      axios.put(
        `${GroupsCameraRoot}/${groupID}?id=${groupID}`,
        bodyParameters,
        config
      ).then(response => {

        if (response.status === 200) {

          this.setLoadingStatus(false);

          NotificationManager.success(
            <IntlMessages id="group.update.message" />,
            <IntlMessages id="group.update.title" />,
            5000,
            null,
            "mb-3"
          );

          window.location.reload();
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus(false)
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
  }


  updateSubGroup = () => {

    const { infoGroup, childGroupIdSelected, childGroupNameSelected } = this.state

    this.setLoadingStatus(true)

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    const bodyParameters = {
      "name": childGroupNameSelected,
      "parentGroupId": infoGroup.id
    };

    axios.put(
      `${GroupsCameraRoot}/${childGroupIdSelected}?id=${childGroupIdSelected}`,
      bodyParameters,
      config
    ).then(response => {

      if (response.status === 200) {

        this.setLoadingStatus(false);

        NotificationManager.success(
          <IntlMessages id="group.update.message" />,
          <IntlMessages id="group.update.title" />,
          5000,
          null,
          "mb-3"
        );

        window.location.reload();
      }
    }
    )
      .catch(err => {
        this.setLoadingStatus(false)
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }


  render() {

    const { loading, currentGroupID, infoGroup,
      childGroupList, childGroupIdSelected, childGroupNameSelected,
      modalDeleteGroupOpen, childGroupIdDelete
    } = this.state
    const { modalOpen, toggleModal, groupSelectedID } = this.props


    if (groupSelectedID !== currentGroupID) {
      this.getGroupInfo()
    }

    return (
      <>
        <Modal
          isOpen={modalOpen}
          toggle={toggleModal}
          wrapClassName="modal-right"
          backdrop="static"
        >

          {infoGroup ?
            <>
              <AvForm
                className="av-tooltip tooltip-label-right"
                onSubmit={(event, errors, values) => this.updateGroup(event, errors, values, infoGroup.id, null)}
              >

                <ModalHeader toggle={toggleModal}>
                  <IntlMessages id="group.data" />
                </ModalHeader>

                <ModalBody>

                  <AvGroup>
                    <Label> <IntlMessages id="group.name" /></Label>
                    <AvInput name="inputName" required value={infoGroup ? infoGroup.name : ""} />
                    <AvFeedback><IntlMessages id="group.required.name" /></AvFeedback>
                  </AvGroup>

                </ModalBody>


                <ModalHeader >
                  <IntlMessages id="group.child-data" />
                </ModalHeader>

                <ModalBody>

                  <table className="table table-sm table-borderless">
                    <tbody>

                      {childGroupList.map((group) => {
                        return (

                          <tr key={group.id}>

                            <td>
                              {group.id === childGroupIdSelected ?
                                <AvGroup>
                                  <Input name="inputName" required value={childGroupNameSelected} onChange={(event) => this.setGroupChildName(event)} />
                                </AvGroup>
                                :
                                <span className="font-weight-medium">
                                  {group.name}
                                </span>
                              }


                            </td>
                            <td className="text-right">

                              {group.id === childGroupIdSelected ?
                                <>
                                  {loading ?
                                    <Spinner type="grow" color="primary" />
                                    :
                                    <Button color="primary" onClick={() => this.updateSubGroup()}>
                                      <IntlMessages id="button.edit" />
                                    </Button>
                                  }
                                </>

                                :

                                <span className="text-muted">
                                  <button
                                    className="header-icon btn btn-empty d-none d-sm-inline-block"
                                    type="button"
                                    onClick={() => this.setModalDeleteGroupOpen(!modalDeleteGroupOpen, group.id)}
                                  >
                                    <i className="simple-icon-trash d-block" />
                                  </button>

                                  <button
                                    className="header-icon btn btn-empty d-none d-sm-inline-block"
                                    type="button"
                                    onClick={() => this.setChildGroupIdSelected(group)}
                                  >
                                    <i className="simple-icon-pencil d-block" />
                                  </button>

                                </span>
                              }

                            </td>

                          </tr>

                        );
                      })}
                    </tbody>
                  </table>
                </ModalBody>

                <ModalFooter>
                  <Button color="secondary" outline onClick={toggleModal}>
                    <IntlMessages id="button.cancel" />
                  </Button>

                  {loading ?
                    <Spinner type="grow" color="primary" />
                    :
                    <Button color="primary" type="Submit">
                      <IntlMessages id="button.update" />
                    </Button>
                  }
                  {' '}
                </ModalFooter>

              </AvForm>

            </>
            :
            <Spinner type="grow" color="primary" />

          }
        </Modal>

        <DeleteGroupModal
          modalOpen={modalDeleteGroupOpen}
          toggleModal={() => this.setModalDeleteGroupOpen(!modalDeleteGroupOpen)}
          groupSelectedID={childGroupIdDelete}
        />

      </>
    );
  }
};

export default UpdateGroupModal;
