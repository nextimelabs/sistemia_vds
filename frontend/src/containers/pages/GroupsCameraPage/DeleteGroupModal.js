import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Spinner
} from 'reactstrap';

import IntlMessages from 'helpers/IntlMessages';
import { GroupsCameraRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

const axios = require('axios');


class DeleteGroupModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentGroupID: undefined,
      loading: false,
    }
  }

  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }

  deleteGroup = () => {

    const { currentGroupID } = this.state

    this.setLoadingStatus()

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.delete(
      `${GroupsCameraRoot}/${currentGroupID}?id=${currentGroupID}`,
      config
    ).then(response => {

      if (response.status === 200) {
        this.setLoadingStatus();

        NotificationManager.success(
          <IntlMessages id="group.deletion.message" />,
          <IntlMessages id="group.deletion.title" />,
          5000,
          null,
          "mb-3"
        );

        window.location.reload();
      } else if (response.status === 401) {
        NotificationManager.error(
          'Unauthorized',
          'Error',
          5000,
          null,
          "mb-3"
        );
      }
    }
    )
      .catch(err => {
        this.setLoadingStatus()
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id= {errorMessage} />,
          <IntlMessages id= "text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });

  }

  render() {

    const { currentGroupID, loading } = this.state
    const { modalOpen, toggleModal, groupSelectedID } = this.props

    if (groupSelectedID !== currentGroupID) {
      this.setState({
        currentGroupID: groupSelectedID
      })
    }

    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
      >
        <ModalHeader>
          <IntlMessages id="group.delete" />
        </ModalHeader>
        <ModalBody>
          <IntlMessages id="group.delete.confirm-message" />
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" outline onClick={toggleModal}>
            <IntlMessages id="button.cancel" />
          </Button>

          {loading ?
            <Spinner type="grow" color="primary" />
            :
            <Button color="primary" onClick={this.deleteGroup}>
              <IntlMessages id="button.delete" />
            </Button>
          }

        </ModalFooter>
      </Modal>
    );
  }
};

export default DeleteGroupModal;
