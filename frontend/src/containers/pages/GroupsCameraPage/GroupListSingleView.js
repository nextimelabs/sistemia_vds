import React, { useState } from 'react';
import { Card } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import { getFormatIsoDate } from 'helpers/Utils';


const GroupListSingleView = ({ group, collect, toggleUpdateGroupModal, toggleDeleteGroupModal, toggleSetGroupID }) => {

  function groupUpdate() {
    toggleUpdateGroupModal(true);
    toggleSetGroupID(group.id);
  }

  function groupDelete() {
    toggleSetGroupID(group.id);
    toggleDeleteGroupModal(true);
  }

  return (
      <Colxx xxs="12" className="mb-3">
        <ContextMenuTrigger id="menu_id" data={group.id} collect={collect}>
          <Card
            className={classnames('d-flex flex-row')}
          >
            <div className="pl-2 d-flex flex-grow-1 min-width-zero">
              <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
                <NavLink to={`?p=${group.id}`} className="w-40 w-sm-100">
                  <p className="list-item-heading mb-1 truncate">
                    {group.name ? group.name : ""}
                  </p>
                </NavLink>
                <p className="mb-1 text-muted text-small w-15 w-sm-100">
                  {group.group ? group.group.name : ""}
                </p>
                <p className="mb-1 text-muted text-small w-15 w-sm-100">
                  {group.parentGroupId ? group.parentGroupId : ""}
                </p>
                <p className="mb-1 text-muted text-small w-15 w-sm-100">
                  {group.updatedAt ? getFormatIsoDate(group.updatedAt) : ""}
                </p>
              </div>
              <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">

                <button
                  className="header-icon btn btn-empty d-none d-sm-inline-block"
                  type="button"
                  onClick={() => groupDelete()}
                >
                  <i className="simple-icon-trash d-block" />
                </button>

                <button
                  className="header-icon btn btn-empty d-none d-sm-inline-block"
                  type="button"
                  onClick={() => groupUpdate()}
                >
                  <i className="simple-icon-pencil d-block" />
                </button>
              </div>
            </div>
          </Card>
        </ContextMenuTrigger>
      </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(GroupListSingleView);

