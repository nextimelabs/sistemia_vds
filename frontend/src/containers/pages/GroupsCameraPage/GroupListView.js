import React, { useState } from 'react';
import { Row } from 'reactstrap';
import Pagination from '../Pagination';
import ContextMenuContainer from '../ContextMenuContainer';
import GroupListSingleView from './GroupListSingleView';


function collect(props) {
  return { data: props.data };
}

const GroupListView = ({
  items,
  currentPage,
  totalPage,
  onContextMenuClick,
  onContextMenu,
  onChangePage,
  toggleUpdateGroupModal,
  toggleDeleteGroupModal,
  toggleSetGroupID
}) => {

  return (
    <Row>
      {items.map((group) => {
        return (
          <GroupListSingleView
            key={group.id}
            group={group}
            collect={collect}
            toggleUpdateGroupModal={toggleUpdateGroupModal}
            toggleDeleteGroupModal={toggleDeleteGroupModal}
            toggleSetGroupID={toggleSetGroupID}
          />
        );
      })}
      <Pagination
        currentPage={currentPage}
        totalPage={totalPage}
        onChangePage={(i) => onChangePage(i)}
      />
      <ContextMenuContainer
        onContextMenuClick={onContextMenuClick}
        onContextMenu={onContextMenu}
      />
    </Row>
  );
};

export default React.memo(GroupListView);

