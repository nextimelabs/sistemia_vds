import React from 'react';
import { ContextMenu, MenuItem } from 'react-contextmenu';
import IntlMessages from 'helpers/IntlMessages';

const VideoMouseMenu = ({ onContextMenu, onContextMenuClick }) => {
  return (
    <ContextMenu id="menu_id" onShow={(e) => onContextMenu(e, e.detail.data)}>
      <MenuItem onClick={onContextMenuClick} data={{ action: 'details' }}>
        <i className="simple-icon-info" /> <span><IntlMessages id="button.details"/></span>
      </MenuItem>
     
    </ContextMenu>
  );
};

export default VideoMouseMenu;
