import React from 'react';
import { Row } from 'reactstrap';
import Pagination from '../Pagination';
import VideoMouseMenu from './VideoMouseMenu';
import VideoSingleListView from './VideoSingleListView';

function collect(props) {
  return { data: props.data };
}

const VideoListView = ({
  items,
  currentPage,
  totalPage,
  onContextMenuClick,
  onContextMenu,
  onChangePage
}) => {

  return (
    <Row>
      {items.map((video) => {
        return (
          <VideoSingleListView
            key={video.id}
            video={video}
            collect={collect}
          />
        );
      })}
      <Pagination
        currentPage={currentPage}
        totalPage={totalPage}
        onChangePage={(i) => onChangePage(i)}
      />
      <VideoMouseMenu
        onContextMenuClick={onContextMenuClick}
        onContextMenu={onContextMenu}
      />
    </Row>
  );
};

export default React.memo(VideoListView);

