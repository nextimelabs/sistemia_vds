import React from 'react';
import { Card, CustomInput, Badge } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import { adminRoot } from 'constants/defaultValues'
import { getFormatIsoDate } from 'helpers/Utils';

const VideoSingleListView = ({ video, isSelect, collect }) => {
  return (
    <Colxx xxs="12" key={video.id} className="mb-3">
      <ContextMenuTrigger id="menu_id" data={video.id} collect={collect}>
        <NavLink
          to={{
            pathname: `${adminRoot}/management/video-details`,
            videoID: video.id,
          }}
          exact
        >
          <Card
            className={classnames('d-flex flex-row', {
              active: isSelect,
            })}
          >


            <div className="pl-2 d-flex flex-grow-1 min-width-zero">
              <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">

                <i className="iconsminds-video-5 list-item-heading mb-1 pr-4" />

                <p className="mb-1 text-muted text-small w-15 w-sm-100">
                  {(video.duration/60).toFixed(2)} min
                </p>
                <p className="mb-1 text-muted text-small w-40 w-sm-100">
                  {getFormatIsoDate(video.startTime)}
                </p>
                <p className="mb-1 text-muted text-small w-40 w-sm-100">
                  {getFormatIsoDate(video.endTime)}
                </p>
                <div className="w-15 w-sm-100">
                  <Badge pill color={video.audio ? "success" : "dark"}>
                    <IntlMessages id={`video.audio.${video.audio}`} />
                  </Badge>
                </div>
              </div>
            </div>
          </Card>
        </NavLink>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(VideoSingleListView);
