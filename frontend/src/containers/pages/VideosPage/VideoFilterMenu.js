/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable react/no-array-index-key */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Input } from 'reactstrap';
import PerfectScrollbar from 'react-perfect-scrollbar';

import IntlMessages from 'helpers/IntlMessages';
import ApplicationMenu from 'components/common/ApplicationMenu';
import { getTodoListWithFilter } from 'redux/actions';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

/* API */
import axios from 'axios';
import { CameraRoot } from 'constants/requestAPI';
import { getCurrentToken, subtractDayToDate } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

class VideoFilterMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cameraList: [],
    }
  }

  componentDidMount() {
    this.getCameraList()
  }

  /* Lettura Telecamere */
  getCameraList = () => {
    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };
    axios
      .get(
        `${CameraRoot}`,
        config
      )
      .then((res) => {
        return res.data;
      })
      .then((data) => {
        this.setState({
          cameraList: data.results
        })
      })
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });

  }

  render() {
    const {
      fromTimeSelected,
      setFromTimeSelected,
      toTimeSelected,
      setToTimeSelected,
      setCameraID
    } = this.props

    const { cameraList } = this.state

    return (
      <ApplicationMenu>
        <PerfectScrollbar
          options={{ suppressScrollX: true, wheelPropagation: false }}
        >
          <div className="p-4">

            <p className="text-muted text-small">
              <IntlMessages id="camera.select" />
            </p>

            <ul className="list-unstyled mb-5">
              <Input type="select"
                onChange={e => setCameraID(e.target.value)}
              >
                <option key={-1} value=""></option>
                {cameraList.map((camera) => (
                  <option key={camera.id} value={camera.id}>{camera.alias}</option>
                ))
                }
              </Input>
            </ul>


            <ul className="list-unstyled mb-5">

              <p className="text-muted text-small">
                <IntlMessages id="alarm.date-range" />
              </p>

              <div >
                <p className="text-muted text-small">
                  <IntlMessages id="text.from" />
                </p>
                <DatePicker
                  selected={new Date(fromTimeSelected)}
                  onChange={(date) => setFromTimeSelected(date)}
                  dateFormat="dd/MM/yyyy"
                />
              </div>

              <div style={{ marginTop: '13px' }} >
                <p className="text-muted text-small">
                  <IntlMessages id="text.to" />
                </p>
                <DatePicker
                  selected={toTimeSelected || new Date()}
                  onChange={(date) => setToTimeSelected(date)}
                  dateFormat="dd/MM/yyyy"
                />
              </div>

            </ul>
          </div>

        </PerfectScrollbar>
      </ApplicationMenu>

    );
  }
};

const mapStateToProps = ({ todoApp }) => {
  const {
    todoItems,
    filter,
    allTodoItems,
    loading,
    labels,
    categories,
  } = todoApp;

  return {
    todoItems,
    filter,
    allTodoItems,
    loading,
    labels,
    categories,
  };
};
export default connect(mapStateToProps, {
  getTodoListWithFilterAction: getTodoListWithFilter,
})(VideoFilterMenu);
