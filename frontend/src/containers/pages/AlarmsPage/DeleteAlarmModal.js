import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Spinner
} from 'reactstrap';

import IntlMessages from 'helpers/IntlMessages';
import { AlarmsAPI } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

const axios = require('axios');


class DeleteAlarmModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    }
  }

  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }

  deleteAlarm = () => {

    const { alarmSelectedID } = this.props

    this.setLoadingStatus()

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.delete(
      `${AlarmsAPI}/${alarmSelectedID}?id=${alarmSelectedID}`,
      config
    ).then(response => {

      if (response.status === 200) {
        this.setLoadingStatus();

        NotificationManager.success(
          <IntlMessages id="alarm.deletion.message" />,
          <IntlMessages id="alarm.deletion.title" />,
          5000,
          null,
          "mb-3"
        );

        window.location.reload();
        
      } else if (response.status === 401) {
        NotificationManager.error(
          'Unauthorized',
          'Error',
          5000,
          null,
          "mb-3"
        );
      }
    }
    )
      .catch(err => {
        this.setLoadingStatus()
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id= {errorMessage} />,
          <IntlMessages id= "text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });

  }

  render() {

    const { loading } = this.state
    const { modalOpen, toggleModal } = this.props

    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
      >
        <ModalHeader>
          <IntlMessages id="alarm.delete" />
        </ModalHeader>
        <ModalBody>
          <IntlMessages id="alarm.delete.confirm-message" />
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" outline onClick={toggleModal}>
            <IntlMessages id="button.cancel" />
          </Button>

          {loading ?
            <Spinner type="grow" color="primary" />
            :
            <Button color="primary" onClick={this.deleteAlarm}>
              <IntlMessages id="button.delete" />
            </Button>
          }

        </ModalFooter>
      </Modal>
    );
  }
};

export default DeleteAlarmModal;
