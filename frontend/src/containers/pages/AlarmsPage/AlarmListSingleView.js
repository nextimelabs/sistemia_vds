import React, { useState } from 'react';
import { Card, Badge } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import { getFormatIsoDate } from 'helpers/Utils';

const AlarmListSingleView = ({ alarm, collect, toggleUpdateAlarmOpenModal, toggleDeleteAlarmModal, toggleSetAlarmID }) => {

  function alarmInfoOpen() {
    toggleUpdateAlarmOpenModal(true);
    toggleSetAlarmID(alarm.id);
  }

  function groupDelete() {
    toggleSetAlarmID(alarm.id);
    toggleDeleteAlarmModal(true);
  }

  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={alarm.id} collect={collect}>
        <Card
          className={classnames('d-flex flex-row')}
        >
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <div className="w-15 w-sm-100">
                <p className="list-item-heading mb-1 truncate">
                  {alarm.name ? alarm.name : ""}
                </p>
              </div>

              <p className="mb-1 text-muted text-small w-15 w-sm-100">
                {alarm.alarmType ? alarm.alarmType : ""}
              </p>

              <p className="mb-1 text-muted text-small w-15 w-sm-100">
                {alarm.updatedAt ? getFormatIsoDate(alarm.updatedAt) : ""}
              </p>

              <div className="w-15 w-sm-100">
                {alarm.enabled ?
                  <Badge style={{ width: "100%" }} pill color="success">
                    <IntlMessages id="text.active" />
                  </Badge>
                  :
                  <Badge style={{ width: "100%" }} pill color="light">
                    <IntlMessages id="text.disabled" />
                  </Badge>
                }
              </div>

            </div>




            <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">

              <button
                className="header-icon btn btn-empty d-none d-sm-inline-block"
                type="button"
                onClick={() => groupDelete()}
              >
                <i className="simple-icon-trash d-block" />
              </button>

              <button
                className="header-icon btn btn-empty d-none d-sm-inline-block"
                type="button"
                onClick={() => alarmInfoOpen()}
              >
                <i className="simple-icon-pencil d-block" />
              </button>
            </div>
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(AlarmListSingleView);

