import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Spinner,
  Label,
  FormGroup
} from 'reactstrap';


import {
  AvForm,
  AvField,
  AvGroup,
  AvInput,
} from 'availity-reactstrap-validation';
import Switch from 'rc-switch';


import IntlMessages from 'helpers/IntlMessages';
import { AlarmsConfigurationsAPI, AlarmsAPI } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

const axios = require('axios');


class AddNewAlarmModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      isAlarmEnabled: true,
      alarmSelectedOptions: [],
      AlarmConfigurationsList: [],
      newAlarmConfigurations: {}

    }

    this.getAlarmConfigurationsList()
  }

  alarmTypeSelect = (e) => {

    const { AlarmConfigurationsList } = this.state

    const alarmSelected = AlarmConfigurationsList.find((item) => {
      return item.alarmType === e.target.value;
    })

    this.setState({
      alarmSelectedOptions: alarmSelected ? alarmSelected.options : [],
      newAlarmConfigurations: {}
    })
  }

  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }

  setAlarmEnable = () => {
    this.setState(prev => ({
      isAlarmEnabled: !prev.isAlarmEnabled
    }))
  }

  AddOptionConfiguration = (e, key) => {
    const { newAlarmConfigurations } = this.state

    const newInput = { [key]: e.target.value }

    this.setState({
      newAlarmConfigurations: { ...newAlarmConfigurations, ...newInput }
    })

  }

  getAlarmConfigurationsList = () => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.get(
      AlarmsConfigurationsAPI,
      config
    ).then((res) => {
      return res.data;
    })
      .then(data => {
        this.setState({
          AlarmConfigurationsList: data,
        })
      }
      )
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  addNewAlarm = (event, errors, values) => {

    const { newAlarmConfigurations, isAlarmEnabled } = this.state


    if (errors.length === 0 && event.type === "submit" && Object.entries(newAlarmConfigurations).length !== 0) {


      this.setLoadingStatus()

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      const bodyParameters = {
        "name": values.alarmName,
        "alarmType": values.alarmType,
        "configuration": newAlarmConfigurations,
        "enabled": isAlarmEnabled
      };


      axios.post(
        AlarmsAPI,
        bodyParameters,
        config
      ).then(response => {

        if (response.status === 201) {
          this.setLoadingStatus();

          NotificationManager.success(
            <IntlMessages id="alarm.creation.message" />,
            <IntlMessages id="alarm.creation.title" />,
            5000,
            null,
            "mb-3"
          );

          window.location.reload();
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus()
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    } else {

      NotificationManager.error(
        <IntlMessages id="text.values_required" />,
        <IntlMessages id="text.error" />,
        5000,
        null,
        "mb-3"
      );
    }
  }

  render() {

    const { loading, AlarmConfigurationsList, alarmSelectedOptions, isAlarmEnabled } = this.state
    const { modalOpen, toggleModal } = this.props

    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
        wrapClassName="modal-right"
        backdrop="static"
      >

        <AvForm
          className="av-tooltip tooltip-label-right"
          onSubmit={(event, errors, values) => this.addNewAlarm(event, errors, values)}
        >

          <ModalHeader toggle={toggleModal}>
            <IntlMessages id="settings.alarm.new" />
          </ModalHeader>

          <ModalBody>

            <AvGroup >
                <Label><IntlMessages id="alarm.name" /></Label>
              <AvInput type="text" name="alarmName" required  />
            </AvGroup>

            <AvField
              type="select"
              name="alarmType"
              label={<IntlMessages id="alarm.select.type" />}
              errorMessage={<IntlMessages id="alarm.select.type" />}
              onChange={(e) => this.alarmTypeSelect(e)}
              required
            >
              <option key={-1} value={-1}></option>
              {
                AlarmConfigurationsList.map(item => {
                  return (
                    <option key={item.alarmType} value={item.alarmType}>{item.alarmType}</option>
                  )
                })
              }
            </AvField>

            {alarmSelectedOptions.map(option => {
              return (
                <AvGroup key={option.key}>
                  {option.required ?
                    <Label>{option.description} *</Label>
                    :
                    <Label>{option.description}</Label>
                  }
                  <AvInput type={option.type} name={option.key} required={option.required} onChange={(e) => this.AddOptionConfiguration(e, option.key)} />
                </AvGroup>)
            })
            }
            <Label>
              <IntlMessages id="alarm.enabled" />
            </Label>
            <Switch
              className="custom-switch custom-switch-primary"
              checked={isAlarmEnabled}
              onChange={(primary) => this.setAlarmEnable(primary)}
            />

          </ModalBody>
          <ModalFooter>
            <Button color="secondary" outline onClick={toggleModal}>
              <IntlMessages id="button.cancel" />
            </Button>

            {loading ?
              <Spinner type="grow" color="primary" />
              :
              <Button color="primary" type="Submit">
                <IntlMessages id="button.confirm" />
              </Button>
            }

          </ModalFooter>
        </AvForm>
      </Modal>
    );
  }
};

export default AddNewAlarmModal;
