import React, { useState } from 'react';
import { Row } from 'reactstrap';
import Pagination from '../Pagination';
import ContextMenuContainer from '../ContextMenuContainer';
import AlarmListSingleView from './AlarmListSingleView';


function collect(props) {
  return { data: props.data };
}

const AlarmListView = ({
  items,
  currentPage,
  totalPage,
  onContextMenuClick,
  onContextMenu,
  onChangePage,
  toggleUpdateAlarmOpenModal,
  toggleDeleteAlarmModal,
  toggleSetAlarmID
}) => {

  return (
    <Row>
      {items.map((alarm) => {
        return (
          <AlarmListSingleView
            key={alarm.id}
            alarm={alarm}
            collect={collect}
            toggleUpdateAlarmOpenModal={toggleUpdateAlarmOpenModal}
            toggleDeleteAlarmModal={toggleDeleteAlarmModal}
            toggleSetAlarmID={toggleSetAlarmID}
          />
        );
      })}
      <Pagination
        currentPage={currentPage}
        totalPage={totalPage}
        onChangePage={(i) => onChangePage(i)}
      />
      <ContextMenuContainer
        onContextMenuClick={onContextMenuClick}
        onContextMenu={onContextMenu}
      />
    </Row>
  );
};

export default React.memo(AlarmListView);

