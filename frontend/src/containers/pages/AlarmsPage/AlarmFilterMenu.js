/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable react/no-array-index-key */
import React, { useState, useEffect, Component } from 'react';
import { connect } from 'react-redux';
import { NavItem, Input } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import classnames from 'classnames';

import IntlMessages from 'helpers/IntlMessages';
import ApplicationMenu from 'components/common/ApplicationMenu';
import { getTodoListWithFilter } from 'redux/actions';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

/* API */
import axios from 'axios';
import { AlarmsAPI } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

class AlarmFilterMenu extends Component {

  constructor(props) {
    super(props);

    this.state = {
      alarmList: [],
    }
  }

  componentDidMount(){
  this.getAlarms()
  }

  /* Lettura allarmi configurati */
  getAlarms = () => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios
      .get(
        `${AlarmsAPI}`,
        config
      )
      .then((res) => {
        return res.data;
      })
      .then((data) => {
        this.setState({ alarmList : data.results});
      })
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  };

  render() {

    const {
      fromTimeSelected,
      setFromTimeSelected,
      toTimeSelected,
      setToTimeSelected,
      setStatus,
      status,
      setAlarmID
    } = this.props

    const { alarmList } = this.state


    return (
      <ApplicationMenu>
        <PerfectScrollbar
          options={{ suppressScrollX: true, wheelPropagation: false }}
        >
          <div className="p-4">
            <p className="text-muted text-small">
              <IntlMessages id="alarm.status" />
            </p>
            <ul className="list-unstyled mb-5">

              <NavItem className={classnames({ active: status === '' })}>
                <NavLink to="#" onClick={() => setStatus('')} location={{}}>
                  <i className="iconsminds-bell" />
                  <IntlMessages id="alarm.all" />
                </NavLink>
              </NavItem>


              <NavItem
                className={classnames({
                  active:
                    status === 'successful',
                })}>

                <NavLink
                  location={{}}
                  to="#"
                  onClick={() => setStatus('successful')}
                >
                  <i className="iconsminds-mail-send" />
                  <IntlMessages id="alarm.status-successful" />

                </NavLink>
              </NavItem>
              <NavItem
                className={classnames({
                  active:
                    status === 'error',
                })}
              >
                <NavLink
                  to="#"
                  location={{}}
                  onClick={() => setStatus('error')}
                >
                  <i className="iconsminds-mail-block" />
                  <IntlMessages id="alarm.status-error" />

                </NavLink>
              </NavItem>
            </ul>

            <p className="text-muted text-small">
              <IntlMessages id="alarm.select" />
            </p>

            <ul className="list-unstyled mb-5">
              <Input type="select"
                onChange={e => setAlarmID(e.target.value)}
              >
                <option key={-1} value=""></option>
                {alarmList.map((alarm) => (
                  <option key={alarm.id} value={alarm.id}>{alarm.alarmType}</option>
                ))
                }
              </Input>
            </ul>


            <ul className="list-unstyled mb-5">

              <p className="text-muted text-small">
                <IntlMessages id="alarm.date-range" />
              </p>

              <div >
                <p className="text-muted text-small">
                  <IntlMessages id="text.from" />
                </p>
                <DatePicker
                  selected={new Date(fromTimeSelected)}
                  onChange={(date) => setFromTimeSelected(date)}
                  dateFormat="dd/MM/yyyy"
                />
              </div>

              <div style={{ marginTop: '13px' }} >
                <p className="text-muted text-small">
                  <IntlMessages id="text.to" />
                </p>
                <DatePicker
                  selected={new Date(toTimeSelected)}
                  onChange={(date) => setToTimeSelected(date)}
                  dateFormat="dd/MM/yyyy"
                />
              </div>

            </ul>
          </div>

        </PerfectScrollbar>
      </ApplicationMenu>

    );
  }
};

const mapStateToProps = ({ todoApp }) => {
  const {
    todoItems,
    filter,
    allTodoItems,
    loading,
    labels,
    categories,
  } = todoApp;

  return {
    todoItems,
    filter,
    allTodoItems,
    loading,
    labels,
    categories,
  };
};
export default connect(mapStateToProps, {
  getTodoListWithFilterAction: getTodoListWithFilter,
})(AlarmFilterMenu);
