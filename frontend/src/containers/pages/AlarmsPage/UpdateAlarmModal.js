import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  Spinner,
  Input
} from 'reactstrap';


import {
  AvForm,
  AvField,
  AvGroup,
  AvInput,
  AvFeedback,
} from 'availity-reactstrap-validation';
import Switch from 'rc-switch';


import IntlMessages from 'helpers/IntlMessages';
import { AlarmsAPI, AlarmsConfigurationsAPI } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

const axios = require('axios');


class UpdateGroupModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentAlarmID: undefined,
      loading: false,

      /* Group info */
      infoAlarm: undefined,

      /* form */
      newAlarmConfigurations: undefined,
      isAlarmEnabled: false

    }
  }

  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }

  setAlarmEnable = () => {
    this.setState(prev => ({
      isAlarmEnabled: !prev.isAlarmEnabled
    }))
  }

  AddOptionConfiguration = (e, key) => {
    const { newAlarmConfigurations } = this.state

    const newInput = { [key]: e.target.value }

    this.setState({
      newAlarmConfigurations: { ...newAlarmConfigurations, ...newInput }
    })

  }

  getAlarmInfo = (alarmSelectedID) => {

    this.setState({
      currentAlarmID: alarmSelectedID
    }, () => {

      this.setLoadingStatus()

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      axios.get(
        `${AlarmsAPI}/${alarmSelectedID}`,
        config
      ).then((res) => {
        return res.data;
      })
        .then((data) => {
          this.setState({
            infoAlarm: data,
            isAlarmEnabled: data.enabled
          }, () => {
            this.setLoadingStatus();
          })

        }
        )
        .catch(err => {
          this.setLoadingStatus()
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });

    })
  }




  updateAlarm = (event, errors, values, alarmID) => {

    const { newAlarmConfigurations, isAlarmEnabled, infoAlarm } = this.state

    if (errors.length === 0 && event.type === "submit") {


      this.setLoadingStatus()

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      const bodyParameters = {
        "name": values.alarmName,
        "alarmType": infoAlarm.alarmType,
        "configuration": newAlarmConfigurations || infoAlarm.configuration,
        "enabled": isAlarmEnabled
      };


      axios.put(
        `${AlarmsAPI}/${alarmID}?id=${alarmID}`,
        bodyParameters,
        config
      ).then(response => {

        if (response.status === 200) {
          this.setLoadingStatus();

          NotificationManager.success(
            <IntlMessages id="alarm.updated.message" />,
            <IntlMessages id="alarm.updated.title" />,
            5000,
            null,
            "mb-3"
          );

          window.location.reload();
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus()
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
  }

  render() {

    const { loading, currentAlarmID, infoAlarm, isAlarmEnabled } = this.state
    const { modalOpen, toggleModal, alarmSelectedID } = this.props


    if (alarmSelectedID !== currentAlarmID && alarmSelectedID !== -1) {
      this.getAlarmInfo(alarmSelectedID)
    }

    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
        wrapClassName="modal-right"
        backdrop="static"
      >

        <ModalHeader toggle={toggleModal}>
          <IntlMessages id="alarm.data" />
        </ModalHeader>

        {infoAlarm ?
          <>
            <AvForm
              className="av-tooltip tooltip-label-right"
              onSubmit={(event, errors, values) => this.updateAlarm(event, errors, values, infoAlarm.id)}
            >

              <ModalBody>

                <AvGroup >
                  <Label><IntlMessages id="alarm.type" /> </Label>
                  <Input value={infoAlarm.alarmType} disabled />
                </AvGroup>

                <AvGroup >
                  <Label><IntlMessages id="alarm.name" /></Label>
                  <AvInput type="text" value={infoAlarm.name} name="alarmName" required />
                </AvGroup>

                {
                  Object.keys(infoAlarm.configuration).map((key, i) => (
                  
                    <AvGroup key={key}>
                      <Label>{key}</Label>
                      <AvInput required name={key} value={infoAlarm.configuration[key]} onChange={(e) => this.AddOptionConfiguration(e, key)} />
                    </AvGroup>
                  ))
                }

                <Switch
                  className="custom-switch custom-switch-primary"
                  checked={isAlarmEnabled}
                  onChange={(primary) => this.setAlarmEnable(primary)}
                />



              </ModalBody>

              <ModalFooter>
                <Button color="secondary" outline onClick={toggleModal}>
                  <IntlMessages id="button.cancel" />
                </Button>

                {loading ?
                  <Spinner type="grow" color="primary" />
                  :
                  <Button color="primary" type="Submit">
                    <IntlMessages id="button.update" />
                  </Button>
                }
                {' '}
              </ModalFooter>
            </AvForm>


          </>
          :
          <Spinner type="grow" color="primary" />

        }
      </Modal>
    );
  }
};

export default UpdateGroupModal;
