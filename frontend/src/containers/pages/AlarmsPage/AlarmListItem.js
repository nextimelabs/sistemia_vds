import React from 'react';
import { Card, CardBody, Badge, CustomInput } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { Colxx } from 'components/common/CustomBootstrap';
import { getBadgeAlarmEnabled } from 'constants/defaultValues';
import IntlMessages from 'helpers/IntlMessages';

const AlarmListItem = ({ alarm, handleCheckChange, isSelected }) => {
  return (
    <Colxx xxs="12">
      <Card className="card d-flex mb-3">
        <div className="d-flex flex-grow-1 min-width-zero">
          <CardBody className="align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
            <NavLink
              to="#"
              location={{}}
              id={`toggler${alarm.id}`}
              className="list-item-heading mb-0 truncate w-40 w-xs-100  mb-1 mt-1"
            >
              <i
                className={`${
                  alarm.status === 'successful'
                    ? 'simple-icon-check heading-icon'
                    : 'simple-icon-refresh heading-icon'
                }`}
              />
              <span className="align-middle d-inline-block">{alarm.alarm.alarmType}</span>
            </NavLink>
            <p className="mb-1 text-muted text-small w-15 w-xs-100">
              {alarm.alarm.alarmType}
            </p>
            <p className="mb-1 text-muted text-small w-15 w-xs-100">
              {alarm.triggeredAt}
            </p>
            <div className="w-15 w-xs-100">
              <Badge color={getBadgeAlarmEnabled(alarm.alarm.enabled)} pill>
                {alarm.alarm.enabled ? <IntlMessages id="text.active" /> : <IntlMessages id="text.disabled" />}
              </Badge>
            </div>
          </CardBody>
        </div>
        <div className="card-body pt-1">
          <p className="mb-0">{alarm.info}</p>
        </div>
      </Card>
    </Colxx>
  );
};

export default React.memo(AlarmListItem);
