import React from 'react';
import { Row } from 'reactstrap';
import Pagination from '../Pagination';
import ContextMenuContainer from './EntitiesContextMenuContainer';
import EntitySingleListPreView from './EntitySingleListPreView';

function collect(props) {
  return { data: props.data };
}

const EntitiesListListView = ({
  items,
  onCheckItem,
  currentPage,
  totalPage,
  onContextMenuClick,
  onContextMenu,
  onChangePage,
}) => {
  return (
    <Row>
      {items.map((entity) => {       
        return (
        <EntitySingleListPreView
              key={entity.id}
              entity={entity}
              collect={collect}
              onCheckItem={onCheckItem}
            />
        );
      })}
      <Pagination
        currentPage={currentPage}
        totalPage={totalPage}
        onChangePage={(i) => onChangePage(i)}
      />
      <ContextMenuContainer
        onContextMenuClick={onContextMenuClick}
        onContextMenu={onContextMenu}
      />
    </Row>
  );
};

export default React.memo(EntitiesListListView);
