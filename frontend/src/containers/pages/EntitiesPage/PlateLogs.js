/* eslint-disable react/no-array-index-key */
import React from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardBody, CardTitle, Button } from 'reactstrap';

import IntlMessages from 'helpers/IntlMessages';
import data from 'data/examplePlateLog';
import { NavLink } from 'react-router-dom';
import { platesListRoot, entityDetailsRoot } from 'constants/defaultValues'

const PlateLogs = () => {
  return (
    <div>
      <Card>
        <CardBody>
          <CardTitle>
            <IntlMessages id="monitoring.plate.detect" />
          </CardTitle>
          <div className="dashboard-logs">
            <PerfectScrollbar
              options={{ suppressScrollX: true, wheelPropagation: false }}
            >
              <table className="table table-sm table-borderless">
                <tbody>
                  {data.map((entity, index) => {
                    return (

                      <tr key={index}>

                        <td>
                          <span
                            className="log-indicator align-middle" />
                        </td>

                        <td>
                          <span className="font-weight-medium">
                            {entity.label}
                          </span>
                        </td>

                        <td className="text-right">
                          <span className="text-muted">{entity.time}</span>
                        </td>

                        <td className="text-right">
                          <NavLink
                            to={{
                              pathname: entityDetailsRoot,
                              entitySelected: entity
                            }}
                            exact
                            key={index}
                          >
                            <i className="simple-icon-info" />
                          </NavLink>

                        </td>

                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </PerfectScrollbar>
            <div style={{ textAlign: 'right', color: '#236591' }}>
              <NavLink className="btn-link" to={platesListRoot}>
                <IntlMessages id="button.view-all" />
              </NavLink>
            </div>
          </div>
        </CardBody>
      </Card>
    </div>
  );
};
export default PlateLogs;
