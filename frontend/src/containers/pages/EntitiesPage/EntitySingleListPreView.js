import React from 'react';
import {
  Row,
  Card,
  CardBody,
  CardSubtitle,
  CardImg,
  CardText,
  Badge,
} from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import { entityDetailsRoot } from 'constants/defaultValues'

const EntitySingleListPreView = ({ entity, collect }) => {
  return (
    <Colxx sm="6" lg="4" xl="3" className="mb-3" key={entity.id}>
      <ContextMenuTrigger id="menu_id" data={entity.id} collect={collect}>

          <NavLink
            to={{
              pathname: entityDetailsRoot,
              entitySelected: entity
            }}
            exact
          >
          
          <Card>
            <div className="position-relative">
              <CardImg top alt={entity.title} src={entity.img} />
              <Badge
                color={entity.statusColor}
                pill
                className="position-absolute badge-top-left"
              >
                {entity.reliability}%
              </Badge>
            </div>
            <CardBody>
              <Row>
                <Colxx xxs="10" className="mb-3">
                  <CardSubtitle>{entity.value}</CardSubtitle>
                  <CardText className="text-muted text-small mb-0 font-weight-light">
                    {entity.startDetectdate}
                  </CardText>
                </Colxx>
              </Row>
            </CardBody>
          </Card>
        </NavLink>

      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(EntitySingleListPreView);
