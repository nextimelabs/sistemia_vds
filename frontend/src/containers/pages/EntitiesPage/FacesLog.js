/* eslint-disable react/no-array-index-key */
import React from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardBody, CardTitle } from 'reactstrap';

import IntlMessages from 'helpers/IntlMessages';
import data from 'data/exampleFacesLog';
import { NavLink } from 'react-router-dom';
import { facesListRoot } from 'constants/defaultValues'

const FacesLog = () => {
  return (
    <div>
      <Card>
        <CardBody>
          <CardTitle>
            <IntlMessages id="monitoring.faces.detect" />
          </CardTitle>
          <div className="dashboard-logs">
            <PerfectScrollbar
              options={{ suppressScrollX: true, wheelPropagation: false }}
            >
              <table className="table table-sm table-borderless">
                <tbody>
                  {data.map((log, index) => {
                    return (
                      <tr key={index}>
                        <td>
                          <span
                            className={`log-indicator align-middle ${log.color}`}
                          />
                        </td>
                        <td>
                          <span className="font-weight-medium">
                            {log.label}
                          </span>
                        </td>
                        <td className="text-right">
                          <span className="text-muted">{log.time}</span>
                        </td>

                        <td className="text-right">
                          <i className="simple-icon-info" />
                        </td>

                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </PerfectScrollbar>
            <div style={{ textAlign: 'right', color: '#236591' }}>
              <NavLink className="btn-link" to={facesListRoot} >
                <IntlMessages id="button.view-all" />
              </NavLink>
            </div>
          </div>
        </CardBody>
      </Card>
    </div>
  );
};
export default FacesLog;
