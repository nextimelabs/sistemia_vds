import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  Spinner
} from 'reactstrap';


import {
  AvForm,
  AvField,
  AvGroup,
  AvInput,
  AvFeedback,
  AvRadioGroup,
  AvRadio,
} from 'availity-reactstrap-validation';


import IntlMessages from 'helpers/IntlMessages';
import { UserRoleList } from 'constants/defaultValues'
import { UserRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

const axios = require('axios');


class AddNewModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    }
  }

  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }



  addNewUser = (event, errors, values) => {

    if (errors.length === 0 && event.type === "submit") {

      this.setLoadingStatus()

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      const bodyParameters = {
        "username": values.username,
        "password": values.userPassword,
        "role": values.userRole,
        "status": values.userStatus
      };

      axios.post(
        UserRoot,
        bodyParameters,
        config
      ).then(response => {


        if (response.status === 201) {
          this.setLoadingStatus();

            NotificationManager.success(
          <IntlMessages id="user.creation.message" />,
          <IntlMessages id="user.creation.title" />,
          5000,
          null,
          "mb-3"
        );

          window.location.reload();
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus()
         const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id= {errorMessage} />,
          <IntlMessages id= "text.error" />,
          5000,
          null,
          "mb-3"
        );
          throw err;
        });
  }


}

render() {

  const { loading } = this.state
  const { modalOpen, toggleModal } = this.props

  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >

      <AvForm
        className="av-tooltip tooltip-label-right"
        onSubmit={(event, errors, values) => this.addNewUser(event, errors, values)}
      >

        <ModalHeader toggle={toggleModal}>
          <IntlMessages id="new.user" />
        </ModalHeader>

        <ModalBody>

          <AvGroup>
            <Label> <IntlMessages id="user.username" /></Label>
            <AvInput name="username" required />
            <AvFeedback>username is required!</AvFeedback>
          </AvGroup>

          <AvGroup>
            <Label className="mt-4"> <IntlMessages id="user.password" /></Label>
            <AvInput name="userPassword"
              validate={{
                required: { value: true, errorMessage: 'Please enter a password' },
                minLength: { value: 8, errorMessage: 'Your password must be between 6 and 16 characters' },
                maxLength: { value: 16, errorMessage: 'Your password must be between 6 and 16 characters' }
              }}
            />
            <AvFeedback>Password is required!</AvFeedback>
          </AvGroup>


          <AvField
            type="select"
            name="userRole"
            required
            label={<IntlMessages id="user.role" />}
            errorMessage="Please select an option!"

          >
            <option value="0" />
            {
              UserRoleList.map(item => {
                return (
                  <option key={item.key} value={item.value}>{item.label}</option>
                )
              })
            }
          </AvField>
          <AvRadioGroup
            className="error-l-150"
            name="userStatus"
            required
          >
            <Label className="mt-4">
              <IntlMessages id="user.status" />
            </Label>
            <AvRadio customInput label="Attivo" value="Attivo" />
            <AvRadio customInput label="Sospeso" value="Sospeso" />
          </AvRadioGroup>


        </ModalBody>
        <ModalFooter>
          <Button color="secondary" outline onClick={toggleModal}>
            <IntlMessages id="button.cancel" />
          </Button>

          {loading ?
            <Spinner type="grow" color="primary" />
            :
            <Button color="primary" type="Submit">
              <IntlMessages id="button.confirm" />
            </Button>
          }

        </ModalFooter>
      </AvForm>
    </Modal>
  );
}
};

export default AddNewModal;
