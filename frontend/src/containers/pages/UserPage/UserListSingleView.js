import React from 'react';
import { Card, Badge, CustomInput } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import { getFormatIsoDate } from 'helpers/Utils';

const UserListSingleView = ({ user, isSelect, collect, toggleUpdateUserModal, toggleSetUserID, onCheckItem }) => {

  function userUpdate() {
    toggleUpdateUserModal(true);
    toggleSetUserID(user.id);
  }

  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={user.id} collect={collect}>
        <Card
          className={classnames('d-flex flex-row', {
            active: isSelect,
          })}
        >
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <div to={`?p=${user.id}`} className="w-40 w-sm-100">
                <p className="list-item-heading mb-1 truncate">
                  {user.username}
                </p>
              </div>
              <p className="mb-1 text-muted text-small w-15 w-sm-100">
                {user.role}
              </p>
              <p className="mb-1 text-muted text-small w-15 w-sm-100">
                {user.updatedAt? getFormatIsoDate(user.updatedAt) : ""}
              </p>
              <div className="w-15 w-sm-100">
                <Badge pill color={user.status === "Attivo" ? "success" : "warning"}>
                  {user.status}
                </Badge>
              </div>
            </div>
            <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">

              <button
                className="header-icon btn btn-empty d-none d-sm-inline-block"
                type="button"
                id="fullScreenButton"
                onClick={() => userUpdate()}
              >
                <i className="simple-icon-pencil d-block" />
              </button>
            </div>

            <div className="custom-control custom-checkbox pl-1 align-self-center pr-4" >
              <CustomInput
                type="checkbox"
                id={`check_${user.id}`}
                checked={isSelect}
                onChange={(event) => onCheckItem(event, user.id)}
                label=""
              />

            </div>
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(UserListSingleView);
