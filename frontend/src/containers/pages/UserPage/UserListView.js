import React from 'react';
import { Row } from 'reactstrap';
import Pagination from '../Pagination';
import ContextMenuContainer from '../ContextMenuContainer';
import UserListSingleView from './UserListSingleView';

function collect(props) {
  return { data: props.data };
}

const UserListView = ({
  items,
  selectedItems,
  onCheckItem,
  currentPage,
  totalPage,
  onContextMenuClick,
  onContextMenu,
  onChangePage,
  toggleUpdateUserModal,
  toggleDeleteUserModal,
  toggleSetUserID
}) => {

  return (
    <Row>
      {items.map((user) => {
  
        return (
          <UserListSingleView
            key={user.id}
            user={user}
            isSelect={selectedItems.includes(user.id)}
            onCheckItem={onCheckItem}
            collect={collect}
            toggleUpdateUserModal={toggleUpdateUserModal}
            toggleDeleteUserModal={toggleDeleteUserModal}
            toggleSetUserID={toggleSetUserID}
          />
        );
      })}
      <Pagination
        currentPage={currentPage}
        totalPage={totalPage}
        onChangePage={(i) => onChangePage(i)}
      />
      <ContextMenuContainer
        onContextMenuClick={onContextMenuClick}
        onContextMenu={onContextMenu}
      />
    </Row>
  );
};

export default React.memo(UserListView);
