import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  Spinner
} from 'reactstrap';


import {
  AvForm,
  AvField,
  AvGroup,
  AvInput,
  AvFeedback,
  AvRadioGroup,
  AvRadio,
} from 'availity-reactstrap-validation';


import IntlMessages from 'helpers/IntlMessages';
import { UserRoleList } from 'constants/defaultValues'
import { UserRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

import axios from 'axios';


class UpdateUserModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentUserID: undefined,
      loading: false,

      /* User info */
      infoUsername: undefined,
      infoRole: undefined,
      infoStatus: undefined,
    }
  }

  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }


  getUserInfo = (userSelectedID) => {

    this.setState({
      currentUserID: userSelectedID
    }, () => {

      this.setLoadingStatus()

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };


      axios.get(
        `${UserRoot}/${userSelectedID}`,
        config
      ).then(response => {

        if (response.status === 200) {

          const infoUser = response.data

          this.setState({
            infoUsername: infoUser.username,
            infoRole: infoUser.role,
            infoStatus: infoUser.status
          }, () => {
            this.setLoadingStatus();
          })
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus()
          const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id= {errorMessage} />,
          <IntlMessages id= "text.error" />,
          5000,
          null,
          "mb-3"
        );
          throw err;
        });

    })

  }


  updateUser = (event, errors, values) => {

    const { currentUserID } = this.state

    if (errors.length === 0 && event.type === "submit") {

      this.setLoadingStatus()

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };


      const bodyParameters = {
        "username": values.username,
        "role": values.userRole,
        "status": values.userStatus
      };


      axios.put(
        `${UserRoot}/${currentUserID}`,
        bodyParameters,
        config
      ).then(response => {

        if (response.status === 200) {
          this.setLoadingStatus();

           NotificationManager.success(
          <IntlMessages id="user.update.message" />,
          <IntlMessages id="user.update.title" />,
          5000,
          null,
          "mb-3"
        );

          window.location.reload();
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus()
            this.setLoadingStatus()
   const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id= {errorMessage} />,
          <IntlMessages id= "text.error" />,
          5000,
          null,
          "mb-3"
        );
          throw err;
        });
    }


  }

  render() {

    const { loading, currentUserID, infoUsername, infoRole, infoStatus } = this.state
    const { modalOpen, toggleModal, userSelectedID } = this.props

    if (userSelectedID !== currentUserID && userSelectedID !== -1) {
      this.getUserInfo(userSelectedID)
    }



    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
        wrapClassName="modal-right"
        backdrop="static"
      >

        <AvForm
          className="av-tooltip tooltip-label-right"
          onSubmit={(event, errors, values) => this.updateUser(event, errors, values)}
        >

          <ModalHeader toggle={toggleModal}>
            <IntlMessages id="update.user" />
          </ModalHeader>

          <ModalBody>

            <AvGroup>
              <Label> <IntlMessages id="user.username" /></Label>
              <AvInput name="username" required value={infoUsername || ""} />
              <AvFeedback>username is required!</AvFeedback>
            </AvGroup>

            <AvField
              type="select"
              name="userRole"
              required
              label={<IntlMessages id="user.role" />}
              errorMessage="Please select an option!"
              value={infoRole || "0"}
            >
              <option value="0" />
              {
                UserRoleList.map(item => {
                  return (
                    <option key={item.key} value={item.value}>{item.label}</option>
                  )
                })
              }
            </AvField>
            <AvRadioGroup
              className="error-l-150"
              name="userStatus"
              required
              value={infoStatus || ""}
            >
              <Label className="mt-4">
                <IntlMessages id="user.status" />
              </Label>
              <AvRadio customInput label="Attivo" value="Attivo" />
              <AvRadio customInput label="Sospeso" value="Sospeso" />
            </AvRadioGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="secondary" outline onClick={toggleModal}>
              <IntlMessages id="button.cancel" />
            </Button>

            {loading ?
              <Spinner type="grow" color="primary" />
              :
              <Button color="primary" type="Submit">
                <IntlMessages id="button.update" />
              </Button>
            }

          </ModalFooter>
        </AvForm>
      </Modal>
    );
  }
};

export default UpdateUserModal;
