import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Spinner
} from 'reactstrap';

import IntlMessages from 'helpers/IntlMessages';
import { UserRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

const axios = require('axios');


class DeleteUserModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    }
  }

  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }

  deleteUser = () => {

    const { usersIdSelected } = this.props

    this.setLoadingStatus()


    axios.delete(
      `${UserRoot}/bulk`,
      {
        headers: {
          Authorization: `Bearer ${getCurrentToken()}`
        },
        data: {
          ids: usersIdSelected
        }
      }
    ).then(response => {

      if (response.status === 200) {
        this.setLoadingStatus();

        NotificationManager.success(
          <IntlMessages id="user.deletion.message" />,
          <IntlMessages id="user.deletion.title" />,
          5000,
          null,
          "mb-3"
        );

        window.location.reload();
      } else if (response.status === 401) {
        NotificationManager.error(
          'Unauthorized',
          'Error',
          5000,
          null,
          "mb-3"
        );
      }
    }
    )
      .catch(err => {
        this.setLoadingStatus()
       const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id= {errorMessage} />,
          <IntlMessages id= "text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });

  }

  render() {

    const { loading } = this.state
    const { modalOpen, toggleModal } = this.props

  
    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
      >
        <ModalHeader>
          <IntlMessages id="user.delete" />
        </ModalHeader>
        <ModalBody>
          <IntlMessages id="user.delete.confirm-message" />
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" outline onClick={toggleModal}>
            <IntlMessages id="button.cancel" />
          </Button>

          {loading ?
            <Spinner type="grow" color="primary" />
            :
            <Button color="primary" onClick={this.deleteUser}>
              <IntlMessages id="button.delete" />
            </Button>
          }

        </ModalFooter>
      </Modal>
    );
  }
};

export default DeleteUserModal;
