import React from 'react';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { EntitiesNumberCards, PeopleChart, VehicleChart } from 'components/dashboard_components';
import { Row } from 'reactstrap';

const DashboardPage = ({ match }) => (
  <div>
    <Row>
      <Colxx xxs="12">
        <Breadcrumb heading="menu.dashboard" match={match} />
        <Separator className="mb-5" />
      </Colxx>
    </Row>
    <Row>
      <Colxx xxs="12">
        <EntitiesNumberCards />
      </Colxx>
    </Row>
    <Row>
    </Row>
    <Row>
      <Colxx sm="12" md="6" className="mb-4">
        <PeopleChart />
      </Colxx>
      <Colxx sm="12" md="6" className="mb-4">
        <VehicleChart />
      </Colxx>
    </Row>
  </div>
);
export default DashboardPage;
