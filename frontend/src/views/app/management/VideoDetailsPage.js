import React, { Component } from 'react';
import {
  Row,
  Card,
  CardBody,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  TabContent,
  TabPane,
  CardHeader,
  Table,
} from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { injectIntl } from 'react-intl';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { Separator, Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import DeleteVideoModal from 'containers/pages/VideosPage/DeleteVideoModal'

import { SegmentsAPI } from 'constants/requestAPI';
import { getCurrentToken, getFormatIsoDate } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';
import axios from 'axios';

class DetailsPages extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      activeTab: 'details',
      isModalDeleteVideo: false,

      videoID: props.location ? props.location.videoID : "",
      videoInfo: {}
    }
    
  }

  componentDidMount() {
     this.getVideoInfo()
  }

  setActiveTab = (tab) => {
    this.setState({
      activeTab: tab
    })
  }

  setIsModalVideoDelete = (state) => {
    this.setState({
      isModalDeleteVideo: state
    })
  }

  getVideoInfo = () => {
    const { videoID } = this.state


    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.get(
      `${SegmentsAPI}/${videoID}?id=${videoID}`,
      config
    ).then((res) => {
      return res.data;
    })
      .then((data) => {
        this.setState({
          videoInfo: data
        })
      }
      )
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  render() {

    const { match, intl, history } = this.props
    const { activeTab, loading, videoInfo, isModalDeleteVideo } = this.state
    const { messages } = intl;

    return (
      <>
        <Row>
          <Colxx xxs="12">
            <h1><IntlMessages id="video.details" /> </h1>
            <div className="text-zero top-right-button-container">
              <UncontrolledDropdown>
                <DropdownToggle
                  caret
                  color="primary"
                  size="lg"
                  outline
                  className="top-right-button top-right-button-single"
                >
                  <IntlMessages id="button.actions" />
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem  onClick = {() => this.setIsModalVideoDelete(true)}>
                    <IntlMessages id="button.delete" />
                  </DropdownItem>

                </DropdownMenu>
              </UncontrolledDropdown>
            </div>

            <Breadcrumb match={match} />
            <Separator className="mb-5" />

            <Row>
              <Colxx xxs="12" xl="12" className="col-left">

                <Card className="mb-4">
                  <CardHeader>
                    <Nav tabs className="card-header-tabs ">
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: activeTab === 'details',
                            'nav-link': true,
                          })}
                          onClick={() => this.setActiveTab('details')}
                          to="#"
                          location={{}}
                        >
                          <IntlMessages id="video.details" />
                        </NavLink>
                      </NavItem>
                    </Nav>
                  </CardHeader>

                  <TabContent activeTab={activeTab}>
                    <TabPane tabId="details">
                      <Row>
                        <Colxx sm="12">
                          <CardBody>

                            <Table borderless>
                              <tbody>
                                <tr>
                                  <th scope="row"><IntlMessages id="video.startTime" /></th>
                                  <td>{getFormatIsoDate(videoInfo.startTime)}</td>
                                </tr>
                                <tr>
                                  <th scope="row"><IntlMessages id="video.endTime" /></th>
                                  <td>{getFormatIsoDate(videoInfo.endTime)}</td>
                                </tr>
                                <tr>
                                  <th scope="row"><IntlMessages id="video.duration" /></th>
                                  <td>{(videoInfo.duration/60).toFixed(2)} min</td>
                                </tr>
                                <tr>
                                  <th scope="row"><IntlMessages id="video.fileSize" /></th>
                                  <td>{(videoInfo.fileSize/1020).toFixed(2)} Mb</td>
                                </tr>
                                <tr>
                                  <th scope="row"><IntlMessages id="video.filePath" /></th>
                                  <td>{videoInfo.filePath}</td>
                                </tr>
                                <tr>
                                  <th scope="row"><IntlMessages id="video.audio" /></th>
                                  <td>{videoInfo.audio ?
                                    <IntlMessages id="text.yes" />
                                    :
                                    <IntlMessages id="text.no" />
                                  }</td>
                                </tr>
                                <tr>
                                  <th scope="row"><IntlMessages id="video.camera" /></th>
                                  <td>{videoInfo.cameraId}</td>
                                </tr>
                              </tbody>
                            </Table>
                          </CardBody>
                        </Colxx>
                      </Row>
                    </TabPane>
                  </TabContent>
                </Card>
              </Colxx>
            </Row>
          </Colxx>
        </Row>

        <DeleteVideoModal
          modalOpen={isModalDeleteVideo}
          toggleModal={() => this.setIsModalVideoDelete(false)}
          videoID={videoInfo.id}
        />

      </>
    );
  }
};
export default injectIntl(DetailsPages);
