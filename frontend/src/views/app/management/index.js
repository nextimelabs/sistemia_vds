import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const CameasManagementPage = React.lazy(() =>
  import( './camerasManagementPage')
);

const AlarmsManagementPage = React.lazy(() =>
  import( './alarmsManagementPage')
);

const VideosManagementPage = React.lazy(() =>
  import( './VideosManagementPage')
);

const VideoDetailsPage = React.lazy(() =>
  import( './VideoDetailsPage')
);

const vds = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/cameras`} />

      <Route
        path={`${match.url}/cameras`}
        render={(props) => <CameasManagementPage {...props} />}
      />

       <Route
        path={`${match.url}/alarms`}
        render={(props) => <AlarmsManagementPage {...props} />}
      />

        <Route
        path={`${match.url}/videos`}
        render={(props) => <VideosManagementPage {...props} />}
      />

       <Route
        path={`${match.url}/video-details`}
        render={(props) => <VideoDetailsPage {...props} />}
      />
      
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default vds;
