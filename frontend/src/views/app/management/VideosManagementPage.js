import React, { useState, useEffect } from 'react';

/* API */
import axios from 'axios';
import { SegmentsAPI } from 'constants/requestAPI';
import { getCurrentToken, subtractDayToISODate } from 'helpers/Utils';

import ListPageHeading from 'containers/pages/VideosPage/VideoListPageHeading';
import VideoFilterMenu from 'containers/pages/VideosPage/VideoFilterMenu';

import VideoListView from 'containers/pages/VideosPage/VideoListView';
import IntlMessages from 'helpers/IntlMessages';
import { Redirect } from 'react-router-dom';
import { adminRoot } from 'constants/defaultValues'

import {
  Row,
} from 'reactstrap';

const orderByOption = [
  { column: 'ASC', label: 'ASC' },
  { column: 'DESC', label: 'DESC' },
];

const orderColumnOptions = [
  { column: 'id', label: <IntlMessages id="video.id" /> },
  { column: 'startTime', label: <IntlMessages id="video.startTime" /> },
  { column: 'endTime', label: <IntlMessages id="video.endTime" /> },
  { column: 'fileSize', label: <IntlMessages id="video.fileSize" /> },
  { column: 'cameraId', label: <IntlMessages id="camera.id" /> },

];

const pageSizes = [4, 8, 12, 20];

const VideosManagementPage = ({ match }) => {

  const [isLoaded, setIsLoaded] = useState(false);
  const [isOpenDetails, setIsOpenDetails] = useState(false);

  // Paginazione
  const [currentPage, setCurrentPage] = useState(0);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [totalItemCount, setTotalItemCount] = useState(1);
  const [totalPage, setTotalPage] = useState(0);

  // Dati
  const [items, setItems] = useState([]);
  const [videoIDSelected, setVideoIDSelected] = useState([]);

  // Filtri Header
  const [selectedOrderByOption, setSelectedOrderByOption] = useState({
    column: 'ASC',
    label: 'ASC',
  });
  const [selectedOrderColumnOptions, setOrderColumnOptions] = useState({
    column: 'id',
    label: <IntlMessages id="video.id" />,
  });

  /* Filtri Menu */
  const [toTimeSelected, setToTimeSelected] = useState(new Date());
  const [fromTimeSelected, setFromTimeSelected] = useState(subtractDayToISODate(15));
  const [cameraID, setCameraID] = useState("");

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderByOption, selectedOrderColumnOptions]);


  /* Lettura elenco telecamere */
  useEffect(() => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    const page = currentPage > 0 ? currentPage - 1 : 0

    /* Conversione date in IsoDate */
    const fromTimeISO = new Date(fromTimeSelected).toISOString();
    const toTimeISO = new Date(toTimeSelected).toISOString();

    async function fetchData() {
      axios
        .get(
          `${SegmentsAPI}?fromTime=${fromTimeISO}&toTime=${toTimeISO}&cameraId=${cameraID}&limit=${selectedPageSize}&offset=${page * selectedPageSize}&orderColumn=${selectedOrderColumnOptions.column}&orderBy=${selectedOrderByOption.column}`,
          config
        )
        .then((res) => {
          return res.data;
        })
        .then((data) => {
          setTotalPage(Math.ceil(data.total / data.limit));
          setItems(data.results);
          setTotalItemCount(data.total);
          setIsLoaded(true);
        });
    }
    fetchData();
  }, [selectedPageSize, currentPage, selectedOrderByOption, selectedOrderColumnOptions, cameraID, fromTimeSelected, toTimeSelected]);


  /* COMANDI MOUSE */
  const onContextMenuClick = (e, data) => {

    switch (data.action) {
      case 'details':
        setIsOpenDetails(true)
        break;
      default:
    }
  };

  const onContextMenu = (e, data) => {
    setVideoIDSelected(data.data);
    return true;
  };



  const startIndex = currentPage * selectedPageSize;
  const endIndex = currentPage - selectedPageSize;

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>

      {isOpenDetails ? (
        <Redirect push to={{
          pathname: `${adminRoot}/management/video-details`,
          videoID: videoIDSelected,
        }} />
      ) : null}

      <Row className="app-row survey-app">
        <ListPageHeading
          heading="title.videos.management"
          changeOrderBy={(column) => {
            setSelectedOrderByOption(
              orderByOption.find((x) => x.column === column)
            );
          }}
          changeOrderColumn={(column) => {
            setOrderColumnOptions(
              orderColumnOptions.find((x) => x.column === column)
            );
          }}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderByOption={selectedOrderByOption}
          selectedOrderColumnOptions={selectedOrderColumnOptions}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          itemsLength={items ? items.length : 0}
          orderOptions={orderByOption}
          orderColumnOptions={orderColumnOptions}
          pageSizes={pageSizes}
        />


        <VideoListView
          items={items}
          currentPage={currentPage}
          totalPage={totalPage}
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
          onChangePage={setCurrentPage}
        />

        <VideoFilterMenu
          fromTimeSelected={fromTimeSelected}
          setFromTimeSelected={setFromTimeSelected}
          toTimeSelected={toTimeSelected}
          setToTimeSelected={setToTimeSelected}
          setCameraID={setCameraID}
        />

      </Row>
    </>
  );
};

export default VideosManagementPage;
