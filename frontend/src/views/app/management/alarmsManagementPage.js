import React, { useState, useEffect } from 'react';

/* API */
import axios from 'axios';
import { AlarmsAPI } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';

import ListPageHeading from 'containers/pages/AlarmsPage/AlarmsManagementPageHeading';
import AddNewAlarmModal from 'containers/pages/AlarmsPage/AddNewAlarmModal';
import AddUpdateAlarmModal from 'containers/pages/AlarmsPage/UpdateAlarmModal';
import DeleteAlarmModal from 'containers/pages/AlarmsPage/DeleteAlarmModal';

import AlarmListView from 'containers/pages/AlarmsPage/AlarmListView';

import useMousetrap from 'hooks/use-mousetrap';
import IntlMessages from 'helpers/IntlMessages';


const orderGroupListOptions = [
  { column: 'id', label: <IntlMessages id="alarm.id" /> },
  { column: 'alarmType', label: <IntlMessages id="alarm.type" /> },
  { column: 'createdAt', label: <IntlMessages id="alarm.creation-date" /> },
];

const pageSizes = [2, 4, 8, 12, 20];

const AlarmsManagementPage = ({ match }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'id',
    label: <IntlMessages id="alarm.id" />,
  });

  const [modalNewAlarmOpen, setModalNewAlarmOpen] = useState(false);
  const [modalUpdateAlarmOpen, setModalUpdateAlarmOpen] = useState(false);

  const [modalDeleteAlarmOpen, setModalDeleteAlarmOpen] = useState(false);
  const [alarmSelectedID, setAlarmSelectedID] = useState(-1);

  const [totalItemCount, setTotalItemCount] = useState(1);
  const [totalPage, setTotalPage] = useState(0);
  const [search, setSearch] = useState('');
  const [selectedItems, setSelectedItems] = useState([]);
  const [items, setItems] = useState([]);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);


  /* Lettura elenco telecamere */
  useEffect(() => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    async function fetchData() {
      axios
        .get(
          `${AlarmsAPI}?limit=${selectedPageSize}&offset=${(currentPage - 1) * selectedPageSize}&orderColumn=${selectedOrderOption.column}&search=${search}`,
          config
        )
        .then((res) => {
          return res.data;
        })
        .then((data) => {
          setTotalPage(Math.ceil(data.total / data.limit));
          setItems(data.results);
          setSelectedItems([]);
          setTotalItemCount(data.total);
          setIsLoaded(true);
        });
    }
    fetchData();
  }, [selectedPageSize, currentPage, selectedOrderOption, search]);


  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= items.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };


  const onContextMenuClick = (e, data) => {

    switch (data.action) {
      case 'update':
        setModalNewAlarmOpen(!modalNewAlarmOpen)
        break;
      case 'delete':
        setModalDeleteAlarmOpen(!modalDeleteAlarmOpen)
        break;
      default:
    }
  };

  const onContextMenu = (e, data) => {
    const clickedCameraId = data.data;
    setAlarmSelectedID(clickedCameraId)
    return true;
  };

  useMousetrap(['ctrl+a', 'command+a'], () => {
    handleChangeSelectAll(false);
  });

  useMousetrap(['ctrl+d', 'command+d'], () => {
    setSelectedItems([]);
    return false;
  });

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <div className="disable-text-selection">
        <ListPageHeading
          heading="title.alarms.management"
          handleChangeSelectAll={handleChangeSelectAll}
          changeOrderBy={(column) => {
            setSelectedOrderOption(
              orderGroupListOptions.find((x) => x.column === column)
            );
          }}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderOption={selectedOrderOption}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          selectedItemsLength={selectedItems ? selectedItems.length : 0}
          itemsLength={items ? items.length : 0}
          onSearchKey={(e) => {
            if (e.key === 'Enter') {
              setSearch(e.target.value.toLowerCase());
            }
          }}
          orderOptions={orderGroupListOptions}
          pageSizes={pageSizes}
          actionHeaderButton={() => setModalNewAlarmOpen(!modalNewAlarmOpen)}
        />

        <AddNewAlarmModal
          modalOpen={modalNewAlarmOpen}
          toggleModal={() => setModalNewAlarmOpen(!modalNewAlarmOpen)}
        />

        <AddUpdateAlarmModal
          modalOpen={modalUpdateAlarmOpen}
          toggleModal={() => setModalUpdateAlarmOpen(!modalUpdateAlarmOpen)}
          alarmSelectedID={alarmSelectedID}
        />

            <DeleteAlarmModal
          modalOpen={modalDeleteAlarmOpen}
          toggleModal={() => setModalDeleteAlarmOpen(!modalDeleteAlarmOpen)}
          alarmSelectedID={alarmSelectedID}
        />

        <AlarmListView
          items={items}
          selectedItems={selectedItems}
          currentPage={currentPage}
          totalPage={totalPage}
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
          onChangePage={setCurrentPage}
          toggleUpdateAlarmOpenModal={setModalUpdateAlarmOpen}
          toggleDeleteAlarmModal={setModalDeleteAlarmOpen}
          toggleSetAlarmID={setAlarmSelectedID}
        />

      </div>
    </>
  );
};

export default AlarmsManagementPage;
