import React, { useState } from 'react';
import {
  Row,
  Card,
  Collapse,
  CardBody,
  Label
} from 'reactstrap';
import { injectIntl } from 'react-intl';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { Separator, Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import Switch from 'rc-switch';
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation';

/* Components */
import EntityDetectionSettings from 'containers/pages/CamerasPage/CameraEntityDetectionSettings'
import CameraModuleSettings from 'containers/pages/CamerasPage/CameraModuleSettings'

const CamerasManagementPage = ({ match, intl }) => {
  const [enableVehicleDetect, setEnableVehicleDetect] = useState(false);
  const [enablePeopleDetect, setEnablePeopleDetect] = useState(false);

  const { messages } = intl;

  const onSubmit = (event, errors, values) => {
    console.log(errors);
    console.log(values);
    if (errors.length === 0) {
      // submit
    }
  };


  return (
    <>
      <Row>
        <Colxx xxs="12" xl="12">
          <h1><IntlMessages id="title.camera.management" /></h1>
          <Breadcrumb match={match} />
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <CameraModuleSettings />

      <Separator className="mb-5" />

      <EntityDetectionSettings />

    </>
  );
};
export default injectIntl(CamerasManagementPage);
