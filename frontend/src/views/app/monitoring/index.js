import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const EntitiesPage = React.lazy(() =>
  import('./entitiesMonitoringPage')
);


const EventsPage = React.lazy(() =>
  import('./eventsMonitoringPage')
);
const AlarmsPage = React.lazy(() =>
  import('./alarmsMonitoringPage')
);


const SecondMenu = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>

      <Redirect exact from={`${match.url}/`} to={`${match.url}/entities`} />

      <Route
        path={`${match.url}/entities`}
        render={(props) => <EntitiesPage {...props} />}
      />
      <Route
        path={`${match.url}/events`}
        render={(props) => <EventsPage {...props} />}
      />
      <Route
        path={`${match.url}/alarms`}
        render={(props) => <AlarmsPage {...props} />}
      />
      
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default SecondMenu;
