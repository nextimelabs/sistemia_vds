/* eslint-disable react/no-array-index-key */
import React, { useState, useEffect } from 'react';
import {
  Row,
  Button,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Collapse,
} from 'reactstrap';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import IntlMessages from 'helpers/IntlMessages';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';

import {
  getTodoList,
  getTodoListWithOrder,
  getTodoListSearch,
  selectedTodoItemsChange,
} from 'redux/actions';
import EventListItem from 'containers/pages/EventsPage/EventListItem';
import EventFilterMenu from 'containers/pages/EventsPage/EventFilterMenu';
import { OrderByOptions } from 'constants/defaultValues'

/* API */
import axios from 'axios';
import { EventsAPI, CameraRoot } from 'constants/requestAPI';
import { getCurrentToken, getCurrentIsoDate, subtractDayToISODate } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';
import Pagination from 'containers/pages/Pagination';


const orderOptions = [
  { column: 'id', label: <IntlMessages id="event.id" /> },
  { column: 'eventType', label: <IntlMessages id="event.eventType" /> },
  { column: 'timestamp', label: <IntlMessages id="event.timestamp" /> },
  { column: 'cameraId', label: <IntlMessages id="event.cameraId" /> },
];

const pageSizes = [4, 8, 12, 20];

const EventsMonitoringPage = ({
  match,
  intl,
  loading,
  getTodoListAction,
}) => {

  const [displayOptionsIsOpen, setDisplayOptionsIsOpen] = useState(false);

  /* Lista, paginazione e ricerca */
  const [items, setItems] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [totalPage, setTotalPage] = useState();
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'id',
    label: <IntlMessages id="event.id" />,
  });
  const [selectedOrderByOption, setSelectedOrderByOption] = useState({
    column: 'ASC',
    label: "ASC",
  });
  const [search, setSearch] = useState('');
  const [selectedItems, setSelectedItems] = useState([]);
  const [totalItemCount, setTotalItemCount] = useState(0);

  /* Filtri Menu */
  const [toTimeSelected, setToTimeSelected] = useState(new Date());
  const [fromTimeSelected, setFromTimeSelected] = useState(subtractDayToISODate(15));
  const [eventTypeID, setEventTypeID] = useState('');
  const [eventTypeList, setEventTypeList] = useState([]);
  const [cameraID, setCameraID] = useState([]);
  const [cameraList, setCameraList] = useState([]);


  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);

  /* Lettura eventi */
  useEffect(() => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    const page = currentPage > 0 ? currentPage - 1 : 0

    /* Conversione date in IsoDate */
    const fromTimeISO = new Date(fromTimeSelected).toISOString();
    const toTimeISO = new Date(toTimeSelected).toISOString();

    async function fetchData() {
      axios
        .get(
          `${EventsAPI}?cameraId=${cameraID}&fromTime=${fromTimeISO}&toTime=${toTimeISO}&eventType=${eventTypeID}&limit=${selectedPageSize}&offset=${page * selectedPageSize}&orderColumn=${selectedOrderOption.column}&search=${search}&orderBy=${selectedOrderByOption.label}`,
          config
        )
        .then((res) => {
          return res.data;
        })
        .then((data) => {
          setTotalPage(Math.ceil(data.total / data.limit));
          setItems(data.results);
          setSelectedItems([]);
          setTotalItemCount(data.total);
        })
        .catch(err => {
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
    fetchData();
  }, [selectedPageSize, cameraID, currentPage, selectedOrderOption, selectedOrderByOption, search, eventTypeID, fromTimeSelected, toTimeSelected]);


 


  useEffect(() => {
    document.body.classList.add('right-menu');
    getTodoListAction();

    return () => {
      document.body.classList.remove('right-menu');
    };
  }, [getTodoListAction]);

  const { messages } = intl;
  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  return (
    <>
      <Row className="app-row survey-app">
        <Colxx xxs="12">
          <div className="mb-2">
            <h1>
              <IntlMessages id="title.events.list" />
            </h1>
            <Breadcrumb match={match} />
          </div>

          <div className="mb-2">
            <Button
              color="empty"
              className="pt-0 pl-0 d-inline-block d-md-none"
              onClick={() => setDisplayOptionsIsOpen(!displayOptionsIsOpen)}
            >
              <IntlMessages id="pages.display-options" />{' '}
              <i className="simple-icon-arrow-down align-middle" />
            </Button>
            <Collapse
              id="displayOptions"
              className="d-md-block"
              isOpen={displayOptionsIsOpen}
            >
              <div className="d-block mb-2 d-md-inline-block">
                <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1">
                  <DropdownToggle caret color="outline-dark" size="xs">
                    <IntlMessages id="text.orderby" />
                    {selectedOrderOption.label}
                  </DropdownToggle>
                  <DropdownMenu>
                    {orderOptions.map((order, index) => {
                      return (
                        <DropdownItem
                          key={index}
                          onClick={() => {
                            setSelectedOrderOption(
                              orderOptions.find((x) => x.column === order.column)
                            );
                          }}
                        >
                          {order.label}
                        </DropdownItem>
                      );
                    })}
                  </DropdownMenu>
                </UncontrolledDropdown>

                <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1">
                  <DropdownToggle caret color="outline-dark" size="xs">
                    <IntlMessages id="text.order" />
                    {selectedOrderByOption.label}
                  </DropdownToggle>
                  <DropdownMenu>
                    {OrderByOptions.map((order, index) => {
                      return (
                        <DropdownItem
                          key={index}
                          onClick={() => {
                            setSelectedOrderByOption(
                              OrderByOptions.find((x) => x.column === order.column)
                            );
                          }}
                        >
                          {order.label}
                        </DropdownItem>
                      );
                    })}
                  </DropdownMenu>
                </UncontrolledDropdown>

              </div>
              <div className="float-md-right pt-1">
                <span className="text-muted text-small mr-1">{`${startIndex}-${endIndex} of ${totalItemCount} `}</span>
                <UncontrolledDropdown className="d-inline-block">
                  <DropdownToggle caret color="outline-dark" size="xs">
                    {selectedPageSize}
                  </DropdownToggle>
                  <DropdownMenu right>
                    {pageSizes.map((size, index) => {
                      return (
                        <DropdownItem
                          key={index}
                          onClick={() => setSelectedPageSize(size)}
                        >
                          {size}
                        </DropdownItem>
                      );
                    })}
                  </DropdownMenu>
                </UncontrolledDropdown>
              </div>
            </Collapse>
          </div>
          <Separator className="mb-5" />
          <Row>
            {loading ? (
              items.map((item, index) => (
                <EventListItem
                  key={`todo_item_${index}`}
                  event={item}
                  isSelected={loading ? selectedItems.includes(item.id) : false}
                />
              ))
            ) : (
              <div className="loading" />
            )}
            <Pagination
              currentPage={currentPage}
              totalPage={totalPage}
              onChangePage={(i) => setCurrentPage(i)}
            />
          </Row>
        </Colxx>
      </Row>
      {loading
        &&
        <EventFilterMenu
          fromTimeSelected={fromTimeSelected}
          setFromTimeSelected={setFromTimeSelected}
          toTimeSelected={toTimeSelected}
          setToTimeSelected={setToTimeSelected}
          setEventTypeID={setEventTypeID}
          eventTypeList={eventTypeList}
          cameraList={cameraList}
          setCameraID={setCameraID}
        />

      }
    </>
  );
};

const mapStateToProps = ({ todoApp }) => {
  const {
    todoItems,
    searchKeyword,
    loading,
    orderColumn,
  } = todoApp;
  return {
    todoItems,
    searchKeyword,
    loading,
    orderColumn,
  };
};


export default injectIntl(
  connect(mapStateToProps, {
    getTodoListAction: getTodoList,
    getTodoListWithOrderAction: getTodoListWithOrder,
    getTodoListSearchAction: getTodoListSearch,
    selectedTodoItemsChangeAction: selectedTodoItemsChange,
  })(EventsMonitoringPage)
);
