import React from 'react';
import { injectIntl } from 'react-intl';
import { Row, Card, CardBody } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';
import PlateLogs from 'containers/pages/EntitiesPage/PlateLogs';
import FacesLogs from 'containers/pages/EntitiesPage/FacesLog';
import GradientWithRadialProgressCard from 'components/cards/GradientWithRadialProgressCard';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
} from 'react-google-maps';

import HeatmapLayer from "react-google-maps/lib/components/visualization/HeatmapLayer";

const entitiesMonitoringPage = ({ intl, match }) => {
  const { messages } = intl;

  const MapWithAMarker = withScriptjs(
    withGoogleMap(() => (
      <GoogleMap defaultZoom={15} defaultCenter={{ lat: 37.4447144485548, lng: 15.047330340065132 }}>

      </GoogleMap>
    ))
  );

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="title.monitoring.entities" match={match} />
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <Row>
        <Colxx lg="4" xl="4" xxs="12" className="mb-4">
          <GradientWithRadialProgressCard
            icon="iconsminds-male-female"
            title={messages['monitoring.people.detect']}
            detail={messages['monitoring.number.people.present']}
            percent={(5 * 100) / 12}
            progressText="5/12"
          />
        </Colxx>
        <Colxx lg="4" xl="4" xxs="12" className="mb-4">
          <GradientWithRadialProgressCard
            icon="iconsminds-car"
            title={messages['monitoring.vehicle.detect']}
            detail={messages['monitoring.number.vehicle.detect']}
            percent={(4 * 100) / 6}
            progressText="4/6"
          />
        </Colxx>
        <Colxx lg="4" xl="4" xxs="12" className="mb-4">
          <GradientWithRadialProgressCard
            icon="iconsminds-target-market"
            title={messages['monitoring.other.entities']}
            detail={messages['monitoring.number.other.entities']}
            percent={(8 * 100) / 10}
            progressText="8/10"
          />
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12" className="mb-4">
          <Card className="mb-4">
            <CardBody>
              <div style={{ height: '450px', width: '100%' }}>
                <MapWithAMarker
                  googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCO8MfadmlotuuHC8wmjwL_46I5QAMIiRU&v=3.exp&libraries=geometry,drawing,places"
                  loadingElement={<div style={{ height: `100%` }} />}
                  containerElement={<div style={{ height: `450px` }} />}
                  mapElement={<div style={{ height: `100%` }} />}
                >
                </MapWithAMarker>

              </div>
            </CardBody>
          </Card>
        </Colxx>
      </Row>

      <Row>
        <Colxx lg="6" md="6" className="mb-4">
          <PlateLogs />
        </Colxx>
        <Colxx lg="6" md="6" className="mb-4">
          <FacesLogs />
        </Colxx>
      </Row>

    </>
  );
};
export default injectIntl(entitiesMonitoringPage);
