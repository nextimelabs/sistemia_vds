/* eslint-disable react/no-array-index-key */
import React, { useState, useEffect } from 'react';
import {
  Row,
  Button,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Collapse,
} from 'reactstrap';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import IntlMessages from 'helpers/IntlMessages';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';

import {
  getTodoList,
  getTodoListWithOrder,
  getTodoListSearch,
  selectedTodoItemsChange,
} from 'redux/actions';
import AlarmListItem from 'containers/pages/AlarmsPage/AlarmListItem'
import AlarmFilterMenu from 'containers/pages/AlarmsPage/AlarmFilterMenu';
import { OrderByOptions } from 'constants/defaultValues'

/* API */
import axios from 'axios';
import { AlarmTriggeredAPI, AlarmsAPI } from 'constants/requestAPI';
import { getCurrentToken, getCurrentIsoDate, subtractDayToISODate } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';
import Pagination from 'containers/pages/Pagination';


const orderOptions = [
  { column: 'alarmId', label: <IntlMessages id="alarm.id" /> },
  { column: 'status', label: <IntlMessages id="alarm.status" /> },
];

const pageSizes = [4, 8, 12, 20];

const EventsMonitoringPage = ({
  match,
  intl,
  loading,
  getTodoListAction,
}) => {

  const [displayOptionsIsOpen, setDisplayOptionsIsOpen] = useState(false);

  /* Lista, paginazione e ricerca */
  const [items, setItems] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [totalPage, setTotalPage] = useState();
  const [selectedOrderColumnOption, setSelectedOrderColumnOption] = useState({
    column: 'alarmId',
    label: <IntlMessages id="alarm.id" />,
  });
  const [selectedOrderByOption, setSelectedOrderByOption] = useState({
    column: 'ASC',
    label: "ASC",
  });
  const [selectedItems, setSelectedItems] = useState([]);
  const [totalItemCount, setTotalItemCount] = useState(0);

  /* Filtri Menu */
  const [toTimeSelected, setToTimeSelected] = useState(new Date());
  const [fromTimeSelected, setFromTimeSelected] = useState(subtractDayToISODate(15));
  const [alarmID, setAlarmID] = useState('');
  const [triggerID, setTriggerID] = useState('');
  const [status, setStatus] = useState('');


  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderColumnOption]);

  /* Lettura allarmi eseguiti */
  useEffect(() => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    const page = currentPage > 0 ? currentPage - 1 : 0

    /* Conversione date in IsoDate */
    const fromTimeISO = new Date(fromTimeSelected).toISOString();
    const toTimeISO = new Date(toTimeSelected).toISOString();

    async function fetchData() {
      axios
        .get(
          `${AlarmTriggeredAPI}?alarmId=${alarmID}&triggerId=${triggerID}&fromTime=${fromTimeISO}&toTime=${toTimeISO}&status=${status}&orderBy=${selectedOrderByOption.label}&limit=${selectedPageSize}&offset=${page * selectedPageSize}&orderColumn=${selectedOrderColumnOption.column}`,
          config
        )
        .then((res) => {
          return res.data;
        })
        .then((data) => {
          setTotalPage(Math.ceil(data.total / data.limit));
          setItems(data.results);
          setTotalItemCount(data.total);
        })
        .catch(err => {
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
    fetchData();
  }, [selectedPageSize, currentPage, selectedOrderColumnOption, selectedOrderByOption, alarmID, triggerID, fromTimeSelected, toTimeSelected, status]);


  


  useEffect(() => {
    document.body.classList.add('right-menu');
    getTodoListAction();

    return () => {
      document.body.classList.remove('right-menu');
    };
  }, [getTodoListAction]);

  const { messages } = intl;
  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  return (
    <>
      <Row className="app-row survey-app">
        <Colxx xxs="12">
          <div className="mb-2">
            <h1>
              <IntlMessages id="title.alarms.list" />
            </h1>
            <Breadcrumb match={match} />
          </div>

          <div className="mb-2">
            <Button
              color="empty"
              className="pt-0 pl-0 d-inline-block d-md-none"
              onClick={() => setDisplayOptionsIsOpen(!displayOptionsIsOpen)}
            >
              <IntlMessages id="pages.display-options" />{' '}
              <i className="simple-icon-arrow-down align-middle" />
            </Button>
            <Collapse
              id="displayOptions"
              className="d-md-block"
              isOpen={displayOptionsIsOpen}
            >
              <div className="d-block mb-2 d-md-inline-block">
                <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1">
                  <DropdownToggle caret color="outline-dark" size="xs">
                    <IntlMessages id="text.orderby" />
                    {selectedOrderColumnOption.label}
                  </DropdownToggle>
                  <DropdownMenu>
                    {orderOptions.map((order, index) => {
                      return (
                        <DropdownItem
                          key={index}
                          onClick={() => {
                            setSelectedOrderColumnOption(
                              orderOptions.find((x) => x.column === order.column)
                            );
                          }}
                        >
                          {order.label}
                        </DropdownItem>
                      );
                    })}
                  </DropdownMenu>
                </UncontrolledDropdown>

                <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1">
                  <DropdownToggle caret color="outline-dark" size="xs">
                    <IntlMessages id="text.order" />
                    {selectedOrderByOption.label}
                  </DropdownToggle>
                  <DropdownMenu>
                    {OrderByOptions.map((order, index) => {
                      return (
                        <DropdownItem
                          key={index}
                          onClick={() => {
                            setSelectedOrderByOption(
                              OrderByOptions.find((x) => x.column === order.column)
                            );
                          }}
                        >
                          {order.label}
                        </DropdownItem>
                      );
                    })}
                  </DropdownMenu>
                </UncontrolledDropdown>

              </div>
              <div className="float-md-right pt-1">
                <span className="text-muted text-small mr-1">{`${startIndex}-${endIndex} of ${totalItemCount} `}</span>
                <UncontrolledDropdown className="d-inline-block">
                  <DropdownToggle caret color="outline-dark" size="xs">
                    {selectedPageSize}
                  </DropdownToggle>
                  <DropdownMenu right>
                    {pageSizes.map((size, index) => {
                      return (
                        <DropdownItem
                          key={index}
                          onClick={() => setSelectedPageSize(size)}
                        >
                          {size}
                        </DropdownItem>
                      );
                    })}
                  </DropdownMenu>
                </UncontrolledDropdown>
              </div>
            </Collapse>
          </div>
          <Separator className="mb-5" />
          <Row>
            {loading ? (
              items.map((item, index) => (
                <AlarmListItem
                  key={`todo_item_${index}`}
                  alarm={item}
                  isSelected={loading ? selectedItems.includes(item.id) : false}
                />
              ))
            ) : (
              <div className="loading" />
            )}
            <Pagination
              currentPage={currentPage}
              totalPage={totalPage}
              onChangePage={(i) => setCurrentPage(i)}
            />
          </Row>
        </Colxx>
      </Row>
      {loading
        &&
        <AlarmFilterMenu
          fromTimeSelected={fromTimeSelected}
          setFromTimeSelected={setFromTimeSelected}
          toTimeSelected={toTimeSelected}
          setToTimeSelected={setToTimeSelected}
          setStatus={setStatus}
          setAlarmID={setAlarmID}
          setTriggerID={setTriggerID}
        />

      }
    </>
  );
};

const mapStateToProps = ({ todoApp }) => {
  const {
    todoItems,
    searchKeyword,
    loading,
    orderColumn,
  } = todoApp;
  return {
    todoItems,
    searchKeyword,
    loading,
    orderColumn,
  };
};


export default injectIntl(
  connect(mapStateToProps, {
    getTodoListAction: getTodoList,
    getTodoListWithOrderAction: getTodoListWithOrder,
    getTodoListSearchAction: getTodoListSearch,
    selectedTodoItemsChangeAction: selectedTodoItemsChange,
  })(EventsMonitoringPage)
);
