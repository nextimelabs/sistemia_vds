import React, { useState, useEffect } from 'react';

import axios from 'axios';
import { HostServer } from 'constants/requestAPI';
import ListPageHeading from 'containers/pages/EntitiesPage/EntitiesListPageHeading';
import ListPageListing from 'containers/pages/EntitiesPage/EntitiesListListView';
import IntlMessages from 'helpers/IntlMessages';


const apiUrl = `${HostServer}/cakes/paging`;

const orderOptions = [
  { column: 'id', label: <IntlMessages id="text.id" /> },
  { column: 'date', label: <IntlMessages id="monitoring.date" /> },
];


const pageSizes = [4, 8, 12, 20];

const PlatesDetectListPage = ({ match }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'id',
    label: <IntlMessages id="text.id" />,
  });

  const [modalOpen, setModalOpen] = useState(false);
  const [totalItemCount, setTotalItemCount] = useState(0);
  const [totalPage, setTotalPage] = useState(1);
  const [search, setSearch] = useState('');
  const [items, setItems] = useState([]);
  const [lastChecked, setLastChecked] = useState(null);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);

  useEffect(() => {
    async function fetchData() {
      axios
        .get(
          `${apiUrl}?pageSize=${selectedPageSize}&currentPage=${currentPage}&orderBy=${selectedOrderOption.column}&search=${search}`
        )
        .then((res) => {
          return [
            {
              value: "FK896AE",
              reliability: "99",
              img: "/assets/img/plate-example.jpg",
              startDetectdate: "27/08/2021 14.03",
              endDetectdate: "27/08/2021 15.53"
            }
          ];
        })
        .then((data) => {
          setTotalPage(Math.ceil(data.total / data.limit));
          setItems(
            data.map((x) => {
              return { ...x, img: x.img.replace('img/', 'img/image-example/') };
            })
          );
          setTotalItemCount(data.totalItem);
          setIsLoaded(true);
        });
    }
    fetchData();
  }, [selectedPageSize, currentPage, selectedOrderOption, search]);




  const onContextMenuClick = (e, data) => {
    console.log('onContextMenuClick - selected items');
    console.log('onContextMenuClick - action : ', data.action);
  };

  const onContextMenu = (e, data) => {
    const clickedProductId = data.data;

    return true;
  };

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <div className="disable-text-selection">
        <ListPageHeading
          heading="title.plates.detect-list"
          changeOrderBy={(column) => {
            setSelectedOrderOption(
              orderOptions.find((x) => x.column === column)
            );
          }}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderOption={selectedOrderOption}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          itemsLength={items ? items.length : 0}
          onSearchKey={(e) => {
            if (e.key === 'Enter') {
              setSearch(e.target.value.toLowerCase());
            }
          }}
          orderOptions={orderOptions}
          pageSizes={pageSizes}
          toggleModal={() => setModalOpen(!modalOpen)}
        />
        <ListPageListing
          items={items}
          currentPage={currentPage}
          totalPage={totalPage}
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
          onChangePage={setCurrentPage}
        />
      </div>
    </>
  );
};

export default PlatesDetectListPage;
