import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const PlatesListPage = React.lazy(() =>
  import('./PlatesDetectListPage')
);

const FacesListPage = React.lazy(() =>
  import('./FacesDetectListPage')
);

const EntityDetailsPage = React.lazy(() =>
  import('./EntityDetailsPage')
);


const SecondMenu = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/cameras-list`} />

      <Route
        path={`${match.url}/plates`}
        render={(props) => <PlatesListPage {...props} />}
      />

      <Route
        path={`${match.url}/faces`}
        render={(props) => <FacesListPage {...props} />}
      />

      <Route
        path={`${match.url}/details`}
        render={(props) => <EntityDetailsPage {...props} />}
      />

      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default SecondMenu;
