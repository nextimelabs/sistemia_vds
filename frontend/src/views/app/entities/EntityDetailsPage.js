import React, { Component } from 'react';
import {
  Row,
  Card,
  CardBody,
  Nav,
  NavItem,
  CardImg,
  TabContent,
  TabPane,
  CardHeader,
  Button
} from 'reactstrap';
import classnames from 'classnames';
import { injectIntl } from 'react-intl';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { Separator, Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';

class FacesDetectListPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      activeTab: 'map',
      entity: props.location.entitySelected ? props.location.entitySelected : {}

    }
  }

  setLoadingStatus = (status) => {
    this.setState({
      loading: status
    })
  }

  setActiveTab = (tab) => {
    this.setState({
      activeTab: tab
    })
  }


  render() {
    const { intl, match, history } = this.props
    const { loading, activeTab, entity } = this.state
    const { messages } = intl;

    const MapWithAMarker = withScriptjs(
      withGoogleMap(() => (
        <GoogleMap defaultZoom={15} defaultCenter={{ lat: 37.4447144485548, lng: 15.047330340065132 }}>
          <Marker position={{ lat: 37.4447144485548, lng: 15.047330340065132 }} />
        </GoogleMap>
      ))
    );

    return (
      <>
        <Row>

          <Colxx xxs="12">
            <Button outline onClick={() => history.goBack()}>
              <i className="iconsminds-arrow-back-3" />
            </Button>

            <h1 style={{ marginLeft: '21px' }}>{entity.value}</h1>

            <Breadcrumb match={match} />
            <Separator className="mb-5" />

            <Row>
              <Colxx xxs="12" xl="8" className="col-left">
                <Card className="mb-4">
                  <CardBody>
                    <CardImg top alt={entity.title} src={entity.img} />
                  </CardBody>
                </Card>
                <Card className="mb-4">
                  <CardHeader>
                    <Nav tabs className="card-header-tabs ">

                      <NavItem>
                        <Button
                          className={classnames({
                            active: activeTab === 'map',
                            'nav-link': true,
                          })}
                          style={{ minWidth: '144px' }}
                          onClick={() => this.setActiveTab('map')}
                        >
                          <IntlMessages id="menu.movement-map" />
                        </Button>
                      </NavItem>

                      <NavItem>
                        <Button
                          className={classnames({
                            active: activeTab === 'details',
                            'nav-link': true,
                          })}
                          style={{ minWidth: '144px' }}
                          onClick={() => this.setActiveTab('details')}
                        >
                          <IntlMessages id="menu.details" />
                        </Button>
                      </NavItem>

                    </Nav>
                  </CardHeader>

                  <TabContent activeTab={activeTab}>

                    <TabPane tabId="map">
                      <Row>
                        <Colxx sm="12">
                          <CardBody>

                            <div style={{ height: '450px', width: '100%' }}>
                              <MapWithAMarker
                                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCO8MfadmlotuuHC8wmjwL_46I5QAMIiRU&v=3.exp&libraries=geometry,drawing,places"
                                loadingElement={<div style={{ height: `100%` }} />}
                                containerElement={<div style={{ height: `450px` }} />}
                                mapElement={<div style={{ height: `100%` }} />}
                              />
                            </div>

                          </CardBody>
                        </Colxx>
                      </Row>
                    </TabPane>

                    <TabPane tabId="details">
                      <Row>
                        <Colxx sm="12">
                          <CardBody>


                            <br />
                            <p className="font-weight-bold">
                              Phasellus Efficitur
                            </p>
                            <p>
                              Etiam tincidunt orci in nisi aliquam placerat.
                              Aliquam finibus in sem utvehicula. Morbi eget
                              consectetur leo. Quisque consectetur lectus eros,
                              sedsodales libero ornare cursus. Etiam elementum ut
                              dolor eget hendrerit.Suspendisse eu lacus eu eros
                              lacinia feugiat sit amet non purus.
                              <br />

                            </p>
                            <br />

                          </CardBody>
                        </Colxx>
                      </Row>
                    </TabPane>
                  </TabContent>
                </Card>
              </Colxx>

              <Colxx xxs="12" xl="4" className="col-right">
                <Card className="mb-4">
                  <CardBody>

                    <p className="mb-3">
                      Vivamus ultricies augue vitae commodo condimentum. Nullam
                      faucibus eros eu mauris feugiat, eget consectetur tortor
                      tempus.
                      <br />
                      <br />
                      Sed volutpat mollis dui eget fringilla. Vestibulum blandit
                      urna ut tellus lobortis tristique. Vestibulum ante ipsum
                      primis in faucibus orci luctus et ultrices posuere cubilia
                      Curae; Pellentesque quis cursus mauris.
                      <br />
                      <br />
                      Nulla non purus fermentum, pulvinar dui condimentum,
                      malesuada nibh. Sed viverra quam urna, at condimentum ante
                      viverra non. Mauris posuere erat sapien, a convallis libero
                      lobortis sit amet. Suspendisse in orci tellus.
                    </p>
                    <p className="text-muted text-small mb-2">
                      {messages['forms.tags']}
                    </p>

                  </CardBody>
                </Card>

              </Colxx>
            </Row>
          </Colxx>
        </Row>
      </>
    );
  }
};
export default injectIntl(FacesDetectListPage);
