import React, { Component } from 'react';
import { Row, Card, CardBody } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';

import { defaultCenterMap } from 'constants/defaultValues'

/* API */
import axios from 'axios';
import { CameraRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';

const MapWithAMarker = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap defaultZoom={15} defaultCenter={defaultCenterMap}>

      {props.camerasList.map(camera => {
        return (
          <Marker key={camera.id} position={{ lat: camera.latitude, lng: camera.longitude }} />
        )
      })}

    </GoogleMap>
  ))
);

class camerasMapPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      camerasList: [],
      isLoaded: false,

    }
  }

  componentDidMount() {
    this.getCameras()
  }

  setLoadingStatus = (state) => {
    this.setState({
      isLoaded: state
    })
  }

  getCameras = () => {

    this.setLoadingStatus(true)

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios
      .get(
        CameraRoot,
        config
      )
      .then((res) => {
        this.setState({
          camerasList: res.data.results,
          isLoaded: false
        })
      });
  }

  render() {

    const { match } = this.props
    const { isLoaded, camerasList } = this.state

    return (
      <>
        <Row>
          <Colxx xxs="12">
            <Breadcrumb heading="title.camera.map" match={match} />
            <Separator className="mb-5" />
          </Colxx>
        </Row>


        <Row>
          <Colxx xxs="12">
            <Card className="mb-4" >
              <CardBody>

                {camerasList.length !== 0 ?
                  <div style={{ height: '550px', width: '100%' }}>
                    <MapWithAMarker
                      googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCO8MfadmlotuuHC8wmjwL_46I5QAMIiRU&v=3.exp&libraries=geometry,drawing,places"
                      loadingElement={<div style={{ height: `100%` }} />}
                      containerElement={<div style={{ height: `550px` }} />}
                      mapElement={<div style={{ height: `100%` }} />}
                      camerasList={camerasList}
                    />

                  </div>
                  :
                  <p></p>
                }

              </CardBody>
            </Card>
          </Colxx>
        </Row>
      </>
    );
  }
};
export default camerasMapPage;
