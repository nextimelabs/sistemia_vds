import React, { Component } from 'react';

/* API */
import axios from 'axios';
import { OnvifAPI } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';

import ListPageHeading from 'containers/pages/CameraSearchPage/CameraSearchListPageHeading';
import CameraSearchListView from 'containers/pages/CameraSearchPage/CameraSearchListView';


class ScannerONVIFPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isLoaded: false,

      /* Paginazione */
      items: [],
      currentPage: 1,
      pageSizes: [4, 8, 12, 20],
      selectedPageSize: 8,
      totalItemCount: 0,
    }
  }

  componentDidMount() {
    this.startScanner()
  }

  setIsLoaded = (state) => {
    this.setState({
      isLoaded: state
    })
  }

  setSelectedPageSize = (number) => {
    this.setState({
      selectedPageSize: number
    })
  }

  setCurrentPage = (page) => {
    this.setState({
      currentPage: Number(page)
    })
  }

  /* Avvia Scanner ONVIF */
  startScanner = () => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios
      .get(
        OnvifAPI,
        config
      )
      .then((res) => {
        return res.data;
      })
      .then((data) => {

        this.setState({
          items: data,
          isLoaded: true
        })
      });
  };


  render() {

    const { match } = this.props
    const {
      currentPage, selectedPageSize, isLoaded,
      totalItemCount, items, pageSizes
    } = this.state

    /* Numero elementi visualizzati */
    const startIndex = currentPage * selectedPageSize;
    const endIndex = startIndex - selectedPageSize;
    const currentItems = items.slice(endIndex, startIndex);

    /* Paginazione elementi */
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(items.length / selectedPageSize); i += 1) {
      pageNumbers.push(i);
    }


    return (
      <>
        <ListPageHeading
          heading="title.camera.search"
          changePageSize={this.setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          itemsLength={items ? items.length : 0}
          onSearchKey={(e) => {
            if (e.key === 'Enter') {
              this.setSearch(e.target.value.toLowerCase());
            }
          }}
          pageSizes={pageSizes}
        />

        {!isLoaded ?
          <div className="loading" />
          :
          <div className="disable-text-selection">


            <CameraSearchListView
              items={currentItems}
              currentPage={currentPage}
              totalPage={pageNumbers.length}
              onChangePage={this.setCurrentPage}
            />
          </div>
        }
      </>

    );
  }
};

export default ScannerONVIFPage;
