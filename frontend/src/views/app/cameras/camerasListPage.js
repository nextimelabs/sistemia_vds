import React, { useState, useEffect } from 'react';

/* API */
import axios from 'axios';
import { CameraRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';

import ListPageHeading from 'containers/pages/CamerasPage/CameraListPageHeading';
import NewCameraModalView from 'containers/pages/CamerasPage/NewCameraModalView';
import UpdateCameraModal from 'containers/pages/CamerasPage/UpdateCameraModal';
import DeleteCameraModal from 'containers/pages/CamerasPage/DeleteCameraModal';

import CameraListView from 'containers/pages/CamerasPage/CameraListView';
import useMousetrap from 'hooks/use-mousetrap';
import IntlMessages from 'helpers/IntlMessages';


const orderCameraListOptions = [
  { column: 'id', label: <IntlMessages id="alarm.id" /> },
  { column: 'status', label: <IntlMessages id="alarm.status" /> },
  { column: 'createdAt', label: <IntlMessages id="alarm.creation-date" /> },
];

const pageSizes = [2, 4, 8, 12, 20];

const CameraListPage = ({ match }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [displayMode, setDisplayMode] = useState('list');
  const [currentPage, setCurrentPage] = useState(0);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'id',
    label: <IntlMessages id="alarm.id" />,
  });

  const [modalNewCameraOpen, setModalNewCameraOpen] = useState(false);
  const [modalUpdateCameraOpen, setModalUpdateCameraOpen] = useState(false);
  const [modalDeleteCameraOpen, setModalDeleteCameraOpen] = useState(false);
  const [cameraSelectedID, setCameraSelectedID] = useState(-1);

  const [totalItemCount, setTotalItemCount] = useState(1);
  const [totalPage, setTotalPage] = useState(0);
  const [search, setSearch] = useState('');

  const [selectedItems, setSelectedItems] = useState([]);
  const [items, setItems] = useState([]);
  const [lastChecked, setLastChecked] = useState(null);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);


  /* Lettura elenco telecamere */
  useEffect(() => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    const page = currentPage > 0 ? currentPage - 1 : 0

    async function fetchData() {
      axios
        .get(
          `${CameraRoot}?limit=${selectedPageSize}&offset=${page * selectedPageSize}&orderColumn=${selectedOrderOption.column}&search=${search}`,
          config
        )
        .then((res) => {
          return res.data;
        })
        .then((data) => {
          setTotalPage(Math.ceil(data.total / data.limit));
          setItems(data.results);
          setSelectedItems([]);
          setTotalItemCount(data.total);
          setIsLoaded(true);
        });
    }
    fetchData();
  }, [selectedPageSize, currentPage, selectedOrderOption, search]);


  /* SELEZIONA VOCI */
  const getIndex = (value, arr, prop) => {
    for (let i = 0; i < arr.length; i += 1) {
      if (arr[i][prop] === value) {
        return i;
      }
    }
    return -1;
  };

  const onCheckItem = (event, id) => {  

  console.log("TESTAPP onCheckItem", id)
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastChecked === null) {
      setLastChecked(id);
    }

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...items];
      const start = getIndex(id, newItems, 'id');
      const end = getIndex(lastChecked, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= items.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  /* COMANDI MOUSE */
  const onContextMenuClick = (e, data) => {

    switch (data.action) {
      case 'update':
        setModalUpdateCameraOpen(!modalUpdateCameraOpen)
        break;
      case 'delete':
        setModalDeleteCameraOpen(!modalDeleteCameraOpen)
        break;
      default:
    }
  };

  const onContextMenu = (e, data) => {
    const clickedCameraId = data.data;
    setCameraSelectedID(clickedCameraId)
    return true;
  };

  useMousetrap(['ctrl+a', 'command+a'], () => {
    handleChangeSelectAll(false);
  });

  useMousetrap(['ctrl+d', 'command+d'], () => {
    setSelectedItems([]);
    return false;
  });

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <div className="disable-text-selection">
        <ListPageHeading
          heading="title.camera.list"
          displayMode={displayMode}
          changeDisplayMode={setDisplayMode}
          handleChangeSelectAll={handleChangeSelectAll}
          changeOrderBy={(column) => {
            setSelectedOrderOption(
              orderCameraListOptions.find((x) => x.column === column)
            );
          }}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderOption={selectedOrderOption}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          selectedItemsLength={selectedItems ? selectedItems.length : 0}
          itemsLength={items ? items.length : 0}
          onSearchKey={(e) => {
            if (e.key === 'Enter') {
              setSearch(e.target.value.toLowerCase());
            }
          }}
          orderOptions={orderCameraListOptions}
          pageSizes={pageSizes}
          actionHeaderButton={() => setModalNewCameraOpen(!modalNewCameraOpen)}
          setModalDeleteCameraOpen = {() => setModalDeleteCameraOpen(!modalDeleteCameraOpen)}
        />
        <NewCameraModalView
          modalOpen={modalNewCameraOpen}
          toggleModal={() => setModalNewCameraOpen(!modalNewCameraOpen)}
        />
        <UpdateCameraModal
          modalOpen={modalUpdateCameraOpen}
          toggleModal={() => setModalUpdateCameraOpen(!modalUpdateCameraOpen)}
          cameraSelectedID={cameraSelectedID}
        />

        <DeleteCameraModal
          modalOpen={modalDeleteCameraOpen}
          toggleModal={() => setModalDeleteCameraOpen(!modalDeleteCameraOpen)}
          camerasIdSelected = {selectedItems}
        />

        <CameraListView
          items={items}
          displayMode={displayMode}
          selectedItems={selectedItems}
          currentPage={currentPage}
          totalPage={totalPage}
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
          onChangePage={setCurrentPage}
          toggleUpdateCameraModal={setModalUpdateCameraOpen}
          toggleDeleteCameraModal={setModalDeleteCameraOpen}
          toggleSetCameraID={setCameraSelectedID}
          onCheckItem={onCheckItem}
        />
      </div>
    </>
  );
};

export default CameraListPage;
