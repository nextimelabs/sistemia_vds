import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const CamerasList = React.lazy(() =>
  import('./camerasListPage')
);

const CamerasMap = React.lazy(() =>
  import('./camerasMapPage')
);

const CamerasGroups = React.lazy(() =>
  import('./camerasGroupsListPage')
);

const CamerasSearch = React.lazy(() =>
  import('./ScannerONVIFPage')
);

const CamerasDetails = React.lazy(() =>
  import('./CameraDetailsOnvifPage')
);


const SecondMenu = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/cameras-list`} />
      <Route
        path={`${match.url}/cameras-list`}
        render={(props) => <CamerasList {...props} />}
      />
         <Route
        path={`${match.url}/cameras-map`}
        render={(props) => <CamerasMap {...props} />}
      />
       <Route
        path={`${match.url}/cameras-groups`}
        render={(props) => <CamerasGroups {...props} />}
      />
        <Route
        path={`${match.url}/cameras-scan`}
        render={(props) => <CamerasSearch {...props} />}
      />

       <Route
        path={`${match.url}/cameras-details`}
        render={(props) => <CamerasDetails {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default SecondMenu;
