import React, { useState, useEffect } from 'react';

/* API */
import axios from 'axios';
import { GroupsCameraRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';

import ListPageHeading from 'containers/pages/GroupsCameraPage/GroupsListPageHeading';
import NewGroupModal from 'containers/pages/GroupsCameraPage/AddNewGroupModal';
import UpdateGroupModal from 'containers/pages/GroupsCameraPage/UpdateGroupModal';
import DeleteGroupModal from 'containers/pages/GroupsCameraPage/DeleteGroupModal';

import GroupListView from 'containers/pages/GroupsCameraPage/GroupListView';
import useMousetrap from 'hooks/use-mousetrap';
import IntlMessages from 'helpers/IntlMessages';


const orderGroupListOptions = [
  { column: 'id', label: <IntlMessages id="group.id" /> },
  { column: 'alias', label: <IntlMessages id="group.name" /> },
  { column: 'createdAt', label: <IntlMessages id="group.creation-date" /> },
];

const pageSizes = [2, 4, 8, 12, 20];

const GroupListPage = ({ match }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'id',
    label: <IntlMessages id="group.id" />,
  });

  const [modalNewGroupOpen, setModalNewGroupOpen] = useState(false);
  const [modalUpdateGroupOpen, setModalUpdateGroupOpen] = useState(false);
  const [modalDeleteGroupOpen, setModalDeleteGroupOpen] = useState(false);
  const [groupSelectedID, setGroupSelectedID] = useState(-1);

  const [totalItemCount, setTotalItemCount] = useState(1);
  const [totalPage, setTotalPage] = useState(0);
  const [search, setSearch] = useState('');
  const [selectedItems, setSelectedItems] = useState([]);
  const [items, setItems] = useState([]);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);


  /* Lettura elenco telecamere */
  useEffect(() => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    async function fetchData() {
      axios
        .get(
          `${GroupsCameraRoot}?limit=${selectedPageSize}&offset=${(currentPage - 1) * selectedPageSize}&orderColumn=${selectedOrderOption.column}&search=${search}`,
          config
        )
        .then((res) => {
          return res.data;
        })
        .then((data) => {
          setTotalPage(Math.ceil(data.total / data.limit));
          setItems(data.results);
          setSelectedItems([]);
          setTotalItemCount(data.total);
          setIsLoaded(true);
        });
    }
    fetchData();
  }, [selectedPageSize, currentPage, selectedOrderOption, search]);


  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= items.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };
  

  const onContextMenuClick = (e, data) => {

    switch (data.action) {
      case 'update':
        setModalUpdateGroupOpen(!modalUpdateGroupOpen)
        break;
      case 'delete':
        setModalDeleteGroupOpen(!modalDeleteGroupOpen)
        break;
      default:
    }
  };

  const onContextMenu = (e, data) => {
    const clickedCameraId = data.data;
    setGroupSelectedID(clickedCameraId)
    return true;
  };

  useMousetrap(['ctrl+a', 'command+a'], () => {
    handleChangeSelectAll(false);
  });

  useMousetrap(['ctrl+d', 'command+d'], () => {
    setSelectedItems([]);
    return false;
  });

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <div className="disable-text-selection">
        <ListPageHeading
          heading="title.camera.group"
          handleChangeSelectAll={handleChangeSelectAll}
          changeOrderBy={(column) => {
            setSelectedOrderOption(
              orderGroupListOptions.find((x) => x.column === column)
            );
          }}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderOption={selectedOrderOption}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          selectedItemsLength={selectedItems ? selectedItems.length : 0}
          itemsLength={items ? items.length : 0}
          onSearchKey={(e) => {
            if (e.key === 'Enter') {
              setSearch(e.target.value.toLowerCase());
            }
          }}
          orderOptions={orderGroupListOptions}
          pageSizes={pageSizes}
          actionHeaderButton={() => setModalNewGroupOpen(!modalNewGroupOpen)}
        />
        <NewGroupModal
          modalOpen={modalNewGroupOpen}
          toggleModal={() => setModalNewGroupOpen(!modalNewGroupOpen)}
        />
        <UpdateGroupModal
          modalOpen={modalUpdateGroupOpen}
          toggleModal={() => setModalUpdateGroupOpen(!modalUpdateGroupOpen)}
          groupSelectedID={groupSelectedID}
        />

        <DeleteGroupModal
          modalOpen={modalDeleteGroupOpen}
          toggleModal={() => setModalDeleteGroupOpen(!modalDeleteGroupOpen)}
          groupSelectedID={groupSelectedID}
        />

          <GroupListView
          items={items}
          selectedItems={selectedItems}
          currentPage={currentPage}
          totalPage={totalPage}
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
          onChangePage={setCurrentPage}
          toggleUpdateGroupModal={setModalUpdateGroupOpen}
          toggleDeleteGroupModal={setModalDeleteGroupOpen}
          toggleSetGroupID={setGroupSelectedID}
        />

 
      </div>
    </>
  );
};

export default GroupListPage;
