import React, { Component } from 'react';
import {
  Row,
  Card,
  CardTitle,
  CardBody,
  Nav,
  NavItem,
  Button,
  TabContent,
  TabPane,
  Label,
  CardHeader,
  Spinner,
  Table,
} from 'reactstrap';
import classnames from 'classnames';
import { injectIntl } from 'react-intl';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { Separator, Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import CameraOnvifAuthModalView from 'containers/pages/CameraSearchPage/CameraOnvifAuthModalView';

import {
  AvField,
  AvForm,
  AvGroup,
  AvInput,
  AvFeedback,
} from 'availity-reactstrap-validation';
import Switch from 'rc-switch';
import { SliderMegaByteyteTooltips } from 'components/common/SliderMegaByteyteTooltips';

import { CameraRoot, GroupsCameraRoot, StreamerAPI, RecorderAPI, OnvifAPI } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';

import axios from 'axios';

import { FPSOptions, TimeSegmentLimitOptions } from 'constants/defaultValues'

class CameraDetailsOnvifPage extends Component {


  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      activeTab: 'details',

      /* Camera onvif data */
      MainGroupList: [],
      ChildGroupList: [],
      cameraOnvifAuthModalOpen: false,
      infoCameraOnvif: undefined,
      groupIDSelected: -1,

      /* New camera settings */
      infoNewCamera: null,
      isAudioEnabled: false,
      isRecorderEnabled: false,
      isStreamingEnabled: false,
      segmentsSizeLimit: 0,
    }
  }

  componentDidMount() {
    this.openOnvifAuth()
    this.getMainGroupList()
  }

  setLoadingStatus = (status) => {
    this.setState({
      loading: status
    })
  }

  setAudioSetting = (status) => {
    this.setState({
      isAudioEnabled: status
    })
  }

  setRecorderSetting = (status) => {
    this.setState({
      isRecorderEnabled: status
    })
  }

  setStreamingSetting = (status) => {
    this.setState({
      isStreamingEnabled: status
    })
  }

  setSegmentsSizeLimit = (value) => {
    this.setState({
      segmentsSizeLimit: value
    })
  }


  setActiveTab = (tab) => {
    this.setState({
      activeTab: tab
    })
  }

  setCameraOnvifAuthModalOpen = (status) => {
    this.setState({
      cameraOnvifAuthModalOpen: status
    })

  }

  setInfoCameraOnvif = (onvifInfo) => {
    this.setState({
      infoCameraOnvif: onvifInfo
    })
  }

  openOnvifAuth = () => {


    this.setLoadingStatus()

    const { location } = this.props;

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.get(
      `${OnvifAPI}/info?xaddr=${location.xaddr}&user=admin&password=`,
      config
    ).then(response => {

      if (response.status === 200) {
        this.setInfoCameraOnvif(response.data)
        this.setLoadingStatus();
      }
    }
    )
      .catch(err => {
        this.setLoadingStatus()
        this.setCameraOnvifAuthModalOpen(true);
        throw err;
      });
  }

  setGroupIDSelected = (groupID) => {
    this.setState({
      groupIDSelected: groupID
    })
  }

  updateCameraSettings = (event, errors, values) => {

    const { isStreamingEnabled, isRecorderEnabled } = this.state

    if (errors.length === 0 && event.type === "submit") {
      if (isStreamingEnabled) this.updateStreamerModule(values)
      if (isRecorderEnabled) this.updateRecorderModule(values)
    }
  }

  getMainGroupList = () => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    axios.get(
      `${GroupsCameraRoot}?search=`,
      config
    ).then((res) => {
      return res.data;
    })
      .then((data) => {
        this.setState({
          MainGroupList: data.results,
        })
      }
      )
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  getGroupChild = (groupId) => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    this.setGroupIDSelected(groupId)

    axios.get(
      `${GroupsCameraRoot}/${groupId}/child?id=${groupId}&search=`,
      config
    ).then((res) => {
      return res.data;
    })
      .then((data) => {
        this.setState({
          ChildGroupList: data.results,
        })
      })
      .catch(err => {
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  addNewCamera = (event, errors, values) => {

    const { groupIDSelected } = this.state

    if (errors.length === 0 && event.type === "submit") {

      this.setLoadingStatus(true)

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };

      const bodyParameters = {
        "alias": values.inputAlias,
        "camSourcePath": values.inputCamSourcePath,
        "groupId": Number(groupIDSelected)
      };

      axios.post(
        CameraRoot,
        bodyParameters,
        config
      ).then(response => {

        if (response.status === 201) {

          this.setState({
            infoNewCamera: response.data,
            isRecorderEnabled: response.data.recorderOptions ? response.data.recorderOptions.audio : false,
            isAudioEnabled: response.data.recorderOptions ? response.data.recorderOptions.audio : false,
            isStreamingEnabled: response.data.streamerOptions ? response.data.streamerOptions.enabled : false,
          }, () => {
            this.getGroupChild(response.data.group.id)
            this.setLoadingStatus(false);

            NotificationManager.success(
              <IntlMessages id="camera.creation.message" />,
              <IntlMessages id="camera.creation.title" />,
              5000,
              () => {
                alert('callback');
              },
              null,
              "mb-3"
            );

          })
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus(false)
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
  }

  updateStreamerModule = (values) => {
    this.setLoadingStatus(true);

    const { infoNewCamera, isStreamingEnabled, isRecorderEnabled } = this.state

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    const bodyParameters = {
      "fps": values.inputStreamerFTP,
      "enabled": isStreamingEnabled,
    };

    axios.put(
      `${StreamerAPI}/${infoNewCamera.id}?id=${infoNewCamera.id}`,
      bodyParameters,
      config
    ).then(response => {

      if (response.status === 200) {
        if (!isRecorderEnabled) {
          this.setLoadingStatus(false);

          NotificationManager.success(
            <IntlMessages id="streaming.update.message" />,
            <IntlMessages id="streaming.update.title" />,
            5000,
            () => {
              alert('callback');
            },
            null,
            "mb-3"
          );

          window.location.reload();
        }
      }
    }
    )
      .catch(err => {
        this.setLoadingStatus(false)
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }

  updateRecorderModule = (values) => {
    this.setLoadingStatus(true);

    const { infoNewCamera, isRecorderEnabled, isStreamingEnabled, isAudioEnabled, segmentsSizeLimit } = this.state

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    const bodyParameters = {
      "fps": values.inputRecorderFPS,
      "timeSegmentLimit": values.inputTimeSegmentLimit,
      "segmentsSizeLimit": segmentsSizeLimit,
      "segmentsFolder": values.inputSegmentsFolder,
      "audio": isAudioEnabled,
      "enabled": isRecorderEnabled,
    };

    axios.put(
      `${RecorderAPI}/${infoNewCamera.id}?id=${infoNewCamera.id}`,
      bodyParameters,
      config
    ).then(response => {

      if (response.status === 200) {
        this.setLoadingStatus(false);

        NotificationManager.success(
          <IntlMessages id="recorder.update.message" />,
          <IntlMessages id="recorder.update.title" />,
          5000,
          () => {
            alert('callback');
          },
          null,
          "mb-3"
        );
        if (!isStreamingEnabled) {
          window.location.reload();
        }
      }
    }
    )
      .catch(err => {
        this.setLoadingStatus(false)
        const errorMessage = err.response.data.message
        NotificationManager.error(
          <IntlMessages id={errorMessage} />,
          <IntlMessages id="text.error" />,
          5000,
          null,
          "mb-3"
        );
        throw err;
      });
  }


  render() {
    const { match, intl, history, location } = this.props

    const {
      activeTab, cameraOnvifAuthModalOpen,
      infoCameraOnvif, infoNewCamera,
      loading, isAudioEnabled,
      isRecorderEnabled, isStreamingEnabled,
      MainGroupList, ChildGroupList
    } = this.state

    const { messages } = intl;

    return (
      <>
        {infoCameraOnvif ?
          <Row>
            <Colxx xxs="12">

              <Button outline onClick={() => history.goBack()}>
                <i className="iconsminds-arrow-back-3" />
              </Button>

              <h1 style={{ marginLeft: '21px' }}>{infoCameraOnvif.manufacturer} - {infoCameraOnvif.model}</h1>

              <Breadcrumb match={match} />
              <Separator className="mb-5" />

              <Row>
                <Colxx xxs="12" xl="8" className="col-left">
                  <Card className="mb-4">
                    <CardHeader>
                      <Nav tabs className="card-header-tabs ">
                        <NavItem>
                          <Button
                            className={classnames({
                              active: activeTab === 'details',
                              'nav-link': true,
                            })}
                            style={{ minWidth: '144px' }}
                            onClick={() => this.setActiveTab('details')}
                          >
                            <IntlMessages id="camera.info" />
                          </Button>
                        </NavItem>

                      </Nav>
                    </CardHeader>

                    <TabContent activeTab={activeTab}>
                      <TabPane tabId="details">
                        <Row>
                          <Colxx sm="12">
                            <CardBody>
                              <br />
                              <Table borderless>

                                <tbody>
                                  <tr>
                                    <th scope="row"><IntlMessages id="camera.manufacturer" /></th>
                                    <td>{infoCameraOnvif ? infoCameraOnvif.manufacturer : ""}</td>
                                  </tr>

                                  <tr>
                                    <th scope="row"><IntlMessages id="camera.model" /></th>
                                    <td>{infoCameraOnvif ? infoCameraOnvif.model : ""}</td>
                                  </tr>

                                  <tr>
                                    <th scope="row"><IntlMessages id="camera.firmwareVersion" /></th>
                                    <td>{infoCameraOnvif ? infoCameraOnvif.firmwareVersion : ""}</td>
                                  </tr>

                                  <tr>
                                    <th scope="row"><IntlMessages id="camera.serialNumber" /></th>
                                    <td>{infoCameraOnvif ? infoCameraOnvif.serialNumber : ""}</td>
                                  </tr>

                                  <tr>
                                    <th scope="row"><IntlMessages id="camera.hardwareId" /></th>
                                    <td>{infoCameraOnvif ? infoCameraOnvif.hardwareId : ""}</td>
                                  </tr>

                                  <tr>
                                    <th scope="row"><IntlMessages id="camera.streamUrl" /></th>
                                    <td>{infoCameraOnvif ? infoCameraOnvif.streamUrl : ""}</td>
                                  </tr>

                                </tbody>
                              </Table>
                            </CardBody>
                          </Colxx>
                        </Row>
                      </TabPane>
                    </TabContent>
                  </Card>
                </Colxx>

                <Colxx xxs="12" xl="4" className="col-right">
                  <Card className="mb-4">

                    <CardBody >

                      <CardTitle>
                        <IntlMessages id="add.camera-system" />
                      </CardTitle>

                      <AvForm
                        className="av-tooltip tooltip-label-right"
                        onSubmit={(event, errors, values) => this.addNewCamera(event, errors, values)}
                      >

                        <AvGroup >
                          <Label> <IntlMessages id="camera.name" /></Label>
                          <AvInput name="inputAlias" required disabled={infoNewCamera} />
                          <AvFeedback><IntlMessages id="camera.required.name" /></AvFeedback>
                        </AvGroup>

                        <AvGroup >
                          <Label> <IntlMessages id="camera.camSourcePath" /></Label>
                          <AvInput name="inputCamSourcePath" disabled value={infoCameraOnvif ? infoCameraOnvif.streamUrl : ""} required />
                          <AvFeedback><IntlMessages id="camera.required.camSourcePath" /></AvFeedback>
                        </AvGroup>

                        <AvField
                          type="select"
                          name="inputGroupId"
                          required
                          disabled={infoNewCamera}
                          label={<IntlMessages id="group.select" />}
                          errorMessage={messages['camera.required.maingroup']}
                          onChange={e => this.getGroupChild(e.target.value)}
                        >
                          <option key={-1} value={-1}></option>
                          {
                            MainGroupList.map(item => {
                              return (
                                <option key={item.id} value={item.id}>{item.name}</option>
                              )
                            })
                          }
                        </AvField>



                        <AvField
                          type="select"
                          name="inputCameraChildGroup"
                          disabled={infoNewCamera}
                          label={<IntlMessages id="group.child-select" />}
                          onChange={e => this.setGroupIDSelected(e.target.value)}
                        >
                          <option key={-1} value={-1}></option>

                          {
                            ChildGroupList.map(item => {
                              return (
                                <option key={item.id} value={item.id}>{item.name}</option>
                              )
                            })
                          }
                        </AvField>

                        <div style={{ textAlign: 'center' }}>
                          {loading ?
                            <Spinner type="grow" color="primary" />
                            :
                            <>
                              {!infoNewCamera ?
                                <Button color="primary" type="Submit">
                                  <IntlMessages id="button.add" />
                                </Button>
                                :
                                null
                              }
                            </>
                          }

                        </div>
                      </AvForm>



                    </CardBody>
                  </Card>
                  {infoNewCamera ?

                    <Card className="mb-4">
                      <CardBody>

                        <CardTitle>
                          <IntlMessages id="settings.camera" />
                        </CardTitle>



                        <AvForm
                          className="av-tooltip tooltip-label-right"
                          onSubmit={(event, errors, values) => this.updateCameraSettings(event, errors, values)}
                        >

                          <AvGroup>
                            <Label style={{ marginTop: '21px' }}>
                              <IntlMessages id="settings.enabled-streaming" />
                            </Label>
                            <Switch
                              className="custom-switch custom-switch-primary"
                              checked={isStreamingEnabled}
                              onChange={(primary) => this.setStreamingSetting(primary)}
                            />
                          </AvGroup>

                          {isStreamingEnabled ?
                            <>
                              <h6 style={{ marginBottom: '21px', marginTop: '34px' }}> <IntlMessages id="settings.streamingOptions" /></h6>

                              <AvGroup >
                                <AvField
                                  type="select"
                                  name="inputStreamerFTP"
                                  required
                                  label={<IntlMessages id="settings.fps" />}
                                  errorMessage="Please select an option!"
                                  value={infoNewCamera.streamerOptions.fps || ""}
                                >
                                  {
                                    FPSOptions.map(item => {
                                      return (
                                        <option key={item.key} value={item.value}>{item.label}</option>
                                      )
                                    })
                                  }
                                </AvField>
                              </AvGroup>
                            </>

                            :
                            null
                          }

                          <AvGroup>
                            <Label style={{ marginTop: '21px' }}>
                              <IntlMessages id="settings.enabled-recorder" />
                            </Label>
                            <Switch
                              className="custom-switch custom-switch-primary"
                              checked={isRecorderEnabled}
                              onChange={(primary) => this.setRecorderSetting(primary)}
                            />
                          </AvGroup>

                          {isRecorderEnabled ?
                            <>
                              <h6 style={{ marginBottom: '21px', marginTop: '34px' }}> <IntlMessages id="settings.recorderOptions" /></h6>

                              <AvGroup >
                                <Label> <IntlMessages id="settings.segmentsFolder" /></Label>
                                <AvInput name="inputSegmentsFolder" required value={infoNewCamera.recorderOptions ? infoNewCamera.recorderOptions.segmentsFolder || "" : ""} />
                                <AvFeedback>folder name is required!</AvFeedback>
                              </AvGroup>

                              <AvGroup >
                                <AvField
                                  type="select"
                                  name="inputRecorderFPS"
                                  required
                                  label={<IntlMessages id="settings.fps" />}
                                  errorMessage="Please select an option!"
                                  value={infoNewCamera.recorderOptions ? infoNewCamera.recorderOptions.fps || "" : ""}
                                >
                                  {
                                    FPSOptions.map(item => {
                                      return (
                                        <option key={item.key} value={item.value}>{item.label}</option>
                                      )
                                    })
                                  }
                                </AvField>
                              </AvGroup>

                              <AvGroup >
                                <AvField
                                  type="select"
                                  name="inputTimeSegmentLimit"
                                  required
                                  label={<IntlMessages id="settings.timeSegmentLimit" />}
                                  errorMessage="Please select an option!"
                                  value={infoNewCamera.recorderOptions ? infoNewCamera.recorderOptions.timeSegmentLimit || "" : ""}
                                  onChange={e => this.getGroupChild(e.target.value)}
                                >
                                  {
                                    TimeSegmentLimitOptions.map(item => {
                                      return (
                                        <option key={item.key} value={item.value}>{item.label}</option>
                                      )
                                    })
                                  }
                                </AvField>
                              </AvGroup>

                              <AvGroup>

                                <Label>
                                  <IntlMessages id="settings.segmentsSizeLimit" />
                                </Label>
                                <SliderMegaByteyteTooltips
                                  min={0}
                                  max={2500}
                                  defaultValue={infoNewCamera.recorderOptions ? infoNewCamera.recorderOptions.segmentsSizeLimit / 1024 || 0 : 0}
                                  className="mb-5"
                                  step={1}
                                  onChange={() => this.setSegmentsSizeLimit()}
                                />
                              </AvGroup>

                              <AvGroup>
                                <Label style={{ marginTop: '21px' }}>
                                  <IntlMessages id="settings.audio" />
                                </Label>
                                <Switch
                                  className="custom-switch custom-switch-primary"
                                  checked={isAudioEnabled}
                                  onChange={(primary) => this.setAudioSetting(primary)}
                                />
                              </AvGroup>

                            </>
                            :
                            null
                          }
                          <div style={{ textAlign: 'center', marginTop: '21px' }}>

                            {loading ?
                              <Spinner type="grow" color="primary" />
                              :
                              <Button color="primary" type="Submit">
                                <IntlMessages id="button.update" />
                              </Button>
                            }
                          </div>
                        </AvForm>



                      </CardBody>
                    </Card>
                    :
                    null
                  }
                </Colxx>
              </Row>
            </Colxx>
          </Row>
          :
          null
        }
        <CameraOnvifAuthModalView
          modalOpen={cameraOnvifAuthModalOpen}
          toggleModal={() => history.goBack()}
          cameraSelectedXaddr={location.xaddr}
          setInfoCameraOnvif={this.setInfoCameraOnvif}
        />

      </>
    );
  }
};
export default injectIntl(CameraDetailsOnvifPage);
