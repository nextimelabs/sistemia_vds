import React, { useState, useEffect } from 'react';

/* API */
import axios from 'axios';
import { UserRoot } from 'constants/requestAPI';
import { getCurrentToken } from 'helpers/Utils';

import ListPageHeading from 'containers/pages/UserPage/UserListPageHeading';
import NewUserModal from 'containers/pages/UserPage/AddNewUserModal';
import UpdateUserModal from 'containers/pages/UserPage/UpdateUserModal';
import DeleteUserModal from 'containers/pages/UserPage/DeleteUserModal';
import IntlMessages from 'helpers/IntlMessages';
import UserListPageListing from 'containers/pages/UserPage/UserListView';
import useMousetrap from 'hooks/use-mousetrap';

const getIndex = (value, arr, prop) => {
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i][prop] === value) {
      return i;
    }
  }
  return -1;
};

const orderOptions = [
  { column: 'id', label: <IntlMessages id="user.id" /> },
  { column: 'username', label: <IntlMessages id="user.name" /> },
  { column: 'role', label: <IntlMessages id="user.role" /> },
  { column: 'status', label: <IntlMessages id="user.status" /> },
  { column: 'createdAt', label: <IntlMessages id="text.creation-date" /> }
];

const pageSizes = [2, 4, 8, 12, 20];


const UserListPage = ({ match }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'id',
    label: <IntlMessages id="user.id" />,
  });

  const [modalNewUserOpen, setModalNewUserOpen] = useState(false);
  const [modalUpdateUserOpen, setModalUpdateUserOpen] = useState(false);
  const [modalDeleteUserOpen, setModalDeleteUserOpen] = useState(false);
  console.log("TESTAPP", modalUpdateUserOpen, modalDeleteUserOpen )
  const [userSelectedID, setUserSelectedID] = useState(-1);

  const [totalItemCount, setTotalItemCount] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [search, setSearch] = useState('');
  const [selectedItems, setSelectedItems] = useState([]);
  const [items, setItems] = useState([]);
  const [lastChecked, setLastChecked] = useState(null);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);


  /* Lettura elenco utenti */
  useEffect(() => {

    const config = {
      headers: { Authorization: `Bearer ${getCurrentToken()}` }
    };

    async function fetchData() {
      axios
        .get(
          `${UserRoot}?limit=${selectedPageSize}&offset=${(currentPage - 1) * selectedPageSize}&orderColumn=${selectedOrderOption.column}&search=${search}`,
          config
        )
        .then((res) => {
          return res.data;
        })
        .then((data) => {
          setTotalPage(Math.ceil(data.total / data.limit));
          setItems(data.results);
          setSelectedItems([]);
          setTotalItemCount(data.total);
          setIsLoaded(true);
        });
    }
    fetchData();
  }, [selectedPageSize, currentPage, selectedOrderOption, search]);

  const onCheckItem = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastChecked === null) {
      setLastChecked(id);
    }

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...items];
      const start = getIndex(id, newItems, 'id');
      const end = getIndex(lastChecked, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= items.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onContextMenuClick = (e, data) => {
    switch (data.action) {
      case 'update':
        setModalUpdateUserOpen(!modalUpdateUserOpen)
        break;
      case 'delete':
        setModalDeleteUserOpen(!modalDeleteUserOpen)
        break;
      default:
    }
  };

  const onContextMenu = (e, data) => {
    const clickedUserId = data.data;
    setUserSelectedID(clickedUserId)
    return true;
  };

  useMousetrap(['ctrl+a', 'command+a'], () => {
    handleChangeSelectAll(false);
  });

  useMousetrap(['ctrl+d', 'command+d'], () => {
    setSelectedItems([]);
    return false;
  });

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <div className="disable-text-selection">
        <ListPageHeading
          heading="title.user.list"
          handleChangeSelectAll={handleChangeSelectAll}
          changeOrderBy={(column) => {
            setSelectedOrderOption(
              orderOptions.find((x) => x.column === column)
            );
          }}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderOption={selectedOrderOption}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          selectedItemsLength={selectedItems ? selectedItems.length : 0}
          itemsLength={items ? items.length : 0}
          onSearchKey={(e) => {
            if (e.key === 'Enter') {
              setSearch(e.target.value.toLowerCase());
            }
          }}
          orderOptions={orderOptions}
          pageSizes={pageSizes}
          actionHeaderButton={() => setModalNewUserOpen(!modalNewUserOpen)}
          actionDeleteUser = {setModalDeleteUserOpen}
        />
        <NewUserModal
          modalOpen={modalNewUserOpen}
          toggleModal={() => setModalNewUserOpen(!modalNewUserOpen)}
        />
        <UpdateUserModal
          modalOpen={modalUpdateUserOpen}
          toggleModal={() => setModalUpdateUserOpen(!modalUpdateUserOpen)}
          userSelectedID={userSelectedID}
        />
        <DeleteUserModal
          modalOpen={modalDeleteUserOpen}
          toggleModal={() => setModalDeleteUserOpen(!modalDeleteUserOpen)}
          usersIdSelected={selectedItems}
        />
        <UserListPageListing
          items={items}
          selectedItems={selectedItems}
          onCheckItem={onCheckItem}
          currentPage={currentPage}
          totalPage={totalPage}
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
          onChangePage={setCurrentPage}
          toggleUpdateUserModal={setModalUpdateUserOpen}
          toggleDeleteUserModal={setModalDeleteUserOpen}
          toggleSetUserID={setUserSelectedID}
        />
      </div>
    </>
  );
};

export default UserListPage;
