import React, { Component } from 'react';
import {
  Row,
  Card,
  CardBody,
  CardFooter,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Label,
  Button,
  Spinner
} from 'reactstrap';
import {
  AvForm,
  AvField,
  AvGroup,
  AvInput,
  AvFeedback,
  AvRadioGroup,
  AvRadio,
} from 'availity-reactstrap-validation';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { Colxx } from 'components/common/CustomBootstrap';
import SingleLightbox from 'components/pages/SingleLightbox';

import IntlMessages from 'helpers/IntlMessages';
import { UserRoleList } from 'constants/defaultValues'
import { UserRoot } from 'constants/requestAPI';
import { getCurrentToken, getCurrentUser } from 'helpers/Utils';
import { NotificationManager } from 'components/common/react-notifications';
import axios from 'axios';


class ProfileSocial extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,

      /* User info */
      infoUser: getCurrentUser()

    }
  }


  setLoadingStatus = () => {
    this.setState(prev => ({
      loading: !prev.loading
    }))
  }

  updateUser = (event, errors, values) => {

    const { infoUser } = this.state

    if (errors.length === 0 && event.type === "submit") {

      this.setLoadingStatus()

      const config = {
        headers: { Authorization: `Bearer ${getCurrentToken()}` }
      };


      const bodyParameters = {
        "username": values.username,
        "role": values.userRole,
        "status": values.userStatus
      };


      axios.put(
        `${UserRoot}/${infoUser.id}`,
        bodyParameters,
        config
      ).then(response => {

        if (response.status === 200) {
          this.setLoadingStatus();

          NotificationManager.success(
            <IntlMessages id="user.update.message" />,
            <IntlMessages id="user.update.title" />,
            5000,
            null,
            "mb-3"
          );

          window.location.reload();
        }
      }
      )
        .catch(err => {
          this.setLoadingStatus()
          this.setLoadingStatus()
          const errorMessage = err.response.data.message
          NotificationManager.error(
            <IntlMessages id={errorMessage} />,
            <IntlMessages id="text.error" />,
            5000,
            null,
            "mb-3"
          );
          throw err;
        });
    }
  }

  render() {

    const { match } = this.props
    const { infoUser, loading } = this.state

    return (
      <>
        <Row>
          <Colxx xxs="12">
            <h1>{infoUser ? infoUser.username : ""}</h1>
  
            <Breadcrumb match={match} />

            <Row style={{ marginTop: "89px" }}>
              <Colxx xxs="12" className="mb-5">
                <SingleLightbox
                  thumb="/assets/img/profiles/l-1.jpg"
                  large="/assets/img/profiles/1.jpg"
                  className="img-thumbnail card-img social-profile-img"
                />

                <Card className="mb-4">
                  <CardBody>
                    <div className="text-center pt-4">
                      <p className="list-item-heading pt-2">{infoUser ? infoUser.username : ""}</p>
                    </div>

                    <AvForm
                      className="av-tooltip tooltip-label-right"
                      onSubmit={(event, errors, values) => this.updateUser(event, errors, values)}
                    >


                      <AvGroup>
                        <Label> <IntlMessages id="user.username" /></Label>
                        <AvInput name="username" required value={infoUser ? infoUser.username : ""} />
                        <AvFeedback>username is required!</AvFeedback>
                      </AvGroup>

                      <AvField
                        type="select"
                        name="userRole"
                        required
                        label={<IntlMessages id="user.role" />}
                        errorMessage="Please select an option!"
                        value={infoUser ? infoUser.role : "0"}
                        disabled
                      >
                        <option value="0" />
                        {
                          UserRoleList.map(item => {
                            return (
                              <option key={item.key} value={item.value}>{item.label}</option>
                            )
                          })
                        }
                      </AvField>
                      <AvRadioGroup
                        className="error-l-150"
                        name="userStatus"
                        required
                        value={infoUser ? infoUser.status : ""}
                        disabled
                      >
                        <Label className="mt-4">
                          <IntlMessages id="user.status" />
                        </Label>
                        <AvRadio customInput label="Attivo" value="Attivo" />
                        <AvRadio customInput label="Sospeso" value="Sospeso" />
                      </AvRadioGroup>

                      <div style={{ textAlign: 'right' }}>
                        {loading ?
                          <Spinner type="grow" color="primary" />
                          :
                          <Button color="primary" type="Submit">
                            <IntlMessages id="button.update" />
                          </Button>
                        }
                      </div>
                    </AvForm>

                  </CardBody>
                </Card>
              </Colxx>

            </Row>


          </Colxx>
        </Row>
      </>
    );
  }
};
export default ProfileSocial;
