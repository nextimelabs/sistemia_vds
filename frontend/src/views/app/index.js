import React, { Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from 'layout/AppLayout';
// import { ProtectedRoute, UserRole } from 'helpers/authHelper';

const Dashboard = React.lazy(() =>
  import(/* webpackChunkName: "viwes-vds" */ './dashboard')
);

const SecondMenu = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ './cameras')
);

const UsersPage = React.lazy(() =>
  import('./users')
);

const MonitoringPage = React.lazy(() =>
  import('./monitoring')
);

const EntitiesPage = React.lazy(() =>
  import('./entities')
);

const Settings = React.lazy(() =>
  import('./management')
);

const App = ({ match }) => {
  return (
    <AppLayout>
      <div className="dashboard-wrapper">
        <Suspense fallback={<div className="loading" />}>
          <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/vds`} />

            <Route
              path={`${match.url}/vds`}
              render={(props) => <Dashboard {...props} />}
            />

            <Route
              path={`${match.url}/cameras`}
              render={(props) => <SecondMenu {...props} />}
            />

            <Route
              path={`${match.url}/users`}
              render={(props) => <UsersPage {...props} />}
            />

            <Route
              path={`${match.url}/monitoring`}
              render={(props) => <MonitoringPage {...props} />}
            />

            <Route
              path={`${match.url}/entities`}
              render={(props) => <EntitiesPage {...props} />}
            />

            <Route
              path={`${match.url}/management`}
              render={(props) => <Settings {...props} />}
            />

            <Redirect to="/error" />
          </Switch>
        </Suspense>
      </div>
    </AppLayout>
  );
};

const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(App));
