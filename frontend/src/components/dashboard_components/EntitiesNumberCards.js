import React, { useState } from 'react';
import { Row, Card, CardBody } from 'reactstrap';
import { ReactSortable } from 'react-sortablejs';
import IntlMessages from 'helpers/IntlMessages';
import { Colxx } from 'components/common/CustomBootstrap';

const SortableUi = () => {
  const [listColumns, setListColumns] = useState([
    {
      icon: 'simple-icon-people',
      title: <IntlMessages id="entity.people.detect" />,
      value: 14,
      id: 1,
    },
    {
      icon: 'iconsminds-car',
      title: <IntlMessages id="entity.vehicle.detect" />,
      value: 32,
      id: 2,
    },
    {
      icon: 'iconsminds-credit-card-3',
      title: <IntlMessages id="entity.plates.detect" />,
      value: 29,
      id: 3,
    },
    {
      icon: 'iconsminds-monitoring',
      title: <IntlMessages id="entity.cameras.actived" />,
      value: 25,
      id: 4,
    },
  ]);



  return (
    <div>

      <Row>
        <Colxx xxs="12">

          <ReactSortable
            className="row icon-cards-row mb-2"
            list={listColumns}
            setList={(list) => setListColumns(list)}
          >
            {listColumns.map((item) => (
              <Colxx
                key={`column_${item.id}`}
                xxs="6"
                sm="4"
                md="3"
                className="mb-4"
              >
                <Card>
                  <CardBody className="text-center">
                    <i className={item.icon} />
                    <p className="card-text font-weight-semibold mb-0">
                      {item.title}
                    </p>
                    <p className="lead text-center">{item.value}</p>
                  </CardBody>
                </Card>
              </Colxx>
            ))}
          </ReactSortable>
        </Colxx>
      </Row>
    </div>
  );
};

export default SortableUi;
