import EntitiesNumberCards from './EntitiesNumberCards';
import PeopleChart from './PeopleChart';
import VehicleChart from './VehicleChart';


export {
  EntitiesNumberCards,
  PeopleChart,
  VehicleChart
};
