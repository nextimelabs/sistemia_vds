/* Dati */
export const HostServer = 'http://localhost:3000';

/* API */
export const LoginAPI = `${HostServer}/auth/login`;
export const UserRoot = `${HostServer}/users`;

/* CAMERA */
export const CameraRoot = `${HostServer}/cameras`;
export const GroupsCameraRoot = `${HostServer}/groups`;

/* STREAMER MODULE */
export const StreamerAPI = `${HostServer}/streamer`;
export const StreamerAllStartAPI = `${HostServer}/streamer/start`;
export const StreamerAllStopAPI = `${HostServer}/streamer/stop`;

/* RECORDER MODULE */
export const RecorderAPI = `${HostServer}/recorder`;
export const RecorderAllStartAPI = `${HostServer}/recorder/start`;
export const RecorderAllStopAPI = `${HostServer}/recorder/stop`;

/* ONVIF */
export const OnvifAPI = `${HostServer}/cameras/onvif`;

/* ALARMS  */
export const AlarmsAPI = `${HostServer}/alarms`;
export const AlarmTriggeredAPI = `${HostServer}/alarms-triggered`;
export const AlarmsConfigurationsAPI = `${HostServer}/alarms/configurations`;

/* EVENTS */
export const EventsAPI = `${HostServer}/events`;

/* SEGMENTS */
export const SegmentsAPI = `${HostServer}/segments`;
