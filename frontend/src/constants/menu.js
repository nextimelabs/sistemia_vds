import { adminRoot, UserRole } from './defaultValues';

const data = [
  {
    id: 'dashboard-menu',
    icon: 'iconsminds-dashboard',
    label: 'menu.dashboard',
    to: `${adminRoot}/vds/dashboard`,
    roles: [UserRole.Amministratore],
  },
  {
    id: 'cameras-menu',
    icon: 'iconsminds-monitoring',
    label: 'menu.cameras',
    to: `${adminRoot}/cameras`,
    roles: [UserRole.Amministratore],
    subs: [
      {
        icon: 'simple-icon-list',
        label: 'menu.list',
        to: `${adminRoot}/cameras/cameras-list`,
      },
      {
        icon: 'simple-icon-map',
        label: 'menu.cameras-map',
        to: `${adminRoot}/cameras/cameras-map`,
      },
      {
        icon: 'simple-icon-layers',
        label: 'menu.cameras-groups',
        to: `${adminRoot}/cameras/cameras-groups`,
      },
      {
        icon: 'iconsminds-magnifi-glass',
        label: 'menu.cameras-scan',
        to: `${adminRoot}/cameras/cameras-scan`,
      },
    ],
  },
  {
    id: 'alarms-menu',
    icon: 'iconsminds-monitor-analytics',
    label: 'menu.monitoring',
    roles: [UserRole.Amministratore],
    to: `${adminRoot}/monitoring/entities`,
    subs: [
      {
        icon: 'iconsminds-target-market',
        label: 'menu.entities',
        to: `${adminRoot}/monitoring/entities`,
      },
      {
        icon: 'simple-icon-event',
        label: 'menu.monitoring.events',
        to: `${adminRoot}/monitoring/events`,
      },
      {
        icon: 'simple-icon-bell',
        label: 'menu.monitoring.alarms',
        to: `${adminRoot}/monitoring/alarms`,
      }
    ],
  },
  {
    id: 'management-menu',
    icon: 'simple-icon-settings',
    label: 'menu.management',
    roles: [UserRole.Amministratore],
    to: `${adminRoot}/management`,
    subs: [
      {
        icon: 'iconsminds-monitoring',
        label: 'menu.cameras',
        to: `${adminRoot}/management/cameras`,
      },
        {
        icon: 'simple-icon-bell',
        label: 'menu.alarms',
        to: `${adminRoot}/management/alarms`,
      },
      {
        icon: 'iconsminds-film-video',
        label: 'menu.videos',
        to: `${adminRoot}/management/videos`,
      },
    ]
  },
  {
    id: 'users-menu',
    icon: 'iconsminds-male-female',
    label: 'menu.users',
    roles: [UserRole.Amministratore],
    to: `${adminRoot}/users/list`
  },
  {
    id: 'ai-module-menu',
    icon: 'simple-icon-grid',
    label: 'menu.ai.module',
    roles: [UserRole.Amministratore],
    to: `${adminRoot}/alarms`,
  },
];
export default data;
