export const defaultCenterMap = {lat: 37.4447144485548, lng: 15.047330340065132 }


export const UserRole = {
  Amministratore: "Amministratore",
  Sistemista: "Sistemista",
  Operatore: "Operatore"
};

export const UserRoleList = [
  { label: 'Amministratore', value: 'Amministratore', key: 0 },
  { label: 'Operatore', value: 'Operatore', key: 1 },
  { label: 'Sistemista', value: 'Sistemista', key: 2 }
];


export const OrderByOptions = [
  { label: 'ASC', value: 'ASC', key: 0 },
  { label: 'DESC', value: 'DESC', key: 1 },
];

/* CAMERA OPTION */
export const FPSOptions = [
  { label: '24p', value: '24', key: 0 },
  { label: '30p', value: '30', key: 1 },
  { label: '50p', value: '50', key: 2 },
  { label: '120p', value: '120', key: 3 }
];

export const TimeSegmentLimitOptions = [
  { label: '1 min', value: '60', key: 0 },
  { label: '10 min', value: '600', key: 1 },
  { label: '30 min', value: '1800', key: 2 },
  { label: '60 min', value: '3600', key: 3 }
];

export const getBadgeCameraStatus = (status) => {
  switch (status) {
    case 'Starting':
      return 'success';
    case 'Running':
      return 'success';
    case 'Stopping':
      return 'warning';
    case 'Stopped':
      return 'danger';
    case 'Error':
      return 'danger';
    default:
      return 'dark';
  }
}

/* ALARM OPTIONS */
export const getBadgeAlarmEnabled = (status) => {
  switch (status) {
    case true:
      return 'success';
    case false:
      return 'danger';
    default:
      return 'dark';
  }
}


export const defaultMenuType = 'menu-default';

export const subHiddenBreakpoint = 1440;
export const menuHiddenBreakpoint = 768;
export const defaultLocale = 'it';
export const localeOptions = [
  { id: 'it', name: 'Italiano', direction: 'ltr' },
  { id: 'en', name: 'English', direction: 'ltr' },
];


export const adminRoot = '/app';
export const searchPath = `${adminRoot}/#`;
export const platesListRoot = `${adminRoot}/entities/plates`
export const facesListRoot = `${adminRoot}/entities/faces`
export const entityDetailsRoot = `${adminRoot}/entities/details`

/* TEMPLATE */
export const themeColorStorageKey = '__theme_selected_color';
export const isMultiColorActive = false;
export const defaultColor = 'light.bluenavy';
export const isDarkSwitchActive = true;
export const defaultDirection = 'ltr';
export const themeRadiusStorageKey = '__theme_radius';
export const isAuthGuardActive = true;
export const colors = [
  'bluenavy',
  'blueyale',
  'blueolympic',
  'greenmoss',
  'greenlime',
  'purplemonster',
  'orangecarrot',
  'redruby',
  'yellowgranola',
  'greysteel',
];

